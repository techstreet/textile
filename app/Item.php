<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Item extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function item_list()
	{
		$company = Auth::user()->company_id;
		$item = DB::table('item')
			->select('item.*', 'unit.name as unit', 'brand.name as brand', 'manufacturer.name as manufacturer', 'form.name as form')
			->where([
			['item.status','1'],
			['item.company_id',$company]
			])
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('manufacturer', 'item.manufacturer_id', '=', 'manufacturer.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
		foreach ($item as $key=>$value){
			$item[$key]->slug = $this->has_parent($value->category_id);			
		}
		return $item;
	}
	protected  function has_parent($id)
	{
		$data = DB::table('category')->where('id',$id)->get();
		$p_id = $data[0]->parent_id;
		$p_name = $data[0]->name;	
		if($p_id)
		{
		$child = new self($p_id);
		return $child->has_parent($p_id)  . ' >> ' . $p_name;;
		}	
		else
		return $p_name;
	}
    public function item_add($company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name)
    {
		$user_id = Auth::id();
		try{
			DB::transaction(function () use ($user_id,$company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name) {
			    $item_id = DB::table('item')->insertGetId(
				    ['company_id' => $company,'name' => $name,'price' => $price,'currency' => $currency,'category_id' => $category,'strength' => $strength,'unit_id' => $unit,'brand_id' => $brand,'manufacturer_id' => $manufacturer,'form_id' => $form,'pack_size' => $pack_size,'notify_quantity' => $notify_quantity,'hsn_code' => $hsn_code,'created_at' => $this->date,'created_by' => $user_id]
				);
				$item_price = DB::table('item_price')->insert(
				    ['company_id' => $company,'item_id' => $item_id,'price' => $price,'currency' => $currency,'entry_date' => $this->date]
				);
				for ($i = 0; $i < count($reference_name); ++$i) {
					$reference = $reference_name[$i];
					if($reference!=''){
						DB::table('item_reference')->insert(
							['company_id' => $company,'item_id' => $item_id,'reference' => $reference]
						);
					}
				}
			});
			return TRUE;
		}
		catch ( \Exception $e ){
			return FALSE;
		}
    }
	public function item_edit($id)
	{
		return DB::table('item')->where('id',$id)->get();
	}
	public function item_reference($id)
	{
		return DB::table('item_reference')
		->where([
			['item_id',$id],
			['status','1']
			])
		->get();
	}
	public function item_view($id)
	{
		$item = DB::table('item')
			->select('item.*', 'unit.name as unit', 'brand.name as brand', 'manufacturer.name as manufacturer', 'form.name as form')
			->where('item.id',$id)
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('manufacturer', 'item.manufacturer_id', '=', 'manufacturer.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
        foreach ($item as $key=>$value){
			$item[$key]->slug = $this->has_parent($value->category_id);			
		}
		return $item;
	}
	public function item_update($id,$company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name)
    {
		$user_id = Auth::id();

		try{
			DB::transaction(function () use ($user_id,$id,$company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name) {
			    DB::table('item')
		            ->where('id', $id)
		            ->update(['company_id' => $company,'name' => $name,'price' => $price,'currency' => $currency,'category_id' => $category,'strength' => $strength,'unit_id' => $unit,'brand_id' => $brand,'manufacturer_id' => $manufacturer,'form_id' => $form,'pack_size' => $pack_size,'notify_quantity' => $notify_quantity,'hsn_code' => $hsn_code,'updated_at' => $this->date,'updated_by' => $user_id]);
		        $item_price = DB::table('item_price')->insert(
				    ['company_id' => $company,'item_id' => $id,'price' => $price,'currency' => $currency,'entry_date' => $this->date]
				);
				DB::table('item_reference')->where('item_id', $id)->update(['status' => '0']);
				for ($i = 0; $i < count($reference_name); ++$i) {
					$reference = $reference_name[$i];
					if($reference!=''){
						DB::table('item_reference')->insert(
							['company_id' => $company,'item_id' => $id,'reference' => $reference]
						);
					}
				}
			});
			return TRUE;
		}
		catch ( \Exception $e ){
			echo $e->getmessage();
			die;
			return FALSE;
		}
    }
    public function item_delete($id)
	{
		$user_id = Auth::id();
		try{
			DB::transaction(function () use ($user_id,$id) {
		        DB::table('item')
		            ->where('id', $id)
		            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
			});
			return TRUE;
		}
		catch ( \Exception $e ){
			return FALSE;
		}
	}
}
