<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Brand extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function brand_list()
	{
		$company = Auth::user()->company_id;
		$brand = DB::table('brand')
			->where([
			['is_active','1'],
			['status','1'],
			['company_id',$company]
			])
			->get();
		return $brand;
	}
	public function getBrandName($id)
	{
		$brand = DB::table('brand')->where('id',$id)->get();
		foreach($brand as $value){
			$name = $value->name;
		}
		return $name;
	}
    public function brand_add($company,$category,$name,$status)
    {
		$user_id = Auth::id();
		$brand_id = DB::table('brand')->insertGetId(
		    ['company_id' => $company,'category_id' => $category,'name' => $name,'is_active' => $status,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $brand_id;
    }
	public function brand_edit($id)
	{
		return DB::table('brand')->where('id',$id)->get();
	}
	public function brand_update($id,$company,$category,$name,$status)
    {
		$user_id = Auth::id();
		return DB::table('brand')
            ->where('id', $id)
            ->update(['company_id' => $company,'category_id' => $category,'name' => $name,'is_active' => $status,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function brand_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('brand')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
