<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Salereturn extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function salereturn_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('salereturn')
			->select('salereturn.*', 'client.name as client_name')
			->where([
			['salereturn.status','1'],
			['salereturn.company_id',$company],
			['saleregister.company_id',$company]
			])
			->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
            ->orderBy('salereturn.id', 'desc')
            ->get();
	}
	public function item_list($category_id)
	{
		return DB::table('item')->where('category_id',$category_id)->get();
	}
	public function category_list($id)
	{
		$item = DB::table('item')->where('id',$id)->get();
		foreach ($item as $value){
			$company_id = $value->company_id;
		}
		$category = DB::table('category')->where('company_id',$company_id)->get();
		return $category;
	}
	public function category_id($item_id)
	{
		$item = DB::table('item')->where('id',$item_id)->get();
		foreach ($item as $value){
			$category_id = $value->category_id;
		}
		return $category_id;
	}
	public function getSalereturnName($id)
	{
		$salereturn = DB::table('salereturn')->where('id',$id)->get();
		foreach($salereturn as $value){
			$name = $value->name;
		}
		return $name;
	}
	public function getBalance($client_id)
	{
		$res = DB::table('client_register')
			->where([
			['client_id',$client_id]
			])
			->orderBy('id','desc')
			->first();
		return $res->balance;
	}
    public function salereturn_add($company,$creditnote_no,$creditnote_date,$invoice_no,$return_reason,$remarks,$transport,$gr_no,$booking_date,$item_id,$stockregister_id,$reference,$unit_id,$quantity,$return_quantity,$rate,$amount,$return_amount,$cgstA,$sgstA,$igstA,$discount,$other_expense)
    {
		$result = DB::transaction(function () use($company,$creditnote_no,$creditnote_date,$invoice_no,$return_reason,$remarks,$transport,$gr_no,$booking_date,$item_id,$stockregister_id,$reference,$unit_id,$quantity,$return_quantity,$rate,$amount,$return_amount,$cgstA,$sgstA,$igstA,$discount,$other_expense)
		{
			$user_id = Auth::id();
			if(!empty($creditnote_date)){
				$creditnote_date = date_format(date_create($creditnote_date),"Y-m-d");
			}
			if(!empty($booking_date)){
				$booking_date = date_format(date_create($booking_date),"Y-m-d");
			}
			$sub_total = collect($return_amount)->sum();
			$total = $sub_total - $discount;
			$cgst = collect($cgstA)->sum();
			$sgst = collect($sgstA)->sum();
			$igst = collect($igstA)->sum();
			$tax = $cgst+$sgst+$igst;
			$final_total = $sub_total-$discount+$other_expense+$tax;
			$salereturn_id = DB::table('salereturn')->insertGetId(
			    ['company_id' => $company,'creditnote_no' => $creditnote_no,'creditnote_date' => $creditnote_date,'invoice_no' => $invoice_no,'return_reason' => $return_reason,'remarks' => $remarks,'transport_id' => $transport,'gr_no' => $gr_no,'booking_date' => $booking_date,'sub_total' => $sub_total,'discount' => $discount,'other_expense' => $other_expense,'cgst' => $cgst,'sgst' => $sgst,'igst' => $igst,'tax' => $tax,'total' => $final_total,'created_at' => $this->date,'created_by' => $user_id]
			);
			
			if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$reference1 = $reference[$i];
					$stockregister_id1 = $stockregister_id[$i];
					$unit_id1 = $unit_id[$i];
					$quantity1 = $quantity[$i];
					$return_quantity1 = $return_quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					$return_amount1 = $return_amount[$i];
					if($item_id1!=''){
						DB::table('salereturn_item')->insert(['parent_id' => $salereturn_id,'item_id' => $item_id1,'stockregister_id' => $stockregister_id1,'reference' => $reference1,'quantity' => $quantity1,'return_quantity' => $return_quantity1,'rate' => $rate1,'amount' => $amount1,'return_amount' => $return_amount1]);
						DB::table('stockregister')
						->where('id', $stockregister_id1)
						->increment('quantity', $return_quantity1);
					}
				}
			}

			$saleregister = DB::table('saleregister')->where('invoice_no',$invoice_no)->first();
			$client = $saleregister->client_id;

			DB::table('client_register')->insert(
			    ['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'stype' => 'sale_return','stype_id' => $salereturn_id,'type' => '3','record_type' => '1','particular' => 'Sale return against '.$invoice_no,'credit' => $sub_total,'balance' => $this->getBalance($client)-$sub_total,'created_at' => $this->date]
			);

			if($this->getBalance($client) < 0){
				DB::table('client_register')->insert(
					['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '4','record_type' => '1','particular' => 'Cash return against '.$invoice_no,'debit' => abs($this->getBalance($client)),'created_at' => $this->date]
				);
			}

			return $salereturn_id;
		});
		return $result;
    }
	public function salereturn_edit($id)
	{
		return DB::table('salereturn')
			->select('salereturn.*', 'saleregister.client_id', 'client.name as client_name', 'client.mobile as client_mobile')
			->where('salereturn.id',$id)
            ->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
            ->get();
	}
	//function updated by anurag company address
	public function salereturn_view($id)
	{
		return DB::table('salereturn')
			->select('salereturn.*', 'saleregister.entry_date as invoice_date', 'transport.name as transport_name', 'client.name as client_name', 'client.mobile as client_mobile', 'client.gstin as client_gstin', 'client.address as client_address','company.name as company_name','users.address as address')
			->where('salereturn.id',$id)
			->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
			->leftJoin('company', 'salereturn.company_id', '=', 'company.id')
			->leftJoin('transport', 'salereturn.transport_id', '=', 'transport.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->get();
	}
	public function salereturn_item($id)
	{
        return DB::table('salereturn')
			->select('salereturn_item.*','item.name as item_name','item.strength','item.hsn_code','unit.name as unit_name','form.id as form_id','form.name as form_name')
			->where([
			['salereturn.id',$id],
			['salereturn.status','1']
			])
			->leftJoin('salereturn_item', 'salereturn_item.parent_id', '=', 'salereturn.id')
            ->leftJoin('item', 'salereturn_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
	}
	public function salereturn_update($id,$company,$creditnote_date,$invoice_no,$return_reason,$remarks,$item_id,$unit_id,$quantity,$return_quantity,$rate,$amount,$return_amount)
    {
		$result = DB::transaction(function () use($id,$company,$creditnote_date,$invoice_no,$return_reason,$remarks,$item_id,$unit_id,$quantity,$return_quantity,$rate,$amount,$return_amount)
		{
			$user_id = Auth::id();
			if(!empty($creditnote_date)){
				$creditnote_date = date_format(date_create($creditnote_date),"Y-m-d");
			}
			$sub_total = collect($return_amount)->sum();
			
			DB::table('salereturn')
	            ->where('id', $id)
	            ->update(['company_id' => $company,'creditnote_date' => $creditnote_date,'return_reason' => $return_reason,'remarks' => $remarks,'sub_total' => $sub_total,'total' => $sub_total,'updated_at' => $this->date,'updated_by' => $user_id]);
	            
	        DB::table('salereturn_item')
	            ->where('parent_id', $id)
	            ->delete();
	            
	        if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$unit_id1 = $unit_id[$i];
					$quantity1 = $quantity[$i];
					$return_quantity1 = $return_quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					$return_amount1 = $return_amount[$i];
					if($item_id1!=''){
						DB::table('salereturn_item')->insert(['parent_id' => $id,'item_id' => $item_id1,'quantity' => $quantity1,'return_quantity' => $return_quantity1,'rate' => $rate1,'amount' => $amount1,'return_amount' => $return_amount1]);
					}
				}
			}

			$salereturn = DB::table('salereturn')->where('id',$id)->first();
			$invoice_no = $salereturn->invoice_no;

			$saleregister = DB::table('saleregister')->where('invoice_no',$invoice_no)->first();
			$client = $saleregister->client_id;

			DB::table('client_register')
				->where([
					['company_id', $company],
					['invoice_no', $invoice_no],
					['record_type', '1']
				])
				->delete();
				
			DB::table('client_register')->insert(
				['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '3','record_type' => '1','particular' => 'Sale return against '.$invoice_no,'credit' => $sub_total,'balance' => $this->getBalance($client)-$sub_total,'created_at' => $this->date]
			);

			if($this->getBalance($client) < 0){
				DB::table('client_register')->insert(
					['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '4','record_type' => '1','particular' => 'Cash return against '.$invoice_no,'debit' => abs($this->getBalance($client)),'created_at' => $this->date]
				);
			}
			
			return TRUE;
		});
		return $result;
    }
    public function salereturn_delete($id)
	{
        $result = DB::transaction(function () use($id)
		{
			$user_id = Auth::id();
			$company = Auth::user()->company_id;
			$salereturn_data = DB::table('salereturn')
				->select('salereturn.*', 'saleregister.client_id as client_id')
				->where([
				['salereturn.id',$id],
				['saleregister.status','1'],
				['saleregister.company_id',$company],
				])
				->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
				->first();

			$invoice_no = $salereturn_data->invoice_no;
			$client = $salereturn_data->client_id;
			$total = $salereturn_data->total;
			
			//salereturn manage
        	DB::table('salereturn')
	            ->where('id', $id)
				->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
			
			//stockregister manage
			$items = DB::table('salereturn')
				->select('salereturn_item.stockregister_id','salereturn_item.quantity')
				->where([
				['salereturn.id',$id],
				['salereturn.status','1']
				])
				->leftJoin('salereturn_item', 'salereturn_item.parent_id', '=', 'salereturn.id')
				->get();
			foreach($items as $key=>$value){
				$stockregister_id = $value->stockregister_id;
				$quantity = $value->quantity;
				DB::table('stockregister')
					->where('id', $stockregister_id)
					->decrement('quantity', $quantity);
			}
			//clientregister manage
			DB::table('client_register')
				->where([
				['company_id', $company],
				['invoice_no', $invoice_no],
				['record_type','1'],
				])
				->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
            
			return TRUE;
		});
		return $result;
	}
	public function ajax($company_id)
	{
		if(!empty($company_id)){
			$category = DB::table('category')->where([
			    ['status', '1'],
			    ['company_id', $company_id]
			])->get();
			$category_count = $category->count();
			if($category_count>0){
				?><option value="">Select</option><?php
				foreach ($category as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function ajax2($category_id)
	{
		if(!empty($category_id)){
			$item = DB::table('item')->where([
			    ['status', '1'],
			    ['category_id', $category_id]
			])->get();
			$item_count = $item->count();
			if($item_count>0){
				?><option value="">Select</option><?php
				foreach ($item as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function additems($invoice_no)
	{
		$company_id = Auth::user()->company_id;
		$items = DB::table('saleregister')
			->select('saleregister_item.*','item.name as item_name','item.strength','item.hsn_code as hsn_code','unit.name as unit_name','form.id as form_id','form.name as form_name','client.mobile as client_mobile','client.name as client_name','client.gstin as client_gstin')
            ->leftJoin('saleregister_item', 'saleregister_item.parent_id', '=', 'saleregister.id')
            ->leftJoin('item', 'saleregister_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->leftJoin('client', 'client.id', '=', 'saleregister.client_id')
            ->where([
			['saleregister.invoice_no', $invoice_no],
			['saleregister.company_id', $company_id],
            ['saleregister.status', '1'],
            ])
			->get(); 
		foreach ($items as $key=>$value){
			$items[$key]->quantity = getSaleStock($value->item_id,'','',$invoice_no,$company_id)-getSaleReturnStock($value->item_id,'','',$invoice_no,$company_id);
		}
		print_r(json_encode( array($items)));
	}
	
}
