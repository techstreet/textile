<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Lr extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function lr_list()
	{
		return DB::table('purchaseregister_lr')
			->select('purchaseregister_lr.*','purchaseregister.purchasevoucher_no')
			->leftJoin('purchaseregister','purchaseregister.id','=','purchaseregister_lr.parent_id')
			->where([
			['purchaseregister_lr.status','1'],
			])
			->get();
	}
	public function lr_edit($id)
	{
		return DB::table('purchaseregister_lr')
			->select('purchaseregister_lr.*','purchaseregister.purchasevoucher_no')
			->leftJoin('purchaseregister','purchaseregister.id','=','purchaseregister_lr.parent_id')
			->where([
			['purchaseregister_lr.id',$id],
			])
			->first();
	}
	public function lr_update($id,$amount,$payment_status)
    {
		$user_id = Auth::id();
		return DB::table('purchaseregister_lr')
            ->where('id', $id)
            ->update(['amount' => $amount,'payment_status' => $payment_status,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
}
