<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Saleregister extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function saleregister_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('saleregister')
			->select('saleregister.*','client.name as client_name','client.address as client_address','client.phone as client_phone','client.mobile as client_mobile')
			->where([
			['saleregister.status','1'],
			['saleregister.company_id',$company]
			])
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
			->orderBy('saleregister.id', 'desc')
			->groupBy('saleregister.invoice_no')
            ->get();
	}
	public function item_list($category_id)
	{
		return DB::table('item')->where('category_id',$category_id)->get();
	}
	public function category_list($id)
	{
		$item = DB::table('item')->where('id',$id)->get();
		foreach ($item as $value){
			$company_id = $value->company_id;
		}
		$category = DB::table('category')->where('company_id',$company_id)->get();
		return $category;
	}
	public function category_id($item_id)
	{
		$item = DB::table('item')->where('id',$item_id)->get();
		foreach ($item as $value){
			$category_id = $value->category_id;
		}
		return $category_id;
	}
	public function getSaleregisterName($id)
	{
		$saleregister = DB::table('saleregister')->where('id',$id)->get();
		foreach($saleregister as $value){
			$name = $value->name;
		}
		return $name;
	}
	public function getBalance($client_id)
	{
		$res = DB::table('client_register')
			->where([
			['client_id',$client_id],
			['status','1']
			])
			->orderBy('id','desc')
			->first();
		if($res){
			return $res->balance;
		}
		else{
			return 0;
		}
	}
	public function invoiceAmount($invoice_no,$client)
	{
		$res = DB::table('client_register')
			->where([
			['type','1'],
			['invoice_no',$invoice_no]
			])
			->orderBy('id','desc')
			->first();
		return $res->invoice_amt;
	}
    public function saleregister_add($company,$invoice_no,$entry_date,$client,$shipping_client,$sale_type,$transport,$gr_no,$booking_date,$item,$ref,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$av_qty,$quantity,$rate,$amount,$discount,$tax_percentage,$cgstA,$sgstA,$igstA,$other_expense)
    {		
		$result = DB::transaction(function () use ($company,$invoice_no,$entry_date,$client,$shipping_client,$sale_type,$transport,$gr_no,$booking_date,$item,$ref,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$av_qty,$quantity,$rate,$amount,$discount,$tax_percentage,$cgstA,$sgstA,$igstA,$other_expense) {
			
			$user_id = Auth::id();
			$entry_date = date_format(date_create($entry_date),"Y-m-d");
			$booking_date = date_format(date_create($booking_date),"Y-m-d");
			$sub_total = collect($amount)->sum();
			
			$total = $sub_total - $discount;

			$cgst = collect($cgstA)->sum();
			$sgst = collect($sgstA)->sum();
			$igst = collect($igstA)->sum();
			$tax = $cgst+$sgst+$igst;
			$final_total = $sub_total-$discount+$other_expense+$tax;
			
			$saleregister_id = DB::table('saleregister')->insertGetId(
			    ['company_id' => $company,'invoice_no' => $invoice_no,'entry_date' => $entry_date,'client_id' => $client,'shipping_client_id' => $shipping_client,'sale_type' => $sale_type,'transport_id' => $transport,'gr_no' => $gr_no,'booking_date' => $booking_date,'sub_total' => $sub_total,'discount' => $discount,'other_expense' => $other_expense,'cgst' => $cgst,'sgst' => $sgst,'igst' => $igst,'tax' => $tax,'total' => $final_total,'ref' => $ref,'remarks' => $remarks,'created_at' => $this->date,'created_by' => $user_id]
			);
			
			if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];

					$item_id_arr = explode("|",$item_id1);
					$item_id1 = $item_id_arr[0];
					$reference = $item_id_arr[1];

					$unit_id1 = $unit_id[$i];
					$barcode1 = $barcode[$i];
					$expiry_date1 = $expiry_date[$i];
					if(!empty($expiry_date1)){
						$expiry_date1 = date_format(date_create($expiry_date1),"Y-m-d");
					}
					$stockregister_id = $av_qty[$i];
					$quantity1 = $quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					if($item_id1!=''){
						DB::table('saleregister_item')->insert(['parent_id' => $saleregister_id,'stockregister_id' => $stockregister_id,'item_id' => $item_id1,'reference' => $reference,'quantity' => $quantity1,'rate' => $rate1,'amount' => $amount1,'barcode' => $barcode1,'expiry_date' => $expiry_date1]);
						$stockregister = DB::table('stockregister')
						->where([
						['id',$stockregister_id],
						])
						->first();
						if($stockregister){
							$stock_qty = $stockregister->quantity;
							DB::table('stockregister')
							->where('id', $stockregister_id)
							->update(['quantity' => $stock_qty-$quantity1,'updated_at' => $this->date,'updated_by' => $user_id]);
						}
					}
				}
			}
			
			DB::table('client_register')->insert(
			    ['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'stype' => 'sale','stype_id' => $saleregister_id,'type' => '1','record_type' => '0','particular' => 'Sale against '.$invoice_no,'debit' => $final_total,'balance' => $this->getBalance($client)+$final_total,'invoice_amt' => $final_total,'created_at' => $this->date]
			);
			
			if($sale_type == 'Cash'){
				DB::table('client_register')->insert(
				    ['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'stype' => 'sale','stype_id' => $saleregister_id,'type' => '2','record_type' => '0','particular' => 'Payment received against '.$invoice_no,'credit' => $final_total,'balance' => $this->getBalance($client)-$final_total,'created_at' => $this->date]
				);
			}
			
			return $saleregister_id;
		});
		return $result;
    }
	public function saleregister_edit($id)
	{
		return DB::table('saleregister')
			->select('saleregister.*', 'client.name as client_name')
			->where('saleregister.id',$id)
            ->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
            ->get();
	}
	public function saleregister_view($id)
	{
		return DB::table('saleregister')
			->select('saleregister.*', 'transport.name as transport_name','client.name as client_name','client.mobile as client_mobile','client.gstin as client_gstin','client.address as client_address','company.name as company_name','company.address as address')
			->where('saleregister.id',$id)
			->leftJoin('client', 'saleregister.client_id', '=', 'client.id')
			->leftJoin('transport', 'saleregister.transport_id', '=', 'transport.id')
            ->leftJoin('company', 'saleregister.company_id', '=', 'company.id')
            ->get();
	}
	public function shipping_client($id)
	{
		return DB::table('saleregister')
			->select('client.name as client_name','client.mobile as client_mobile','client.gstin as client_gstin','client.address as client_address')
			->where('saleregister.id',$id)
			->leftJoin('client', 'saleregister.shipping_client_id', '=', 'client.id')
            ->first();
	}
	public function saleregister_item($id)
	{
		$company_id = Auth::user()->company_id;
        $items = DB::table('saleregister')
			->select(DB::raw('SUM(saleregister_item.pcs) as pcs_total'),DB::raw('SUM(saleregister_item.quantity) as quantity_total'),'saleregister_item.*','item.name as item_name','item.hsn_code','brand.name as brand_name' ,'item.strength','item.form_id','unit.name as unit_name','form.name as form_name')
			->where([
			['saleregister.id',$id],
			['saleregister.status','1']
			])
			->leftJoin('saleregister_item', 'saleregister_item.parent_id', '=', 'saleregister.id')
            ->leftJoin('item', 'saleregister_item.item_id', '=', 'item.id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
			->leftJoin('form', 'item.form_id', '=', 'form.id')
			->groupBy('saleregister_item.item_id')
            ->get();
            
        foreach($items as $key=>$value){
			$item_id = $value->item_id;
			$in_stock = getStock($item_id);
			$items[$key]->in_stock = $in_stock;
		}
		return $items;
	}
	public function saleregister_update($id,$company,$entry_date,$client,$sale_type,$item,$ref,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$discount,$tax_percentage)
    {
		$result = DB::transaction(function () use($id,$company,$entry_date,$client,$sale_type,$item,$ref,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$discount,$tax_percentage)
		{
			$user_id = Auth::id();
			$entry_date = date_format(date_create($entry_date),"Y-m-d");
			$sub_total = collect($amount)->sum();
			
			$total = $sub_total - $discount;
			$tax = $total*$tax_percentage/100;
			$final_total = $sub_total-$discount+$tax;
			
			$saleregister = DB::table('saleregister')
	            ->where('id', $id)
	            ->update(['company_id' => $company,'entry_date' => $entry_date,'client_id' => $client,'sale_type' => $sale_type,'sub_total' => $sub_total,'discount' => $discount,'tax' => $tax,'total' => $final_total,'ref' => $ref,'remarks' => $remarks,'updated_at' => $this->date,'updated_by' => $user_id]);
	            
	        DB::table('saleregister_item')
	            ->where('parent_id', $id)
	            ->delete();
	            
	        if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$unit_id1 = $unit_id[$i];
					$barcode1 = $barcode[$i];
					$expiry_date1 = $expiry_date[$i];
					if(!empty($expiry_date1)){
						$expiry_date1 = date_format(date_create($expiry_date1),"Y-m-d");
					}
					$quantity1 = $quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					if($item_id1!=''){
						DB::table('saleregister_item')->insert(['parent_id' => $id,'item_id' => $item_id1,'quantity' => $quantity1,'rate' => $rate1,'amount' => $amount1,'barcode' => $barcode1,'expiry_date' => $expiry_date1]);
					}
				}
			}

			$saleregister_data = DB::table('saleregister')->where('id',$id)->first();
			$invoice_no = $saleregister_data->invoice_no;

			DB::table('client_register')
				->where([
				['company_id',$company],
				['invoice_no',$invoice_no],
				])
	            ->delete();
			
			if($sale_type == 'Cash'){

				DB::table('client_register')->insert(
					['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '1','record_type' => '0','particular' => 'Sale return against '.$invoice_no,'debit' => $final_total,'balance' => $this->getBalance($client)+$final_total,'created_at' => $this->date]
				);

				DB::table('client_register')->insert(
				    ['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '2','particular' => 'Payment received against '.$invoice_no,'credit' => $final_total,'balance' => $this->getBalance($client)-$final_total,'created_at' => $this->date]
				);
			}
			else{
				$prev_amt = $this->invoiceAmount($invoice_no,$client);
				$curr_amt = $final_total;
				if($prev_amt>$curr_amt){
					$final_amt = $prev_amt - $curr_amt;
					DB::table('client_register')->insert(
						['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '1','particular' => 'Sale return against '.$invoice_no,'credit' => $final_amt,'balance' => $this->getBalance($client)-$final_amt,'invoice_amt' => $curr_amt,'created_at' => $this->date]
					);
					if($this->getBalance($client)-$final_amt < 0){
						DB::table('client_register')->insert(
							['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '5','particular' => 'Cash return against '.$invoice_no,'debit' => abs($this->getBalance($client)-$final_amt),'created_at' => $this->date]
						);
					}
				}
				else{
					$final_amt = $curr_amt - $prev_amt;
					DB::table('client_register')->insert(
						['company_id' => $company,'client_id' => $client,'invoice_no' => $invoice_no,'type' => '1','particular' => 'Sale updated against '.$invoice_no,'debit' => $final_amt,'balance' => $this->getBalance($client)+$final_amt,'invoice_amt' => $curr_amt,'created_at' => $this->date]
					);
				}
				
				
			}

	        return $saleregister;    
		});
		return $result;
    }
    public function saleregister_delete($id)
	{
        $result = DB::transaction(function () use($id)
		{
			$user_id = Auth::id();
			$company = Auth::user()->company_id;
			$saleregister_data = DB::table('saleregister')->where('id',$id)->first();
			$invoice_no = $saleregister_data->invoice_no;
			$client = $saleregister_data->client_id;
			$total = $saleregister_data->total;
			$sale_type = $saleregister_data->sale_type;

			$sale_return = DB::table('salereturn')
			->where([
			['status','1'],
			['invoice_no',$invoice_no],
			['company_id',$company]
			])
			->orderBy('id','desc')
			->first();

			if($sale_return){
				return "warning";
			}
			else{
				$items = DB::table('saleregister')
					->select('saleregister_item.stockregister_id','saleregister_item.quantity')
					->where([
					['saleregister.id',$id],
					['saleregister.status','1']
					])
					->leftJoin('saleregister_item', 'saleregister_item.parent_id', '=', 'saleregister.id')
					->get();
				foreach($items as $key=>$value){
					$stockregister_id = $value->stockregister_id;
					$quantity = $value->quantity;
					DB::table('stockregister')
						->where('id', $stockregister_id)
						->increment('quantity', $quantity);
				}

				DB::table('saleregister')
					->where('id', $id)
					->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);

				DB::table('client_register')
					->where([
					['stype', 'sale'],
					['stype_id', $id],
					])
					->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);

				return "success";
			}
		});
		return $result; 
	}
	public function ajax($company_id)
	{
		if(!empty($company_id)){
			$category = DB::table('category')->where([
			    ['status', '1'],
			    ['company_id', $company_id]
			])->get();
			$category_count = $category->count();
			if($category_count>0){
				?><option value="">Select</option><?php
				foreach ($category as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function ajax2($category_id)
	{
		if(!empty($category_id)){
			$item = DB::table('item')->where([
			    ['status', '1'],
			    ['category_id', $category_id]
			])->get();
			$item_count = $item->count();
			if($item_count>0){
				?><option value="">Select</option><?php
				foreach ($item as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function additems($item_id)
	{
		$company_id = Auth::user()->company_id;
		$items = DB::table('item')
			->select('item.*','unit.name as unit_name','form.name as form_name')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->where('item.id', $item_id)
            ->get();
            
        $in_stock = getStock($item_id);
        
        foreach($items as $key=>$value){
			$items[$key]->in_stock = $in_stock;
		}
        
		print_r(json_encode( array($items)));
	}
	public function getUnexpiredStockItems($item_id)
    {
		function getOpeningStockItems($item_id){
			$data = DB::table('openingstock')
				->select('item_id','barcode','expiry_date')
				->where([
				['item_id',$item_id],
				['expiry_date','>=',Carbon::today()],
				['status','1'],
				])
	            ->get();
 
			return $data;
		}
		function getPurchaseItems($item_id){
			$data = DB::table('purchaseregister')
				->select('purchaseregister_item.item_id','purchaseregister_item.barcode','purchaseregister_item.expiry_date')
				->leftJoin('purchaseregister_item','purchaseregister_item.parent_id','=','purchaseregister.id')
				->where([
				['purchaseregister_item.item_id',$item_id],
				['purchaseregister_item.expiry_date','>',Carbon::today()],
				['purchaseregister.status','1'],
				])
				->groupBy('barcode','expiry_date')
	            ->get();
  
			return $data;
		}
		
		$array1 = getOpeningStockItems('1')->toArray();
		$array2 = getPurchaseItems('1')->toArray();
		
		$input = array_merge($array1,$array2);
		$unique_input = array_map("unserialize", array_unique(array_map("serialize", $input)));
		foreach($unique_input as $key=>$value){
			$unique_input[$key]->stock = 0;
		}
		print_r(json_encode($unique_input));
		
    }
	public function barcodesearch($barcode)
	{
		$company = Auth::user()->company_id;
		
		function getOpeningStockItems($company,$barcode){
			$data = DB::table('openingstock')
				->select('openingstock.item_id','openingstock.barcode','openingstock.expiry_date','item.name as item_name','item.strength','item.price as rate','unit.name as unit_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('item','item.id','=','openingstock.item_id')
				->leftJoin('unit','unit.id','=','item.unit_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['openingstock.company_id',$company],
				['openingstock.item_id',$barcode],
				['openingstock.status','1'],
				])
	            ->get();

			return $data;
		}
		function getPurchaseItems($company,$barcode){
			$data = DB::table('purchaseregister')
				->select('purchaseregister_item.item_id','purchaseregister_item.barcode','purchaseregister_item.expiry_date','item.name as item_name','item.strength','item.price as rate','unit.name as unit_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('purchaseregister_item','purchaseregister_item.parent_id','=','purchaseregister.id')
				->leftJoin('item','item.id','=','purchaseregister_item.item_id')
				->leftJoin('unit','unit.id','=','item.unit_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['purchaseregister.company_id',$company],
				['purchaseregister_item.item_id',$barcode],
				['purchaseregister.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		function getPurchaseReturnItems($company,$barcode){
			$data = DB::table('purchasereturn')
				->select('purchasereturn_item.item_id','purchasereturn_item.barcode','purchasereturn_item.expiry_date','item.name as item_name','item.strength','item.price as rate','unit.name as unit_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('purchasereturn_item','purchasereturn_item.parent_id','=','purchasereturn.id')
				->leftJoin('item','item.id','=','purchasereturn_item.item_id')
				->leftJoin('unit','unit.id','=','item.unit_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['purchasereturn.company_id',$company],
				['purchasereturn_item.item_id',$barcode],
				['purchasereturn.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		function getSaleItems($company,$barcode){
			$data = DB::table('saleregister')
				->select('saleregister_item.item_id','saleregister_item.barcode','saleregister_item.expiry_date','item.name as item_name','item.strength','item.price as rate','unit.name as unit_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('saleregister_item','saleregister_item.parent_id','=','saleregister.id')
				->leftJoin('item','item.id','=','saleregister_item.item_id')
				->leftJoin('unit','unit.id','=','item.unit_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['saleregister.company_id',$company],
				['saleregister_item.item_id',$barcode],
				['saleregister.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		function getSaleReturnItems($company,$barcode){
			$data = DB::table('salereturn')
				->select('salereturn_item.item_id','salereturn_item.barcode','salereturn_item.expiry_date','item.name as item_name','item.strength','item.price as rate','unit.name as unit_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('salereturn_item','salereturn_item.parent_id','=','salereturn.id')
				->leftJoin('item','item.id','=','salereturn_item.item_id')
				->leftJoin('unit','unit.id','=','item.unit_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['salereturn.company_id',$company],
				['salereturn_item.item_id',$barcode],
				['salereturn.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		
		$array1 = getOpeningStockItems($company,$barcode)->toArray();
		$array2 = getPurchaseItems($company,$barcode)->toArray();
		$array3 = getPurchaseReturnItems($company,$barcode)->toArray();
		$array4 = getSaleItems($company,$barcode)->toArray();
		$array5 = getSaleReturnItems($company,$barcode)->toArray();
		$input = array_merge($array1,$array2,$array3,$array4,$array5);
		$unique_input = array_map("unserialize", array_unique(array_map("serialize", $input)));

		// print_r($array2);
		// die;
		
		foreach($unique_input as $key=>$value){
		 	$unique_input[$key]->stock = getStock($value->item_id);
		 	$unique_input[$key]->expiry_date = date_dfy($value->expiry_date);
		 	if($unique_input[$key]->stock == 0){
				unset($unique_input[$key]);
			}
		}

		$name_arr = array();
		$name_arr[] = $unique_input[0]->item_name;

		$item_reference = DB::table('item_reference')
			->select('item_reference.reference as reference')
			->where([
				['item_reference.item_id', $barcode],
				['item_reference.status', '1'],
				])
			->get();
		$reference_arr = array();
		foreach($item_reference as $key=>$value){
			$item_reference[$key]->reference = $value->reference;
			$reference_arr[] = $item_reference[$key]->reference;
		}
		$item_quantity = DB::table('stockregister')
			->where([
				['stockregister.company_id', $company],
				['stockregister.item_id', $barcode],
				['stockregister.status', '1'],
				])
			->get();
		$quantity_arr = array();
		foreach($item_quantity as $key=>$value){
			$item_quantity[$key]->quantity = $value->quantity;
			if($value->quantity > 0){
				$quantity_arr[] = $value->id.'|'.$item_quantity[$key]->quantity;
			}
		}

		$name_merge = array_merge($name_arr,$reference_arr);
		$final_name = implode(",",$name_merge);
		$unique_input[0]->item_name = $final_name;

		$avail_qty = implode(",",$quantity_arr);
		$unique_input[0]->avail_qty = $avail_qty;

		print_r(json_encode($unique_input));
	}
	public function itemsearch($item_id)
	{
		$company = Auth::user()->company_id;
		$date = $this->date;
		
		$item = DB::table('item')
			->select('item.id as item_id','item.name as item_name','item.strength','item.price as rate','unit.name as unit_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('category', 'item.category_id', '=', 'category.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->leftJoin('brand','brand.id','=','item.brand_id')
			->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
            ->where('item.id', $item_id)
            ->first();
		
		function getOpeningStockItems($company,$item_id,$date){
			$data = DB::table('openingstock')
				->select('openingstock.barcode','openingstock.expiry_date')
				->where([
				['openingstock.company_id',$company],
				['openingstock.item_id',$item_id],
				['openingstock.expiry_date','>',$date],
				['openingstock.status','1'],
				])
	            ->get();

			return $data;
		}
		function getPurchaseItems($company,$item_id,$date){
			$data = DB::table('purchaseregister')
				->select('purchaseregister_item.barcode','purchaseregister_item.expiry_date')
				->leftJoin('purchaseregister_item','purchaseregister_item.parent_id','=','purchaseregister.id')
				->where([
				['purchaseregister.company_id',$company],
				['purchaseregister_item.item_id',$item_id],
				['purchaseregister_item.expiry_date','>',$date],
				['purchaseregister.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		function getPurchaseReturnItems($company,$item_id,$date){
			$data = DB::table('purchasereturn')
				->select('purchasereturn_item.barcode','purchasereturn_item.expiry_date')
				->leftJoin('purchasereturn_item','purchasereturn_item.parent_id','=','purchasereturn.id')
				->where([
				['purchasereturn.company_id',$company],
				['purchasereturn_item.item_id',$item_id],
				['purchasereturn_item.expiry_date','>',$date],
				['purchasereturn.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		function getSaleItems($company,$item_id,$date){
			$data = DB::table('saleregister')
				->select('saleregister_item.barcode','saleregister_item.expiry_date')
				->leftJoin('saleregister_item','saleregister_item.parent_id','=','saleregister.id')
				->where([
				['saleregister.company_id',$company],
				['saleregister_item.item_id',$item_id],
				['saleregister_item.expiry_date','>',$date],
				['saleregister.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		function getSaleReturnItems($company,$item_id,$date){
			$data = DB::table('salereturn')
				->select('salereturn_item.barcode','salereturn_item.expiry_date')
				->leftJoin('salereturn_item','salereturn_item.parent_id','=','salereturn.id')
				->where([
				['salereturn.company_id',$company],
				['salereturn_item.item_id',$item_id],
				['salereturn_item.expiry_date','>',$date],
				['salereturn.status','1'],
				])
	            ->get();
	            
			return $data;
		}
		
		$array1 = getOpeningStockItems($company,$item_id,$date)->toArray();
		$array2 = getPurchaseItems($company,$item_id,$date)->toArray();
		$array3 = getPurchaseReturnItems($company,$item_id,$date)->toArray();
		$array4 = getSaleItems($company,$item_id,$date)->toArray();
		$array5 = getSaleReturnItems($company,$item_id,$date)->toArray();
		$input = array_merge($array1,$array2,$array3,$array4,$array5);
		$unique_input = array_map("unserialize", array_unique(array_map("serialize", $input)));
		
		foreach($unique_input as $key=>$value){
		 	$unique_input[$key]->stock = getStock($item_id);
		 	$unique_input[$key]->expiry_date = date_dfy($value->expiry_date);
		 	if($unique_input[$key]->stock == 0){
				unset($unique_input[$key]);
			}
		}
		
		$item->stock = getStock($item_id);
		$item->barcode = array_unique(array_column($unique_input, 'barcode'));
		if(empty($item->barcode)){
			$item->barcode = array("");
		}
		$item->expiry_date = array_unique(array_column($unique_input, 'expiry_date'));
		if(empty($item->expiry_date)){
			$item->expiry_date = array("");
		}


		$name_arr = array();
		$name_arr[] = $item->item_name;

		$item_reference = DB::table('item_reference')
			->select('item_reference.reference as reference')
			->where([
				['item_reference.item_id', $item_id],
				['item_reference.status', '1'],
				])
			->get();
		$reference_arr = array();
		foreach($item_reference as $key=>$value){
			$item_reference[$key]->reference = $value->reference;
			$reference_arr[] = $item_reference[$key]->reference;
		}
		$item_quantity = DB::table('stockregister')
			->where([
				['stockregister.company_id', $company],
				['stockregister.item_id', $item_id],
				['stockregister.status', '1'],
				])
			->get();
		$quantity_arr = array();
		foreach($item_quantity as $key=>$value){
			$item_quantity[$key]->quantity = $value->quantity;
			if($value->quantity > 0){
				$quantity_arr[] = $value->id.'|'.$item_quantity[$key]->quantity;
			}
		}

		$name_merge = array_merge($name_arr,$reference_arr);
		$final_name = implode(",",$name_merge);
		$item->item_name = $final_name;

		$avail_qty = implode(",",$quantity_arr);
		$item->avail_qty = $avail_qty;

		print_r(json_encode($item));
	}
}
