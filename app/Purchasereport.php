<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Purchasereport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchasereport_list($from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");
		
		$result = DB::table('purchaseregister')
			->select('purchaseregister.*','vendor.name as vendor_name', 'company.name as company_name','users.address as address')
            ->leftJoin('company', 'purchaseregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->groupby('id')
			->leftJoin('vendor', 'vendor.id', '=', 'purchaseregister.vendor_id')
			->where([
			['purchaseregister.status','1'],
			['purchaseregister.company_id',$company]
			])
			->whereBetween('purchaseregister.entry_date', [$from_date, $to_date])
            ->get();
            
		return $result;
	}
}
