<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Transport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function transport_list()
	{
		$company = Auth::user()->company_id;
		$transport = DB::table('transport')
			->where([
			['status','1'],
			['company_id',$company]
			])
			->get();
		return $transport;
	}
	public function getTransportName($id)
	{
		$transport = DB::table('transport')->where('id',$id)->get();
		foreach($transport as $value){
			$name = $value->name;
		}
		return $name;
	}
    public function transport_add($company,$name,$contact_person,$mobile,$phone,$address)
    {
		$user_id = Auth::id();
		$transport_id = DB::table('transport')->insertGetId(
		    ['company_id' => $company,'name' => $name,'contact_person' => $contact_person,'mobile' => $mobile,'phone' => $phone,'address' => $address,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $transport_id;
    }
	public function transport_edit($id)
	{
		return DB::table('transport')->where('id',$id)->get();
	}
	public function transport_update($id,$company,$name,$contact_person,$mobile,$phone,$address)
    {
		$user_id = Auth::id();
		return DB::table('transport')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'contact_person' => $contact_person,'mobile' => $mobile,'phone' => $phone,'address' => $address,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function transport_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('transport')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
