<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Openingstock extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function openingstock_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('openingstock')
			->select('openingstock.*', 'item.name as item_name','brand.name as brand_name')
			->where([
			['openingstock.status','1'],
			['openingstock.company_id',$company]
			])
            ->leftJoin('item', 'openingstock.item_id', '=', 'item.id')
			->leftJoin('brand', 'openingstock.brand_id', '=', 'brand.id')

            ->get();
	}
	// public function item_list($category_id)
	// {
	// 	return DB::table('item')->where('category_id',$category_id)->get();
	// }
	public function item_list($brand_id)
	{
		return DB::table('item')->where('brand_id',$brand_id)->get();
	}
	public function brand_list($item_name)
	{
		$company = Auth::user()->company_id;
		$item = DB::table('item')->where([
			['status', '1'],
			['id', $item_name]
		])->get();

		$name=$item[0]->name;

		$item = DB::table('item')
		->select('brand.id as id','brand.name as name')
		->where([
		['item.status','1'],
		['item.name',$name],
		['item.company_id',$company]
		])
		->leftJoin('brand','brand.id','=','item.brand_id')
		->get();

		return $item; 
			//DB::table('brand')->where('company_id',$company)->get();
	}
	public function category_list($id)
	{
		$item = DB::table('item')->where('id',$id)->get();
		foreach ($item as $value){
			$company_id = $value->company_id;
		}
		$category = DB::table('category')
			->where([
			['status','1'],
			['company_id',$company_id]
			])
			->get();
		return $category;
	}
	public function category_id($item_id)
	{
		$item = DB::table('item')->where('id',$item_id)->get();
		foreach ($item as $value){
			$category_id = $value->category_id;
		}
		return $category_id;
	}
	public function brand_id($item_id)
	{
		$item = DB::table('item')->where('id',$item_id)->get();
		foreach ($item as $value){
			$brand_id = $value->brand_id;
		}
		return $brand_id;
	}
	public function getOpeningstockName($id)
	{
		$openingstock = DB::table('openingstock')->where('id',$id)->get();
		foreach($openingstock as $value){
			$name = $value->name;
		}
		return $name;
	}
	public function purchaseRegisterCount()
    {
		$company = Auth::user()->company_id;
		$purchaseregister = DB::table('purchaseregister')
			->select('id')
			->where([
				['status','1'],
				['company_id',$company]
				])
            ->get();
        return $purchaseregister->count();
    }
    public function openingstock_add($company,$item,$brand,$quantity,$rate,$amount,$barcode,$expiry_date,$on_date)
    {
		$result = DB::transaction(function () use($company,$item,$brand,$quantity,$rate,$amount,$barcode,$expiry_date,$on_date)
		{
			$user_id = Auth::id();
			$expiry_date = date_format(date_create($expiry_date),"Y-m-d");
			$on_date = date_format(date_create($on_date),"Y-m-d");
			
			$openingstock_id = DB::table('openingstock')->insertGetId(
			    ['company_id' => $company,'item_id' => $item,'brand_id' => $brand,'quantity' => $quantity,'rate' => $rate,'amount' => $amount,'barcode' => $barcode,'expiry_date' => $expiry_date,'on_date' => $on_date,'created_at' => $this->date,'created_by' => $user_id]
			);
			
			return TRUE;
		});
		return $result;
    }
	public function openingstock_edit($id)
	{
		// return DB::table('openingstock')->where('id',$id)->get();
		return DB::table('openingstock')
			->select('openingstock.*','brand.name as brand_name')
			->where([
			['openingstock.id',$id],
			])
			->leftJoin('brand','brand.id','=','openingstock.brand_id')
            ->get();
	}
	public function openingstock_update($id,$company,$item,$brand,$quantity,$rate,$amount,$barcode,$expiry_date,$on_date)
    {
		$result = DB::transaction(function () use($id,$company,$item,$brand,$quantity,$rate,$amount,$barcode,$expiry_date,$on_date)
		{
		    $user_id = Auth::id();
			$expiry_date = date_format(date_create($expiry_date),"Y-m-d");
			$on_date = date_format(date_create($on_date),"Y-m-d");
			
			DB::table('openingstock')
	            ->where('id', $id)
	            ->update(['company_id' => $company,'item_id' => $item,'brand_id'=>$brand,'quantity' => $quantity,'rate' => $rate,'amount' => $amount,'barcode' => $barcode,'expiry_date' => $expiry_date,'on_date' => $on_date,'updated_at' => $this->date,'updated_by' => $user_id]);
	          
		    return TRUE;
		});
		return $result;
    }
    public function openingstock_delete($id)
	{
		$result = DB::transaction(function () use($id)
		{
			$user_id = Auth::id();
			
			DB::table('openingstock')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	        
		    return TRUE;
		});
		return $result;
	}
	public function ajax($company_id)
	{
		if(!empty($company_id)){
			$category = DB::table('category')->where([
			    ['status', '1'],
			    ['company_id', $company_id]
			])->get();
			$category_count = $category->count();
			if($category_count>0){
				?><option value="">Select</option><?php
				foreach ($category as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function ajax2($category_id)
	{
		if(!empty($category_id)){
			$item = DB::table('item')->where([
			    ['status', '1'],
			    ['category_id', $category_id]
			])->get();
			$item_count = $item->count();
			if($item_count>0){
				?><option value="">Select</option><?php
				foreach ($item as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function ajax3($item_name,$company)
	{
		if(!empty($item_name)){
			
			$item = DB::table('item')->where([
			    ['status', '1'],
			    ['id', $item_name]
			])->get();
			
			$name=$item[0]->name;

			$item = DB::table('item')
			->select('brand.id as brand_id','brand.name as brand_name')
			->where([
			['item.status','1'],
			['item.name',$name],
			['item.company_id',$company]
			])
			->leftJoin('brand','brand.id','=','item.brand_id')
            ->get();
			
			$item_count = $item->count();
			if($item_count>0){
				?><option value="">Select</option><?php
				foreach ($item as $value){ ?>
			  		<option value="<?php echo $value->brand_id; ?>"><?php echo $value->brand_name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}

	public function ajax4($item_name,$company)
	{
		if(!empty($item_name)){
			
			$item = DB::table('item')
			->select('brand.id as brand_id','brand.name as brand_name')
			->where([
			['item.id',$item_name],
			])
			->leftJoin('brand','brand.id','=','item.brand_id')
			->first();
			?>
			<input type="text" class="form-control" value="<?php echo $item->brand_name; ?>" disabled="">
			<input type="hidden" name="brand" id="brand" value=<?php echo $item->brand_id; ?>>
			<?php
		}
		else{
			?><input type="text" class="form-control" name="brand" id="brand" value="" disabled=""><?php
		}
	}
	public function ajax5($brand_id)
	{
		if(!empty($brand_id)){
			$item = DB::table('item')->where([
			    ['status', '1'],
			    ['brand_id', $brand_id]
			])->get();
			$item_count = $item->count();
			if($item_count>0){
				?><option value="">Select</option><?php
				foreach ($item as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
}
