<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Purchaseorderreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchaseorderreport_list($from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");

		$result = DB::table('purchaseorder')
			->select('purchaseorder.*','vendor.name as vendor_name', 'company.name as company_name','users.address as address')
            ->leftJoin('company', 'purchaseorder.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->groupby('id')
			->leftJoin('vendor', 'vendor.id', '=', 'purchaseorder.vendor_id')
			->where([
			['purchaseorder.status','1'],
			['purchaseorder.company_id',$company]
			])
			->whereBetween('purchaseorder.entry_date', [$from_date, $to_date])
			->get();
			
		// print_r($result);
		// die;
            
		return $result;
	}
}
