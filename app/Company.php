<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Company extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function company_list()
	{
		return DB::table('company')->where('status','1')->get();
	}
    public function company_add($name,$address,$phone,$mobile)
    {
		$user_id = Auth::id();
		return DB::table('company')->insert(
		    ['name' => $name,'address' => $address,'phone' => $phone,'mobile' => $mobile,'created_at' => $this->date,'created_by' => $user_id]
		);
    }
	public function company_edit($id)
	{
		return DB::table('company')->where('id',$id)->get();
	}
	public function company_view($id)
	{
		return DB::table('company')->where('id',$id)->first();
	}
	public function company_update($id,$name,$address,$phone,$mobile)
    {
		$user_id = Auth::id();
		return DB::table('company')
            ->where('id', $id)
            ->update(['name' => $name,'address' => $address,'phone' => $phone,'mobile' => $mobile,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function company_delete($id)
	{
		$user_id = Auth::id();
		$company = DB::table('company')->where('id',$id)->get();
		foreach($company as $value){
			$name = $value->name;
			$address = $value->address;
			$phone = $value->phone;
			$mobile = $value->mobile;
		}
		return DB::table('company')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
