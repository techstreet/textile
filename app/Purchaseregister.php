<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Purchaseregister extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchaseregister_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('purchaseregister')
			->select('purchaseregister.*', 'vendor.name as vendor_name')
			->where([
			['purchaseregister.status','1'],
			['purchaseregister.company_id',$company]
			])
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->orderBy('purchaseregister.id', 'desc')
            ->get();
	}
	public function item_list($category_id)
	{
		return DB::table('item')->where('category_id',$category_id)->get();
	}
	public function category_list($id)
	{
		$item = DB::table('item')->where('id',$id)->get();
		foreach ($item as $value){
			$company_id = $value->company_id;
		}
		$category = DB::table('category')->where('company_id',$company_id)->get();
		return $category;
	}
	public function category_id($item_id)
	{
		$item = DB::table('item')->where('id',$item_id)->get();
		foreach ($item as $value){
			$category_id = $value->category_id;
		}
		return $category_id;
	}
	public function getPurchaseregisterName($id)
	{
		$purchaseregister = DB::table('purchaseregister')->where('id',$id)->get();
		foreach($purchaseregister as $value){
			$name = $value->name;
		}
		return $name;
	}
    public function purchaseregister_addbyitem($company,$voucher_no,$purchaseorder_no,$transport,$lr_no,$challan_no,$entry_date,$bill_no,$bill_date,$vendor,$item,$remarks,$item_id,$unit_id,$hsn_code,$barcode,$expiry_date,$quantity,$rate,$amount,$cgstA,$sgstA,$igstA,$discount,$other_expense,$tax_percentage)
    {
		$result = DB::transaction(function () use($company,$voucher_no,$purchaseorder_no,$transport,$lr_no,$challan_no,$entry_date,$bill_no,$bill_date,$vendor,$item,$remarks,$item_id,$unit_id,$hsn_code,$barcode,$expiry_date,$quantity,$rate,$amount,$cgstA,$sgstA,$igstA,$discount,$other_expense,$tax_percentage)
		{
			$user_id = Auth::id();
			if(!empty($entry_date)){
				$entry_date = date_format(date_create($entry_date),"Y-m-d");
			}
			if(!empty($bill_date)){
				$bill_date = date_format(date_create($bill_date),"Y-m-d");
			}
			$sub_total = collect($amount)->sum();
			
			$total = $sub_total - $discount;
			$cgst = collect($cgstA)->sum();
			$sgst = collect($sgstA)->sum();
			$igst = collect($igstA)->sum();
			$tax = $cgst+$sgst+$igst;
			$final_total = $sub_total-$discount+$other_expense+$tax;
			
			$purchaseregister_id = DB::table('purchaseregister')->insertGetId(
			    ['company_id' => $company,'purchasevoucher_no' => $voucher_no,'purchaseorder_no' => $purchaseorder_no,'transport_id' => $transport,'challan_no' => $challan_no,'entry_date' => $entry_date,'bill_no' => $bill_no,'bill_date' => $bill_date,'vendor_id' => $vendor,'sub_total' => $sub_total,'discount' => $discount,'other_expense' => $other_expense,'cgst' => $cgst,'sgst' => $sgst,'igst' => $igst,'tax' => $tax,'total' => $final_total,'remarks' => $remarks,'created_at' => $this->date,'created_by' => $user_id]
			);

			$trimmed_lr = str_replace(' ', '', $lr_no);
			$lr_arr = explode(",",$trimmed_lr);
			if(count($lr_arr>0)){
				for ($i = 0; $i < count($lr_arr); ++$i){
					$lr_no = $lr_arr[$i];
					DB::table('purchaseregister_lr')->insert(
						['parent_id' => $purchaseregister_id,'lr_no' => $lr_no]
					);
				}
			}
			
			if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$unit_id1 = $unit_id[$i];
					$hsn_code1 = $hsn_code[$i];
					$barcode1 = $barcode[$i];
					$expiry_date1 = $expiry_date[$i];
					if(!empty($expiry_date1)){
						$expiry_date1 = date_format(date_create($expiry_date1),"Y-m-d");
					}
					$quantity1 = $quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					if($item_id1!=''){
						
						DB::table('item')
						->where('id', $item_id1)
						->update(['hsn_code' => $hsn_code1,'updated_at' => $this->date,'updated_by' => $user_id]);

						if(empty($taxP1)){
							$taxP1 = 0;
						}

						$stockregister_id = DB::table('stockregister')->insertGetId(['company_id' => $company,'type' => 'purchase','type_id' => $purchaseregister_id,'item_id' => $item_id1,'quantity' => $quantity1,'created_at' => $this->date,'created_by' => $user_id]);

						DB::table('purchaseregister_item')->insert(['parent_id' => $purchaseregister_id,'stockregister_id' => $stockregister_id,'item_id' => $item_id1,'quantity' => $quantity1,'rate' => $rate1,'amount' => $amount1,'barcode' => $barcode1,'expiry_date' => $expiry_date1]);
						
						
					}
				}
			}
			return $purchaseregister_id;
		});
		
		return $result;
    }
	public function purchaseregister_addbypo($company,$voucher_no,$transport,$gr_no,$challan_no,$entry_date,$bill_no,$bill_date,$vendor,$purchaseorder_no,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$taxP,$taxA,$discount,$tax_percentage)
    {
		$result = DB::transaction(function () use($company,$voucher_no,$entry_date,$bill_no,$bill_date,$vendor,$purchaseorder_no,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$taxP,$taxA,$discount,$tax_percentage)
		{
			$user_id = Auth::id();
			$purchaseorder_no = strtoupper($purchaseorder_no);
			if(!empty($entry_date)){
				$entry_date = date_format(date_create($entry_date),"Y-m-d");
			}
			if(!empty($bill_date)){
				$bill_date = date_format(date_create($bill_date),"Y-m-d");
			}
			$purchasevoucher_no = 'PV_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT);
			$sub_total = collect($amount)->sum();
			$total = $sub_total - $discount;
			$tax = collect($taxA)->sum();
			$final_total = $sub_total-$discount+$tax;
			
			$purchaseregister_id = DB::table('purchaseregister')->insertGetId(
			    ['company_id' => $company,'purchasevoucher_no' => $purchasevoucher_no,'entry_date' => $entry_date,'bill_no' => $bill_no,'bill_date' => $bill_date,'vendor_id' => $vendor,'purchaseorder_no' => $purchaseorder_no,'sub_total' => $sub_total,'discount' => $discount,'tax' => $tax,'total' => $final_total,'remarks' => $remarks,'created_at' => $this->date,'created_by' => $user_id]
			);
			
			if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$unit_id1 = $unit_id[$i];
					$barcode1 = $barcode[$i];
					$expiry_date1 = $expiry_date[$i];
					if(!empty($expiry_date1)){
						$expiry_date1 = date_format(date_create($expiry_date1),"Y-m-d");
					}
					$quantity1 = $quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					$taxP1 = $taxP[$i];
					$taxA1 = $taxA[$i];
					if($item_id1!=''){
						
						if(empty($taxP1)){
							$taxP1 = 0;
						}
						
						DB::table('purchaseregister_item')->insert(['parent_id' => $purchaseregister_id,'item_id' => $item_id1,'quantity' => $quantity1,'rate' => $rate1,'amount' => $amount1,'tax_rate' => $taxP1,'tax_amount' => $taxA1,'barcode' => $barcode1,'expiry_date' => $expiry_date1]);
						
					}
				}
			}
			return $purchaseregister_id;
		});
		return $result;
    }
	public function purchaseregister_edit($id)
	{
		return DB::table('purchaseregister')
			->select('purchaseregister.*', 'vendor.name as vendor_name')
			->where('purchaseregister.id',$id)
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->get();
	}
	//function updated by anurag company address
	public function purchaseregister_view($id)
	{
		return DB::table('purchaseregister')
			->select('purchaseregister.*','transport.name as transport_name', 'vendor.name as vendor_name', 'vendor.mobile as vendor_mobile', 'vendor.gstin as vendor_gstin', 'vendor.address as vendor_address','company.name as company_name','users.address as address')
			->where('purchaseregister.id',$id)
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
			->leftJoin('company', 'purchaseregister.company_id', '=', 'company.id')
			->leftJoin('transport', 'purchaseregister.transport_id', '=', 'transport.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->get();
	}
	public function purchaseregister_lr($id)
	{
		return DB::table('purchaseregister_lr')
			->select('purchaseregister_lr.*','purchaseregister.purchasevoucher_no')
			->leftJoin('purchaseregister','purchaseregister.id','=','purchaseregister_lr.parent_id')
			->where([
			['purchaseregister_lr.parent_id',$id],
			['purchaseregister_lr.status','1'],
			])
			->get();
	}
	public function lr_str($id)
	{
		$purchaseregister_lr = DB::table('purchaseregister_lr')
			->select('purchaseregister_lr.*','purchaseregister.purchasevoucher_no')
			->leftJoin('purchaseregister','purchaseregister.id','=','purchaseregister_lr.parent_id')
			->where([
			['purchaseregister_lr.parent_id',$id],
			['purchaseregister_lr.status','1'],
			])
			->get();
		$lr = '';
		$flag = 1;
		$lr_count = count($purchaseregister_lr);
		foreach($purchaseregister_lr as $key=>$value){
			$purchaseregister_lr[$key]->lr = $value->lr_no;
			$lr .= $value->lr_no;
			if($flag < $lr_count){
				$lr .=',';
			}
			$flag++;
		}
		return $lr;
	}
	public function other_expense($id)
	{
		$purchaseregister_lr = DB::table('purchaseregister_lr')
			->select(DB::raw('SUM(amount) as other_expense'))
			->where([
			['purchaseregister_lr.parent_id',$id],
			['purchaseregister_lr.payment_status','1'],
			['purchaseregister_lr.status','1'],
			])
			->first();
		return $purchaseregister_lr->other_expense;
	}
	public function purchaseregister_item($id)
	{
        return DB::table('purchaseregister')
			//->select('purchaseregister_item.*','item.name as item_name','item.strength','unit.id as unit_id','unit.name as unit_name','brand.name as brand_name','brand.id as brand_id','form.id as form_id','form.name as form_name')
			->select(DB::raw('SUM(purchaseregister_item.pcs) as pcs_total'),DB::raw('SUM(purchaseregister_item.quantity) as quantity_total'),'purchaseregister_item.*','item.name as item_name','item.strength','item.hsn_code','unit.id as unit_id','unit.name as unit_name','brand.name as brand_name','brand.id as brand_id','form.id as form_id','form.name as form_name')
			->where([
			['purchaseregister.id',$id],
			['purchaseregister.status','1'],
			])
			->leftJoin('purchaseregister_item', 'purchaseregister_item.parent_id', '=', 'purchaseregister.id')
            ->leftJoin('item', 'purchaseregister_item.item_id', '=', 'item.id')
			->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
			->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
			->leftJoin('form', 'item.form_id', '=', 'form.id')
			->groupBy('purchaseregister_item.item_id')
            ->get();
	}
	public function purchaseregister_updatebyitem($id,$company,$entry_date,$bill_no,$bill_date,$vendor,$item,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$taxP,$taxA,$discount,$tax_percentage)
    {
		$result = DB::transaction(function () use($id,$company,$entry_date,$bill_no,$bill_date,$vendor,$item,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$taxP,$taxA,$discount,$tax_percentage)
		{
			$user_id = Auth::id();
			if(!empty($entry_date)){
				$entry_date = date_format(date_create($entry_date),"Y-m-d");
			}
			if(!empty($bill_date)){
				$bill_date = date_format(date_create($bill_date),"Y-m-d");
			}
			$sub_total = collect($amount)->sum();
			
			$total = $sub_total - $discount;
			$tax = collect($taxA)->sum();
			$final_total = $sub_total-$discount+$tax;
			
			DB::table('purchaseregister')
	            ->where('id', $id)
	            ->update(['company_id' => $company,'entry_date' => $entry_date,'bill_no' => $bill_no,'bill_date' => $bill_date,'vendor_id' => $vendor,'sub_total' => $sub_total,'discount' => $discount,'tax' => $tax,'total' => $final_total,'remarks' => $remarks,'updated_at' => $this->date,'updated_by' => $user_id]);
	        
	        DB::table('purchaseregister_item')
	            ->where('parent_id', $id)
	            ->delete();
	            
	        if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$unit_id1 = $unit_id[$i];
					$barcode1 = $barcode[$i];
					$expiry_date1 = $expiry_date[$i];
					if(!empty($expiry_date1)){
						$expiry_date1 = date_format(date_create($expiry_date1),"Y-m-d");
					}
					$quantity1 = $quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					$taxP1 = $taxP[$i];
					$taxA1 = $taxA[$i];
					if($item_id1!=''){
						
						if(empty($taxP1)){
							$taxP1 = 0;
						}
						
						DB::table('purchaseregister_item')->insert(['parent_id' => $id,'item_id' => $item_id1,'quantity' => $quantity1,'rate' => $rate1,'amount' => $amount1,'tax_rate' => $taxP1,'tax_amount' => $taxA1,'barcode' => $barcode1,'expiry_date' => $expiry_date1]);
					}
				}
			}
			return TRUE;            
		});
		
		return $result;
    }
    public function purchaseregister_delete($id)
	{        
        $result = DB::transaction(function () use($id)
		{
			$user_id = Auth::id();
			
        	DB::table('purchaseregister')
            ->where('id', $id)
			->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
			
			DB::table('stockregister')
				->where([
					['type', 'purchase'],
					['type_id', $id]
				])
				->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);

			DB::table('purchaseregister_lr')
				->where('parent_id', $id)
				->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
            
			return TRUE;
		});
		return $result;
	}
	public function ajax($company_id)
	{
		if(!empty($company_id)){
			$category = DB::table('category')->where([
			    ['status', '1'],
			    ['company_id', $company_id]
			])->get();
			$category_count = $category->count();
			if($category_count>0){
				?><option value="">Select</option><?php
				foreach ($category as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function ajax2($category_id)
	{
		if(!empty($category_id)){
			$item = DB::table('item')->where([
			    ['status', '1'],
			    ['category_id', $category_id]
			])->get();
			$item_count = $item->count();
			if($item_count>0){
				?><option value="">Select</option><?php
				foreach ($item as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function additems($item_id)
	{
		$items = DB::table('item')
			->select('item.*','unit.name as unit_name','form.name as form_name','brand.name as brand_name')
			->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
			->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->where('item.id', $item_id)
            ->get();  
		print_r(json_encode( array($items)));
	}
	public function additemsbypo($purchaseorder_no)
	{
		$company_id = Auth::user()->company_id;
		$items = DB::table('purchaseorder')
			->select('purchaseorder_item.*','item.name','item.hsn_code','brand.name as brand_name','unit.name as unit_name','vendor.id as vendor_id','vendor.name as vendor_name','vendor.gstin as vendor_gstin')
			->leftJoin('vendor', 'purchaseorder.vendor_id', '=', 'vendor.id')
            ->leftJoin('purchaseorder_item', 'purchaseorder_item.parent_id', '=', 'purchaseorder.id')
            ->leftJoin('item', 'purchaseorder_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
			->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
			->where([
				['purchaseorder.purchaseorder_no', $purchaseorder_no],
				['purchaseorder.company_id',$company_id],
				['purchaseorder.status','1'],
				])
            ->get();  
		print_r(json_encode( array($items)));
	}
}
