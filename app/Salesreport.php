<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Salesreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function salesreport_list($category,$from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");
		
		if($category == 'All'){
			$result = DB::table('saleregister')
			->select('saleregister.*','client.name as client_name','company.name as company_name','users.address as address')
			->leftJoin('client', 'client.id', '=', 'saleregister.client_id')
            ->leftJoin('company', 'saleregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
               ->groupby('saleregister.invoice_no')
			->where([
			['saleregister.status','1'],
			['saleregister.company_id',$company]
			])
			->whereBetween('saleregister.entry_date', [$from_date, $to_date])
            ->get();
		}
		else{
			$result = DB::table('saleregister')
			->select('saleregister.*','client.name as client_name','company.name as company_name','users.address as address')
			->leftJoin('client', 'client.id', '=', 'saleregister.client_id')
            ->leftJoin('company', 'saleregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
                ->groupby('saleregister.invoice_no')
			->where([
			['saleregister.sale_type',$category],
			['saleregister.status','1'],
			['saleregister.company_id',$company]
			])
			->whereBetween('saleregister.entry_date', [$from_date, $to_date])
            ->get();
		}
            
		return $result;
	}
}
