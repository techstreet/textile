<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Purchasereturn extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchasereturn_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('purchasereturn')
			->select('purchasereturn.*', 'vendor.name as vendor_name')
			->where([
			['purchasereturn.status','1'],
			['purchasereturn.company_id',$company],
			['purchaseregister.company_id',$company]
			])
            ->leftJoin('purchaseregister', 'purchaseregister.purchasevoucher_no', '=', 'purchasereturn.purchasevoucher_no')
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->orderBy('purchasereturn.id', 'desc')
            ->get();
	}
	public function item_list($category_id)
	{
		return DB::table('item')->where('category_id',$category_id)->get();
	}
	public function category_list($id)
	{
		$item = DB::table('item')->where('id',$id)->get();
		foreach ($item as $value){
			$company_id = $value->company_id;
		}
		$category = DB::table('category')->where('company_id',$company_id)->get();
		return $category;
	}
	public function category_id($item_id)
	{
		$item = DB::table('item')->where('id',$item_id)->get();
		foreach ($item as $value){
			$category_id = $value->category_id;
		}
		return $category_id;
	}
	public function getPurchasereturnName($id)
	{
		$purchasereturn = DB::table('purchasereturn')->where('id',$id)->get();
		foreach($purchasereturn as $value){
			$name = $value->name;
		}
		return $name;
	}
    public function purchasereturn_add($company,$debitnote_no,$debitnote_date,$purchasevoucher_no,$remarks,$transport,$gr_no,$booking_date,$item_id,$stockregister_id,$unit_id,$barcode,$expiry_date,$quantity,$return_quantity,$rate,$amount,$return_amount,$cgstA,$sgstA,$igstA,$discount,$other_expense)
    {
		$result = DB::transaction(function () use($company,$debitnote_no,$debitnote_date,$purchasevoucher_no,$remarks,$transport,$gr_no,$booking_date,$item_id,$stockregister_id,$unit_id,$barcode,$expiry_date,$quantity,$return_quantity,$rate,$amount,$return_amount,$cgstA,$sgstA,$igstA,$discount,$other_expense)
		{
			$user_id = Auth::id();
			if(!empty($debitnote_date)){
				$debitnote_date = date_format(date_create($debitnote_date),"Y-m-d");
			}
			if(!empty($booking_date)){
				$booking_date = date_format(date_create($booking_date),"Y-m-d");
			}
			
			$sub_total = collect($return_amount)->sum();

			$total = $sub_total - $discount;
			$cgst = collect($cgstA)->sum();
			$sgst = collect($sgstA)->sum();
			$igst = collect($igstA)->sum();
			$tax = $cgst+$sgst+$igst;
			$final_total = $sub_total-$discount+$other_expense+$tax;
			$purchasereturn_id = DB::table('purchasereturn')->insertGetId(
			    ['company_id' => $company,'debitnote_no' => $debitnote_no,'debitnote_date' => $debitnote_date,'purchasevoucher_no' => $purchasevoucher_no,'remarks' => $remarks,'transport_id' => $transport,'gr_no' => $gr_no,'booking_date' => $booking_date,'sub_total' => $sub_total,'discount' => $discount,'other_expense' => $other_expense,'cgst' => $cgst,'sgst' => $sgst,'igst' => $igst,'tax' => $tax,'total' => $final_total,'created_at' => $this->date,'created_by' => $user_id]
			);
			
			if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$stockregister_id1 = $stockregister_id[$i];
					$unit_id1 = $unit_id[$i];
					$barcode1 = $barcode[$i];
					$expiry_date1 = $expiry_date[$i];
					if(!empty($expiry_date1)){
						$expiry_date1 = date_format(date_create($expiry_date1),"Y-m-d");
					}
					$quantity1 = $quantity[$i];
					$return_quantity1 = $return_quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					$return_amount1 = $return_amount[$i];
					if($item_id1!=''){
						DB::table('purchasereturn_item')->insert(['parent_id' => $purchasereturn_id,'item_id' => $item_id1,'stockregister_id' => $stockregister_id1,'quantity' => $quantity1,'return_quantity' => $return_quantity1,'rate' => $rate1,'amount' => $amount1,'return_amount' => $return_amount1,'barcode' => $barcode1,'expiry_date' => $expiry_date1]);
						DB::table('stockregister')
						->where('id', $stockregister_id1)
						->decrement('quantity', $return_quantity1);
					}
				}
			}
			return TRUE;
		});
		return $result;
    }
	public function purchasereturn_edit($id)
	{
		return DB::table('purchasereturn')
			->select('purchasereturn.*', 'vendor.name as vendor_name')
			->where('purchasereturn.id',$id)
            ->leftJoin('purchaseregister', 'purchaseregister.purchasevoucher_no', '=', 'purchasereturn.purchasevoucher_no')
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
            ->get();
	}
	//function updated by anurag company address
	public function purchasereturn_view($id)
	{
		return DB::table('purchasereturn')
			->select('purchasereturn.*', 'purchaseregister.purchaseorder_no', 'purchaseregister.entry_date as purchasevoucher_date', 'transport.name as transport_name', 'vendor.name as vendor_name', 'vendor.mobile as vendor_mobile', 'vendor.gstin as vendor_gstin', 'vendor.address as vendor_address','company.name as company_name','users.address as address')
			->where('purchasereturn.id',$id)
            ->leftJoin('purchaseregister', 'purchaseregister.purchasevoucher_no', '=', 'purchasereturn.purchasevoucher_no')
            ->leftJoin('vendor', 'purchaseregister.vendor_id', '=', 'vendor.id')
			->leftJoin('company', 'purchasereturn.company_id', '=', 'company.id')
			->leftJoin('transport', 'purchasereturn.transport_id', '=', 'transport.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->get();
	}
	public function purchasereturn_item($id)
	{
        return DB::table('purchasereturn')
			->select('purchasereturn_item.*','item.name as item_name','item.strength','item.form_id','item.hsn_code','unit.name as unit_name','form.name as form_name')
			->where([
			['purchasereturn.id',$id],
			['purchasereturn.status','1']
			])
			->leftJoin('purchasereturn_item', 'purchasereturn_item.parent_id', '=', 'purchasereturn.id')
            ->leftJoin('item', 'purchasereturn_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->get();
	}
	public function purchasereturn_update($id,$company,$debitnote_date,$purchasevoucher_no,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$return_quantity,$rate,$amount,$return_amount)
    {
		$result = DB::transaction(function () use($id,$company,$debitnote_date,$purchasevoucher_no,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$return_quantity,$rate,$amount,$return_amount)
		{
			$user_id = Auth::id();
			if(!empty($debitnote_date)){
				$debitnote_date = date_format(date_create($debitnote_date),"Y-m-d");
			}
			$sub_total = collect($return_amount)->sum();
			
			DB::table('purchasereturn')
	            ->where('id', $id)
	            ->update(['company_id' => $company,'debitnote_date' => $debitnote_date,'remarks' => $remarks,'sub_total' => $sub_total,'total' => $sub_total,'updated_at' => $this->date,'updated_by' => $user_id]);
	            
	        DB::table('purchasereturn_item')
	            ->where('parent_id', $id)
	            ->delete();
	            
	        if(count($item_id>0)){
				for ($i = 0; $i < count($item_id); ++$i){
					$item_id1 = $item_id[$i];
					$unit_id1 = $unit_id[$i];
					$barcode1 = $barcode[$i];
					$expiry_date1 = $expiry_date[$i];
					if(!empty($expiry_date1)){
						$expiry_date1 = date_format(date_create($expiry_date1),"Y-m-d");
					}
					$quantity1 = $quantity[$i];
					$return_quantity1 = $return_quantity[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					$return_amount1 = $return_amount[$i];
					if($item_id1!=''){
						DB::table('purchasereturn_item')->insert(['parent_id' => $id,'item_id' => $item_id1,'quantity' => $quantity1,'return_quantity' => $return_quantity1,'rate' => $rate1,'amount' => $amount1,'return_amount' => $return_amount1,'barcode' => $barcode1,'expiry_date' => $expiry_date1]);
					}
				}
			}
			return TRUE;
		});
		return $result;
    }
    public function purchasereturn_delete($id)
	{
		$result = DB::transaction(function () use($id)
		{
			$user_id = Auth::id();

			$purchasereturn = DB::table('purchasereturn')
				->select('purchasereturn_item.*')
				->where([
				['purchasereturn.id',$id],
				['purchasereturn.status','1'],
				['purchasereturn_item.return_quantity','>','0']
				])
				->leftJoin('purchasereturn_item', 'purchasereturn_item.parent_id', '=', 'purchasereturn.id')
				->get();
			foreach($purchasereturn as $value){
				$stockregister_id = $value->stockregister_id;
				$return_quantity = $value->return_quantity;
				DB::table('stockregister')
					->where('id', $stockregister_id)
					->increment('quantity', $return_quantity);
			}
			
        	DB::table('purchasereturn')
	            ->where('id', $id)
	            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
            
			return TRUE;
		});
		return $result;
	}
	public function ajax($company_id)
	{
		if(!empty($company_id)){
			$category = DB::table('category')->where([
			    ['status', '1'],
			    ['company_id', $company_id]
			])->get();
			$category_count = $category->count();
			if($category_count>0){
				?><option value="">Select</option><?php
				foreach ($category as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function ajax2($category_id)
	{
		if(!empty($category_id)){
			$item = DB::table('item')->where([
			    ['status', '1'],
			    ['category_id', $category_id]
			])->get();
			$item_count = $item->count();
			if($item_count>0){
				?><option value="">Select</option><?php
				foreach ($item as $value){ ?>
			  		<option value="<?php echo $value->id; ?>"><?php echo $value->name; ?></option>
			  	<?php }
			}
			else{
				?><option value="">Select</option><?php
			}
		}
		else{
			?><option value="">Select</option><?php
		}
	}
	public function additems($purchasevoucher_no)
	{
		$company_id = Auth::user()->company_id;
		$items = DB::table('purchaseregister')
			->select('purchaseregister_item.*','item.name as item_name','item.strength','item.hsn_code as hsn_code','item.form_id','unit.name as unit_name','form.name as form_name','vendor.mobile as vendor_mobile','vendor.name as vendor_name','vendor.gstin as vendor_gstin')
            ->leftJoin('purchaseregister_item', 'purchaseregister_item.parent_id', '=', 'purchaseregister.id')
            ->leftJoin('item', 'purchaseregister_item.item_id', '=', 'item.id')
            ->leftJoin('unit', 'item.unit_id', '=', 'unit.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
			->leftJoin('vendor', 'vendor.id', '=', 'purchaseregister.vendor_id')
			->where([
				['purchaseregister.purchasevoucher_no', $purchasevoucher_no],
				['purchaseregister.company_id', $company_id],
				['purchaseregister.status', '1'],
				])
            ->get();
		foreach($items as $key=>$value){
			$items[$key]->expiry_date = date_dfy($value->expiry_date);
		}
		print_r(json_encode( array($items)));
	}
	
}
