<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Manufacturer extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function manufacturer_list()
	{
		$company = Auth::user()->company_id;
		$manufacturer = DB::table('manufacturer')
			->where([
			['is_active','1'],
			['status','1'],
			['company_id',$company]
			])
			->get();
		return $manufacturer;
	}
	public function getManufacturerName($id)
	{
		$manufacturer = DB::table('manufacturer')->where('id',$id)->get();
		foreach($manufacturer as $value){
			$name = $value->name;
		}
		return $name;
	}
    public function manufacturer_add($company,$name,$status)
    {
		$user_id = Auth::id();
		$manufacturer_id = DB::table('manufacturer')->insertGetId(
		    ['company_id' => $company,'name' => $name,'is_active' => $status,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $manufacturer_id;
    }
	public function manufacturer_edit($id)
	{
		return DB::table('manufacturer')->where('id',$id)->get();
	}
	public function manufacturer_update($id,$company,$name,$status)
    {
		$user_id = Auth::id();
		return DB::table('manufacturer')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'is_active' => $status,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function manufacturer_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('manufacturer')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
