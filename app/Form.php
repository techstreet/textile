<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Form extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function form_list()
	{
		$company = Auth::user()->company_id;
		$form = DB::table('form')
			->where([
			['is_active','1'],
			['status','1'],
			['company_id',$company]
			])
			->get();
		return $form;
	}
	public function getFormName($id)
	{
		$form = DB::table('form')->where('id',$id)->get();
		foreach($form as $value){
			$name = $value->name;
		}
		return $name;
	}
    public function form_add($company,$name,$status)
    {
		$user_id = Auth::id();
		$form_id = DB::table('form')->insertGetId(
		    ['company_id' => $company,'name' => $name,'is_active' => $status,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $form_id;
    }
	public function form_edit($id)
	{
		return DB::table('form')->where('id',$id)->get();
	}
	public function form_update($id,$company,$name,$status)
    {
		$user_id = Auth::id();
		return DB::table('form')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'is_active' => $status,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function form_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('form')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
