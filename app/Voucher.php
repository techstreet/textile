<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Voucher extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
	}
	
    public function voucher_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('voucher')
			->select('voucher.*','client.name as client_name','client.address as client_address','client.phone as client_phone','client.mobile as client_mobile')
			->where([
			['voucher.status','1'],
			['voucher.company_id',$company]
			])
            ->leftJoin('client', 'voucher.client_id', '=', 'client.id')
			->orderBy('voucher.voucher_no', 'desc')
			->groupBy('voucher.voucher_no')
            ->get();
	}

	public function voucher_add($company,$voucher_no,$entry_date,$client,$shipping_client,$transport,$gr_no,$booking_date,$ref,$remarks,$item_name,$pcs,$quantity,$unit_name,$brand_name,$rate,$amount,$discount,$cgstA,$sgstA,$igstA,$other_expense)
    {		
		$result = DB::transaction(function () use ($company,$voucher_no,$entry_date,$client,$shipping_client,$transport,$gr_no,$booking_date,$ref,$remarks,$item_name,$pcs,$quantity,$unit_name,$brand_name,$rate,$amount,$discount,$cgstA,$sgstA,$igstA,$other_expense) {
			
			$user_id = Auth::id();
			$entry_date = date_format(date_create($entry_date),"Y-m-d");
			$booking_date = date_format(date_create($booking_date),"Y-m-d");
			$sub_total = collect($amount)->sum();
			
			$total = $sub_total - $discount;

			$cgst = collect($cgstA)->sum();
			$sgst = collect($sgstA)->sum();
			$igst = collect($igstA)->sum();
			$tax = $cgst+$sgst+$igst;
			$final_total = $sub_total-$discount+$other_expense+$tax;
			
			$voucher_id = DB::table('voucher')->insertGetId(
			    ['company_id' => $company,'voucher_no' => $voucher_no,'entry_date' => $entry_date,'client_id' => $client,'shipping_client_id' => $shipping_client,'transport_id' => $transport,'gr_no' => $gr_no,'booking_date' => $booking_date,'sub_total' => $sub_total,'discount' => $discount,'other_expense' => $other_expense,'cgst' => $cgst,'sgst' => $sgst,'igst' => $igst,'total' => $final_total,'ref' => $ref,'remarks' => $remarks,'created_at' => $this->date,'created_by' => $user_id]
			);
			
			if(count($item_name>0)){
				for ($i = 0; $i < count($item_name); ++$i){
					$item_name1 = $item_name[$i];
					$pcs1 = $pcs[$i];
					$quantity1 = $quantity[$i];
					$unit_name1 = $unit_name[$i];
					$brand_name1 = $brand_name[$i];
					$rate1 = $rate[$i];
					$amount1 = $amount[$i];
					if($item_name1!=''){
						DB::table('voucher_item')->insert(['parent_id' => $voucher_id,'item_name' => $item_name1,'pcs' => $pcs1,'quantity' => $quantity1,'unit_name' => $unit_name1,'brand_name' => $brand_name1,'rate' => $rate1,'amount' => $amount1]);
					}
				}
			}
			
			return $voucher_id;
		});
		return $result;
    }

	public function voucher_view($id)
	{
		return DB::table('voucher')
			->select('voucher.*', 'transport.name as transport_name', 'client.name as client_name','client.mobile as client_mobile','client.gstin as client_gstin','client.address as client_address','company.name as company_name','company.address as address')
			->where('voucher.id',$id)
            ->leftJoin('client', 'voucher.client_id', '=', 'client.id')
			->leftJoin('company', 'voucher.company_id', '=', 'company.id')
			->leftJoin('transport', 'voucher.transport_id', '=', 'transport.id')
            ->get();
	}

	public function shipping_client($id)
	{
		return DB::table('voucher')
			->select('client.name as client_name','client.mobile as client_mobile','client.gstin as client_gstin','client.address as client_address')
			->where('voucher.id',$id)
			->leftJoin('client', 'voucher.shipping_client_id', '=', 'client.id')
            ->first();
	}

	public function voucher_item($id)
	{
        $items = DB::table('voucher')
			->select('voucher_item.*')
			->where([
			['voucher.id',$id],
			['voucher.status','1']
			])
			->leftJoin('voucher_item', 'voucher_item.parent_id', '=', 'voucher.id')
            ->get();
		return $items;
	}

	public function voucher_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('voucher')
			->where('id', $id)
			->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
	
}
