<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Pricevariation extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
	public function pricevariation_list($item_id,$from_date,$to_date)
	{
	    $company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");
		
        return DB::table('purchaseregister')
			->select('purchaseregister.*','purchaseregister_item.rate','purchaseregister_item.quantity','item.name','item.currency','category.name as category_name','brand.name as brand_name','company.name as company_name','users.address as address')
            ->leftJoin('company', 'purchaseregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->groupby('id')
			->leftJoin('purchaseregister_item', 'purchaseregister_item.parent_id', '=', 'purchaseregister.id')
			->leftJoin('item', 'item.id', '=', 'purchaseregister_item.item_id')
			->leftJoin('category','category.id','=','item.category_id')
			->leftJoin('brand','brand.id','=','item.brand_id')
			->where([
			['purchaseregister_item.item_id',$item_id],
			['purchaseregister.company_id',$company],
			['purchaseregister.status','1']
			])
			->whereBetween('purchaseregister.entry_date', [$from_date, $to_date])
			->orderBy('purchaseregister.id','desc')
			->get();
	}
}
