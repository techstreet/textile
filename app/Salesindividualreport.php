<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Salesindividualreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function salesreport_list($item,$from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");
		
		$result = DB::table('saleregister')
			->select('saleregister.*','client.name as client_name','company.name as company_name','users.address as address','item.name as item_name','saleregister_item.quantity as quantity','saleregister_item.rate as rate','saleregister_item.amount as amount')
			->leftJoin('saleregister_item', 'saleregister.id', '=', 'saleregister_item.parent_id')
			->leftJoin('item', 'item.id', '=', 'saleregister_item.item_id')
			->leftJoin('client', 'client.id', '=', 'saleregister.client_id')
            ->leftJoin('company', 'saleregister.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
               ->groupby('saleregister.invoice_no')
			->where([
			['saleregister.status','1'],
			['item.id',$item],
			['saleregister.company_id',$company]
			])
			->whereBetween('saleregister.entry_date', [$from_date, $to_date])
			->get();
            
		return $result;
	}
}
