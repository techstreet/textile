<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Discrepancyreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function discrepancyreport_list($category_id,$form_id,$manufacturer_id,$brand_id,$date)
	{

		$company = Auth::user()->company_id;
		
		$param1 = array();
		$param2 = array();
		$param3 = array();
		$param4 = array();
		
		if(!empty($category_id)){
			$param1 = array(
			    'item.category_id' => $category_id,
			);
		}
		if(!empty($form_id)){
			$param2 = array(
			    'item.form_id' => $form_id,
			);
		}
		if(!empty($manufacturer_id)){
			$param3 = array(
			    'item.manufacturer_id' => $manufacturer_id,
			);
		}
		if(!empty($brand_id)){
			$param4 = array(
			    'item.brand_id' => $brand_id,
			);
		}
		
		$param5 = array(
		    'item.status' => '1'
		);
		
		$search_param = array_merge($param1,$param2,$param3,$param4,$param5);
	    
	    $items = DB::table('item')
			->select('item.*','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name','company.name as company_name','users.address as address')
            ->leftJoin('company', 'item.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->groupby('id')
			->where($search_param)
            ->leftJoin('category','category.id','=','item.category_id')
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->leftJoin('manufacturer', 'item.manufacturer_id', '=', 'manufacturer.id')
            ->leftJoin('form', 'item.form_id', '=', 'form.id')
            ->where('item.company_id',$company)
            ->groupBy('item.id')
            ->get();
            
		foreach ($items as $key=>$value){
			$items[$key]->opening_stock = getOpeningStockByDate($value->id,$date);
			$items[$key]->purchase = getPurchaseStockByDate($value->id,$date);
			$items[$key]->purchase_return = getPurchaseReturnStockByDate($value->id,$date);
			$items[$key]->purchase_total = getPurchaseStockByDate($value->id,$date) - getPurchaseReturnStockByDate($value->id,$date);
			$items[$key]->sale = getTotalSalesByDate($value->id,$date);
			$items[$key]->sale_return = getTotalSaleReturnStockByDate($value->id,$date);
			$items[$key]->sale_total = getTotalSalesByDate($value->id,$date) - getTotalSaleReturnStockByDate($value->id,$date);
			$items[$key]->closing_stock = $items[$key]->opening_stock + $items[$key]->purchase_total - $items[$key]->sale_total;
			$items[$key]->actual_stock = getActualStock($value->id,$date);
			$items[$key]->unit_price = getPrice($value->id);
			$items[$key]->stock_value = getStock($value->id) * getPrice($value->id);
		}
		
		return $items;
	}
}
