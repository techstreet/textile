<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Custom extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function overall_tax()
	{
		$company = Auth::user()->company_id;
		$tax_percentage = 0;
		$data = DB::table('tax')
			->select(DB::raw('SUM(rate) as tax_percentage'))
			->where([
			['status','1'],
			['is_active','1'],
			['company_id',$company],
			['effective_from','<=',$this->date]
			])
			->groupBy('id')
			->first();
		if(!empty($data)){
			$tax_percentage = $data->tax_percentage;
		}
		return $tax_percentage;
	}
	public function cgst_tax()
	{
		$company = Auth::user()->company_id;
		$tax_percentage = 0;
		$data = DB::table('tax')
			->select(DB::raw('SUM(rate) as tax_percentage'))
			->where([
			['status','1'],
			['name','CGST'],
			['is_active','1'],
			['company_id',$company],
			['effective_from','<=',$this->date]
			])
			->groupBy('id')
			->first();
		if(!empty($data)){
			$tax_percentage = $data->tax_percentage;
		}
		return $tax_percentage;
	}
	public function sgst_tax()
	{
		$company = Auth::user()->company_id;
		$tax_percentage = 0;
		$data = DB::table('tax')
			->select(DB::raw('SUM(rate) as tax_percentage'))
			->where([
			['status','1'],
			['name','SGST'],
			['is_active','1'],
			['company_id',$company],
			['effective_from','<=',$this->date]
			])
			->groupBy('id')
			->first();
		if(!empty($data)){
			$tax_percentage = $data->tax_percentage;
		}
		return $tax_percentage;
	}
	public function igst_tax()
	{
		$company = Auth::user()->company_id;
		$tax_percentage = 0;
		$data = DB::table('tax')
			->select(DB::raw('SUM(rate) as tax_percentage'))
			->where([
			['status','1'],
			['name','IGST'],
			['is_active','1'],
			['company_id',$company],
			['effective_from','<=',$this->date]
			])
			->groupBy('id')
			->first();
		if(!empty($data)){
			$tax_percentage = $data->tax_percentage;
		}
		return $tax_percentage;
	}
}
