<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Salesreturnreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function salesreturnreport_list($from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");
		
		$result = DB::table('salereturn')
			->select('salereturn.*','client.name as client_name','company.name as company_name','users.address as address')
			->leftJoin('saleregister', 'saleregister.invoice_no', '=', 'salereturn.invoice_no')
			->leftJoin('client', 'client.id', '=', 'saleregister.client_id')
                  ->leftJoin('company', 'salereturn.company_id', '=', 'company.id')
                ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->groupby('id')
			->where([
			['salereturn.status','1'],
			['salereturn.company_id',$company]
			])
			->whereBetween('salereturn.creditnote_date', [$from_date, $to_date])
            ->get();
           
		return $result;
	}
}
