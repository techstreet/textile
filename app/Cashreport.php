<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Cashreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function cashreport_list($cash_from,$cash_to,$flag)
	{
		$company = Auth::user()->company_id;
		$cash_from = date_format(date_create($cash_from),"Y-m-d");
		$cash_to = date_format(date_create($cash_to),"Y-m-d");
		
		if($flag == 1){
			$data = DB::table('client_register')
			->select(DB::raw('client.id as customer_id,client.name as customer_name,client.contact_name,client.email,client.phone,client.mobile,client.address,sum(client_register.debit) as debit,sum(client_register.credit) as credit'))
			->leftJoin('client','client.id','=','client_register.client_id')
			->where([
			['client.company_id',$company],
			['client.status','1'],
			['client_register.status','1'],
			])
			->groupBy('client_register.client_id')
            ->orderBy('client.id','DESC')
			->get();
		}
		else{
			$data = DB::table('client_register')
			->select(DB::raw('client.id as customer_id,client.name as customer_name,client.contact_name,client.email,client.phone,client.mobile,client.address,sum(client_register.debit) as debit,sum(client_register.credit) as credit'))
			->leftJoin('client','client.id','=','client_register.client_id')
			->where([
			['client.company_id',$company],
			['client.status','1'],
			['client_register.status','1'],
			])
			->whereBetween('client_register.created_at', array($cash_from, $cash_to))
			->groupBy('client_register.client_id')
            ->orderBy('client.id','DESC')
			->get();
		}
            
		return $data;
	}
}
