<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Vendor extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function vendor_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('vendor')
			->where([
			['status','1'],
			['company_id',$company]
			])
			->get();
	}
    public function vendor_add($company,$name,$contact_name,$email,$address,$phone,$mobile,$gstin)
    {
		$user_id = Auth::id();
		$vendor_id = DB::table('vendor')->insertGetId(
		    ['company_id' => $company,'name' => $name,'contact_name' => $contact_name,'email' => $email,'address' => $address,'phone' => $phone,'mobile' => $mobile,'gstin' => $gstin,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $vendor_id;
    }
	public function vendor_edit($id)
	{
		return DB::table('vendor')->where('id',$id)->get();
	}
	public function vendor_update($id,$company,$name,$contact_name,$email,$address,$phone,$mobile,$gstin)
    {
		$user_id = Auth::id();
		return DB::table('vendor')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'contact_name' => $contact_name,'email' => $email,'address' => $address,'phone' => $phone,'mobile' => $mobile,'gstin' => $gstin,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function vendor_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('vendor')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
