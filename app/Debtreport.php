<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Debtreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function debtreport_list($debt_from,$debt_to,$flag)
	{
		$company = Auth::user()->company_id;
		$debt_from = date_format(date_create($debt_from),"Y-m-d");
		$debt_to = date_format(date_create($debt_to),"Y-m-d");
		
		if($flag == 1){
			$data = DB::table('client_register')
			->select(DB::raw('client.id as customer_id,client.name as customer_name,client.contact_name,client.email,client.phone,client.mobile,client.address,sum(client_register.debit) as debit,sum(client_register.credit) as credit'))
			->leftJoin('client','client.id','=','client_register.client_id')
			->leftJoin('saleregister','saleregister.client_id','=','client.id')
			->where([
			['client.company_id',$company],
			['saleregister.sale_type','Credit'],
			['client.status','1'],
			['client_register.status','1'],
			['saleregister.status','1'],
			])
			->groupBy('client_register.client_id')
            ->orderBy('client.id','DESC')
			->get();
		}
		else{
			$data = DB::table('client_register')
			->select(DB::raw('client.id as customer_id,client.name as customer_name,client.contact_name,client.email,client.phone,client.mobile,client.address,sum(client_register.debit) as debit,sum(client_register.credit) as credit'))
			->leftJoin('client','client.id','=','client_register.client_id')
			->leftJoin('saleregister','saleregister.client_id','=','client.id')
			->where([
			['client.company_id',$company],
			['saleregister.sale_type','Credit'],
			['client.status','1'],
			['client_register.status','1'],
			['saleregister.status','1'],
			])
			->whereBetween('saleregister.entry_date', array($debt_from, $debt_to))
			->groupBy('client_register.client_id')
            ->orderBy('client.id','DESC')
			->get();
		}
            
		return $data;
	}
}
