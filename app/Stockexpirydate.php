<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Stockexpirydate extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function stockexpirydate_list($expiry_from,$expiry_to)
	{
		$company = Auth::user()->company_id;
            
        function getOpeningStockItems($company,$expiry_from,$expiry_to){
			$data = DB::table('openingstock')
				->select('openingstock.item_id','openingstock.barcode','openingstock.expiry_date','item.name as item_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('item','item.id','=','openingstock.item_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['openingstock.company_id',$company],
				['openingstock.status','1'],
				])
				->whereBetween('openingstock.expiry_date', array($expiry_from, $expiry_to))
	            ->get();
	            
			return $data;
		}
		function getPurchaseItems($company,$expiry_from,$expiry_to){
			$data = DB::table('purchaseregister')
				->select('purchaseregister_item.item_id','purchaseregister_item.barcode','purchaseregister_item.expiry_date','item.name as item_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('purchaseregister_item','purchaseregister_item.parent_id','=','purchaseregister.id')
				->leftJoin('item','item.id','=','purchaseregister_item.item_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['purchaseregister.company_id',$company],
				['purchaseregister.status','1'],
				])
				->whereBetween('purchaseregister_item.expiry_date', array($expiry_from,$expiry_to))
	            ->get();
	            
			return $data;
		}
		function getPurchaseReturnItems($company,$expiry_from,$expiry_to){
			$data = DB::table('purchasereturn')
				->select('purchasereturn_item.item_id','purchasereturn_item.barcode','purchasereturn_item.expiry_date','item.name as item_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('purchasereturn_item','purchasereturn_item.parent_id','=','purchasereturn.id')
				->leftJoin('item','item.id','=','purchasereturn_item.item_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['purchasereturn.company_id',$company],
				['purchasereturn.status','1'],
				])
				->whereBetween('purchasereturn_item.expiry_date', array($expiry_from,$expiry_to))
	            ->get();
	            
			return $data;
		}
		function getSaleItems($company,$expiry_from,$expiry_to){
			$data = DB::table('saleregister')
				->select('saleregister_item.item_id','saleregister_item.barcode','saleregister_item.expiry_date','item.name as item_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('saleregister_item','saleregister_item.parent_id','=','saleregister.id')
				->leftJoin('item','item.id','=','saleregister_item.item_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['saleregister.company_id',$company],
				['saleregister.status','1'],
				])
				->whereBetween('saleregister_item.expiry_date', array($expiry_from,$expiry_to))
				->get();

			return $data;
		}
		function getSaleReturnItems($company,$expiry_from,$expiry_to){
			$data = DB::table('salereturn')
				->select('salereturn_item.item_id','salereturn_item.barcode','salereturn_item.expiry_date','item.name as item_name','category.name as category_name','brand.name as brand_name','manufacturer.name as manufacturer_name','form.name as form_name')
				->leftJoin('salereturn_item','salereturn_item.parent_id','=','salereturn.id')
				->leftJoin('item','item.id','=','salereturn_item.item_id')
				->leftJoin('category','category.id','=','item.category_id')
				->leftJoin('brand','brand.id','=','item.brand_id')
				->leftJoin('manufacturer','manufacturer.id','=','item.manufacturer_id')
				->leftJoin('form','form.id','=','item.form_id')
				->where([
				['salereturn.company_id',$company],
				['salereturn.status','1'],
				])
				->whereBetween('salereturn_item.expiry_date', array($expiry_from,$expiry_to))
	            ->get();
	            
			return $data;
		}

		$array1 = getOpeningStockItems($company,$expiry_from,$expiry_to)->toArray();
		$array2 = getPurchaseItems($company,$expiry_from,$expiry_to)->toArray();
		$array3 = getPurchaseReturnItems($company,$expiry_from,$expiry_to)->toArray();
		$array4 = getSaleItems($company,$expiry_from,$expiry_to)->toArray();
		$array5 = getSaleReturnItems($company,$expiry_from,$expiry_to)->toArray();

		// print_r($array5);
		// die;
		
		$input = array_merge($array1,$array2,$array3,$array4,$array5);
		$unique_input = array_map("unserialize", array_unique(array_map("serialize", $input)));
		foreach($unique_input as $key=>$value){
			$stock = getStock($value->item_id,$value->barcode,$value->expiry_date);
			$unique_input[$key]->stock = $stock;
			if($unique_input[$key]->stock <= 0){
				unset($unique_input[$key]);
			}
		}
		// echo "<pre>";
		// print_r($unique_input);
		// die;
            
		return $unique_input;
	}
}
