<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Users extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function users_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('users')
			->select('users.*','company.name as company_name')
			->where([
			['users.status', '1'],
			['users.company_id', $company],
			['users.user_group', '3']
			])
            ->leftJoin('company', 'users.company_id', '=', 'company.id')
            ->get();
	}
    public function users_add($company,$name,$gender,$image_name,$email,$password,$encrypted_password,$confirm_password,$mobile,$address)
    {
		$user_id = Auth::id();
		return DB::table('users')->insert(
		    ['company_id' => $company,'user_group' => '3','name' => $name,'gender' => $gender,'image' => $image_name,'email' => $email,'password' => $encrypted_password,'mobile' => $mobile,'address' => $address,'created_at' => $this->date,'created_by' => $user_id]
		);
    }
	public function users_edit($id)
	{
		return DB::table('users')->where('id',$id)->get();
	}
	public function users_view($id)
	{
		return DB::table('users')
			->select('users.*','company.name as company_name')
			->where('users.id',$id)
            ->leftJoin('company', 'users.company_id', '=', 'company.id')
            ->get();
	}
	public function users_update($id,$company,$name,$gender,$image_name,$email,$password,$encrypted_password,$confirm_password,$mobile,$address)
    {
		$user_id = Auth::id();
		if($image_name == ''){
			return DB::table('users')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'gender' => $gender,'email' => $email,'password' => $encrypted_password,'mobile' => $mobile,'address' => $address,'updated_at' => $this->date,'updated_by' => $user_id]);
		}
		return DB::table('users')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'gender' => $gender,'image' => $image_name,'email' => $email,'password' => $encrypted_password,'mobile' => $mobile,'address' => $address,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function users_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('users')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
