<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Purchaseregister;
use App\Transport;
use App\Category;
use App\Vendor;
use App\Custom;
use App\Brand;
use App\Company;

class PurchaseregisterController extends Controller
{
    public function __construct()
    {
		$this->purchaseregister = new Purchaseregister();
		$this->category = new Category();
		$this->brand = new Brand();
		$this->vendor = new Vendor();
		$this->custom = new Custom();
		$this->transport = new Transport();
		$this->company = new Company();
    }
    public function index()
    {
		$purchaseregister = $this->purchaseregister->purchaseregister_list();
		$count = $purchaseregister->count();
		return view('purchaseregister/list',['purchaseregister'=>$purchaseregister,'count'=>$count]);
    }
    public function add()
    {
    	$company_id = Auth::user()->company_id;
		$category = $this->category->category_list();
		$brand = $this->brand->brand_list();
		$vendor = $this->vendor->vendor_list();
		$transport = $this->transport->transport_list();
		$purchaseregister = DB::table('purchaseregister')->where('company_id',$company_id)->orderBy('id', 'desc')->first();
		$voucher_no = 0;
		if(!empty($purchaseregister)){
			$last_voucher_no = $purchaseregister->purchasevoucher_no;
			$arr = explode('_',$last_voucher_no);
			$voucher_no = $arr[1];
		}
		
		$tax_percentage = $this->custom->overall_tax();
		$cgst_percentage = $this->custom->cgst_tax();
		$sgst_percentage = $this->custom->sgst_tax();
		$igst_percentage = $this->custom->igst_tax();
		
		return view('purchaseregister/add',['category'=>$category,'transport'=>$transport,'brand'=>$brand,'vendor'=>$vendor,'voucher_no'=>$voucher_no,'tax_percentage'=>$tax_percentage,'cgst_percentage'=>$cgst_percentage,'sgst_percentage'=>$sgst_percentage,'igst_percentage'=>$igst_percentage]);
	}
	public function addbypo()
    {
    	$company_id = Auth::user()->company_id;
		$category = $this->category->category_list();
		$brand = $this->brand->brand_list();
		$vendor = $this->vendor->vendor_list();
		$transport = $this->transport->transport_list();
		$purchaseregister = DB::table('purchaseregister')->where('company_id',$company_id)->orderBy('id', 'desc')->first();
		$voucher_no = 0;
		if(!empty($purchaseregister)){
			$last_voucher_no = $purchaseregister->purchasevoucher_no;
			$arr = explode('_',$last_voucher_no);
			$voucher_no = $arr[1];
		}
		
		$tax_percentage = $this->custom->overall_tax();
		$cgst_percentage = $this->custom->cgst_tax();
		$sgst_percentage = $this->custom->sgst_tax();
		$igst_percentage = $this->custom->igst_tax();
		
		return view('purchaseregister/addbypo',['category'=>$category,'transport'=>$transport,'brand'=>$brand,'vendor'=>$vendor,'voucher_no'=>$voucher_no,'tax_percentage'=>$tax_percentage,'cgst_percentage'=>$cgst_percentage,'sgst_percentage'=>$sgst_percentage,'igst_percentage'=>$igst_percentage]);
    }
    public function savebyitem(Request $request)
    {
		$company = Auth::user()->company_id;
		$entry_date = $request->input('entry_date');
		$bill_no = $request->input('bill_no');
		$bill_date = $request->input('bill_date');
		$vendor_arr = explode("#",$request->input('vendor'));
		$vendor = $vendor_arr[0];

		$po_arr = $request->input('po_arr');
		$purchaseorder_no = '';
		if(!empty($purchaseorder_no)){
			$purchaseorder_no = implode(",",$po_arr);
		}

		$transport = $request->input('transport');
		$lr_no = $request->input('lr_no');
		$challan_no = $request->input('challan_no');

		$category = $request->input('category');
		$item = $request->input('item');
		$remarks = $request->input('remarks');
		$discount = $request->input('discount');
		$other_expense = $request->input('other_expense');
		
		$item_id = $request->input('item_id');
		$unit_id = $request->input('unit_id');
		$hsn_code = $request->input('hsn_code');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		
		$cgstA = $request->input('cgstA');
		$sgstA = $request->input('sgstA');
		$igstA = $request->input('igstA');

		$voucher_no = $request->input('voucher_no');
		
		$this->validate($request,[
			'vendor'=>'required',
			'bill_no'=>'required',
			'bill_date'=>'required|date',
			'entry_date'=>'required|date'
		]);
		
		$tax_percentage = $this->custom->overall_tax();
		
		$result = $this->purchaseregister->purchaseregister_addbyitem($company,$voucher_no,$purchaseorder_no,$transport,$lr_no,$challan_no,$entry_date,$bill_no,$bill_date,$vendor,$item,$remarks,$item_id,$unit_id,$hsn_code,$barcode,$expiry_date,$quantity,$rate,$amount,$cgstA,$sgstA,$igstA,$discount,$other_expense,$tax_percentage);
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
			return redirect()->action(
					'PurchaseregisterController@view', ['id' => $result]
			);
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		
		return redirect()->back();
    }
	public function savebypo(Request $request)
    {
		$company = Auth::user()->company_id;
		$entry_date = $request->input('entry_date');
		$bill_no = $request->input('bill_no');
		$bill_date = $request->input('bill_date');
		$vendor = $request->input('vendor');
		$purchaseorder_no = $request->input('purchaseorder_no');
		$remarks = $request->input('remarks');
		$discount = $request->input('discount2');
		
		$item_id = $request->input('item_id');
		$unit_id = $request->input('unit_id');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		
		$taxP = $request->input('taxP');
		$taxA = $request->input('taxA');

		$purchaseregister = DB::table('purchaseregister')->where('company_id',$company)->orderBy('id', 'desc')->first();
		$voucher_no = 0;
		if(!empty($purchaseregister)){
			$last_voucher_no = $purchaseregister->purchasevoucher_no;
			$arr = explode('_',$last_voucher_no);
			$voucher_no = $arr[1];
		}
		
		$this->validate($request,[
			'vendor'=>'required',
			'purchaseorder_no'=>'required',
			'bill_no'=>'required',
			'bill_date'=>'required|date',
			'entry_date'=>'required|date'
		]);
		
		$tax_percentage = $this->custom->overall_tax();
		
		$result = $this->purchaseregister->purchaseregister_addbypo($company,$voucher_no,$entry_date,$bill_no,$bill_date,$vendor,$purchaseorder_no,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$taxP,$taxA,$discount,$tax_percentage);
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
			return redirect()->action(
					'PurchaseregisterController@view', ['id' => $result]
			);
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$purchaseregister = $this->purchaseregister->purchaseregister_edit($id);
		$purchaseregister_item = $this->purchaseregister->purchaseregister_item($id);
		$category = $this->category->category_list();
		$brand = $this->brand->brand_list();
		$vendor = $this->vendor->vendor_list();
		$tax_percentage = $this->custom->overall_tax();
		return view('purchaseregister/edit',['purchaseregister'=>$purchaseregister,'purchaseregister_item'=>$purchaseregister_item,'category'=>$category,'brand'=>$brand,'vendor'=>$vendor,'tax_percentage'=>$tax_percentage]);
    }
    public function view($id)
    {
		$purchaseregister = $this->purchaseregister->purchaseregister_view($id);
		$purchaseregister_item = $this->purchaseregister->purchaseregister_item($id);
		$other_expense = $this->purchaseregister->other_expense($id);
		$lr_str = $this->purchaseregister->lr_str($id);
		$company = $this->company->company_view(Auth::user()->company_id);
		return view('purchaseregister/view',['purchaseregister'=>$purchaseregister,'purchaseregister_item'=>$purchaseregister_item,'other_expense'=>$other_expense,'lr_str'=>$lr_str,'company'=>$company]);
	}
	public function lr($id)
    {
		$lr = $this->purchaseregister->purchaseregister_lr($id);
		$count = $lr->count();
		return view('purchaseregister/lr',['lr'=>$lr,'count'=>$count]);
    }
    public function updatebyitem(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$entry_date = $request->input('entry_date');
		$bill_no = $request->input('bill_no');
		$bill_date = $request->input('bill_date');
		$vendor = $request->input('vendor');
		$category = $request->input('category');
		$item = $request->input('item');
		$remarks = $request->input('remarks');
		$discount = $request->input('discount');
		
		$item_id = $request->input('item_id');
		$unit_id = $request->input('unit_id');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		
		$taxP = $request->input('taxP');
		$taxA = $request->input('taxA');
		
		$this->validate($request,[
			'vendor'=>'required',
			'bill_no'=>'required',
			'bill_date'=>'required|date',
			'entry_date'=>'required|date'
		]);
		
		$tax_percentage = $this->custom->overall_tax();
		
		$result = $this->purchaseregister->purchaseregister_updatebyitem($id,$company,$entry_date,$bill_no,$bill_date,$vendor,$item,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$taxP,$taxA,$discount,$tax_percentage);
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
			return redirect()->action(
					'PurchaseregisterController@view', ['id' => $id]
			);
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->purchaseregister->purchaseregister_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function ajax(Request $request)
    {
		$company_id = $request->input('company_id');
		$this->purchaseregister->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
		$category_id = $request->input('category_id');
		$this->purchaseregister->ajax2($category_id);
    }
    public function additems(Request $request)
    {
		$item_id = $request->input('item_id');
		$this->purchaseregister->additems($item_id);
    }
    public function additemsbypo(Request $request)
    {
		$purchaseorder_no = $request->input('purchaseorder_no');
		$this->purchaseregister->additemsbypo($purchaseorder_no);
		}
		
		public function barcodeview($id)
    {
			$purchaseregister = DB::table('purchaseregister')
			->select('purchaseregister.*','users.name as company_name','users.address as company_address')
			->where('purchaseregister.id',$id)
			->leftJoin('users', 'users.company_id', '=', 'purchaseregister.company_id')
			->first();
			$purchaseregister_item = $this->purchaseregister->purchaseregister_item($id);
			return view('purchaseregister/barcode',['purchaseregister_item'=>$purchaseregister_item,'purchaseregister'=>$purchaseregister]);
    }
}
