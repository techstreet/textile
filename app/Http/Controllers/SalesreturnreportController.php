<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Salesreturnreport;

class SalesreturnreportController extends Controller
{
    public function __construct()
    {
		$this->salesreturnreport = new Salesreturnreport();
    }
    public function index()
    {
		return view('salesreturnreport/list',['category_selected'=>'']);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
		]);
		$salesreturnreport = $this->salesreturnreport->salesreturnreport_list($from_date,$to_date);
		$count = $salesreturnreport->count();
		return view('salesreturnreport/list',['salesreturnreport'=>$salesreturnreport,'count'=>$count]);
    }
}
