<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lr;

class LrController extends Controller
{
    public function __construct()
    {
			$this->lr = new Lr();
    }
    public function index()
    {
			$lr = $this->lr->lr_list();
			$count = $lr->count();
			return view('lr/list',['lr'=>$lr,'count'=>$count]);
    }
    public function edit($id)
    {
			$lr = $this->lr->lr_edit($id);
			return view('lr/edit',['lr'=>$lr]);
    }
    public function update(Request $request,$id)
    {
			$amount = $request->input('amount');
			$payment_status = $request->input('payment_status');
			$result = $this->lr->lr_update($id,$amount,$payment_status);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
			return redirect()->back();
		}
}
