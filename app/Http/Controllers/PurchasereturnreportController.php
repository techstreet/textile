<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Purchasereturnreport;

class PurchasereturnreportController extends Controller
{
    public function __construct()
    {
		$this->purchasereturnreport = new Purchasereturnreport();
    }
    public function index()
    {
		return view('purchasereturnreport/list',['category_selected'=>'']);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
		]);
		$purchasereturnreport = $this->purchasereturnreport->purchasereturnreport_list($from_date,$to_date);
		$count = $purchasereturnreport->count();
		return view('purchasereturnreport/list',['purchasereturnreport'=>$purchasereturnreport,'count'=>$count]);
    }
}
