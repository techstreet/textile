<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockregister;

class StockregisterController extends Controller
{
    public function __construct()
    {
		$this->stockregister = new Stockregister();
    }
    public function index()
    {
		return view('stockregister/list',['category_selected'=>'']);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$category_selected = $request->input('category');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
			'category'=>'required'
		]);
		$stockregister = $this->stockregister->stockregister_list($category_selected,$from_date,$to_date);
		$count = $stockregister->count();
		return view('stockregister/list',['stockregister'=>$stockregister,'category_selected'=>$category_selected]);
    }
}
