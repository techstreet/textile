<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Voucher;
use App\Transport;
use App\Company;
use App\Client;
use App\Custom;

class VoucherController extends Controller
{
    public function __construct()
    {
			$this->voucher = new Voucher();
			$this->client = new Client();
			$this->custom = new Custom();
			$this->company = new Company();
			$this->transport = new Transport();
    }
    public function index()
    {
		$voucher = $this->voucher->voucher_list();
		$count = $voucher->count();
		return view('voucher/list',['voucher'=>$voucher,'count'=>$count]);
	}
	public function add()
    {
    	$company_id = Auth::user()->company_id;
		$client = $this->client->client_list();
		$transport = $this->transport->transport_list();
		$voucher = DB::table('voucher')->where('company_id',$company_id)->orderBy('voucher_no', 'desc')->first();
		$voucher_no = 0;
		if(!empty($voucher)){
			$last_voucher_no = $voucher->voucher_no;
			$arr = explode('_',$last_voucher_no);
			$voucher_no = $arr[1];
		}
		$cgst_percentage = $this->custom->cgst_tax();
		$sgst_percentage = $this->custom->sgst_tax();
		$igst_percentage = $this->custom->igst_tax();
		return view('voucher/add',['client'=>$client,'transport'=>$transport,'voucher_no'=>$voucher_no,'cgst_percentage'=>$cgst_percentage,'sgst_percentage'=>$sgst_percentage,'igst_percentage'=>$igst_percentage]);
    }
	public function view($id)
	{
		$voucher = $this->voucher->voucher_view($id);
		$shipping_client = $this->voucher->shipping_client($id);
		$voucher_item = $this->voucher->voucher_item($id);
		$company = $this->company->company_view(Auth::user()->company_id);
		return view('voucher/view',['voucher'=>$voucher,'shipping_client'=>$shipping_client,'voucher_item'=>$voucher_item,'company'=>$company]);
	}
	public function delete(Request $request,$id)
	{
		$result = $this->voucher->voucher_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
	}
	public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$voucher_no = $request->input('voucher_no');
		$entry_date = $request->input('entry_date');
		$client_arr = explode("#",$request->input('client'));
		$client = $client_arr[0];
		$shippingclient_arr = explode("#",$request->input('shipping_client'));
		$shipping_client = $shippingclient_arr[0];
		$transport = $request->input('transport');
		$gr_no = $request->input('gr_no');
		$booking_date = $request->input('booking_date');
		$ref = $request->input('ref');
		$remarks = $request->input('remarks');
		$discount = $request->input('discount');
		$other_expense = $request->input('other_expense');
		
		$item_name = $request->input('item_name');
		$pcs = $request->input('pcs');
		$quantity = $request->input('quantity');
		$unit_name = $request->input('unit_name');
		$brand_name = $request->input('brand_name');
		
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		
		$cgstA = $request->input('cgstA');
		$sgstA = $request->input('sgstA');
		$igstA = $request->input('igstA');
		
		$this->validate($request,[
			'client'=>'required',
			'entry_date'=>'required|date'
		]);
		
		$result = $this->voucher->voucher_add($company,$voucher_no,$entry_date,$client,$shipping_client,$transport,$gr_no,$booking_date,$ref,$remarks,$item_name,$pcs,$quantity,$unit_name,$brand_name,$rate,$amount,$discount,$cgstA,$sgstA,$igstA,$other_expense);
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
			return redirect()->action(
					'VoucherController@view', ['id' => $result]
			);
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
			return redirect()->back();
		}
    }
}
