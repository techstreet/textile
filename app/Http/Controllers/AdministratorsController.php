<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Administrators;
use App\Company;

class AdministratorsController extends Controller
{
    public function __construct()
    {
		$this->administrators = new Administrators();
		$this->company = new Company();
    }
    public function index()
    {
		$administrators = $this->administrators->administrators_list();
		$count = $administrators->count();
		$company = $this->company->company_list();
		return view('administrators/list',['administrators'=>$administrators,'count'=>$count,'company'=>$company]);
    }
    public function add()
    {
		$company = $this->company->company_list();
		return view('administrators/add',['company'=>$company]);
    }
    public function save(Request $request)
    {
		$company = $request->input('company');
		$name = $request->input('name');
		$gender = $request->input('gender');
		$image = $request->file('image');
		if($image){
		    $image_name = time().'.'.$image->getClientOriginalExtension();
		    $image->move(public_path('images/profile'), $image_name);
		}
		else{
			$image_name = '';
		}
		$email = $request->input('email');
		$password = $request->input('password');
		$encrypted_password = password_hash($password,PASSWORD_DEFAULT);
		$confirm_password = $request->input('confirm_password');
		$mobile = $request->input('mobile');
		$address = $request->input('address');
		$this->validate($request,[
			'company'=>'required',
			'name'=>'required',
			'gender'=>'required',
			'email'=>'required',
			'password'=>'required|min:6|same:confirm_password',
			'confirm_password'=>'required'
		]);
		$record_exists = record_exists($email,'email','users',$company);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->administrators->administrators_add($company,$name,$gender,$image_name,$email,$password,$encrypted_password,$confirm_password,$mobile,$address);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$administrators = $this->administrators->administrators_edit($id);
		$company = $this->company->company_list();
		return view('administrators/edit',['administrators'=>$administrators,'company'=>$company]);
    }
    public function view($id)
    {
		$administrators = $this->administrators->administrators_view($id);
		return view('administrators/view',['administrators'=>$administrators]);
    }
    public function update(Request $request,$id)
    {
		$company = $request->input('company');
		$name = $request->input('name');
		$gender = $request->input('gender');
		$image = $request->file('image');
		if($image){
		    $image_name = time().'.'.$image->getClientOriginalExtension();
		    $image->move(public_path('images/profile'), $image_name);
		}
		else{
			$image_name = '';
		}
		$email = $request->input('email');
		$password = $request->input('password');
		$encrypted_password = password_hash($password,PASSWORD_DEFAULT);
		$confirm_password = $request->input('confirm_password');
		$mobile = $request->input('mobile');
		$address = $request->input('address');
		$this->validate($request,[
			'company'=>'required',
			'name'=>'required',
			'gender'=>'required',
			'email'=>'required',
			'password'=>'required|min:6|same:confirm_password',
			'confirm_password'=>'required'
		]);
		$record_exists = record_exists($email,'email','users',$company,$id);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->administrators->administrators_update($id,$company,$name,$gender,$image_name,$email,$password,$encrypted_password,$confirm_password,$mobile,$address);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->administrators->administrators_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function login(Request $request,$id)
    {
		$user_group = Auth::user()->user_group;
//		print_r($user_group);
//		print_r($request->session()->all());
		if($request->session()->has('superadmin'))
		$superadmin = $request->session()->get('superadmin');
		if($user_group == '1' || $superadmin){

			$request->session()->put('superadmin', true);
			Auth::loginUsingId($id,true);
			return redirect('/');
		}
		else{
			abort('404');
		}
    }
}
