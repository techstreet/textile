<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Salesreport;

class SalesreportController extends Controller
{
    public function __construct()
    {
		$this->salesreport = new Salesreport();
    }
    public function index()
    {
		return view('salesreport/list',['category_selected'=>'']);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$category_selected = $request->input('category');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
			'category'=>'required'
		]);
		$salesreport = $this->salesreport->salesreport_list($category_selected,$from_date,$to_date);
//		print_r($salesreport);
		$count = $salesreport->count();
		return view('salesreport/list',['salesreport'=>$salesreport,'category_selected'=>$category_selected,'count'=>$count]);
    }
}
