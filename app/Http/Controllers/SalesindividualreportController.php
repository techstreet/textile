<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Salesindividualreport;
use App\Item;

class SalesindividualreportController extends Controller
{
    public function __construct()
    {
		$this->salesreport = new Salesindividualreport();
		$this->item = new Item();
    }
    public function index()
    {
		$items = $this->item->item_list();
		$count = 0;
		return view('salesindividualreport/list',['item_selected'=>'','count'=>$count,'items'=>$items]);
    }
    public function search(Request $request)
    {
		$items = $this->item->item_list();
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$item_selected = $request->input('item');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
			'item'=>'required'
		]);
		$salesreport = $this->salesreport->salesreport_list($item_selected,$from_date,$to_date);
		$count = $salesreport->count();
		return view('salesindividualreport/list',['salesreport'=>$salesreport,'item_selected'=>$item_selected,'count'=>$count,'items'=>$items]);
    }
}
