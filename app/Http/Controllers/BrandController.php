<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Brand;
use App\Category;

class BrandController extends Controller
{
    public function __construct()
    {
			$this->brand = new Brand();
			$this->category = new Category();
    }
    public function index()
    {
			$brand = $this->brand->brand_list();
			$count = $brand->count();
			return view('brand/list',['brand'=>$brand,'count'=>$count]);
    }
    public function add()
    {
			$category = $this->category->category_list();
			return view('brand/add',['category'=>$category]);
    }
    public function save(Request $request)
    {
			$company = Auth::user()->company_id;
			$category = $request->input('category');
			$name = $request->input('name');
			$status = $request->input('status');
			$this->validate($request,[
				'name'=>'required',
				'category'=>'required'
			]);
			// $record_exists = record_exists($name,'name','brand',$company);
			// if($record_exists){
			// 	$request->session()->flash('warning', 'Record already exists!');
			// }
			// else{
			// 	$result = $this->brand->brand_add($company,$category,$name,$status);
			// 	if($result){
			// 		$request->session()->flash('success', 'Record added successfully!');
			// 	}
			// 	else{
			// 		$request->session()->flash('error', 'Something went wrong!');
			// 	}
			// }
			$result = $this->brand->brand_add($company,$category,$name,$status);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('error', 'Something went wrong!');
			}
			return redirect()->back();
    }
    public function edit($id)
    {
			$brand = $this->brand->brand_edit($id);
			$category = $this->category->category_list();
			return view('brand/edit',['brand'=>$brand,'category'=>$category]);
    }
    public function update(Request $request,$id)
    {
			$company = Auth::user()->company_id;
			$category = $request->input('category');
			$name = $request->input('name');
			$status = $request->input('status');
			$this->validate($request,[
				'name'=>'required',
				'category'=>'required'
			]);
			//$record_exists = record_exists($name,'name','brand',$company,$id);
			// if($record_exists){
			// 	$request->session()->flash('warning', 'Record already exists!');
			// }
			// else{
			// 	$result = $this->brand->brand_update($id,$company,$category,$name,$status);
			// 	if($result){
			// 		$request->session()->flash('success', 'Record updated successfully!');
			// 	}
			// 	else{
			// 		$request->session()->flash('error', 'Something went wrong!');
			// 	}
			// }
			$result = $this->brand->brand_update($id,$company,$category,$name,$status);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('error', 'Something went wrong!');
			}
			return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
			$result = $this->brand->brand_delete($id);
			if($result){
				$request->session()->flash('success', 'Record deleted successfully!');
			}
			else{
				$request->session()->flash('error', 'Something went wrong!');
			}
			return redirect()->back();
    }
}
