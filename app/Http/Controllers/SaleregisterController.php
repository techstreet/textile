<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Saleregister;
use App\Transport;
use App\Company;
use App\Category;
use App\Brand;
use App\Client;
use App\Custom;

class SaleregisterController extends Controller
{
    public function __construct()
    {
		$this->saleregister = new Saleregister();
		$this->category = new Category();
		$this->brand = new Brand();
		$this->client = new Client();
		$this->custom = new Custom();
		$this->company = new Company();
		$this->transport = new Transport();
    }
    public function index()
    {
		$saleregister = $this->saleregister->saleregister_list();
		$count = $saleregister->count();
		return view('saleregister/list',['saleregister'=>$saleregister,'count'=>$count]);
    }
    public function add()
    {
    	$company_id = Auth::user()->company_id;
		$category = $this->category->category_list();
		$transport = $this->transport->transport_list();
		$brand = $this->brand->brand_list();
		$client = $this->client->client_list();
		$saleregister = DB::table('saleregister')->where('company_id',$company_id)->orderBy('invoice_no', 'desc')->first();
		$invoice_no = 0;
		if(!empty($saleregister)){
			$last_invoice_no = $saleregister->invoice_no;
			$arr = explode('_',$last_invoice_no);
			$invoice_no = $arr[1];
		}
		$tax_percentage = $this->custom->overall_tax();
		$cgst_percentage = $this->custom->cgst_tax();
		$sgst_percentage = $this->custom->sgst_tax();
		$igst_percentage = $this->custom->igst_tax();
		return view('saleregister/add',['category'=>$category,'transport'=>$transport,'brand'=>$brand,'client'=>$client,'invoice_no'=>$invoice_no,'tax_percentage'=>$tax_percentage,'cgst_percentage'=>$cgst_percentage,'sgst_percentage'=>$sgst_percentage,'igst_percentage'=>$igst_percentage]);
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$entry_date = $request->input('entry_date');
		$client_arr = explode("#",$request->input('client'));
		$client = $client_arr[0];
		$shippingclient_arr = explode("#",$request->input('shipping_client'));
		$shipping_client = $shippingclient_arr[0];
		$sale_type = $request->input('sale_type');
		$category = $request->input('category');
		$transport = $request->input('transport');
		$gr_no = $request->input('gr_no');
		$booking_date = $request->input('booking_date');
		$item = $request->input('item');
		$ref = $request->input('ref');
		$remarks = $request->input('remarks');
		$discount = $request->input('discount');
		$other_expense = $request->input('other_expense');
		
		$item_id = $request->input('item_id');
		$unit_id = $request->input('unit_id');
		$av_qty = $request->input('av_qty');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		$invoice_no = $request->input('invoice_no');

		$cgstA = $request->input('cgstA');
		$sgstA = $request->input('sgstA');
		$igstA = $request->input('igstA');
		
		$this->validate($request,[
			'client'=>'required',
			'sale_type'=>'required',
			'entry_date'=>'required|date'
		]);
		
		$tax_percentage = $this->custom->overall_tax();
		
		$result = $this->saleregister->saleregister_add($company,$invoice_no,$entry_date,$client,$shipping_client,$sale_type,$transport,$gr_no,$booking_date,$item,$ref,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$av_qty,$quantity,$rate,$amount,$discount,$tax_percentage,$cgstA,$sgstA,$igstA,$other_expense);
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
			return redirect()->action(
					'SaleregisterController@view', ['id' => $result]
			);
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
			return redirect()->back();
		}
    }
    public function edit($id)
    {
		$saleregister = $this->saleregister->saleregister_edit($id);
		$saleregister_item = $this->saleregister->saleregister_item($id);
		$category = $this->category->category_list();
		$brand = $this->brand->brand_list();
		$client = $this->client->client_list();
		$tax_percentage = $this->custom->overall_tax();
		return view('saleregister/edit',['saleregister'=>$saleregister,'saleregister_item'=>$saleregister_item,'category'=>$category,'brand'=>$brand,'client'=>$client,'tax_percentage'=>$tax_percentage]);
    }
    public function view($id)
    {
		$saleregister = $this->saleregister->saleregister_view($id);
		$shipping_client = $this->saleregister->shipping_client($id);
		$saleregister_item = $this->saleregister->saleregister_item($id);
		$company = $this->company->company_view(Auth::user()->company_id);
		return view('saleregister/view',['saleregister'=>$saleregister,'shipping_client'=>$shipping_client,'saleregister_item'=>$saleregister_item,'company'=>$company]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$entry_date = $request->input('entry_date');
		$client = $request->input('client');
		$sale_type = $request->input('sale_type');
		$category = $request->input('category');
		$item = $request->input('item');
		$ref = $request->input('ref');
		$remarks = $request->input('remarks');
		$discount = $request->input('discount');
		
		$item_id = $request->input('item_id');
		$unit_id = $request->input('unit_id');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		
		$this->validate($request,[
			'client'=>'required',
			'sale_type'=>'required',
			'entry_date'=>'required|date'
		]);
		
		$tax_percentage = $this->custom->overall_tax();
		
		$result = $this->saleregister->saleregister_update($id,$company,$entry_date,$client,$sale_type,$item,$ref,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$rate,$amount,$discount,$tax_percentage);
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
			return redirect()->action(
					'SaleregisterController@view', ['id' => $id]
			);
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
			return redirect()->back();
		}
    }
    public function delete(Request $request,$id)
    {
		$result = $this->saleregister->saleregister_delete($id);
		if($result == 'success'){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		elseif($result == 'warning'){
			$request->session()->flash('warning', 'Please delete sales return of the invoice first!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function ajax(Request $request)
    {
		$company_id = $request->input('company_id');
		$this->saleregister->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
		$category_id = $request->input('category_id');
		$this->saleregister->ajax2($category_id);
    }
    public function additems(Request $request)
    {
		$item_id = $request->input('item_id');
		//$this->saleregister->additems($item_id);
		$this->saleregister->itemsearch($item_id);
    }
    public function barcodesearch(Request $request)
    {
		$barcode = $request->input('barcode');
		$this->saleregister->barcodesearch($barcode);
		}
		public function barcodeview($id)
    {
			$saleregister = DB::table('saleregister')
			->select('saleregister.*','users.name as company_name','users.address as company_address')
			->where('saleregister.id',$id)
			->leftJoin('users', 'users.company_id', '=', 'saleregister.company_id')
			->first();
			$saleregister_item = $this->saleregister->saleregister_item($id);
			return view('saleregister/barcode',['saleregister_item'=>$saleregister_item,'saleregister'=>$saleregister]);
    }
}
