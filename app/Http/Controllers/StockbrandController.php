<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockbrand;
use App\Brand;

class StockbrandController extends Controller
{
    public function __construct()
    {
		$this->stockbrand = new Stockbrand();
		$this->brand = new Brand();
    }
    public function index()
    {
		//echo config('app.accounting_method');
		$brand = $this->brand->brand_list();
		return view('stockbrand/list',['brand'=>$brand]);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$brand_selected = $request->input('brand');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
			'brand'=>'required'
		]);
		$stockbrand = $this->stockbrand->stockbrand_list($brand_selected,$from_date,$to_date);
		$count = $stockbrand->count();
		$brand = $this->brand->brand_list();
		return view('stockbrand/list',['stockbrand'=>$stockbrand,'brand'=>$brand,'brand_selected'=>$brand_selected]);
    }
}
