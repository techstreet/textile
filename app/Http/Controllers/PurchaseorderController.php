<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Purchaseorder;
use App\Category;
use App\Vendor;
use App\Brand;
use App\Company;
use App\Custom;

class PurchaseorderController extends Controller
{
    public function __construct()
    {
		$this->purchaseorder = new Purchaseorder();
		$this->category = new Category();
		$this->brand = new Brand();
		$this->custom = new Custom();
		$this->vendor = new Vendor();
		$this->company = new Company();
    }
    public function index()
    {
		$purchaseorder = $this->purchaseorder->purchaseorder_list();
		$count = $purchaseorder->count();
		return view('purchaseorder/list',['purchaseorder'=>$purchaseorder,'count'=>$count]);
    }
    public function add()
    {
			$company_id = Auth::user()->company_id;
			$category = $this->category->category_list();
			$brand = $this->brand->brand_list();
			$vendor = $this->vendor->vendor_list();
			$purchaseorder = DB::table('purchaseorder')->where('company_id',$company_id)->orderBy('id', 'desc')->first();
			$po_no = 0;
			if(!empty($purchaseorder)){
				$last_po_no = $purchaseorder->purchaseorder_no;
				$arr = explode('_',$last_po_no);
				$po_no = $arr[1];
			}
			$tax_percentage = $this->custom->overall_tax();
			$cgst_percentage = $this->custom->cgst_tax();
			$sgst_percentage = $this->custom->sgst_tax();
			$igst_percentage = $this->custom->igst_tax();
			return view('purchaseorder/add',['category'=>$category,'brand'=>$brand,'vendor'=>$vendor,'po_no'=>$po_no,'tax_percentage'=>$tax_percentage,'cgst_percentage'=>$cgst_percentage,'sgst_percentage'=>$sgst_percentage,'igst_percentage'=>$igst_percentage]);
    }
    public function save(Request $request)
    {
			$company = Auth::user()->company_id;
			$entry_date = $request->input('entry_date');
			$vendor_arr = explode("#",$request->input('vendor'));
			$vendor = $vendor_arr[0];

			$category = $request->input('category');
			$item = $request->input('item');
			$remarks = $request->input('remarks');
			
			$item_id = $request->input('item_id');
			$unit_id = $request->input('unit_id');
			$hsn_code = $request->input('hsn_code');
			$quantity = $request->input('quantity');
			$rate = $request->input('rate');
			$amount = $request->input('amount');
			$barcode = $request->input('barcode');
			$expiry_date = $request->input('expiry_date');
			
			$cgstA = $request->input('cgstA');
			$sgstA = $request->input('sgstA');
			$igstA = $request->input('igstA');

			$po_no = $request->input('po_no');
			
			$this->validate($request,[
				'vendor'=>'required',
				'entry_date'=>'required|date'
			]);
			
			$tax_percentage = $this->custom->overall_tax();

			if(count($item_id) == 0){
				$request->session()->flash('error', 'No items added yet!');
				return redirect()->back();
			}
			$result = $this->purchaseorder->purchaseorder_add($company,$po_no,$entry_date,$vendor,$item,$remarks,$item_id,$unit_id,$hsn_code,$barcode,$expiry_date,$quantity,$rate,$amount,$cgstA,$sgstA,$igstA,$tax_percentage);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
				return redirect()->action(
						'PurchaseorderController@view', ['id' => $result]
				);
			}
			else{
				$request->session()->flash('error', 'Something went wrong!');
			}
			return redirect()->back();
    }
    public function edit($id)
    {
		$purchaseorder = $this->purchaseorder->purchaseorder_edit($id);
		$purchaseorder_item = $this->purchaseorder->purchaseorder_item($id);
		$category = $this->category->category_list();
		$brand = $this->brand->brand_list();
		$vendor = $this->vendor->vendor_list();
		return view('purchaseorder/edit',['purchaseorder'=>$purchaseorder,'purchaseorder_item'=>$purchaseorder_item,'category'=>$category,'brand'=>$brand,'vendor'=>$vendor]);
    }
    public function view($id)
    {
		$purchaseorder = $this->purchaseorder->purchaseorder_view($id);
		$purchaseorder_item = $this->purchaseorder->purchaseorder_item($id);
		$company = $this->company->company_view(Auth::user()->company_id);
		return view('purchaseorder/view',['purchaseorder'=>$purchaseorder,'purchaseorder_item'=>$purchaseorder_item,'company'=>$company]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$entry_date = $request->input('entry_date');
		$vendor = $request->input('vendor');
		$category = $request->input('category');
		$item = $request->input('item');
		$remarks = $request->input('remarks');
		
		$item_id = $request->input('item_id');
		$unit_id = $request->input('unit_id');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		
		$this->validate($request,[
			'vendor'=>'required',
			'entry_date'=>'required|date'
		]);
		
		if(count($item_id) == 0){
			$request->session()->flash('error', 'No items added yet!');
			return redirect()->back();
		}
		
		$result = $this->purchaseorder->purchaseorder_update($id,$company,$entry_date,$vendor,$item,$remarks,$item_id,$unit_id,$quantity,$rate,$amount);
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->purchaseorder->purchaseorder_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function ajax(Request $request)
    {
		$company_id = $request->input('company_id');
		$this->purchaseorder->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
		$category_id = $request->input('category_id');
		$this->purchaseorder->ajax2($category_id);
    }
    public function additems(Request $request)
    {
		$item_id = $request->input('item_id');
		$this->purchaseorder->additems($item_id);
	}
	public function autocomplete(Request $request)
    {
		$company_id = Auth::user()->company_id;
		$query = $request->get('query','');
        $purchaseorder = DB::table('purchaseorder')
			->select('purchaseorder_no as name')
			->where([
			['status','1'],
			['company_id',$company_id],
			['purchaseorder_no','LIKE','%'.$query.'%'],
			])
			->get();        
        return response()->json($purchaseorder);
    }
}
