<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Client;

class ClientController extends Controller
{
    public function __construct()
    {
		$this->client = new Client();
    }
    public function index()
    {
		$client = $this->client->client_list();
//		echo "<pre>";
//		print_r($client);
		$count = $client->count();
		return view('client/list',['client'=>$client,'count'=>$count]);
    }
    public function add()
    {
		return view('client/add');
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$contact_name = $request->input('contact_name');
		$email = $request->input('email');
		$address = $request->input('address');
		$phone = $request->input('phone');
		$mobile = $request->input('mobile');
		$gstin = $request->input('gstin');
		$this->validate($request,[
			'name'=>'required',
			'address'=>'required',
			'mobile'=>'required'
		]);
		$record_exists = record_exists($name,'name','client',$company);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->client->client_add($company,$name,$contact_name,$email,$address,$phone,$mobile,$gstin);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$client = $this->client->client_edit($id);
		return view('client/edit',['client'=>$client]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$contact_name = $request->input('contact_name');
		$email = $request->input('email');
		$address = $request->input('address');
		$phone = $request->input('phone');
		$mobile = $request->input('mobile');
		$gstin = $request->input('gstin');
		$this->validate($request,[
			'name'=>'required',
			'address'=>'required',
			'mobile'=>'required'
		]);
		$record_exists = record_exists($name,'name','client',$company,$id);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->client->client_update($id,$company,$name,$contact_name,$email,$address,$phone,$mobile,$gstin);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->client->client_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
