<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function __construct()
    {
    }
    public function index()
    {
    $company = Auth::user()->company_id;
		$notification = low_stock($company);
    $count = count($notification);
		return view('notification/list',['notification'=>$notification,'count'=>$count]);
    }
}
