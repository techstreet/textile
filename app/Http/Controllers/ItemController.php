<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;
use App\Category;
use App\Unit;
use App\Brand;
use App\Manufacturer;
use App\Form;

class ItemController extends Controller
{
    public function __construct()
    {
		$this->item = new Item();
		$this->category = new Category();
		$this->unit = new Unit();
		$this->brand = new Brand();
		$this->manufacturer = new Manufacturer();
		$this->form = new Form();
    }
    public function index()
    {
		$item = $this->item->item_list();
		$count = $item->count();
		return view('item/list',['item'=>$item,'count'=>$count]);
    }
    public function add()
    {
		$category = $this->category->category_list();
		$unit = $this->unit->unit_list();
		$brand = $this->brand->brand_list();
		$manufacturer = $this->manufacturer->manufacturer_list();
		$form = $this->form->form_list();
		return view('item/add',['category'=>$category,'unit'=>$unit,'brand'=>$brand,'manufacturer'=>$manufacturer,'form'=>$form]);
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$price = $request->input('price');
		$currency = $request->input('currency');
		$category = $request->input('category');
		$strength = $request->input('strength');
		$unit = $request->input('unit');
		$brand = $request->input('brand');
		$manufacturer = $request->input('manufacturer');
		$form = $request->input('form');
		$pack_size = $request->input('pack_size');
		$notify_quantity = $request->input('notify_quantity');
		$hsn_code = $request->input('hsn_code');
		$reference_name = $request->input('reference_name');
		
		$this->validate($request,[
			'name'=>'required',
			'price'=>'required',
			'currency'=>'required',
			'category'=>'required',
			//'strength'=>'required',
			'unit'=>'required',
			//'brand'=>'required',
//			'manufacturer'=>'required',
			//'form'=>'required',
			//'pack_size'=>'required',
			'notify_quantity'=>'required'
		]);
		
		// $record = DB::table('item')
		// 	->where([
		// 	['status','1'],
		// 	['name',$name],
		// 	['brand_id',$brand],
		// 	['company_id',$company]

		// 	])
        //     ->get();

        // $count = $record->count();

		// if($count>0){
		// 	$request->session()->flash('warning', 'Record already exists!');
		// }
		// else{
		// 	$result = $this->item->item_add($company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name);
		// 	if($result){
		// 		$request->session()->flash('success', 'Record added successfully!');
		// 	}
		// 	else{
		// 		$request->session()->flash('error', 'Something went wrong!');
		// 	}
		// }

		$result = $this->item->item_add($company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name);
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}

		return redirect()->back();
    }
    public function edit($id)
    {
		$item = $this->item->item_edit($id);
		$item_reference = $this->item->item_reference($id);
		$category = $this->category->category_list();
		$unit = $this->unit->unit_list();
		$brand = $this->brand->brand_list();
		$manufacturer = $this->manufacturer->manufacturer_list();
		$form = $this->form->form_list();
		return view('item/edit',['item'=>$item,'item_reference'=>$item_reference,'category'=>$category,'unit'=>$unit,'brand'=>$brand,'manufacturer'=>$manufacturer,'form'=>$form]);
    }
    public function view($id)
    {
		$item = $this->item->item_view($id);
		return view('item/view',['item'=>$item]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$price = $request->input('price');
		$currency = $request->input('currency');
		$category = $request->input('category');
		$strength = $request->input('strength');
		$unit = $request->input('unit');
		$brand = $request->input('brand');
		$manufacturer = $request->input('manufacturer');
		$form = $request->input('form');
		$pack_size = $request->input('pack_size');
		$notify_quantity = $request->input('notify_quantity');
		$hsn_code = $request->input('hsn_code');
		$reference_name = $request->input('reference_name');
		
		$this->validate($request,[
			'name'=>'required',
			'price'=>'required',
			'currency'=>'required',
			'category'=>'required',
			//'strength'=>'required',
			'unit'=>'required',
			//'brand'=>'required',
//			'manufacturer'=>'required',
			//'form'=>'required',
			//'pack_size'=>'required',
			'notify_quantity'=>'required'
		]);
		
		// $record_exists = record_exists($name,'name','item',$company,$id);
		// if($record_exists){
		// 	$request->session()->flash('warning', 'Record already exists!');
		// }
		// else{
		// 	$result = $this->item->item_update($id,$company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name);
		// 	if($result){
		// 		$request->session()->flash('success', 'Record updated successfully!');
		// 	}
		// 	else{
		// 		$request->session()->flash('error', 'Something went wrong!');
		// 	}
		// }

		$result = $this->item->item_update($id,$company,$name,$price,$currency,$category,$strength,$unit,$brand,$manufacturer,$form,$pack_size,$notify_quantity,$hsn_code,$reference_name);
		if($result){
			$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}

		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->item->item_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
