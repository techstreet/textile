<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Purchasereturn;
use App\Category;
use App\Transport;
use App\Vendor;
use App\Custom;
use App\Company;

class PurchasereturnController extends Controller
{
    public function __construct()
    {
		$this->purchasereturn = new Purchasereturn();
		$this->category = new Category();
		$this->vendor = new Vendor();
		$this->custom = new Custom();
		$this->company = new Company();
		$this->transport = new Transport();
    }
    public function index()
    {
		$purchasereturn = $this->purchasereturn->purchasereturn_list();
		$count = $purchasereturn->count();
		return view('purchasereturn/list',['purchasereturn'=>$purchasereturn,'count'=>$count]);
    }
    public function add()
    {
		$company_id = Auth::user()->company_id;
		$category = $this->category->category_list();
		$vendor = $this->vendor->vendor_list();
		$transport = $this->transport->transport_list();
		$purchasereturn = DB::table('purchasereturn')->where('company_id',$company_id)->orderBy('id', 'desc')->first();
		$voucher_no = 0;
		if(!empty($purchasereturn)){
			$last_voucher_no = $purchasereturn->debitnote_no;
			$arr = explode('_',$last_voucher_no);
			$voucher_no = $arr[1];
		}
		$cgst_percentage = $this->custom->cgst_tax();
		$sgst_percentage = $this->custom->sgst_tax();
		$igst_percentage = $this->custom->igst_tax();
		return view('purchasereturn/add',['category'=>$category,'transport'=>$transport,'vendor'=>$vendor,'voucher_no'=>$voucher_no,'cgst_percentage'=>$cgst_percentage,'sgst_percentage'=>$sgst_percentage,'igst_percentage'=>$igst_percentage]);
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$debitnote_no = $request->input('debitnote_no');
		$debitnote_date = $request->input('debitnote_date');
		$purchasevoucher_no = $request->input('purchasevoucher_no');
		$remarks = $request->input('remarks');
		$transport = $request->input('transport');
		$gr_no = $request->input('gr_no');
		$booking_date = $request->input('booking_date');
		
		$item_id = $request->input('item_id');
		$stockregister_id = $request->input('stockregister_id');
		$unit_id = $request->input('unit_id');
		$quantity = $request->input('quantity');
		$return_quantity = $request->input('return_quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		$return_amount = $request->input('return_amount');
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');

		$discount = $request->input('discount');
		$other_expense = $request->input('other_expense');
		$cgstA = $request->input('cgstA');
		$sgstA = $request->input('sgstA');
		$igstA = $request->input('igstA');
		
		$this->validate($request,[
			'debitnote_no'=>'required',
			'debitnote_date'=>'required|date',
			'purchasevoucher_no'=>'required'
		]);
		
		$result = $this->purchasereturn->purchasereturn_add($company,$debitnote_no,$debitnote_date,$purchasevoucher_no,$remarks,$transport,$gr_no,$booking_date,$item_id,$stockregister_id,$unit_id,$barcode,$expiry_date,$quantity,$return_quantity,$rate,$amount,$return_amount,$cgstA,$sgstA,$igstA,$discount,$other_expense);
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
	
    public function edit($id)
    {
		$purchasereturn = $this->purchasereturn->purchasereturn_edit($id);
		$purchasereturn_item = $this->purchasereturn->purchasereturn_item($id);
		$category = $this->category->category_list();
		$vendor = $this->vendor->vendor_list();
		return view('purchasereturn/edit',['purchasereturn'=>$purchasereturn,'purchasereturn_item'=>$purchasereturn_item,'category'=>$category,'vendor'=>$vendor]);
    }
    public function view($id)
    {
		$purchasereturn = $this->purchasereturn->purchasereturn_view($id);
		$purchasereturn_item = $this->purchasereturn->purchasereturn_item($id);
		$company = $this->company->company_view(Auth::user()->company_id);
		return view('purchasereturn/view',['purchasereturn'=>$purchasereturn,'purchasereturn_item'=>$purchasereturn_item,'company'=>$company]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$debitnote_date = $request->input('debitnote_date');
		$purchasevoucher_no = $request->input('purchasevoucher_no');
		$remarks = $request->input('remarks');
		
		$item_id = $request->input('item_id');
		$unit_id = $request->input('unit_id');
		$quantity = $request->input('quantity');
		$return_quantity = $request->input('return_quantity');
		$rate = $request->input('rate');
		$amount = $request->input('amount');
		$return_amount = $request->input('return_amount');
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		
		$this->validate($request,[
			'debitnote_date'=>'required|date'
		]);
		
		$result = $this->purchasereturn->purchasereturn_update($id,$company,$debitnote_date,$purchasevoucher_no,$remarks,$item_id,$unit_id,$barcode,$expiry_date,$quantity,$return_quantity,$rate,$amount,$return_amount);
		if($result){
			$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->purchasereturn->purchasereturn_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function ajax(Request $request)
    {
		$company_id = $request->input('company_id');
		$this->purchasereturn->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
		$category_id = $request->input('category_id');
		$this->purchasereturn->ajax2($category_id);
    }
    public function additems(Request $request)
    {
		$purchasevoucher_no = $request->input('purchasevoucher_no');
		$this->purchasereturn->additems($purchasevoucher_no);
    }
    public function autocomplete(Request $request)
    {
			$company_id = Auth::user()->company_id;
		$query = $request->get('query','');
        $voucher = DB::table('purchaseregister')
			->select('purchasevoucher_no as name')
			->where([
			['status','1'],
			['company_id',$company_id],
			['purchasevoucher_no','LIKE','%'.$query.'%'],
			])
			->get();        
        return response()->json($voucher);
    }
}
