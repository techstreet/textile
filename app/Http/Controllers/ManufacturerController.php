<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Manufacturer;

class ManufacturerController extends Controller
{
    public function __construct()
    {
		$this->manufacturer = new Manufacturer();
    }
    public function index()
    {
		$manufacturer = $this->manufacturer->manufacturer_list();
		$count = $manufacturer->count();
		return view('manufacturer/list',['manufacturer'=>$manufacturer,'count'=>$count]);
    }
    public function add()
    {
		return view('manufacturer/add');
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$status = $request->input('status');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','manufacturer',$company);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->manufacturer->manufacturer_add($company,$name,$status);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$manufacturer = $this->manufacturer->manufacturer_edit($id);
		return view('manufacturer/edit',['manufacturer'=>$manufacturer]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$status = $request->input('status');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','manufacturer',$company,$id);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->manufacturer->manufacturer_update($id,$company,$name,$status);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->manufacturer->manufacturer_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
