<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Purchasereport;

class PurchasereportController extends Controller
{
    public function __construct()
    {
		$this->purchasereport = new Purchasereport();
    }
    public function index()
    {
		return view('purchasereport/list',['category_selected'=>'']);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
		]);
		$purchasereport = $this->purchasereport->purchasereport_list($from_date,$to_date);
		$count = $purchasereport->count();
		return view('purchasereport/list',['purchasereport'=>$purchasereport,'count'=>$count]);
    }
}
