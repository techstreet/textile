<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Debtreport;
use Carbon\Carbon;

class DebtreportController extends Controller
{
    public function __construct()
    {
		$this->debtreport = new Debtreport();
    }
    public function index()
    {
		return view('debtreport/list');
    }
    public function search(Request $request)
    {
		//$debt_selected = $request->input('debt');

		$debt_from = $request->input('from_date');
		$debt_to = $request->input('to_date');

		//$debt_to = Carbon::today();
		$flag = 0;
		// if($debt_selected == '1'){
		// 	$debt_from = Carbon::now()->subMonths(1);
		// }
		// if($debt_selected == '2'){
		// 	$debt_from = Carbon::now()->subMonths(2);
		// }
		// if($debt_selected == '3'){
		// 	$debt_from = Carbon::now()->subMonths(3);
		// }
		// if($debt_selected == '6'){
		// 	$debt_from = Carbon::now()->subMonths(6);
		// }
		// if($debt_selected == '12'){
		// 	$debt_from = Carbon::now()->subMonths(12);
		// }
		// if($debt_selected > 12){
		// 	$debt_from = Carbon::now()->subMonths(12);
		// 	$flag = 1;
		// }
		
		// $this->validate($request,[
		// 	'debt'=>'required'
		// ]);

		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
		]);
		
		$debtreport = $this->debtreport->debtreport_list($debt_from,$debt_to,$flag);
		$count=$debtreport->count();
		//return view('debtreport/list',['debtreport'=>$debtreport,'debt_selected'=>$debt_selected,'count'=>$count]);
		return view('debtreport/list',['debtreport'=>$debtreport,'count'=>$count]);
    }
}
