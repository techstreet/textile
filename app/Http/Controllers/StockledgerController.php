<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockledger;
use App\Item;

class StockledgerController extends Controller
{
    public function __construct()
    {
		$this->stockledger = new Stockledger();
		$this->item = new Item();
    }
    public function index()
    {
		$item = $this->item->item_list();
		return view('stockledger/list',['item'=>$item]);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$item_selected = $request->input('item');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
			'item'=>'required'
		]);
		$stockledger = $this->stockledger->stockledger_list($item_selected,$from_date,$to_date);
		$item = $this->item->item_list();
		return view('stockledger/list',['stockledger'=>$stockledger,'item'=>$item,'item_selected'=>$item_selected]);
    }
}
