<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockreport;
use App\Category;
use App\Form;
use App\Manufacturer;
use App\Brand;

class StockreportController extends Controller
{
    public function __construct()
    {
		$this->stockreport = new Stockreport();
		$this->category = new Category();
		$this->form = new Form();
		$this->manufacturer = new Manufacturer();
		$this->brand = new Brand();
    }
    public function index()
    {
		$category = $this->category->category_list();
		$form = $this->form->form_list();
		$manufacturer = $this->manufacturer->manufacturer_list();
		$brand = $this->brand->brand_list();
		
		return view('stockreport/list',['category'=>$category,'form'=>$form,'manufacturer'=>$manufacturer,'brand'=>$brand]);
    }
    public function search(Request $request)
    {
		$category_selected = $request->input('category');
		$form_selected = $request->input('form');
		$manufacturer_selected = $request->input('manufacturer');
		$brand_selected = $request->input('brand');
		
		$stockreport = $this->stockreport->stockreport_list($category_selected,$form_selected,$manufacturer_selected,$brand_selected);
		$count = $stockreport->count();
		
		$category = $this->category->category_list();
		$form = $this->form->form_list();
		$manufacturer = $this->manufacturer->manufacturer_list();
		$brand = $this->brand->brand_list();
		
		return view('stockreport/list',['stockreport'=>$stockreport,'category'=>$category,'category_selected'=>$category_selected,'form'=>$form,'form_selected'=>$form_selected,'manufacturer'=>$manufacturer,'manufacturer_selected'=>$manufacturer_selected,'brand'=>$brand,'brand_selected'=>$brand_selected,'count'=>$count]);
    }
}
