<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transport;

class TransportController extends Controller
{
    public function __construct()
    {
		$this->transport = new Transport();
    }
    public function index()
    {
		$transport = $this->transport->transport_list();
		$count = $transport->count();
		return view('transport/list',['transport'=>$transport,'count'=>$count]);
    }
    public function add()
    {
		return view('transport/add');
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$contact_person = $request->input('contact_person');
		$mobile = $request->input('mobile');
		$phone = $request->input('phone');
		$address = $request->input('address');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','transport',$company);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->transport->transport_add($company,$name,$contact_person,$mobile,$phone,$address);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$transport = $this->transport->transport_edit($id);
		return view('transport/edit',['transport'=>$transport]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$contact_person = $request->input('contact_person');
		$mobile = $request->input('mobile');
		$phone = $request->input('phone');
		$address = $request->input('address');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','transport',$company,$id);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->transport->transport_update($id,$company,$name,$contact_person,$mobile,$phone,$address);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->transport->transport_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
