<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Users;
use App\Company;

class UsersController extends Controller
{
    public function __construct()
    {
		$this->users = new Users();
		$this->company = new Company();
    }
    public function index()
    {
		$users = $this->users->users_list();
		$count = $users->count();
		$company = $this->company->company_list();
		return view('users/list',['users'=>$users,'count'=>$count,'company'=>$company]);
    }
    public function add()
    {
		return view('users/add');
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$gender = $request->input('gender');
		$image = $request->file('image');
		if($image){
		    $image_name = time().'.'.$image->getClientOriginalExtension();
		    $image->move(public_path('images/profile'), $image_name);
		}
		else{
			$image_name = '';
		}
		$email = $request->input('email');
		$password = $request->input('password');
		$encrypted_password = password_hash($password,PASSWORD_DEFAULT);
		$confirm_password = $request->input('confirm_password');
		$mobile = $request->input('mobile');
		$address = $request->input('address');
		$this->validate($request,[
			'name'=>'required',
			'gender'=>'required',
			'email'=>'required',
			'password'=>'required|min:6|same:confirm_password',
			'confirm_password'=>'required'
		]);
		$record_exists = record_exists($email,'email','users',$company);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->users->users_add($company,$name,$gender,$image_name,$email,$password,$encrypted_password,$confirm_password,$mobile,$address);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$users = $this->users->users_edit($id);
		$company = $this->company->company_list();
		return view('users/edit',['users'=>$users,'company'=>$company]);
    }
    public function view($id)
    {
		$users = $this->users->users_view($id);
		return view('users/view',['users'=>$users]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$gender = $request->input('gender');
		$image = $request->file('image');
		if($image){
		    $image_name = time().'.'.$image->getClientOriginalExtension();
		    $image->move(public_path('images/profile'), $image_name);
		}
		else{
			$image_name = '';
		}
		$email = $request->input('email');
		$password = $request->input('password');
		$encrypted_password = password_hash($password,PASSWORD_DEFAULT);
		$confirm_password = $request->input('confirm_password');
		$mobile = $request->input('mobile');
		$address = $request->input('address');
		$this->validate($request,[
			'name'=>'required',
			'gender'=>'required',
			'email'=>'required',
			'password'=>'required|min:6|same:confirm_password',
			'confirm_password'=>'required'
		]);
		$record_exists = record_exists($email,'email','users',$company,$id);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->users->users_update($id,$company,$name,$gender,$image_name,$email,$password,$encrypted_password,$confirm_password,$mobile,$address);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->users->users_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function login(Request $request,$id)
    {
		$user_group = Auth::user()->user_group;
		if($user_group == '1' || $user_group == '2'){
			Auth::loginUsingId($id);
			return redirect('/');
		}
		else{
			abort('404');
		}
    }
}
