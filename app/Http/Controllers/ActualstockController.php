<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Actualstock;
use App\Category;
use App\Form;
use App\Manufacturer;
use App\Brand;

class ActualstockController extends Controller
{
    public function __construct()
    {
		$this->actualstock = new Actualstock();
		$this->category = new Category();
		$this->form = new Form();
		$this->manufacturer = new Manufacturer();
		$this->brand = new Brand();
    }
    public function index()
    {
		$category = $this->category->category_list();
		$form = $this->form->form_list();
		$manufacturer = $this->manufacturer->manufacturer_list();
		$brand = $this->brand->brand_list();
		
		return view('actualstock/list',['category'=>$category,'form'=>$form,'manufacturer'=>$manufacturer,'brand'=>$brand]);
    }
    public function search(Request $request)
    {
		$category_selected = $request->input('category');
		$form_selected = $request->input('form');
		$manufacturer_selected = $request->input('manufacturer');
		$brand_selected = $request->input('brand');
		$date = date_ymd($request->input('current_date'));
		
		$actualstock = $this->actualstock->actualstock_list($category_selected,$form_selected,$manufacturer_selected,$brand_selected,$date);
		$count = $actualstock->count();
		
		$category = $this->category->category_list();
		$form = $this->form->form_list();
		$manufacturer = $this->manufacturer->manufacturer_list();
		$brand = $this->brand->brand_list();
		
		return view('actualstock/list',['actualstock'=>$actualstock,'category'=>$category,'category_selected'=>$category_selected,'form'=>$form,'form_selected'=>$form_selected,'manufacturer'=>$manufacturer,'manufacturer_selected'=>$manufacturer_selected,'brand'=>$brand,'brand_selected'=>$brand_selected,'count'=>$count]);
		}
		public function save(Request $request)
    {
			$company_id = $request->input('company_id');
			$item_id = $request->input('item_id');
			$closing_stock = $request->input('closing_stock');
			$actual_stock = $request->input('actual_stock');
			$entry_date = $request->input('entry_date');
			$result = $this->actualstock->actualstock_add($company_id,$item_id,$closing_stock,$actual_stock,$entry_date);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('error', 'Something went wrong!');
			}
			return redirect()->back();
		}
}