<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Cashreport;
use Carbon\Carbon;

class CashreportController extends Controller
{
    public function __construct()
    {
		$this->cashreport = new cashreport();
    }
    public function index()
    {
		return view('cashreport/list');
    }
    public function search(Request $request)
    {
		//$cash_selected = $request->input('cash');

		$cash_from = $request->input('from_date');
		$cash_to = $request->input('to_date');

		//$cash_to = Carbon::today();

		$flag = 0;
		// if($cash_selected == '1'){
		// 	$cash_from = Carbon::now()->subMonths(1);
		// }
		// if($cash_selected == '2'){
		// 	$cash_from = Carbon::now()->subMonths(2);
		// }
		// if($cash_selected == '3'){
		// 	$cash_from = Carbon::now()->subMonths(3);
		// }
		// if($cash_selected == '6'){
		// 	$cash_from = Carbon::now()->subMonths(6);
		// }
		// if($cash_selected == '12'){
		// 	$cash_from = Carbon::now()->subMonths(12);
		// }
		// if($cash_selected > 12){
		// 	$cash_from = Carbon::now()->subMonths(12);
		// 	$flag = 1;
		// }
		
		// $this->validate($request,[
		// 	'cash'=>'required'
		// ]);

		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
		]);
		
		$cashreport = $this->cashreport->cashreport_list($cash_from,$cash_to,$flag);
		$count=$cashreport->count();
		//return view('cashreport/list',['cashreport'=>$cashreport,'cash_selected'=>$cash_selected,'count'=>$count]);
		$cash_from = date_format(date_create($cash_from),"Y-m-d");
		$cash_to = date_format(date_create($cash_to),"Y-m-d");
		return view('cashreport/list',['cashreport'=>$cashreport,'count'=>$count,'cash_from'=>$cash_from,'cash_to'=>$cash_to]);
    }
}
