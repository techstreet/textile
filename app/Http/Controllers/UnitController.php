<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Unit;

class UnitController extends Controller
{
    public function __construct()
    {
		$this->unit = new Unit();
    }
    public function index()
    {
		$unit = $this->unit->unit_list();
		$count = $unit->count();
		return view('unit/list',['unit'=>$unit,'count'=>$count]);
    }
    public function add()
    {
		return view('unit/add');
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$description = $request->input('description');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','unit',$company);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->unit->unit_add($company,$name,$description);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$unit = $this->unit->unit_edit($id);
		return view('unit/edit',['unit'=>$unit]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$description = $request->input('description');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','unit',$company,$id);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->unit->unit_update($id,$company,$name,$description);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->unit->unit_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
