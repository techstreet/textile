<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockexpirydate;
use Carbon\Carbon;

class StockexpirydateController extends Controller
{
    public function __construct()
    {
		$this->stockexpirydate = new Stockexpirydate();
    }
    public function index()
    {
		return view('stockexpirydate/list');
    }
    public function search(Request $request)
    {
		$expirydate_selected = $request->input('expirydate');
		$expiry_from = Carbon::today();
		if($expirydate_selected == '1'){
			$expiry_to = Carbon::now()->endOfMonth();
		}
		if($expirydate_selected == '2'){
			$expiry_to = Carbon::now()->endOfMonth()->addMonths(1);
		}
		if($expirydate_selected == '3'){
			$expiry_to = Carbon::now()->endOfMonth()->addMonths(2);
		}
		if($expirydate_selected == '6'){
			$expiry_to = Carbon::now()->endOfMonth()->addMonths(5);
		}
		if($expirydate_selected == '12'){
			$expiry_to = Carbon::now()->endOfMonth()->addMonths(11);
		}
		if($expirydate_selected == '24'){
			$expiry_to = Carbon::now()->endOfMonth()->addMonths(23);
		}
		$this->validate($request,[
			'expirydate'=>'required'
		]);
		$stockexpirydate = $this->stockexpirydate->stockexpirydate_list($expiry_from,$expiry_to);
		return view('stockexpirydate/list',['stockexpirydate'=>$stockexpirydate,'expirydate_selected'=>$expirydate_selected]);
    }
}
