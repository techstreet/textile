<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockmanufacturer;
use App\Manufacturer;

class StockmanufacturerController extends Controller
{
    public function __construct()
    {
		$this->stockmanufacturer = new Stockmanufacturer();
		$this->manufacturer = new Manufacturer();
    }
    public function index()
    {
		//echo config('app.accounting_method');
		$manufacturer = $this->manufacturer->manufacturer_list();
		return view('stockmanufacturer/list',['manufacturer'=>$manufacturer]);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$manufacturer_selected = $request->input('manufacturer');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
			'manufacturer'=>'required'
		]);
		$stockmanufacturer = $this->stockmanufacturer->stockmanufacturer_list($manufacturer_selected,$from_date,$to_date);
		$count = $stockmanufacturer->count();
		$manufacturer = $this->manufacturer->manufacturer_list();
		return view('stockmanufacturer/list',['stockmanufacturer'=>$stockmanufacturer,'manufacturer'=>$manufacturer,'manufacturer_selected'=>$manufacturer_selected]);
    }
}
