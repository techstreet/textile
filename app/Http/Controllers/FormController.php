<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Form;

class FormController extends Controller
{
    public function __construct()
    {
		$this->form = new Form();
    }
    public function index()
    {
		$form = $this->form->form_list();
		$count = $form->count();
		return view('form/list',['form'=>$form,'count'=>$count]);
    }
    public function add()
    {
		return view('form/add');
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$status = $request->input('status');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','form',$company);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->form->form_add($company,$name,$status);
			if($result){
				$request->session()->flash('success', 'Record added successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$form = $this->form->form_edit($id);
		return view('form/edit',['form'=>$form]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$name = $request->input('name');
		$status = $request->input('status');
		$this->validate($request,[
			'name'=>'required'
		]);
		$record_exists = record_exists($name,'name','form',$company,$id);
		if($record_exists){
			$request->session()->flash('warning', 'Record already exists!');
		}
		else{
			$result = $this->form->form_update($id,$company,$name,$status);
			if($result){
				$request->session()->flash('success', 'Record updated successfully!');
			}
			else{
				$request->session()->flash('failed', 'Something went wrong!');
			}
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->form->form_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
}
