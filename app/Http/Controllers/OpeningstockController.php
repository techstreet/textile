<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Openingstock;
use App\Category;
use App\Brand;

class OpeningstockController extends Controller
{
    public function __construct()
    {
		$this->openingstock = new Openingstock();
		$this->category = new Category();
		$this->brand = new Brand();
    }
    public function index()
    {
		$openingstock = $this->openingstock->openingstock_list();
		$count = $openingstock->count();
		return view('openingstock/list',['openingstock'=>$openingstock,'count'=>$count]);
    }
    public function add()
    {
		$category = $this->category->category_list();
		$brand = $this->brand->brand_list();
		return view('openingstock/add',['category'=>$category,'brand'=>$brand]);
    }
    public function save(Request $request)
    {
		$company = Auth::user()->company_id;
		$category = $request->input('category');
		$item = $request->input('item');
		$brand = $request->input('brand');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $quantity*$rate;
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		$on_date = $request->input('on_date');
		$this->validate($request,[
			'category'=>'required',
			'item'=>'required',
			'quantity'=>'required|numeric',
			'rate'=>'required|numeric',
			'on_date'=>'required|date'
		]);
		
		$result = $this->openingstock->openingstock_add($company,$item,$brand,$quantity,$rate,$amount,$barcode,$expiry_date,$on_date);
		
		if($result){
		$request->session()->flash('success', 'Record added successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function edit($id)
    {
		$openingstock = $this->openingstock->openingstock_edit($id);
		foreach($openingstock as $value){
			$item_id = $value->item_id;
		}
		$category = $this->openingstock->category_list($item_id);
		$category_id = $this->openingstock->category_id($item_id);
		$brand_id = $this->openingstock->brand_id($item_id);
		$item = $this->openingstock->item_list($brand_id);
		$brand = $this->brand->brand_list();
		
		return view('openingstock/edit',['openingstock'=>$openingstock,'category'=>$category,'category_id'=>$category_id,'item'=>$item,'brand'=>$brand]);
    }
    public function update(Request $request,$id)
    {
		$company = Auth::user()->company_id;
		$category = $request->input('category');
		$item = $request->input('item');
		$brand = $request->input('brand');
		$quantity = $request->input('quantity');
		$rate = $request->input('rate');
		$amount = $quantity*$rate;
		$barcode = $request->input('barcode');
		$expiry_date = $request->input('expiry_date');
		$on_date = $request->input('on_date');
		$this->validate($request,[
			'category'=>'required',
			'item'=>'required',
			'quantity'=>'required|numeric',
			'rate'=>'required|numeric',
			'expiry_date'=>'required|date',
			'on_date'=>'required|date'
		]);
		$result = $this->openingstock->openingstock_update($id,$company,$item,$brand,$quantity,$rate,$amount,$barcode,$expiry_date,$on_date);
		if($result){
		$request->session()->flash('success', 'Record updated successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function delete(Request $request,$id)
    {
		$result = $this->openingstock->openingstock_delete($id);
		if($result){
			$request->session()->flash('success', 'Record deleted successfully!');
		}
		else{
			$request->session()->flash('failed', 'Something went wrong!');
		}
		return redirect()->back();
    }
    public function ajax(Request $request)
    {
		$company_id = $request->input('company_id');
		$this->openingstock->ajax($company_id);
    }
    public function ajax2(Request $request)
    {
		$category_id = $request->input('category_id');
		$this->openingstock->ajax2($category_id);
    }
    public function ajax3(Request $request)
    {
		$company = Auth::user()->company_id;
		$item_name = $request->input('item_name');
		$this->openingstock->ajax3($item_name,$company);
		}
		public function ajax4(Request $request)
    {
		$company = Auth::user()->company_id;
		$item_name = $request->input('item_name');
		$this->openingstock->ajax4($item_name,$company);
		}
		public function ajax5(Request $request)
    {
		$brand_id = $request->input('brand_id');
		$this->openingstock->ajax5($brand_id);
    }

}
