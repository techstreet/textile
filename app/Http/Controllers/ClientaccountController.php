<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Clientaccount;
use Carbon\Carbon;

class ClientaccountController extends Controller
{
    public function __construct()
    {
		$this->clientaccount = new Clientaccount();
		$this->date = Carbon::now('Asia/Kolkata');
    }
//    public function index()
//    {
//		$clientaccount = $this->clientaccount->clientaccount_list();
//		$count = $clientaccount->count();
//		return view('clientaccount/list',['clientaccount'=>$clientaccount,'count'=>$count]);
//    }
    public function detail($id)
    {
		$clientaccount = $this->clientaccount->clientaccount_detail($id);
		$count = $clientaccount->count();
		$client_name = $this->clientaccount->get_clientname($id);
		return view('clientaccount/detail',['clientaccount'=>$clientaccount,'count'=>$count,'client_name'=>$client_name]);
    }
    public function paymentAdd($id)
    {
		$outstanding_balance = getBalance($id);
		$client_name = $this->clientaccount->get_clientname($id);
		return view('clientaccount/receive',['client_name'=>$client_name,'outstanding_balance'=>$outstanding_balance]);
    }
    public function paymentSave(Request $request,$id)
    {
		$amount = $request->input('amount');
		$payment_date = $request->input('date');
		$particular = $request->input('remarks');
		$this->validate($request,[
			'amount'=>'required',
			'date'=>'required|date',
			'remarks'=>'required',
		]);
		$result = $this->clientaccount->payment_add($id,$amount,$payment_date,$particular);
		if($result){
			$request->session()->flash('success', 'Payment received successfully!');
		}
		else{
			$request->session()->flash('error', 'Something went wrong!');
		}
		return redirect()->back();
		}
		
		public function delete(Request $request,$id)
    {
			$data = DB::table('client_register')
			->where([
			['id',$id],
			])
			->first();

			if($data){
				$invoice_no = $data->invoice_no;
					if(empty($invoice_no)){
					$user_id = Auth::id();
					DB::table('client_register')
								->where('id', $id)
								->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
				}
				$request->session()->flash('success', 'Record deleted successfully!');
				return redirect()->back();
			}
			
    }
}
