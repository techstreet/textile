<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Stockform;
use App\Form;

class StockformController extends Controller
{
    public function __construct()
    {
		$this->stockform = new Stockform();
		$this->form = new Form();
    }
    public function index()
    {
		//echo config('app.accounting_method');
		$form = $this->form->form_list();
		return view('stockform/list',['form'=>$form]);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$form_selected = $request->input('form');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
			'form'=>'required'
		]);
		$stockform = $this->stockform->stockform_list($form_selected,$from_date,$to_date);
		$count = $stockform->count();
		$form = $this->form->form_list();
		return view('stockform/list',['stockform'=>$stockform,'form'=>$form,'form_selected'=>$form_selected]);
    }
}
