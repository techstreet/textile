<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Welcome;

class WelcomeController extends Controller
{
    public function __construct()
    {
		$this->dashboard = new Welcome();
    }
    public function index()
    {
		$user = Auth::user();
		$user_id = $user->id;
		$user_name = $user->name;
		
		$sale = $this->dashboard->sale();
		$sale_today = $this->dashboard->sale_today();
		$sale_thisweek = $this->dashboard->sale_thisweek();
		$sale_thismonth = $this->dashboard->sale_thismonth();
		
		$salereturn = $this->dashboard->salereturn();
		$salereturn_today = $this->dashboard->salereturn_today();
		$salereturn_thisweek = $this->dashboard->salereturn_thisweek();
		$salereturn_thismonth = $this->dashboard->salereturn_thismonth();
		
		$cash = $this->dashboard->cash();
		$cash_today = $this->dashboard->cash_today();
		$cash_thisweek = $this->dashboard->cash_thisweek();
		$cash_thismonth = $this->dashboard->cash_thismonth();
		
		$purchase = $this->dashboard->purchase();
		$purchase_today = $this->dashboard->purchase_today();
		$purchase_thisweek = $this->dashboard->purchase_thisweek();
		$purchase_thismonth = $this->dashboard->purchase_thismonth();
		
		$purchasereturn = $this->dashboard->purchasereturn();
		$purchasereturn_today = $this->dashboard->purchasereturn_today();
		$purchasereturn_thisweek = $this->dashboard->purchasereturn_thisweek();
		$purchasereturn_thismonth = $this->dashboard->purchasereturn_thismonth();
		
		$credit = $this->dashboard->credit();
		$credit_today = $this->dashboard->credit_today();
		$credit_thisweek = $this->dashboard->credit_thisweek();
		$credit_030 = $this->dashboard->credit_030();
		$credit_3060 = $this->dashboard->credit_3060();
		$credit_6090 = $this->dashboard->credit_6090();
		$credit_90120 = $this->dashboard->credit_90120();
		
		$latest_client = $this->dashboard->latest_client();
		$latestclient_count = $latest_client->count();
		
		$latest_vendors = $this->dashboard->latest_vendors();
		$latestvendors_count = $latest_vendors->count();
		
		$remaining_stock = $this->dashboard->remaining_stock();
		
		
		return view('welcome',['user_id'=>$user_id,'user_name'=>$user_name,'sale'=>$sale,'sale_today'=>$sale_today,'sale_thisweek'=>$sale_thisweek,'sale_thismonth'=>$sale_thismonth,'salereturn'=>$salereturn,'salereturn_today'=>$salereturn_today,'salereturn_thisweek'=>$salereturn_thisweek,'salereturn_thismonth'=>$salereturn_thismonth,'cash'=>$cash,'cash_today'=>$cash_today,'cash_thisweek'=>$cash_thisweek,'cash_thismonth'=>$cash_thismonth,'purchase'=>$purchase,'purchase_today'=>$purchase_today,'purchase_thisweek'=>$purchase_thisweek,'purchase_thismonth'=>$purchase_thismonth,'purchasereturn'=>$purchasereturn,'purchasereturn_today'=>$purchasereturn_today,'purchasereturn_thisweek'=>$purchasereturn_thisweek,'purchasereturn_thismonth'=>$purchasereturn_thismonth,'credit'=>$credit,'credit_today'=>$credit_today,'credit_thisweek'=>$credit_thisweek,'credit_030'=>$credit_030,'credit_3060'=>$credit_3060,'credit_6090'=>$credit_6090,'credit_90120'=>$credit_90120,'latest_client'=>$latest_client,'latestclient_count'=>$latestclient_count,'latest_vendors'=>$latest_vendors,'latestvendors_count'=>$latestvendors_count,'remaining_stock'=>$remaining_stock]);
    }
}
