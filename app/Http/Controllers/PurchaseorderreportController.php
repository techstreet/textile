<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Purchaseorderreport;

class PurchaseorderreportController extends Controller
{
    public function __construct()
    {
		$this->purchaseorderreport = new Purchaseorderreport();
    }
    public function index()
    {
		return view('purchaseorderreport/list',['category_selected'=>'']);
    }
    public function search(Request $request)
    {
		$from_date = $request->input('from_date');
		$to_date = $request->input('to_date');
		$this->validate($request,[
			'from_date'=>'required|date',
			'to_date'=>'required|date',
		]);
		$purchaseorderreport = $this->purchaseorderreport->purchaseorderreport_list($from_date,$to_date);
		$count = $purchaseorderreport->count();
		return view('purchaseorderreport/list',['purchaseorderreport'=>$purchaseorderreport,'count'=>$count]);
    }
}
