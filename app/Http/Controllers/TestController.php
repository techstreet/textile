<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index()
    {
		return view('test.add');
    }
    public function autocomplete(Request $request)
    {
		$query = $request->get('query','');
        $voucher = DB::table('purchaseregister')
			->select('purchasevoucher_no as name')
			->where([
			['status','1'],
			['purchasevoucher_no','LIKE','%'.$query.'%'],
			])
			->get();        
        return response()->json($voucher);
    }
}
