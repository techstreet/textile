<?php

namespace App\Http\Middleware;

use Closure;
use session;

class CheckSuperadminSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('superadmin') == '1'){
			return $next($request);
		}
		else{
			return redirect('/');
		}
    }
}
