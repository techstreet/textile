<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Unit extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function unit_list()
	{
		$company = Auth::user()->company_id;
		$unit = DB::table('unit')
			->where([
			['status','1'],
			['company_id',$company]
			])
			->get();
		return $unit;
	}
	public function getUnitName($id)
	{
		$unit = DB::table('unit')->where('id',$id)->get();
		foreach($unit as $value){
			$name = $value->name;
		}
		return $name;
	}
    public function unit_add($company,$name,$description)
    {
		$user_id = Auth::id();
		$unit_id = DB::table('unit')->insertGetId(
		    ['company_id' => $company,'name' => $name,'description' => $description,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $unit_id;
    }
	public function unit_edit($id)
	{
		return DB::table('unit')->where('id',$id)->get();
	}
	public function unit_update($id,$company,$name,$description)
    {
		$user_id = Auth::id();
		return DB::table('unit')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'description' => $description,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function unit_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('unit')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
