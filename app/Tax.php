<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Tax extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function tax_list()
	{
		$company = Auth::user()->company_id;
		return DB::table('tax')
			->where([
			['status','1'],
			['company_id',$company]
			])
			->get();
	}
    public function tax_add($company,$name,$description,$rate,$effective_from,$is_active)
    {
		$user_id = Auth::id();
		$effective_from = date_format(date_create($effective_from),"Y-m-d");
		$tax_id = DB::table('tax')->insertGetId(
		    ['company_id' => $company,'name' => $name,'description' => $description,'rate' => $rate,'effective_from' => $effective_from,'is_active' => $is_active,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $tax_id;
    }
	public function tax_edit($id)
	{
		return DB::table('tax')->where('id',$id)->get();
	}
	public function tax_update($id,$company,$name,$description,$rate,$effective_from,$is_active)
    {
		$user_id = Auth::id();
		$effective_from = date_format(date_create($effective_from),"Y-m-d");
		return DB::table('tax')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'description' => $description,'rate' => $rate,'effective_from' => $effective_from,'is_active' => $is_active,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function tax_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('tax')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
