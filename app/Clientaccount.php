<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Clientaccount extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
//    public function clientaccount_list()
//	{
//		return DB::table('client_register')
//			->select('client_register.*', 'client.name as name')
//			->groupBy('client_register.client_id')
//			->where('client_register.status','1')
//            ->leftJoin('client', 'client_register.client_id', '=', 'client.id')
//            ->get();
//	}
	public function clientaccount_detail($id)
	{
		return DB::table('client_register')
		->where([
		['client_id',$id],
		['status','1'],
		])
		->get();
	}
	public function get_clientname($id)
	{
        $client = DB::table('client')
			->where('id',$id)
            ->first();
        return $client->name;
	}
	public function getBalance($client_id)
	{
		$res = DB::table('client_register')
			->where([
			['client_id',$client_id],
			['status','1'],
			])
			->orderBy('id','desc')
			->first();
		if($res){
			return $res->balance;
		}
		else{
			return 0;
		}
	}
	public function payment_add($id,$amount,$payment_date,$particular)
	{
		$company = Auth::user()->company_id;
		return DB::table('client_register')->insert(
		    ['company_id' => $company,'client_id' => $id,'stype' => 'cash_received','type' => '2','record_type'=>'2','particular' => $particular,'credit' => $amount,'balance' => $this->getBalance($id)-$amount,'created_at' => $this->date]
		);
	}
}
