<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Stockregister extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function stockregister_list($category,$from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");
		
		if($category == 'Purchase Register'){
			$table1 = 'purchaseregister';
			$table2 = 'purchaseregister_item';
			$table_id = 'purchaseregister_id';
		}
		if($category == 'Sale Register'){
			$table1 = 'saleregister';
			$table2 = 'saleregister_item';
			$table_id = 'saleregister_id';
		}
		  
        $items = DB::table($table2)
			->select('item.name as item_name','item.id as item_id','category.name as category_name')
			->where([
			['item.status','1'],
			['item.company_id',$company]
			])
            ->leftJoin('item', 'item.id', '=', $table2.'.item_id')
            ->leftJoin('category', 'item.category_id', '=', 'category.id')
            ->groupBy($table2.'.item_id')
            ->get();
	    
	    function get_price($item_id){
			$item = DB::table('item')
			->select('price')
			->where([
			['id',$item_id],
			['status','1']
			])
			->get();
			return $item[0]->price;
		}
	    
	    function get_quantity($item_id,$company,$from_date,$to_date,$table1,$table2,$table_id){
			$quantity = DB::table($table1)
			->select(DB::raw('SUM('.$table2.'.quantity) as qty'))
			->where([
			[$table1.'.status','1'],
			[$table2.'.status','1'],
			[$table1.'.company_id',$company],
			[$table2.'.item_id',$item_id]
			])
			->whereBetween($table1.'.entry_date', [$from_date, $to_date])
            ->leftJoin($table2, $table2.'.'.$table_id, '=', $table1.'.id')
            ->groupBy($table2.'.item_id')
            ->get();
            $item_quantity = 0;
            if(!empty($quantity)){
				foreach($quantity as $value){
					$item_quantity = $value->qty;
				}
			}
			return $item_quantity;
		}
	    
	    foreach($items as $key=>$value){
			$item_id = $value->item_id;
			$item_quantity = get_quantity($item_id,$company,$from_date,$to_date,$table1,$table2,$table_id);
			$price = get_price($item_id);
			$items[$key]->item_quantity=$item_quantity;
			$items[$key]->price=$price;
		}
        
		return $items;
	}
}
