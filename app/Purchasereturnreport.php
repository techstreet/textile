<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Purchasereturnreport extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function purchasereturnreport_list($from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$from_date = date_format(date_create($from_date),"Y-m-d");
		$to_date = date_format(date_create($to_date),"Y-m-d");
		
		$result = DB::table('purchasereturn')
			->select('purchasereturn.*','vendor.name as vendor_name', 'company.name as company_name','users.address as address')
            ->leftJoin('company', 'purchasereturn.company_id', '=', 'company.id')
            ->leftJoin('users', 'users.company_id', '=', 'company.id')
            ->groupby('id')
			->leftJoin('purchaseregister', 'purchaseregister.purchasevoucher_no', '=', 'purchasereturn.purchasevoucher_no')
			->leftJoin('vendor', 'vendor.id', '=', 'purchaseregister.vendor_id')
			->where([
			['purchasereturn.status','1'],
			['purchasereturn.company_id',$company]
			])
			->whereBetween('purchasereturn.debitnote_date', [$from_date, $to_date])
            ->get();
            
		return $result;
	}
}
