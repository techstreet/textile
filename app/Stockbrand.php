<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Stockbrand extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function stockbrand_list($brand_id,$from_date,$to_date)
	{
		$company = Auth::user()->company_id;
		$step = '+1 day';
		$output_brandat = 'Y-m-d';
		$dates = array();
	    $from_date = strtotime($from_date);
	    $to_date = strtotime($to_date);
	    
	    $items = DB::table('item')
			->select('item.name as item_name','item.id as item_id','brand.name as brand_name')
			->where([
			['item.status','1'],
			['brand.status','1'],
			['brand.company_id',$company],
			['item.company_id',$company],
			['brand.id',$brand_id]
			])
            ->leftJoin('brand', 'item.brand_id', '=', 'brand.id')
            ->get();
	    
	    function get_price($item_id){
			$item = DB::table('item')
			->select('price')
			->where([
			['id',$item_id],
			['status','1']
			])
			->get();
			return $item[0]->price;
		}
	    
	    function get_purchase($item_id,$date,$company,$op){
			$purchase = DB::table('purchaseregister')
			->select(DB::raw('SUM(purchaseregister_item.quantity) as qty'))
			->where([
			['purchaseregister.status','1'],
			['purchaseregister_item.status','1'],
			['purchaseregister.company_id',$company],
			['purchaseregister.entry_date',$op,$date],
			['purchaseregister_item.item_id',$item_id]
			])
            ->leftJoin('purchaseregister_item', 'purchaseregister_item.purchaseregister_id', '=', 'purchaseregister.id')
            ->groupBy('purchaseregister_item.item_id')
            ->get();
            $purchase_qunatity = 0;
            if(!empty($purchase)){
				foreach($purchase as $value){
					$purchase_qunatity = $value->qty;
				}
			}
			return $purchase_qunatity;
		}
		
		function get_sale($item_id,$date,$company,$op){
			$sale = DB::table('saleregister')
			->select(DB::raw('SUM(saleregister_item.quantity) as qty'))
			->where([
			['saleregister.status','1'],
			['saleregister_item.status','1'],
			['saleregister.company_id',$company],
			['saleregister.entry_date',$op,$date],
			['saleregister_item.item_id',$item_id]
			])
            ->leftJoin('saleregister_item', 'saleregister_item.saleregister_id', '=', 'saleregister.id')
            ->groupBy('saleregister_item.item_id')
            ->get();
            $sale_qunatity = 0;
			if(!empty($sale)){
				foreach($sale as $value){
					$sale_qunatity = $value->qty;
				}
			}
			return $sale_qunatity;
		}
  		
		function get_stock($item_id,$date,$company,$op){
			$opening_stock = DB::table('openingstock')
			->select('quantity')
			->where([
			['item_id',$item_id],
			['on_date','<=',$date],
			['status','1']
			])
			->get();
			$count = $opening_stock->count();
			$openingstock_quantity = 0;
			if($count>0){
				$openingstock_quantity = $opening_stock[0]->quantity;
			}
			$purchase_qunatity = get_purchase($item_id,$date,$company,$op);
			$sale_qunatity = get_sale($item_id,$date,$company,$op);
			$stock = $openingstock_quantity+$purchase_qunatity-$sale_qunatity;
			return $stock;
		}
	    
	    while( $from_date <= $to_date ) {
			
			foreach($items as $key=>$value){
				$item_id = $value->item_id;
				$purchase_quantity = get_purchase($item_id,date($output_brandat, $from_date),$company,'=');
				$sale_quantity = get_sale($item_id,date($output_brandat, $from_date),$company,'=');
				$opening_stock = get_stock($item_id,date($output_brandat, $from_date),$company,'<');
				$closing_stock = get_stock($item_id,date($output_brandat, $from_date),$company,'<=');
				$price = get_price($item_id);
				$items[$key]->purchase_quantity=$purchase_quantity;
				$items[$key]->sale_quantity=$sale_quantity;
				$items[$key]->opening_stock=$opening_stock;
				$items[$key]->closing_stock=$closing_stock;
				$items[$key]->price=$price;
			}
			
	        $from_date = strtotime($step, $from_date);
	    }
		return $items;
	}
}
