<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Welcome extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
    }
    public function sale()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('saleregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			])
            ->first();
            return $result->total;
		}
		$result = DB::table('saleregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
            ->first();
        return $result->total;
    }
    public function sale_today()
    {
		$company = Auth::user()->company_id;
		$today = Carbon::today();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('saleregister')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereDate('entry_date',$today)
	            ->first();
			return $result->total;
		}
		
		$result = DB::table('saleregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereDate('entry_date',$today)
            ->first();
		return $result->total;
    }
    public function sale_thisweek()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		$monday = Carbon::now()->startOfWeek();
		$sunday = Carbon::now()->endOfWeek();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('saleregister')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('entry_date', [$monday, $sunday])
	            ->first();
			return $result->total;
		}
		$result = DB::table('saleregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('entry_date', [$monday, $sunday])
            ->first();
        return $result->total;
	}
	
	public function sale_thismonth()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		$start = Carbon::now()->startOfMonth();
		$end = Carbon::now()->endOfMonth();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('saleregister')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('entry_date', [$start, $end])
	            ->first();
			return $result->total;
		}
		$result = DB::table('saleregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('entry_date', [$start, $end])
			->first();
        return $result->total;
    }
    
    public function salereturn()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('salereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
	            ->first();
			return $result->total;
		}
		$result = DB::table('salereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
            ->first();
        return $result->total;
    }
    public function salereturn_today()
    {
		$company = Auth::user()->company_id;
		$today = Carbon::today();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('salereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereDate('creditnote_date',$today)
	            ->first();
			return $result->total;
		}
		$result = DB::table('salereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereDate('creditnote_date',$today)
            ->first();
        return $result->total;
    }
    public function salereturn_thisweek()
    {
		$company = Auth::user()->company_id;
		$monday = Carbon::now()->startOfWeek();
		$sunday = Carbon::now()->endOfWeek();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('salereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('creditnote_date', [$monday, $sunday])
	            ->first();
			return $result->total;
		}
		$result = DB::table('salereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('creditnote_date', [$monday, $sunday])
            ->first();
        return $result->total;
	}
	
	public function salereturn_thismonth()
    {
		$company = Auth::user()->company_id;
		$start = Carbon::now()->startOfMonth();
		$end = Carbon::now()->endOfMonth();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('salereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('creditnote_date', [$start, $end])
	            ->first();
			return $result->total;
		}
		$result = DB::table('salereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('creditnote_date', [$start, $end])
            ->first();
        return $result->total;
    }
    
    public function cash()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->first();

			//$cash_received2 = DB::table('client_register')
					//->select(DB::raw("SUM(credit) as credit"))
					//->leftJoin("client","client.id","client_register.client_id")
					//->where([
					//['client_register.type','2'],
					//['client_register.status','1'],
					//['client_register.invoice_no',NULL],
					//['client.status','1'],
					//])
					//->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->first();
			return $cash_received->credit - $cash_returned->debit;
		}
		else{
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->first();

			//$cash_received2 = DB::table('client_register')
				//	->select(DB::raw("SUM(credit) as credit"))
				//	->leftJoin("client","client.id","client_register.client_id")
				//	->where([
				//	['client_register.type','2'],
				//	['client_register.status','1'],
				//	['client_register.invoice_no',NULL],
				//	['client_register.company_id',$company],
				//	['client.status','1'],
				//	])
				//	->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->first();
			return $cash_received->credit - $cash_returned->debit;
		}
    }
    public function cash_today()
    {
		$company = Auth::user()->company_id;
		$today = Carbon::today();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->whereDate('client_register.created_at',$today)
					->first();

			//$cash_received2 = DB::table('client_register')
			//		->select(DB::raw("SUM(credit) as credit"))
			//		->leftJoin("client","client.id","client_register.client_id")
			//		->where([
			//		['client_register.type','2'],
			//		['client_register.status','1'],
			//		['client_register.invoice_no',NULL],
			//		['client.status','1'],
			//		])
			//		->whereDate('client_register.created_at',$today)
			//		->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->whereDate('client_register.created_at',$today)
					->first();
			return $cash_received->credit  - $cash_returned->debit;
		}
		else{
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->whereDate('client_register.created_at',$today)
					->first();

			//$cash_received2 = DB::table('client_register')
			//		->select(DB::raw("SUM(credit) as credit"))
			//		->leftJoin("client","client.id","client_register.client_id")
			//		->where([
			//		['client_register.type','2'],
			//		['client_register.status','1'],
			//		['client_register.invoice_no',NULL],
			//		['client_register.company_id',$company],
			//		['client.status','1'],
			//		])
			//		->whereDate('client_register.created_at',$today)
			//		->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->whereDate('client_register.created_at',$today)
					->first();
			return $cash_received->credit  - $cash_returned->debit;
		}
    }
    public function cash_thisweek()
    {
		$company = Auth::user()->company_id;
		$monday = Carbon::now()->startOfWeek();
		$sunday = Carbon::now()->endOfWeek();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$monday, $sunday])
					->first();

			//$cash_received2 = DB::table('client_register')
			//		->select(DB::raw("SUM(credit) as credit"))
			//		->leftJoin("client","client.id","client_register.client_id")
			//		->where([
			//		['client_register.type','2'],
			//		['client_register.status','1'],
			//		['client_register.invoice_no',NULL],
			//		['client.status','1'],
			//		])
			//		->whereBetween('client_register.created_at', [$monday, $sunday])
			//		->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$monday, $sunday])
					->first();
			return $cash_received->credit - $cash_returned->debit;
		}
		else{
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$monday, $sunday])
					->first();

			//$cash_received2 = DB::table('client_register')
			//		->select(DB::raw("SUM(credit) as credit"))
			//		->leftJoin("client","client.id","client_register.client_id")
			//		->where([
			//		['client_register.type','2'],
			//		['client_register.status','1'],
			//		['client_register.invoice_no',NULL],
			//		['client_register.company_id',$company],
			//		['client.status','1'],
			//		])
			//		->whereBetween('client_register.created_at', [$monday, $sunday])
			//		->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$monday, $sunday])
					->first();
			return $cash_received->credit - $cash_returned->debit;
		}
	}
	
	public function cash_thismonth()
    {
		$company = Auth::user()->company_id;
		$start = Carbon::now()->startOfMonth();
		$end = Carbon::now()->endOfMonth();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$start, $end])
					->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$start, $end])
					->first();
			return $cash_received->credit - $cash_returned->debit;
		}
		else{
			$cash_received = DB::table('client_register')
					->select(DB::raw("SUM(credit) as credit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','2'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$start, $end])
					->first();

			$cash_returned = DB::table('client_register')
					->select(DB::raw("SUM(debit) as debit"))
					->leftJoin("client","client.id","client_register.client_id")
					->where([
					['client_register.type','4'],
					['client_register.status','1'],
					['client_register.company_id',$company],
					['client.status','1'],
					])
					->whereBetween('client_register.created_at', [$start, $end])
					->first();
			return $cash_received->credit - $cash_returned->debit;
		}
    }

	public function purchase()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchaseregister')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchaseregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
            ->first();
        return $result->total;
    }
    public function purchase_today()
    {
		$company = Auth::user()->company_id;
		$today = Carbon::today();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchaseregister')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereDate('entry_date',$today)
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchaseregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereDate('entry_date',$today)
            ->first();
        return $result->total;
    }
    public function purchase_thisweek()
    {
		$company = Auth::user()->company_id;
		$monday = Carbon::now()->startOfWeek();
		$sunday = Carbon::now()->endOfWeek();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchaseregister')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('entry_date', [$monday, $sunday])
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchaseregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('entry_date', [$monday, $sunday])
            ->first();
        return $result->total;
	}
	
	public function purchase_thismonth()
    {
		$company = Auth::user()->company_id;
		$start = Carbon::now()->startOfMonth();
		$end = Carbon::now()->endOfMonth();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchaseregister')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('entry_date', [$start, $end])
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchaseregister')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('entry_date', [$start, $end])
            ->first();
        return $result->total;
    }

	public function purchasereturn()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchasereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchasereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
            ->first();
        return $result->total;
    }
    public function purchasereturn_today()
    {
		$company = Auth::user()->company_id;
		$today = Carbon::today();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchasereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereDate('debitnote_date',$today)
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchasereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereDate('debitnote_date',$today)
            ->first();
        return $result->total;
    }
    public function purchasereturn_thisweek()
    {
		$company = Auth::user()->company_id;
		$monday = Carbon::now()->startOfWeek();
		$sunday = Carbon::now()->endOfWeek();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchasereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('debitnote_date', [$monday, $sunday])
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchasereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('debitnote_date', [$monday, $sunday])
            ->first();
        return $result->total;
	}
	
	public function purchasereturn_thismonth()
    {
		$company = Auth::user()->company_id;
		$start = Carbon::now()->startOfMonth();
		$end = Carbon::now()->endOfMonth();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('purchasereturn')
				->select(DB::raw("SUM(total) as total"))
				->where([
				['status','1'],
				])
				->whereBetween('debitnote_date', [$start, $end])
	            ->first();
			return $result->total;
		}
		$result = DB::table('purchasereturn')
			->select(DB::raw("SUM(total) as total"))
			->where([
			['status','1'],
			['company_id',$company]
			])
			->whereBetween('debitnote_date', [$start, $end])
            ->first();
        return $result->total;
    }
    
    public function credit()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
				['client_register.status','1'],
				])
	            ->first();
				return $result->debit - $result->credit;
		}
		$result = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            -> leftJoin("client","client.id","client_register.client_id")
			->where([
			['client.status','1'],
			['client_register.status','1'],
		    ['client_register.company_id',$company]
			])
            ->first();
        return $result->debit - $result->credit;
    }
    public function credit_today()
    {
		$company = Auth::user()->company_id;
		$today = Carbon::today();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
				['client_register.status','1'],
				])
				->whereDate('created_at',$today)
	            ->first();
				return $result->debit - $result->credit;
		}
		$result = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            -> leftJoin("client","client.id","client_register.client_id")
			->where([
			['client.status','1'],
			['client_register.status','1'],
		    ['client_register.company_id',$company]
			])
			->whereDate('client_register.created_at',$today)
            ->first();
        return $result->debit - $result->credit;
    }
    public function credit_thisweek()
    {
		$company = Auth::user()->company_id;
		$monday = Carbon::now()->startOfWeek();
		$sunday = Carbon::now()->endOfWeek();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
				['client_register.status','1'],
				])
				->whereBetween('created_at', [$monday, $sunday])
	            ->first();
				return $result->debit - $result->credit;
		}
		$result = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            -> leftJoin("client","client.id","client_register.client_id")
			->where([
			['client.status','1'],
			['client_register.status','1'],
		    ['client_register.company_id',$company]
			])
			->whereBetween('client_register.created_at', [$monday, $sunday])
            ->first();
        return $result->debit - $result->credit;
	}
	
	public function credit_030()
    {
		$company = Auth::user()->company_id;
		$start = Carbon::now()->subMonths(1);
		$end = Carbon::today();
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
				['client_register.status','1'],
				])
				->whereBetween('created_at', [$start, $end])
	            ->first();
				return $result->debit - $result->credit;
		}
		$result = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            -> leftJoin("client","client.id","client_register.client_id")
			->where([
			['client.status','1'],
			['client_register.status','1'],
		    ['client_register.company_id',$company]
			])
			->whereBetween('client_register.created_at', [$start, $end])
            ->first();
        return $result->debit - $result->credit;
	}
	
	public function credit_3060()
    {
		$company = Auth::user()->company_id;
		$start = Carbon::now()->subMonths(2);
		$end = Carbon::now()->subMonths(1);
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
				['client_register.status','1'],
				])
				->whereBetween('created_at', [$start, $end])
	            ->first();
				return $result->debit - $result->credit;
		}
		$result = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            -> leftJoin("client","client.id","client_register.client_id")
			->where([
			['client.status','1'],
			['client_register.status','1'],
		    ['client_register.company_id',$company]
			])
			->whereBetween('client_register.created_at', [$start, $end])
            ->first();
        return $result->debit - $result->credit;
	}
	
	public function credit_6090()
    {
		$company = Auth::user()->company_id;
		$start = Carbon::now()->subMonths(3);
		$end = Carbon::now()->subMonths(2);
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
				['client_register.status','1'],
				])
				->whereBetween('created_at', [$start, $end])
	            ->first();
				return $result->debit - $result->credit;
		}
		$result = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            -> leftJoin("client","client.id","client_register.client_id")
			->where([
			['client.status','1'],
			['client_register.status','1'],
		    ['client_register.company_id',$company]
			])
			->whereBetween('client_register.created_at', [$start, $end])
            ->first();
        return $result->debit - $result->credit;
	}
	
	public function credit_90120()
    {
		$company = Auth::user()->company_id;
		$date = Carbon::now()->subMonths(3);
		$user_group = Auth::user()->user_group;
		if($user_group == '1'){
			$result = DB::table('client_register')
				->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
				->where([
				['client_register.status','1'],
				['client_register.created_at','<=',$date],
				])
	            ->first();
				return $result->debit - $result->credit;
		}
		$result = DB::table('client_register')
			->select(DB::raw("SUM(debit) as debit"),DB::raw("SUM(credit) as credit"))
            -> leftJoin("client","client.id","client_register.client_id")
			->where([
			['client.status','1'],
			['client_register.status','1'],
			['client_register.created_at','<=',$date],
		    ['client_register.company_id',$company]
			])
            ->first();
        return $result->debit - $result->credit;
    }
    
    public function latest_client()
    {
		$company = Auth::user()->company_id;
		return DB::table('client')
			->where([
			['status','1'],
			['company_id',$company]
			])
            ->orderBy('client.id', 'desc')
            ->limit(10)
            ->get();
    }
    public function latest_vendors()
    {
		$company = Auth::user()->company_id;
		return DB::table('vendor')
			->where([
			['status','1'],
			['company_id',$company]
			])
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
    }
    
    public function remaining_stock()
    {
		$company = Auth::user()->company_id;
		$user_group = Auth::user()->user_group;

		if($user_group == '1'){
			$items = DB::table('item')
			->where([
			['status','1'],
			])
			->get();
		}
		else{
			$items = DB::table('item')
			->where([
			['status','1'],
			['company_id',$company]
			])
			->get();
		}
		$opening_stock = 0;
		$purchase = 0;
		$purchase_return = 0;
		$purchase_total = 0;
		$sale = 0;
		$sale_return = 0;
		$sale_total = 0;
		$closing_stock = 0;
		$unit_price = 0;
		$stock_value = 0;
		foreach ($items as $value){
			$opening_stock+= getOpeningStock($value->id);
			$purchase+= getPurchaseStock($value->id);
			$purchase_return+= getPurchaseReturnStock($value->id);
			$purchase_total+= getPurchaseStock($value->id) - getPurchaseReturnStock($value->id);
			$sale+= getTotalSales($value->id);
			$sale_return+= getTotalSaleReturnStock($value->id);
			$sale_total+= getTotalSales($value->id) - getTotalSaleReturnStock($value->id);
			$closing_stock+= $opening_stock + $purchase_total - $sale_total;
			$unit_price+= getPrice($value->id);
			$stock_value+= getStock($value->id) * getPrice($value->id);	
		}
		return $stock_value;
    }

}
