<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use App\Http\Requests;
use Carbon\Carbon;

class Client extends Model
{
	public function __construct()
    {
		$this->date = Carbon::now('Asia/Kolkata');
	}
    public function client_list()
	{
		$company = Auth::user()->company_id;
		//return DB::select( DB::raw() );
		
		return DB::table('client')
			->select(DB::raw('client.*,sum(client_register.debit) as debit,sum(client_register.credit) as credit'))
			->where([
			['client.status','1'],
			['client.company_id',$company]
			])
			->leftJoin('client_register','client_register.client_id','=','client.id')
			->groupBy('client.id')
			->orderBy('client.id','DESC')
			->get();
	}
    public function client_add($company,$name,$contact_name,$email,$address,$phone,$mobile,$gstin)
    {
		$user_id = Auth::id();
		$client_id = DB::table('client')->insertGetId(
		    ['company_id' => $company,'name' => $name,'contact_name' => $contact_name,'email' => $email,'address' => $address,'phone' => $phone,'mobile' => $mobile,'gstin' => $gstin,'created_at' => $this->date,'created_by' => $user_id]
		);
		return $client_id;
    }
	public function client_edit($id)
	{
		return DB::table('client')->where('id',$id)->get();
	}
	public function client_update($id,$company,$name,$contact_name,$email,$address,$phone,$mobile,$gstin)
    {
		$user_id = Auth::id();
		return DB::table('client')
            ->where('id', $id)
            ->update(['company_id' => $company,'name' => $name,'contact_name' => $contact_name,'email' => $email,'address' => $address,'phone' => $phone,'mobile' => $mobile,'gstin' => $gstin,'updated_at' => $this->date,'updated_by' => $user_id]);
    }
    public function client_delete($id)
	{
		$user_id = Auth::id();
		return DB::table('client')
            ->where('id', $id)
            ->update(['status' => '0','updated_at' => $this->date,'updated_by' => $user_id]);
	}
}
