@extends('layouts.website')
@section('content')
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Transports</h4>
        <ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{url('/transport')}}">Transports</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		@include('flashmessage')
      	<form method="post" action="{{ url('/transport/update/'.$transport[0]->id) }}">
      	{{ csrf_field() }}
		  <div class="form-group">
		    <label>*Name:</label>
		    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="<?php echo $transport[0]->name; ?>" required="">
		  </div>
		  <div class="form-group">
		    <label>Contact Person:</label>
		    <input type="text" class="form-control" placeholder="Enter contact person" name="contact_person" id="contact_person" value="{{$transport[0]->contact_person}}">
		  </div>
          <div class="form-group">
		    <label>Mobile:</label>
		    <input type="number" class="form-control" placeholder="Enter mobile" name="mobile" id="mobile" value="{{$transport[0]->mobile}}">
		  </div>
          <div class="form-group">
		    <label>Phone:</label>
		    <input type="number" class="form-control" placeholder="Enter phone" name="phone" id="phone" value="{{$transport[0]->phone}}">
		  </div>
		  <div class="form-group">
		    <label>Address:</label>
		    <textarea rows="4" class="form-control" placeholder="Enter address" name="address" id="address" maxlength="400">{{$transport[0]->address}}</textarea>
		  </div>
		  <div style="text-align: right" id="address_characters"></div>
		  <button type="submit" class="btn btn-primary">Update</button>
		</form>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    var text_max = 400;
    var text_length = $('#address').val().length;
    var text_remaining = text_max - text_length;
    $('#address_characters').html(text_remaining + ' characters left');

    $('#address').keyup(function() {
        var text_length = $('#address').val().length;
        var text_remaining = text_max - text_length;

        $('#address_characters').html(text_remaining + ' characters left');
    });
});
</script>
@endsection