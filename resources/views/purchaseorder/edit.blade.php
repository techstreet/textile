@extends('layouts.website')
@section('content')
<?php
foreach($purchaseorder as $value){
	$purchaseorder_no = $value->purchaseorder_no;
	$vendor_id = $value->vendor_id;
	$vendor_name = $value->vendor_name;
	$remarks = $value->remarks;
	$entry_date = $value->entry_date;
	$sub_total = $value->sub_total;
}
?>
<script type='text/javascript'>
$(function(){
	$("#item").select2();
	$("#vendor").select2();
	$("#category").select2();
	$("#brand").select2();
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	$('.field_wrapper').on('keyup change', '.qtyVal', function(e){
		e.preventDefault();
		var quantity = $(this).val();
		var rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
		$(this).closest('tr').children('td.amount').children('span.amountText').text(quantity*rate);
		$(this).closest('tr').children('td.amount').children('input.amountVal').val(quantity*rate);
		var sub_total = 0; 
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	});
	$('.field_wrapper').on('keyup change', '.rateVal', function(e){
		e.preventDefault();
		var rate = $(this).val();
		var quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
		$(this).closest('tr').children('td.amount').children('span.amountText').text(quantity*rate);
		$(this).closest('tr').children('td.amount').children('input.amountVal').val(quantity*rate);
		var sub_total = 0; 
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	});
	var tableRow = 1;
	$('.field_wrapper').on('click', '.remove_button', function(e){
		e.preventDefault();
		$(this).closest("tr").remove();
		x--;
		var sub_total = 0; 
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
		tableRow = $("#itemTable tr").length-1;
		if(tableRow == 1){
			$(".noitems").show();
		}
	}); 
});
</script>
<script>
var maxField = 101; //Input fields increment limitation
var x = 1; //Initial field counter is 1
var quantity = '5';
function getCategory(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax')}}",
	data:'company_id='+val,
	success: function(data){
		$("#category").html(data);
		$("#item").html('<option value="">Select</option>');
	}
	});
}
function getItems(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax2')}}",
	data:'category_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function getItemsbyBrand(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax5')}}",
	data:'brand_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function in_array(array, id) {
    for(var i=0;i<array.length;i++) {
       if(array[i] == id)
        return true;
    }
    return false;
}
function addItems(val){
	$(".noitems").hide();
	var itemStr = $("input[name='item_id[]']").map(function(){return $(this).val();}).get();
	var itemArr = JSON.parse("["+ itemStr +"]");
	var itemAlreadyAdded = in_array(itemArr, val);
	if(itemAlreadyAdded){
		$("#flashmessage").append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> This item is already added!</div>');
		setTimeout(function(){ $('.alert.alert-danger').fadeOut(300)}, 3000);
		return false;
	}
	if(val != ''){
		$.ajax({
			type: "GET",
			url: "{{url('/purchaseorder/additems')}}",
			data:'item_id='+val,
			success: function(data){
				var obj = JSON.parse(data);
				var item_id = obj[0][0].id;
				var item_name = obj[0][0].name;
				var brand_name = obj[0][0].brand_name;
				var strength = obj[0][0].strength;
				var form_id = obj[0][0].form_id;
				var form_name = obj[0][0].form_name;
				var unit_id = obj[0][0].unit_id;
				var unit_name = obj[0][0].unit_name;
				var rate = obj[0][0].price;

				var fieldHTML = '<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" value="'+item_id+'"></td><td class="center"><span>'+strength+'</span><input type="hidden" name="strength[]" class="strength" value="'+strength+'"></td><td class="center"><span>'+unit_name+'</span><input type="hidden" name="unit_id[]" value="'+unit_id+'"></td><td class="center"><span>'+brand_name+'</span></td><td class="center"><span>'+form_name+'</span><input type="hidden" name="form_id[]" class="form_id" value="'+form_id+'"></td><td class="center qty" style="max-width: 50px"><input class="form-control qtyVal" type="number" name="quantity[]" min="1"></td><td class="center rate" style="max-width: 50px"><input class="form-control rateVal" type="number" name="rate[]" value="'+rate+'" min="0" step="0.01"></td><td class="center amount"><span class="amountText">0</span><input class="amountVal" type="hidden" name="amount[]"></td><td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>';
				if(x < maxField){
					x++;
					$('.field_wrapper').prepend(fieldHTML);
				}
			}
		});
	}
}
</script>
<div class="pageheader">
		<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="media-body">
    	<h4 class="test">Purchase Order</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/purchaseorder') }}">Purchase Order</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
	  <div class="col-md-12" id="flashmessage">
	  	@include('flashmessage')
	  </div>
	</div>
  	<form method="post" action="{{ url('/purchaseorder/update/'.$purchaseorder[0]->id) }}">
  	{{ csrf_field() }}
  	<div class="row">
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>P.O No:</label>
			  <input type="text" class="form-control" name="po_no" id="po_no" value="{{$purchaseorder_no}}" disabled="">
			</div>
  		</div>
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>*Date:</label>
			  <div class="input-group date">
				<input type="text" name="entry_date" id="entry_date" value="{{date_dfy($entry_date)}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="form-group">
			  <label>*Vendor Name:</label>
			  <select style="border: none; padding:0px" class="form-control" name="vendor" id="vendor" required="">
			  		@foreach ($vendor as $value)
			  		<option value="<?php echo $value->id; ?>" <?php if($vendor_id==$value->id){ ?> selected="" <?php }?>><?php echo $value->name.' - '.$value->mobile; ?></option>
			  		@endforeach
			  </select>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-4">
  			<div class="form-group">
			  <label>Category:</label>
			  <select style="border: none; padding:0px" class="form-control" name="category" id="category">
			  		<option value="">Select</option>
			  		@foreach ($category as $value)
			  		<option value="{{ $value->id }}">{{ $value->name }}</option>
			  		@endforeach
			  </select>
			</div>
  		</div>
		<div class="col-md-4">
		  	<div class="form-group">
			  <label>Brand:</label>
			  <select style="border: none; padding: 0px" class="form-control basic-select" name="brand" id="brand" onChange="getItemsbyBrand(this.value);">
			  		<option value="">Select</option>
					@foreach ($brand as $value)
		      		@if (old('brand') == $value->id)
					      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
					@else
					      <option value="{{ $value->id }}">{{ $value->name }}</option>
					@endif
		      		@endforeach
			  </select>
			</div>
  		</div>
  		<div class="col-md-4">
  			<div class="form-group">
			  <label>Item:</label>
			  <select style="border: none; padding: 0px" class="form-control" name="item" id="item" onChange="addItems(this.value);">
			  		<option value="">Select</option>
			  </select>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="table-responsive">
				<table class="table table-bordered" id="itemTable">
					<tr>
		                <th>Name</th>
		                <th>Strength</th>
		                <th>Unit</th>
						<th>Brand</th>
		                <th>Form</th>
		                <th style="max-width: 50px">Quantity</th>
		                <th>Cost</th>
		                <th>Total</th>
		                <th>Action</th>
		            </tr>
					<tbody class="field_wrapper">
						@if(count($purchaseorder_item)==0)
						<tr>
							<td class="noitems" colspan="7">No items added yet!</td>
						</tr>
						@else
						@foreach($purchaseorder_item as $value)
						<tr><td class="center"><span>{{$value->item_name}}</span><input type="hidden" name="item_id[]" value="{{$value->item_id}}"></td><td class="center"><span>{{$value->strength}}</span><input type="hidden" name="strength[]" value="{{$value->strength}}"></td><td class="center"><span>{{$value->unit_name}}</span></td><td class="center"><span>{{$value->brand_name}}</span></td><td class="center"><span>{{$value->form_name}}</span><input type="hidden" name="form_id[]" value="{{$value->form_id}}"></td><td class="center qty" style="max-width: 50px"><input class="form-control qtyVal" type="number" name="quantity[]" value="{{$value->quantity}}" min="1"></td><td class="center rate" style="max-width: 60px"><input class="form-control rateVal" type="number" name="rate[]" value="{{$value->rate}}" min="0" step="0.01"></td><td class="center amount"><span class="amountText">{{$value->amount}}</span><input class="amountVal" type="hidden" name="amount[]" value="{{$value->amount}}"></td><td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>
						@endforeach
						<tr>
							<td colspan="8" class="right-align">Sub-Total:</td>
							<td class="left-align subtotalText"><b>{{$sub_total}}</b></td>
			            </tr>
						@endif
					</tbody>
				</table>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="form-group">
			  <label>Remarks:</label>
			  <input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{$remarks}}">
			</div>
  		</div>
  	</div>
	<button type="submit" class="btn btn-primary">Update</button>
	</form>
</div>
</div>
@endsection