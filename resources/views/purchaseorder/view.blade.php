@extends('layouts.website') @section('content')
<script>
	function printPage() {
		document.title = "Purchase Order";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-shopping-cart"></i>
		</div>
		<div class="media-body">
			<h4>Purchase Order</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/purchaseorder') }}">Purchase Order</a>
				</li>
				<li>View</li>
				<li>{{Request::segment(3)}}</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-9">
				<h4 class="center onlyprint">
					<u>{{$company->name}}</u>
				</h4>
				<h5 class="center onlyprint">{{$company->address}}</h5>
				<h5 class="center onlyprint"><b>GSTIN:</b> {{$company->gstin}}</h5>
				@if(!empty($company->phone) && !empty($company->mobile) )
				<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}, <b>Mobile:</b> {{$company->mobile}}</h5>
				@else
					@if(!empty($company->phone))
					<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}</h5>
					@else
					<h5 class="center onlyprint"><b>Mobile:</b> {{$company->mobile}}</h5>
					@endif
				@endif
				<table class="table table-bordered">
					<tr>
						<th class="left-align">Vendor Details</th>
						<th class="left-align">P.O Details</th>
					</tr>
					<tr>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Vendor Name:</b> {{$purchaseorder[0]->vendor_name}}</p>
							<p><b>Phone:</b> {{$purchaseorder[0]->vendor_mobile}}</p>
							<p><b>GSTIN:</b> {{$purchaseorder[0]->vendor_gstin}}</p>
							<p><b>State Code:</b> {{substr($purchaseorder[0]->vendor_gstin, 0, 2)}}</p>
							<p><b>Address:</b> {{$purchaseorder[0]->vendor_address}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>P.O No:</b> {{$purchaseorder[0]->purchaseorder_no}}</p>
							<p><b>P.O Date:</b> {{date_dfy($purchaseorder[0]->entry_date)}}</p>
							<p><b>Remarks:</b> {{$purchaseorder[0]->remarks}}</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="table-responsive">
					<table class="table table-bordered mb30">
						<tbody>
							<tr>
								<td width="50px" class="left-align">
									<b>S.No</b>
								</td>
								<td class="left-align">
									<b>Name</b>
								</td>
								<td class="left-align">
									<b>HSN Code</b>
								</td>
								<td class="left-align">
									<b>Pcs</b>
								</td>
								<td class="left-align">
									<b>Quantity</b>
								</td>
								<td class="left-align">
									<b>Cost</b>
								</td>
								<td class="left-align">
									<b>Amount</b>
								</td>
							</tr>
							<?php $i=1; foreach ($purchaseorder_item as $value){ ?>
							<tr>
								<td class="left-align">{{ $i }}</td>
								<td class="left-align">{{ $value->item_name.purchaseorder_qty($value->parent_id,$value->item_id) }}</td>
								<!-- <td class="left-align">{{ $value->item_name }}</td> -->
								<td class="left-align">{{ $value->hsn_code }}</td>
								<td class="left-align">{{ $value->pcs_total }}</td>
								<td class="left-align">{{ $value->quantity_total }}</td>
								<td class="left-align">{{ $value->rate }}</td>
								<td class="left-align">{{ $value->quantity_total*$value->rate }}</td>
							</tr>
							<?php $i++; }?>
							<tr>
								<td colspan="6" class="right-align">Sub-Total:</td>
								<td class="left-align">
									<b>{{$purchaseorder[0]->sub_total}}</b>
								</td>
							</tr>
							@if($purchaseorder[0]->cgst > 0)
							<tr>
								<td colspan="6" class="right-align">CGST:</td>
								<td class="left-align">
									<b>{{$purchaseorder[0]->cgst}}</b>
								</td>
							</tr>
							@endif
							@if($purchaseorder[0]->sgst > 0)
							<tr>
								<td colspan="6" class="right-align">SGST:</td>
								<td class="left-align">
									<b>{{$purchaseorder[0]->sgst}}</b>
								</td>
							</tr>
							@endif
							@if($purchaseorder[0]->igst > 0)
							<tr>
								<td colspan="6" class="right-align">IGST:</td>
								<td class="left-align">
									<b>{{$purchaseorder[0]->igst}}</b>
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="6" class="right-align">Total:</td>
								<td class="left-align">
									<b>{{$purchaseorder[0]->total}}</b>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection