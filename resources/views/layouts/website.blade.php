<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Admin Panel</title>
        <link href="{{ url('/css/style.default.css') }}" rel="stylesheet">
        <link href="{{ url('/css/style.print.css') }}" rel="stylesheet">
        <link href="{{ url('/css/select2.css') }}" rel="stylesheet">
        <link href="{{ url('/css/morris.css') }}" rel="stylesheet">
        <link href="{{ url('/css/select2.css') }}" rel="stylesheet" />
        <link href="{{ url('/css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('/datepicker/bootstrap-datepicker3.min.css') }}" rel="stylesheet"/>
        <script src="{{ url('/js/jquery-1.11.1.min.js') }}"></script>
    </head>
    <body>
        <header>
            <div class="headerwrapper">
                <div class="header-left">
                    <a style="color: #fff;font-size: 18px; margin-top: 2px" href="#" class="logo">Admin Panel</a>
                    <div class="pull-right">
                        <a href="" class="menu-collapse" style="border: none">
                            <i style="font-size: 17px" class="fa fa-bars"></i>
                        </a>
                    </div>
                </div>
                <div class="header-right">
                    <div class="pull-right">
                        <div class="btn-group btn-group-list btn-group-messages">
                        	<?php
                                $company = Auth::user()->company_id;
                                $low_stock = low_stock($company);
                                $lowstock_count = count($low_stock);
                                $limit = $lowstock_count;
                                if($lowstock_count>5){
									$limit = 5;
								}
                            ?>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-bell"></i>
                                <span class="badge">@if($lowstock_count>0) {{$lowstock_count}} @endif</span>
                            </button>
                            <div class="dropdown-menu pull-right">
                                <ul class="media-list dropdown-list">
                                    <li class="media">
                                        <div class="media-body">
                                          <strong>{{$lowstock_count}} Products are running out of stock</strong>
                                        </div>
                                    </li>
                                    @if($low_stock)
                                    @for($i=0; $i<$limit;$i++)
                                    <li class="media">
                                        <span class="badge badge-warning">Qty: {{$low_stock[$i]->in_stock}}</span>
                                        <div class="media-body">
                                          <p>{{$low_stock[$i]->name}}</p>
                                        </div>
                                    </li>
                                    @endfor
                                    @endif
                                </ul>
                                <div class="dropdown-footer text-center">
                                    <a href="{{ url('/notification') }}" class="link">See All Notifications</a>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group btn-group-option">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-user"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                              <li><a href="{{ url('/editprofile') }}"><i class="glyphicon glyphicon-user"></i> Edit Profile</a></li>
                              <li><a href="{{ url('/changepassword') }}"><i class="glyphicon glyphicon-cog"></i> Change Password</a></li>
                              <li class="divider"></li>
                              <li><a href="{{ url('/logout') }}"><i class="glyphicon glyphicon-log-out"></i>Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section>
            <div class="mainwrapper">
                <div class="leftpanel">
                    <div class="media profile-left">
                        <a class="pull-left profile-thumb" href="{{url('/')}}">
                        	<?php if(Auth::user()->image != ''){ ?>
					          <img class="img-circle" src="{{url('/images/profile/'.Auth::user()->image)}}" alt="">
					        <?php } else{ ?>
					          <img class="img-circle" src="{{url('/images/default_thumb.png')}}" alt="">
					        <?php }?>
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">{{Auth::user()->name}}</h4>
                            <small class="text-muted">
                            @if(Auth::user()->user_group == '1')
                            Super Administrator
                            @elseif(Auth::user()->user_group == '2')
                            Administrator
                            @else
                            Manager
                            @endif
                            </small>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-stacked">
                        @if(Auth::user()->user_group == '1')
                        <li><a href="{{ url('') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li><a href="{{ url('/company') }}"><i class="fa fa-building-o"></i> <span>Profiles</span></a></li>
                        <li><a href="{{ url('/administrators') }}"><i class="fa fa-user"></i> <span>Companies</span></a></li>
                        @endif
                        @if(Auth::user()->user_group == '2' || Auth::user()->user_group == '4')
                        <li><a href="{{ url('') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li><a href="{{ url('/users') }}"><i class="fa fa-users"></i> <span>Users</span></a></li>
                        @if(session('superadmin') == '1' || Auth::user()->user_group == '2')
                        <li class="parent"><a href=""><i class="fa fa-info"></i> <span>Masters</span></a>
		                    <ul class="children">
		                        <li><a href="{{ url('/item') }}">Items</a></li>
		                        <li><a href="{{ url('/category') }}">Categories</a></li>
		                        <li><a href="{{ url('/brand') }}">Brands</a></li>
		                        <!--<li><a href="{{ url('/manufacturer') }}">Manufacturers</a></li>-->
		                        <li><a href="{{ url('/form') }}">Forms</a></li>
		                        <li><a href="{{ url('/unit') }}">Units</a></li>
		                        <li><a href="{{ url('/tax') }}">Taxes</a></li>
		                        <li><a href="{{ url('/openingstock') }}">Opening Stocks</a></li>
                                <li><a href="{{ url('/actualstock') }}">Actual Stocks</a></li>
                                <li><a href="{{ url('/transport') }}">Transports</a></li>
		                    </ul>
		                </li>
                        @endif
		                <li><a href="{{ url('/customer') }}"><i class="fa fa-user"></i> <span>Customers</span></a></li>
		                <li><a href="{{ url('/supplier') }}"><i class="fa fa-user"></i> <span>Suppliers</span></a></li>
                        @if(session('superadmin') == '1' || Auth::user()->user_group == '2' || Auth::user()->user_group == '4')
		                <li class="parent"><a href=""><i class="fa fa-shopping-cart"></i> <span>Purchase</span></a>
		                    <ul class="children">
		                        <li><a href="{{ url('/purchaseorder') }}">Purchase Order</a></li>
		                        <li><a href="{{ url('/purchaseregister') }}">Purchase Register</a></li>
		                        <li><a href="{{ url('/purchasereturn') }}">Purchase Return</a></li>
                                <li><a href="{{ url('/lr') }}">L.R</a></li>
		                    </ul>
		                </li>
                        @endif
		                <li class="parent"><a href=""><i class="fa fa-exchange"></i> <span>Sales</span></a>
		                    <ul class="children">
		                        <li><a href="{{ url('/saleregister') }}">Sales Register</a></li>
		                        <li><a href="{{ url('/salereturn') }}">Sale Return</a></li>
		                    </ul>
                        </li>
                        <li><a href="{{ url('/voucher') }}"><i class="fa fa-file-text-o"></i> <span>Vouchers</span></a></li>
		                <li class="parent"><a href=""><i class="fa fa-file"></i> <span>Reports</span></a>
		                    <ul class="children">
		                        <li><a href="{{ url('/reports/sales') }}">Sales Report</a></li>
                                <li><a href="{{ url('/reports/sales-individual') }}">Individual Sales Report</a></li>
		                        <li><a href="{{ url('/reports/sales-return') }}">Sales Return Report</a></li>
                                <!-- <li><a href="{{ url('/reports/purchase-order') }}">Purchase Order Report</a></li> -->
		                        <li><a href="{{ url('/reports/purchase') }}">Received Purchases</a></li>
		                        <li><a href="{{ url('/reports/purchase-return') }}">Purchase Return Report</a></li>
		                        <li><a href="{{ url('/reports/stock-summary') }}">Stock Summary Report</a></li>
                                <li><a href="{{ url('/reports/discrepancy') }}">Discrepancy Report</a></li>
		                        <!--<li><a href="{{ url('/reports/stock-ledger') }}">Stock Ledger Report</a></li>-->
		                        <li><a href="{{ url('/reports/expiry') }}">Expiry Report</a></li>
		                        <li><a href="{{ url('/reports/price-variation') }}">Price Variation Report</a></li>
		                        <li><a href="{{ url('/reports/debt') }}">Debt Report</a></li>
		                        <li><a href="{{ url('/reports/cash') }}">Cash Report</a></li>
		                    </ul>
		                </li>
                        @endif
                        @if(Auth::user()->user_group == '3')
                        <li><a href="{{ url('') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                        <li><a href="{{ url('/customer') }}"><i class="fa fa-user"></i> <span>Customers</span></a></li>
                        <li class="parent"><a href=""><i class="fa fa-exchange"></i> <span>Sales</span></a>
		                    <ul class="children">
		                        <li><a href="{{ url('/saleregister') }}">Sales Register</a></li>
		                        <li><a href="{{ url('/salereturn') }}">Sale Return</a></li>
		                    </ul>
		                </li>
		                <!--<li class="parent"><a href=""><i class="fa fa-database"></i> <span>Stock</span></a>
		                    <ul class="children">
		                        <li><a href="{{ url('/stockledger') }}">Stock Ledger</a></li>
		                        <li><a href="{{ url('/stocksummary') }}">Stock Summary</a></li>
		                        <li><a href="{{ url('/stockregister') }}">Stock Register</a></li>
		                    </ul>
		                </li>-->
		                <li class="parent"><a href=""><i class="fa fa-database"></i> <span>Reports</span></a>
		                    <ul class="children">
		                        <li><a href="{{ url('/reports/stock-summary') }}">Stock Summary Report</a></li>
                                <li><a href="{{ url('/reports/discrepancy') }}">Discrepancy Report</a></li>
		                        <!--<li><a href="{{ url('/reports/stock-ledger') }}">Stock Ledger Report</a></li>-->
		                        <li><a href="{{ url('/reports/expiry') }}">Expiry Report</a></li>
		                        <li><a href="{{ url('/reports/debt') }}">Debt Report</a></li>
		                        <li><a href="{{ url('/reports/cash') }}">Cash Report</a></li>
		                    </ul>
		                </li>
                        @endif
                    </ul>
                </div>
                <div class="mainpanel">
                    @yield('content')
                </div>
            </div>
        </section>
        <script src="{{ url('/datepicker/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ url('/js/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ url('/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('/js/jquery.dataTables.min.js') }}"></script>
		<script src="{{ url('/js/dataTables.bootstrap.min.js') }}"></script>
		<script src="{{ url('/js/select2.min.js') }}"></script>
        <script src="{{ url('/js/modernizr.min.js') }}"></script>
        <script src="{{ url('/js/retina.min.js') }}"></script>
        <script src="{{ url('/js/jquery.cookies.js') }}"></script>
        <script src="{{ url('/js/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ url('/js/flot/jquery.flot.resize.min.js') }}"></script>
        <script src="{{ url('/js/flot/jquery.flot.spline.min.js') }}"></script>
        <script src="{{ url('/js/jquery.sparkline.min.js') }}"></script>
        <script src="{{ url('/js/morris.min.js') }}"></script>
        <script src="{{ url('/js/raphael-2.1.0.min.js') }}"></script>
        <script src="{{ url('/js/bootstrap-wizard.min.js') }}"></script>
        <script src="{{ url('/js/select2.min.js') }}"></script>
        <script src="{{ url('/js/custom.js') }}"></script>
        <script src="{{ url('/js/dashboard.js') }}"></script>
    </body>
</html>