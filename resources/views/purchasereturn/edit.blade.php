@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
?>
<script type='text/javascript'>
$(function(){
	// Select 2 Dropdown initialization
	$("#item").select2();
	// Upper case
	$('#purchasevoucher_no').on('keyup change', function(e){
		e.preventDefault();
		var text = $(this).val();
		$('#purchasevoucher_no').val(text.toUpperCase());
	});
	// Input Type Date
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	//Item Table
	var return_quantity = 0;
	var rate = 0;
	$('.field_wrapper').on('keyup change', '.return_qtyVal', function(e){
		e.preventDefault();
		return_quantity = $(this).val();
		rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
		$(this).closest('tr').children('td.amount').children('input.return_amountVal').val(parseFloat(return_quantity*rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('span.return_amountText').text(parseFloat(return_quantity*rate).toFixed(2));
		var sub_total = 0;
	    $('.return_amountVal').each(function () {
	    	sub_total = sub_total + parseFloat($(this).val());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	});
});
</script>
<div class="pageheader">
		<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <div class="media-body">
    	<h4 class="test">Purchase Return</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/purchasereturn') }}">Purchase Return</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
	  <div class="col-md-10" id="flashmessage">
	  	@include('flashmessage')
	  </div>
	</div>
  	<div class="row">
  		<div class="col-md-10">
  			<form method="post" action="{{ url('/purchasereturn/update/'.$purchasereturn[0]->id) }}">
		  	{{ csrf_field() }}
		  	<div class="row">
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>Debit Note No:</label>
					  <input type="text" class="form-control" value="{{$purchasereturn[0]->debitnote_no}}" min="1" disabled="">
					</div>
		  		</div>
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>*Debit Note Date:</label>
					  <div class="input-group date">
						<input type="text" name="debitnote_date" id="debitnote_date" value="{{date_dfy($purchasereturn[0]->debitnote_date)}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					  </div>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-6">
					<div class="form-group">
					  <label>Vendor Name:</label>
					  <input type="text" class="form-control" name="vendor" id="vendor" value="{{$purchasereturn[0]->vendor_name}}" disabled="">
					</div>
				</div>
		  		<div class="col-md-6">
				    <div class="form-group">
					  <label>Purchase Voucher Number:</label>
					  <input type="text" class="form-control" name="purchasevoucher_no" id="purchasevoucher_no" value="{{$purchasereturn[0]->purchasevoucher_no}}" disabled="">
					</div>
				</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="table-responsive">
						<table class="table table-bordered" id="itemTable2">
							<tr>
				                <th>Name</th>
				                <th>Strength</th>
				                <th>Unit</th>
				                <th>Form</th>
				                <th>Barcode</th>
				                <th>Expiry Date</th>
				                <th>Quantity</th>
				                <th>Return Quantity</th>
				                <th>Cost</th>
				                <th>Return Total</th>
				            </tr>
							<tbody class="field_wrapper">
								@foreach($purchasereturn_item as $value)
								<tr><td class="center"><span>{{$value->item_name}}</span><input type="hidden" name="item_id[]" value="{{$value->item_id}}"></td><td class="center"><span>{{$value->strength}}</span><input type="hidden" name="strength[]" value="{{$value->strength}}"></td><td class="center"><span>{{$value->unit_name}}</span></td><td class="center"><span>{{$value->form_name}}</span><input type="hidden" name="form_id[]" value="{{$value->form_id}}"></td>
								<td class="center"><span>{{$value->barcode}}</span><input type="hidden" name="barcode[]" value="{{$value->barcode}}"></td>
								<td class="center"><span>{{date_dfy($value->expiry_date)}}</span><input type="hidden" name="expiry_date[]" value="{{date_dfy($value->expiry_date)}}"></td>
								<td class="center"><span>{{$value->quantity}}</span><input type="hidden" name="quantity[]" value="{{$value->quantity}}"></td><td class="center return_qty" style="max-width: 50px"><input class="form-control return_qtyVal" type="number" name="return_quantity[]" value="{{$value->return_quantity}}" min="0"></td><td class="center rate"><span>{{$value->rate}}</span><input class="rateVal" type="hidden" name="rate[]" value="{{$value->rate}}"></td><td class="center amount"><span class="return_amountText">{{$value->return_amount}}</span><input class="amountVal" type="hidden" name="amount[]" value="{{$value->amount}}"><input class="return_amountVal" type="hidden" name="return_amount[]" value="{{$value->return_amount}}"></td></tr>
								@endforeach
								<tr><td colspan="9" class="right-align">Sub-Total:</td><td class="center-align subtotalText"><b>{{$purchasereturn[0]->sub_total}}</b></td></tr>
							</tbody>
						</table>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="form-group">
					  <label>Remarks:</label>
					  <input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{$purchasereturn[0]->remarks}}">
					</div>
		  		</div>
		  	</div>
			<button type="submit" class="btn btn-primary">Save</button>
			</form>
  		</div>
  	</div>
</div>
</div>
@endsection