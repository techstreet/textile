@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
	    @if(Auth::user()->user_group != '4')
		<div class="pull-right">
			<a href="{{ url('/purchasereturn/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add New</button></a>
		</div>
		@endif
		<div class="pageicon pull-left">
            <i class="fa fa-shopping-cart"></i>
        </div>
		<div class="media-body">
		<h4>Purchase Return</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Purchase Return</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Purchase Voucher No</th>
		        <th>Debit Note No</th>
		        <th>Debit Note Date</th>
		        <th>Vendor Name</th>
		        <th width="150px" style="min-width: 150px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($purchasereturn as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->purchasevoucher_no }}</td>
	            <td>{{ $value->debitnote_no }}</td>
	            <td>{{ date_dfy($value->debitnote_date) }}</td>
	            <td>{{ $value->vendor_name }}</td>
	            
	            <td>
	            <a href="{{ url('/purchasereturn/view/'.$value->id) }}" data-toggle="tooltip" title="View" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a>
	            @if(session('superadmin') == '1')
	            <!-- <a href="{{ url('/purchasereturn/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a> -->
							
							<a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/purchasereturn/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
							@endif	
						</td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    	ordering: false
    });
});
</script>
@endsection