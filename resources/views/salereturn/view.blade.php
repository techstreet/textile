@extends('layouts.website')
@section('content')
<script>
	function printPage() {
	    document.title = "Sale Return";
	    window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
		<div class="media-body">
		<h4>Sale Return</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li><a href="{{ url('/salereturn') }}">Sale Return</a></li>
		    <li>View</li>
		    <li>{{Request::segment(3)}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-8">
		  <h4 class="center onlyprint">
				<u>{{$company->name}}</u>
			</h4>
			<h5 class="center onlyprint">{{$company->address}}</h5>
			<h5 class="center onlyprint"><b>GSTIN:</b> {{$company->gstin}}</h5>
			@if(!empty($company->phone) && !empty($company->mobile) )
			<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}, <b>Mobile:</b> {{$company->mobile}}</h5>
			@else
				@if(!empty($company->phone))
				<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}</h5>
				@else
				<h5 class="center onlyprint"><b>Mobile:</b> {{$company->mobile}}</h5>
				@endif
			@endif
			<table class="table table-bordered">
					<tr>
						<th class="left-align">Client Details</th>
						<th class="left-align">Invoice Details</th>
						<th class="left-align">Transport Details</th>
					</tr>
					<tr>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Client Name:</b> {{$salereturn[0]->client_name}}</p>
							<p><b>Phone:</b> {{$salereturn[0]->client_mobile}}</p>
							<p><b>GSTIN:</b> {{$salereturn[0]->client_gstin}}</p>
							<p><b>State Code:</b> {{substr($salereturn[0]->client_gstin, 0, 2)}}</p>
							<p><b>Address:</b> {{$salereturn[0]->client_address}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Credit Note No:</b> {{$salereturn[0]->creditnote_no}}</p>
							<p><b>Credit Note Date:</b> {{date_dfy($salereturn[0]->creditnote_date)}}</p>
							<p><b>Invoice No:</b> {{$salereturn[0]->invoice_no}}</p>
							<p><b>Invoice Date:</b> {{date_dfy($salereturn[0]->invoice_date)}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Transport:</b> {{$salereturn[0]->transport_name}}</p>
							<p><b>GR No:</b> {{$salereturn[0]->gr_no}}</p>
							<p><b>Booking Date:</b> {{date_dfy($salereturn[0]->booking_date)}}</p>
						</td>
					</tr>
			</table>
      </div>
	</div>
	<div class="row">
	  <div class="col-md-8">
		<div class="table-responsive">
	          <table class="table table-bordered mb30">
	            <tbody>
	              <tr>
	                <td width="50px" class="left-align"><b>S.No</b></td>
	                <td class="left-align"><b>Name</b></td>
	                <td class="left-align"><b>Unit</b></td>
	                <td class="left-align"><b>HSN Code</b></td>
	                <td class="left-align"><b>Order Quantity</b></td>
	                <td class="left-align"><b>Return Quantity</b></td>
	                <td class="left-align"><b>Cost</b></td>
	                <td class="left-align"><b>Return Amount</b></td>
	              </tr>
	              <?php $i=1; foreach ($salereturn_item as $value){ ?>
	              <tr>
	                <td class="left-align">{{ $i }}</td>
	                <td class="left-align">{{ $value->reference }}</td>
	                <td class="left-align">{{ $value->unit_name }}</td>
	                <td class="left-align">{{ $value->hsn_code }}</td>
	                <td class="left-align">{{ $value->quantity }}</td>
	                <td class="left-align">{{ $value->return_quantity }}</td>
	                <td class="left-align">{{ $value->rate }}</td>
	                <td class="left-align">{{ $value->return_amount }}</td>
	              </tr>
	              <?php $i++; }?>
					<tr>
						<td colspan="7" class="right-align">Sub-Total:</td>
						<td class="left-align">
							<b>{{$salereturn[0]->sub_total}}</b>
						</td>
					</tr>
					@if($salereturn[0]->discount > 0)
					<tr>
						<td colspan="7" class="right-align">Discount:</td>
						<td class="left-align">
							<b>{{$salereturn[0]->discount}}</b>
						</td>
					</tr>
					@endif
					@if($salereturn[0]->other_expense > 0)
					<tr>
						<td colspan="7" class="right-align">Other Expense:</td>
						<td class="left-align">
							<b>{{$salereturn[0]->other_expense}}</b>
						</td>
					</tr>
					@endif
					@if($salereturn[0]->cgst > 0)
					<tr>
						<td colspan="7" class="right-align">CGST:</td>
						<td class="left-align">
							<b>{{$salereturn[0]->cgst}}</b>
						</td>
					</tr>
					@endif
					@if($salereturn[0]->sgst > 0)
					<tr>
						<td colspan="7" class="right-align">SGST:</td>
						<td class="left-align">
							<b>{{$salereturn[0]->sgst}}</b>
						</td>
					</tr>
					@endif
					@if($salereturn[0]->igst > 0)
					<tr>
						<td colspan="7" class="right-align">IGST:</td>
						<td class="left-align">
							<b>{{$salereturn[0]->igst}}</b>
						</td>
					</tr>
					@endif
					<tr>
						<td colspan="7" class="right-align">Total:</td>
						<td class="left-align">
							<b>{{$salereturn[0]->total}}</b>
						</td>
					</tr>
	            </tbody>
	          </table>
          </div>
	  </div>
	</div>
</div>
</div>
@endsection