@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
	    @if(Auth::user()->user_group != '4')
		<div class="pull-right">
			<a href="{{ url('/salereturn/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add New</button></a>
		</div>
		@endif
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
		<div class="media-body">
		<h4>Sale Return</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Sale Return</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Invoice No</th>
		        <th>Credit Note No</th>
		        <th>Credit Note Date</th>
		        <th>Client Name</th>
		        <th width="150px" style="min-width: 150px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($salereturn as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->invoice_no }}</td>
	            <td>{{ $value->creditnote_no }}</td>
	            <td>{{ date_dfy($value->creditnote_date) }}</td>
	            <td>{{ $value->client_name }}</td>
	            
	            <td>
	            <a href="{{ url('/salereturn/view/'.$value->id) }}" data-toggle="tooltip" title="View" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a>
	            @if(Auth::user()->user_group != '4')
	            <!-- <a href="{{ url('/salereturn/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a> -->
				@endif			
							@if(session('superadmin') == '1')
							<a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/salereturn/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
							@endif	
						</td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    	ordering: false
    });
});
</script>
@endsection