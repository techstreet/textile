@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
?>
<script type='text/javascript'>
$(function(){
	// Select 2 Dropdown initialization
	$("#item").select2();
	// Upper case
	$('#invoice_no').on('keyup change', function(e){
		e.preventDefault();
		var text = $(this).val();
		$('#invoice_no').val(text.toUpperCase());
	});
	// Input Type Date
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	//Item Table
	var return_quantity = 0;
	var rate = 0;
	$('.field_wrapper').on('keyup change', '.return_qtyVal', function(e){
		e.preventDefault();
		return_quantity = $(this).val();
		rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
		$(this).closest('tr').children('td.amount').children('input.return_amountVal').val(parseFloat(return_quantity*rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('span.return_amountText').text(parseFloat(return_quantity*rate).toFixed(2));
		var sub_total = 0;
	    $('.return_amountVal').each(function () {
	    	sub_total = sub_total + parseFloat($(this).val());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	});
});
</script>
<div class="pageheader">
		<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
        <div class="media-body">
    	<h4 class="test">Sale Return</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/salereturn') }}">Sale Return</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
	  <div class="col-md-10" id="flashmessage">
	  	@include('flashmessage')
	  </div>
	</div>
  	<div class="row">
  		<div class="col-md-10">
  			<form method="post" action="{{ url('/salereturn/update/'.$salereturn[0]->id) }}">
		  	{{ csrf_field() }}
		  	<div class="row">
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>Credit Note No:</label>
					  <input type="text" class="form-control" name="creditnote_no" id="creditnote_no" value="{{$salereturn[0]->creditnote_no}}" disabled="">
					</div>
		  		</div>
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>*Credit Note Date:</label>
					  <div class="input-group date">
						<input type="text" name="creditnote_date" id="creditnote_date" value="{{date_dfy($salereturn[0]->creditnote_date)}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					  </div>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-6">
					<div class="form-group">
					  <label>Client Name:</label>
					  <input type="text" class="form-control" name="client" id="client" value="{{$salereturn[0]->client_name}}" disabled="">
					</div>
				</div>
		  		<div class="col-md-6">
				    <div class="form-group">
					  <label>Invoice Number:</label>
					  <input type="text" class="form-control" name="invoice_no" id="invoice_no" value="{{$salereturn[0]->invoice_no}}" disabled="">
					</div>
				</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="table-responsive">
						<table class="table table-bordered" id="itemTable2">
							<tr>
				                <th>Name</th>
				                <th>Strength</th>
				                <th>Unit</th>
				                <th>Form</th>
				                <th style="max-width: 50px">Quantity</th>
				                <th style="max-width: 50px">Return Quantity</th>
				                <th>Cost</th>
				                <th>Return Total</th>
				            </tr>
							<tbody class="field_wrapper">
								@foreach($salereturn_item as $value)
								<tr><td class="center"><span>{{$value->item_name}}</span><input type="hidden" name="item_id[]" value="{{$value->item_id}}"></td><td class="center"><span>{{$value->strength}}</span><input type="hidden" name="strength[]" value="{{$value->strength}}"></td><td class="center"><span>{{$value->unit_name}}</span></td><td class="center"><span>{{$value->form_name}}</span><input type="hidden" name="form_id[]" value="{{$value->form_id}}"></td></td><td class="center"><span>{{$value->quantity}}</span><input type="hidden" name="quantity[]" value="{{$value->quantity}}"></td><td class="center return_qty" style="max-width: 60px"><input class="form-control return_qtyVal" type="number" name="return_quantity[]" value="{{$value->return_quantity}}" min="0"></td><td class="center rate"><span>{{$value->rate}}</span><input class="rateVal" type="hidden" name="rate[]" value="{{$value->rate}}"></td><td class="center amount"><span class="return_amountText">{{$value->return_amount}}</span><input class="amountVal" type="hidden" name="amount[]" value="{{$value->amount}}"><input class="return_amountVal" type="hidden" name="return_amount[]" value="{{$value->return_amount}}"></td></tr>
								@endforeach
								<tr><td colspan="7" class="right-align">Return Amount:</td><td class="center-align subtotalText"><b>{{$salereturn[0]->sub_total}}</b></td></tr>
							</tbody>
						</table>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="form-group">
					  <label>Reason For Return:</label>
					  <textarea class="form-control" placeholder="Enter reason" name="return_reason" id="return_reason">{{$salereturn[0]->return_reason}}</textarea>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="form-group">
					  <label>Remarks:</label>
					  <input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{$salereturn[0]->remarks}}">
					</div>
		  		</div>
		  	</div>
			<button type="submit" class="btn btn-primary">Save</button>
			</form>
  		</div>
  	</div>
</div>
</div>
@endsection