@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> 
<script src="{{ url('/js/mycustom.js') }}"></script>
<script type='text/javascript'>
$(function(){
	// Select 2 Dropdown initialization
	$("#item").select2();
	$("#transport").select2();
	// Upper case
	$('#invoice_no').on('keyup change', function(e){
		e.preventDefault();
		var text = $(this).val();
		$('#invoice_no').val(text.toUpperCase());
	});
	// Fill by P.O
	$(".search").click(function(){
		var invoice_no = $('#invoice_no').val();
		if(invoice_no != ''){
			$.ajax({
				type: "GET",
				url: "{{url('/salereturn/additems')}}",
				data:'invoice_no='+invoice_no,
				success: function(data){
					var obj = JSON.parse(data);
					if(obj[0].length >0){
						$('.field_wrapper').html('');
						$.each(obj[0], function (index, value) {
						  var item_id = obj[0][index].item_id;
						  var item_name = obj[0][index].reference;
						  var stockregister_id = obj[0][index].stockregister_id;
						  var strength = obj[0][0].strength;
						  var form_id = obj[0][0].form_id;
						  var form_name = obj[0][0].form_name;
						  var unit_id = obj[0][index].unit_id;
						  var unit_name = obj[0][index].unit_name;
						  var quantity = obj[0][index].quantity;
						  var rate = obj[0][index].rate;
						  var amount = obj[0][index].amount;
						  var hsncode = obj[0][index].hsn_code;
						  if(!hsncode){
							hsncode = '';
						  }
						  
						  $('.field_wrapper').append('<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" value="'+item_id+'"><input type="hidden" name="reference[]" value="'+item_name+'"><input type="hidden" name="stockregister_id[]" value="'+stockregister_id+'"></td>'+
						  '<td class="center"><span>'+unit_name+'</span><input type="hidden" name="unit_id[]" value="'+unit_id+'"></td>'+
						  '<td class="center"><span>'+hsncode+'</span><input type="hidden" name="hsncode[]" class="hsncode" value="'+hsncode+'"></td>'+
						  '<td class="center"><span>'+quantity+'</span><input type="hidden" name="quantity[]" value="'+quantity+'"></td>'+
						  '<td class="center return_qty" style="max-width: 50px"><input class="form-control return_qtyVal" type="number" name="return_quantity[]" value="0" min="0" step="0.01"></td>'+
						  '<td class="center rate"><span>'+rate+'</span><input class="rateVal" type="hidden" name="rate[]" value="'+rate+'"></td>'+
						  '<td class="center amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]" value="'+amount+'"><input class="return_amountVal" type="hidden" name="return_amount[]" value="0"></td></tr>');
						});
						$('.field_wrapper').append(
						'<tr>'+
							'<td colspan="6" class="right-align">Sub-Total:</td>'+
							'<td class="center-align subtotalText"><b>0.00</b></td>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="6" class="right-align">(-) Discount:</td>'+
							'<td class="left-align discountText">'+
								'<input type="number" class="form-control" name="discount" id="discount" value="0" min="0" step="0.01" autocomplete="off">'+
							'</td>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="6" class="right-align">(+) Other Expense:</td>'+
							'<td class="left-align otherexpenseText">'+
								'<input type="number" class="form-control" name="other_expense" id="other_expense" value="0" min="0" step="0.01" autocomplete="off">'+
							'</td>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="6" class="right-align">Taxable Amount:</td>'+
							'<td class="left-align taxableText"><b>0.00</b></td>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="6" class="right-align">(+) CGST:</td>'+
							'<td class="left-align cgstText">'+
								'<b>0.00</b>'+
							'</td>'+
							'<input type="hidden" name="cgstA" id="cgstA"/>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="6" class="right-align">(+) SGST:</td>'+
							'<td class="left-align sgstText">'+
								'<b>0.00</b>'+
							'</td>'+
							'<input type="hidden" name="sgstA" id="sgstA"/>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="6" class="right-align">(+) IGST:</td>'+
							'<td class="left-align igstText">'+
								'<b>0.00</b>'+
							'</td>'+
							'<input type="hidden" name="igstA" id="igstA"/>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="6" class="right-align">Total:</td>'+
							'<td class="left-align totalText">'+
								'<b>0.00</b>'+
							'</td>'+
						'</tr>');

						$('#client').val(obj[0][0].client_name);
						$('#gstin').val(obj[0][0].client_gstin);
					}
					else{
						$('.field_wrapper').html('');
						$(".nav-tabs").before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> P.O Number not found!</div>');
						setTimeout(function(){ $('.alert.alert-danger').fadeOut(300)}, 3000);
					}
				}
			});
		}
	});
	// Input Type Date
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	//Item Table
	var return_quantity = 0;
	var rate = 0;
	$('.field_wrapper').on('keyup change', '.return_qtyVal', function(e){
		e.preventDefault();
		return_quantity = $(this).val();
		rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
		$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(return_quantity * rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('input.return_amountVal').val(parseFloat(return_quantity*rate).toFixed(2));
		var sub_total = 0;
	    $('.return_amountVal').each(function () {
	    	sub_total = sub_total + parseFloat($(this).val());
	    });

		var discount = $('#discount').val();
		var other_expense = $('#other_expense').val();
		var taxable = sub_total - discount + parseFloat(other_expense);

		$('.taxableText').text(parseFloat(taxable).toFixed(2));
		
		var cgst_percentage = 0;
		var sgst_percentage = 0;
		var igst_percentage = 0;

		var gstin = $('#gstin').val();
		var f2 = gstin.substring(0, 2);
		if(gstin !=''){
			if(f2=='07'){
				cgst_percentage = {{$cgst_percentage}};
				sgst_percentage = {{$sgst_percentage}};
			}
			else{
				igst_percentage = {{$igst_percentage}};
			}
		}

		var cgst = taxable*cgst_percentage/100;
		var sgst = taxable*sgst_percentage/100;
		var igst = taxable*igst_percentage/100;

		var final_total = taxable+cgst+sgst+igst;

		$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
		$('.taxableText').text(parseFloat(taxable).toFixed(2));
		$('.cgstText').text(parseFloat(cgst).toFixed(2));
		$('#cgstA').val(parseFloat(cgst).toFixed(2));
		$('.sgstText').text(parseFloat(sgst).toFixed(2));
		$('#sgstA').val(parseFloat(sgst).toFixed(2));
		$('.igstText').text(parseFloat(igst).toFixed(2));
		$('#igstA').val(parseFloat(igst).toFixed(2));

		$('.totalText').text(parseFloat(final_total).toFixed(2));

	});
	$('.field_wrapper').on('keyup change', '#discount', function (e) {
		e.preventDefault();
		var sub_total = parseFloat($('.subtotalText').text());
		var discount = $('#discount').val();
		var cgst = parseFloat($('.cgstText').text());
		var sgst = parseFloat($('.sgstText').text());
		var igst = parseFloat($('.igstText').text());
		var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

		var other_expense = $('#other_expense').val();
		var taxable = sub_total - discount + parseFloat(other_expense);
		$('.taxableText').text(parseFloat(taxable).toFixed(2));

		var cgst_percentage = 0;
		var sgst_percentage = 0;
		var igst_percentage = 0;

		var gstin = $('#gstin').val();
		var f2 = gstin.substring(0, 2);
		if(gstin !=''){
			if(f2=='07'){
				cgst_percentage = {{$cgst_percentage}};
				sgst_percentage = {{$sgst_percentage}};
			}
			else{
				igst_percentage = {{$igst_percentage}};
			}
		}

		var cgst = taxable*cgst_percentage/100;
		var sgst = taxable*sgst_percentage/100;
		var igst = taxable*igst_percentage/100;

		var final_total = taxable+cgst+sgst+igst;

		$('.taxableText').text(parseFloat(taxable).toFixed(2));
		$('.cgstText').text(parseFloat(cgst).toFixed(2));
		$('#cgstA').val(parseFloat(cgst).toFixed(2));
		$('.sgstText').text(parseFloat(sgst).toFixed(2));
		$('#sgstA').val(parseFloat(sgst).toFixed(2));
		$('.igstText').text(parseFloat(igst).toFixed(2));
		$('#igstA').val(parseFloat(igst).toFixed(2));
		$('.totalText').text(parseFloat(final_total).toFixed(2));

	});
	$('.field_wrapper').on('keyup change', '#other_expense', function (e) {
		e.preventDefault();
		var sub_total = parseFloat($('.subtotalText').text());
		var discount = $('#discount').val();
		var cgst = parseFloat($('.cgstText').text());
		var sgst = parseFloat($('.sgstText').text());
		var igst = parseFloat($('.igstText').text());
		var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

		var other_expense = $('#other_expense').val();
		var taxable = sub_total - discount + parseFloat(other_expense);
		$('.taxableText').text(parseFloat(taxable).toFixed(2));

		var cgst_percentage = 0;
		var sgst_percentage = 0;
		var igst_percentage = 0;

		var gstin = $('#gstin').val();
		var f2 = gstin.substring(0, 2);
		if(gstin !=''){
			if(f2=='07'){
				cgst_percentage = {{$cgst_percentage}};
				sgst_percentage = {{$sgst_percentage}};
			}
			else{
				igst_percentage = {{$igst_percentage}};
			}
		}

		var cgst = taxable*cgst_percentage/100;
		var sgst = taxable*sgst_percentage/100;
		var igst = taxable*igst_percentage/100;

		var final_total = taxable+cgst+sgst+igst;

		$('.taxableText').text(parseFloat(taxable).toFixed(2));
		$('.cgstText').text(parseFloat(cgst).toFixed(2));
		$('#cgstA').val(parseFloat(cgst).toFixed(2));
		$('.sgstText').text(parseFloat(sgst).toFixed(2));
		$('#sgstA').val(parseFloat(sgst).toFixed(2));
		$('.igstText').text(parseFloat(igst).toFixed(2));
		$('#igstA').val(parseFloat(igst).toFixed(2));
		$('.totalText').text(parseFloat(final_total).toFixed(2));

	});
});
</script>
<div class="pageheader">
		<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
        <div class="media-body">
    	<h4 class="test">Sale Return</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/salereturn') }}">Sale Return</a></li>
            <li>Add</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
	  <div class="col-md-10" id="flashmessage">
	  	@include('flashmessage')
	  </div>
	</div>
  	<div class="row">
  		<div class="col-md-10">
  			<form method="post" action="{{ url('salereturn/save') }}">
		  	{{ csrf_field() }}
		  	<div class="row">
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>Credit Note No:</label>
					  <input type="text" class="form-control" value="{{'CN_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}" disabled="">
					  <input type="hidden" name="creditnote_no" id="creditnote_no" value="{{'CN_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}">
					</div>
		  		</div>
		  		<div class="col-md-6">
		  			<div class="form-group">
					  <label>*Credit Note Date:</label>
					  <div class="input-group date">
						<input type="text" name="creditnote_date" id="creditnote_date" value="{{$entry_date}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					  </div>
					</div>
		  		</div>
		  	</div>
			  <div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Transport:</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="transport" id="transport">
							<option value="">Select</option>
							@foreach ($transport as $value) @if (old('transport') == $value->id)
							<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
							@else
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>G.R No:</label>
						<input class="form-control" type="text" name="gr_no" id="gr_no"/>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Booking Date:</label>
						<div class="input-group date">
							<input type="text" name="booking_date" id="booking_date" value="{{ old('booking_date') }}" class="form-control" autocomplete="off"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
				</div>
			</div>
		  	<div class="row">
		  		<div class="col-md-4">
					<div class="form-group">
				      <label>*Invoice Number:</label>
					  <div class="input-group">
						<input type="text" class="form-control" name="invoice_no" id="invoice_no" value="{{ old('invoice_no') }}" required="" autocomplete="off"><span class="input-group-addon search"><i class="fa fa-search"></i></span>
					  </div>
				    </div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label>Client Name:</label>
					  <input type="text" class="form-control" name="client" id="client" disabled="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label>Client GSTIN:</label>
					  <input type="text" class="form-control" name="gstin" id="gstin" disabled="">
					</div>
				</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="table-responsive">
						<table class="table table-bordered" id="itemTable2">
							<tr>
				                <th>Name</th>
				                <th>Unit</th>
								<th>HSN Code</th>
				                <th>Order Quantity</th>
				                <th>Return Quantity</th>
				                <th>Cost</th>
				                <th>Return Amount</th>
				            </tr>
							<tbody class="field_wrapper">
							<tr>
									<td colspan="6" class="right-align">Sub-Total:</td>
									<td class="center-align subtotalText"><b>0.00</b></td>
					            </tr>
								<tr>
									<td colspan="6" class="right-align">(-) Discount:</td>
									<td class="left-align discountText">
										<input type="number" class="form-control" name="discount" id="discount" value="0" min="0" step="0.01" autocomplete="off">
									</td>
								</tr>
								<tr>
									<td colspan="6" class="right-align">(+) Other Expense:</td>
									<td class="left-align otherexpenseText">
										<input type="number" class="form-control" name="other_expense" id="other_expense" value="0" min="0" step="0.01" autocomplete="off">
									</td>
								</tr>
								<tr>
									<td colspan="6" class="right-align">Taxable Amount:</td>
									<td class="left-align taxableText"><b>0.00</b></td>
								</tr>
								<tr>
									<td colspan="6" class="right-align">(+) CGST:</td>
									<td class="left-align cgstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="cgstA" id="cgstA"/>
								</tr>
								<tr>
									<td colspan="6" class="right-align">(+) SGST:</td>
									<td class="left-align sgstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="sgstA" id="sgstA"/>
								</tr>
								<tr>
									<td colspan="6" class="right-align">(+) IGST:</td>
									<td class="left-align igstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="igstA" id="igstA"/>
								</tr>
								<tr>
									<td colspan="6" class="right-align">Total:</td>
									<td class="left-align totalText">
										<b>0.00</b>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="form-group">
					  <label>Reason For Return:</label>
					  <textarea class="form-control" placeholder="Enter reason" name="return_reason" id="return_reason">{{ old('return_reason') }}</textarea>
					</div>
		  		</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<div class="form-group">
					  <label>Remarks:</label>
					  <input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{ old('remarks') }}">
					</div>
		  		</div>
		  	</div>
			<button type="submit" class="btn btn-primary">Save</button>
			</form>
  		</div>
  	</div>
</div>
</div>
<script type="text/javascript">
    var url = "{{ url('/salereturn/autocomplete') }}";
    $('#invoice_no').typeahead({
        source:  function (query, process) {
        return $.get(url, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
@endsection