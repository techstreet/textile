@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
            <i style="padding: 8px 0 0 3px" class="fa fa-building-o"></i>
        </div>
		<div class="media-body">
		<h4>Profile</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Profile</li>
		    <li>View</li>
		    <li>{{Request::segment(3)}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		  <?php
		  foreach ($company as $value){
		  	$name = $value->name;
		  	$address = $value->address;
		  	$phone = $value->phone;
		  	$mobile = $value->mobile;
		  }
		  ?>
		  <table class="table">
            <thead>
              <tr>
                <th class="left-align" colspan="4">Profile Details</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Name:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$name}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Address:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$address}}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Phone:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$phone}}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Mobile:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{$mobile}}</td>
              </tr>
            </tbody>
	    </table> 
      	</div>
	</div>
</div>
</div>
@endsection