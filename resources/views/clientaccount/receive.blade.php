@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$date1 = date("d-F-Y");
?>
<script type='text/javascript'>
$(function(){
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    //startDate: today
});  
});
</script>
<div class="pageheader">
	<div class="media">
		<div class="media-body">
		<h4>Receive Payment</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li><a href="{{ url('/customer') }}">Customers</a></li>
		    <li>{{$client_name}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
	    @include('flashmessage')
	    <div class="alert alert-info">
		    Total Outstanding Balance: <span class="label label-info">{{slash_decimal($outstanding_balance)}}</span>
		</div>
	    <form method="post" action="">
			{{ csrf_field() }}
			<div class="form-group">
			  <label>*Amount:</label>
			  <input type="text" class="form-control" name="amount" id="amount" placeholder="Enter amount" required="">
			</div>
			<div class="form-group">
			  <label>*Date:</label>
			  <div class="input-group date">
				<input type="text" name="date" id="date" value="<?php echo $date1; ?>" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
			</div>
			<div class="form-group">
		    <label>*Remarks:</label>
		    <textarea rows="2" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" required="">{{ old('particular') }}</textarea>
		  </div>
			<button type="submit" name="submit" class="btn btn-primary" @if($outstanding_balance<=0) disabled @endif>Submit</button>
		</form>
		</div>
	</div>
</div>
</div>
@endsection