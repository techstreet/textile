@extends('layouts.website')
@section('content')
<script>
	function printPage() {
	    document.title = "Account Statement";
	    window.print();
	}
</script>
<style>
	@media print {
		#dataTable_filter{
			display:none;
		}
	}
</style>
<?php
$balance = 0;
?>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
		<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		@if(Auth::user()->user_group != '4')
			<a href="{{ url('/customer/receive-payment/'.Request::segment(3)) }}"><button class="btn btn-primary"><i class="fa fa-money"></i> Receive Payment</button></a>
		@endif
		</div>
		<div class="media-body">
		<h4>{{$client_name}}</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li><a href="{{ url('/customer') }}">Customers</a></li>
		    <li>{{$client_name}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    <h4 class="center onlyprint"><u>{{Auth::user()->name}}</u></h4>
			<h5 class="center onlyprint">{{Auth::user()->address}}</h5>
			<h5 class="onlyprint"><b>Customer:</b> {{$client_name}}</h5>
	    @if(Session::has('success'))
	    <div class="alert alert-success alert-dismissable">
		  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		  {{ Session::get('success') }}
		</div>
		@endif
		@if(Session::has('failed'))
		<div class="alert alert-danger alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		{{ Session::get('failed') }}
		</div>
		@endif
		<?php if($count>0){ ?>
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		      	<th>S.No</th>
		      	<th>Date</th>
						<th>Particular</th>
		        <th>Debit</th>
		        <th>Credit</th>
		        <th>Balance</th>
						@if(session('superadmin') == '1')
						<th class="no-print">Action</th>
						@endif
		      </tr>
		    </thead>
		    <tbody>
		      @foreach ($clientaccount as $key=>$value)
					<?php
					$balance = $balance+($value->debit-$value->credit);
					?>	
					<tr>
	            <td>{{ ++$key }}</td>
							<td>{{ date_dfy($value->created_at) }}</td>
							@if($value->record_type == 0)
							<td><a href="{{url('saleregister/view').'/'.getSaleTypeId($value->invoice_no,$value->record_type,Auth::user()->company_id)}}">{{ $value->particular }}</a></td>
							@elseif($value->record_type == 1)
							<td><a href="{{url('salereturn/view').'/'.getSaleTypeId($value->invoice_no,$value->record_type,Auth::user()->company_id)}}">{{ $value->particular }}</a></td>
							@else
							<td>{{ $value->particular }}</td>
							@endif
	            <td>{{ slash_decimal($value->debit) }}</td>
	            <td>{{ slash_decimal($value->credit) }}</td>
	            <td>{{ slash_decimal($balance) }}</td>
							@if(session('superadmin') == '1')
							<td class="no-print">
							@if($value->invoice_no == NULL)
							<a onclick="return confirm('Are you sure you want to Delete?');" href="{{url('customer/account/delete').'/'.$value->id}}">Delete</a>
							@endif
							</td>
							@endif
	          </tr>
	          @endforeach
		    </tbody>
		  </table>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    });
});
</script>
@endsection