@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="media-body">
		<h4>Items</h4>
		<ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{url('/item')}}">Items</a></li>
            <li>View</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		  <table class="table">
            <thead>
              <tr>
                <th class="left-align" colspan="4">Item Details</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Generic Name:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->name }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Category:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->slug }}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Price:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->price }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Currency:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->currency }}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Unit:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->unit }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Brand:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->brand }}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>HSN Code:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->hsn_code }}</td>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Notify Quantity:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none">{{ $item[0]->notify_quantity }}</td>
              </tr>
              <tr>
                <td style="padding-top: 15px" class="left-align bt-none"><b>Barcode:</b></td>
                <td style="padding-top: 15px" class="left-align bt-none"><img src="data:image/png;base64,{{DNS1D::getBarcodePNG($item[0]->id, 'C39+')}}" alt="barcode" /></td>
              </tr>
            </tbody>
	    </table> 
      	</div>
	</div>
</div>
</div>
@endsection