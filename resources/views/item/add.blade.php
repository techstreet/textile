@extends('layouts.website')
@section('content')
<script type='text/javascript'>
$(function(){
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
	$('.input-group.date').datepicker({
		calendarWeeks: true,
		todayHighlight: true,
		autoclose: true,
		format: "dd-MM-yyyy",
		startDate: today
	});
	var addButton = $('#add-btn');
	var wrapper = $('.field_wrapper');
	$(addButton).click(function(){
		var referenceName = document.getElementById("reference_name").value;
		if(referenceName!=''){
			var fieldHTML = '<div class="input-group" style="margin-top:5px"><input type="text" class="form-control" placeholder="Enter reference name" name="reference_name[]"><span class="input-group-addon remove-btn"><i class="fa fa-minus"></i></span></div>';
			$(wrapper).append(fieldHTML);
		}
		else{
			alert('Please enter reference name');
		}
	});
	$(wrapper).on('click', '.remove-btn', function(e){
		e.preventDefault();
		$(this).closest("div").remove();
	});
});
</script>
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Items</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{ url('/item') }}">Items</a></li>
            <li>Add</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-8">
		@include('flashmessage')
      	<form name="myForm" method="post" action="{{ url('item/save') }}">
      	{{ csrf_field() }}
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Name:</label>
				    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="{{ old('name') }}" required="">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				  <label>*Category:</label>
				  <select class="form-control" name="category" id="category" required="">
				  		<option value="">Select</option>
			      		@foreach ($category as $value)
			      		@if (old('category') == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Price:</label>
				    <input type="number" class="form-control" placeholder="Enter price" name="price" id="price" value="{{ old('price') }}" min="0" step="0.01" required="">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Currency:</label>
				    <select class="form-control" name="currency" id="currency" required="">
						<option value="INR" @if(old('currency') == 'INR') selected @endif>Indian Rupees (INR)</option>
						<option value="USD" @if(old('currency') == 'USD') selected @endif>US Dollar (USD)</option>
			  			<option value="KES" @if(old('currency') == 'KES') selected @endif>Kenyan Schilling (KES)</option>
			  			<option value="SOS" @if(old('currency') == 'SOS') selected @endif>Somali Schilling (SOS)</option>
			  			<option value="ZMK" @if(old('currency') == 'ZMK') selected @endif>Zambian Kwacha (ZMK)</option>
		      		</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>Strength:</label>
				    <input type="number" class="form-control" placeholder="Enter strength" name="strength" id="strength" value="{{ old('strength') }}" min="1">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				  <label>*Unit:</label>
				  <select class="form-control" name="unit" id="unit" required="">
				  		<option value="">Select</option>
			      		@foreach ($unit as $value)
			      		@if (old('unit') == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				  <label>Brand:</label>
				  <select class="form-control" name="brand" id="brand">
				  		<option value="">Select</option>
			      		@foreach ($brand as $value)
			      		@if (old('brand') == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
			<div class="col-md-6" style="display: none">
				<div class="form-group">
				  <label>Manufacturer:</label>
				  <select class="form-control" name="manufacturer" id="manufacturer">
				  		<option value="">Select</option>
			      		@foreach ($manufacturer as $value)
			      		@if (old('manufacturer') == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				  <label>Form:</label>
				  <select class="form-control" name="form" id="form">
				  		<option value="">Select</option>
			      		@foreach ($form as $value)
			      		@if (old('form') == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>Pack Size:</label>
				    <input type="number" class="form-control" placeholder="Enter pack size" name="pack_size" id="pack_size" value="{{ old('pack_size') }}" min="1">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Notify Quantity:</label>
				    <input type="number" class="form-control" placeholder="Enter notify quantity" name="notify_quantity" id="notify_quantity" value="{{ old('notify_quantity') }}" min="1" required="">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group field_wrapper">
				<label>Reference Name:</label>
					<div class="input-group">
					<input type="text" class="form-control" placeholder="Enter reference name" name="reference_name[]" id="reference_name" value="{{ old('reference_name') }}"><span id="add-btn" class="input-group-addon"><i class="fa fa-plus"></i></span>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				    <label>HSN Code:</label>
				    <input type="text" class="form-control" placeholder="Enter HSN code" name="hsn_code" id="hsn_code" value="{{ old('hsn_code') }}">
				</div>
			</div>
		</div>
		  
		<button type="submit" class="btn btn-primary">Submit</button>
		</form>
      </div>
	</div>
</div>
</div>
@endsection