@extends('layouts.website')
@section('content')
<script type='text/javascript'>
$(function(){
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
	$('.input-group.date').datepicker({
		calendarWeeks: true,
		todayHighlight: true,
		autoclose: true,
		format: "dd-MM-yyyy",
		startDate: today
	});
	var addButton = $('#add-more');
	var wrapper = $('.field_wrapper');
	var wrapper2 = $('.field_wrapper2');
	$(addButton).click(function(){
		// var referenceName = document.getElementById("reference_name").value;
		var fieldHTML = '<div class="input-group" style="margin-top:5px"><input type="text" class="form-control" placeholder="Enter reference name" name="reference_name[]"><span class="input-group-addon remove-btn"><i class="fa fa-minus"></i></span></div>';
		$(wrapper).append(fieldHTML);
	});
	$(wrapper2).on('click', '.remove-btn', function(e){
		e.preventDefault();
		$(this).closest("div").remove();
	});  
});
</script>
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>Items</h4>
        <ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li>Masters</li>
            <li><a href="{{url('/item')}}">Items</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-8">
		@include('flashmessage')
      	<form method="post" action="{{ url('/item/update/'.$item[0]->id) }}">
      	{{ csrf_field() }}
      	<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Name:</label>
				    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="{{ $item[0]->name }}" required="">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				  <label>*Category:</label>
				  <select class="form-control" name="category" id="category" required="">
			      		@foreach ($category as $value)
			      		@if ($item[0]->category_id == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Price:</label>
				    <input type="number" class="form-control" placeholder="Enter price" name="price" id="price" value="{{ $item[0]->price }}" min="0" step="0.01" required="">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Currency:</label>
				    <select class="form-control" name="currency" id="currency" required="">
						<option value="INR" @if($item[0]->currency == 'INR') selected @endif>Indian Rupees (INR)</option>
						<option value="USD" @if($item[0]->currency == 'USD') selected @endif>US Dollar (USD)</option>
			  			<option value="KES" @if($item[0]->currency == 'KES') selected @endif>Kenyan Schilling (KES)</option>
			  			<option value="SOS" @if($item[0]->currency == 'SOS') selected @endif>Somali Schilling (SOS)</option>
			  			<option value="ZMK" @if($item[0]->currency == 'ZMK') selected @endif>Zambian Kwacha (ZMK)</option>
		      		</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>Strength:</label>
				    <input type="number" class="form-control" placeholder="Enter strength" name="strength" id="strength" value="{{ $item[0]->strength }}" min="0">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				  <label>*Unit:</label>
				  <select class="form-control" name="unit" id="unit" required="">
			      		@foreach ($unit as $value)
			      		@if ($item[0]->unit_id == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				  <label>Brand:</label>
				  <select class="form-control" name="brand" id="brand">
			      		@foreach ($brand as $value)
			      		@if ($item[0]->brand_id == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
			<div class="col-md-6" style="display: none">
				<div class="form-group">
				  <label>Manufacturer:</label>
				  <select class="form-control" name="manufacturer" id="manufacturer">
			      		@foreach ($manufacturer as $value)
			      		@if ($item[0]->manufacturer_id == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				  <label>Form:</label>
				  <select class="form-control" name="form" id="form">
			      		@foreach ($form as $value)
			      		@if ($item[0]->form_id == $value->id)
						      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
						@else
						      <option value="{{ $value->id }}">{{ $value->name }}</option>
						@endif
			      		@endforeach
				  </select>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
				    <label>Pack Size:</label>
				    <input type="number" class="form-control" placeholder="Enter pack size" name="pack_size" id="pack_size" value="{{ $item[0]->pack_size }}" min="0">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				    <label>*Notify Quantity:</label>
				    <input type="number" class="form-control" placeholder="Enter notify quantity" name="notify_quantity" id="notify_quantity" value="{{ $item[0]->notify_quantity }}" min="1" required="">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group field_wrapper2">
				<label>Reference Name:</label>
					@foreach($item_reference as $value)
					<div style="margin-bottom:5px" class="input-group">
					<input type="text" class="form-control" placeholder="Enter reference name" name="reference_name[]" id="reference_name" value="{{ $value->reference }}"><span class="input-group-addon remove-btn"><i class="fa fa-minus"></i></span>
					</div>
					@endforeach
				</div>
				<div class="form-group field_wrapper">
				<a id="add-more" style="margin-bottom:5px" href="javascript:"><i class="fa fa-plus"></i> Add More</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
				    <label>HSN Code:</label>
				    <input type="text" class="form-control" placeholder="Enter HSN code" name="hsn_code" id="hsn_code" value="{{ $item[0]->hsn_code }}">
				</div>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary">Update</button>
		</form>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    var text_max = 400;
    var text_length = $('#description').val().length;
    var text_remaining = text_max - text_length;
    $('#description_characters').html(text_remaining + ' characters left');

    $('#description').keyup(function() {
        var text_length = $('#description').val().length;
        var text_remaining = text_max - text_length;

        $('#description_characters').html(text_remaining + ' characters left');
    });
});
</script>
@endsection