@extends('layouts.website') @section('content')
<?php
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
<script>
	$(function () {
		$(".basic-select").select2();
		var total = 0;
		$('.total').each(function(){
			total += parseFloat($(this).text());
		});
		$('.total_sum').text(total);
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
	});
	function printPage() {
		document.title = "Debt Report";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4>Debt Report</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>Reports</li>
				<li>Debt Report</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12">
				@include('flashmessage')
				<form class="form-inline" method="post" action="">
					{{ csrf_field() }}
					<!-- <div class="form-group" style="margin-right: 0">
						<div class="input-group">
							<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="debt" id="debt" placeholder="Debt"
							 required="">
								<option value="">Select</option>
								<option value="1">1 Month Old</option>
								<option value="2">2 Month Old</option>
								<option value="3">3 Month Old</option>
								<option value="6">6 Month Old</option>
								<option value="12">1 Year Old</option>
								<option value="13">1+ Year Old</option>
							</select>
						</div>
					</div> -->
					<div class="input-group date">
						<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required="">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required="">
						<span class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</span>
					</div>
					<button type="submit" name="submit" class="btn btn-info">Find</button>
				</form>
			</div>
		</div>
		<?php if($_POST && $_POST > 0){ ?>
		<div class="row" style="margin-top: 10px">
			<div class="col-md-12">
				@include('flashmessage')
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable">
						<thead>
							<tr>
								<th>S.No</th>
								<th>Customer</th>
								<th>Contact name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Mobile</th>
								<th>Address</th>
								<th>Amount</th>
							</tr>
						</thead>
						<tbody>
							<?php if($count>0){ ?> @foreach ($debtreport as $key=>$value)
							<?php if($value->debit-$value->credit > 0){ ?>
							<tr>
								<td>{{ ++$key }}</td>
								<td>
									<a href="{{url('customer/account/'.$value->customer_id)}}">{{ $value->customer_name }}</a>
								</td>
								<td>{{ $value->contact_name }}</td>
								<td>{{ $value->email }}</td>
								<td>{{ $value->phone }}</td>
								<td>{{ $value->mobile }}</td>
								<td>{{ $value->address }}</td>
								<td class="total">{{ getBalance($value->customer_id) }}</td>
							</tr>
							<?php }?> @endforeach
							<tr style="font-weight:bold">
								<td colspan="7" style="text-align:right">Total: </td>
								<td class="total_sum">0</td>
							</tr>
							<?php } else{ ?>
							<tr>
								<td colspan="8">There is no Record Found.</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
@endsection