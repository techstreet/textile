@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		@if(Auth::user()->user_group != '4')
		<div class="pull-right">
			<a href="{{ url('/voucher/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add New</button></a>
		</div>
		@endif
		
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Voucher</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Voucher</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Voucher No</th>
		        <th>Voucher Date</th>
		        <th>Client Name</th>
		        <th>Address</th>
		        <th>Mobile</th>
		        <th width="100px" style="min-width: 50px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($voucher as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->voucher_no }}</td>
	            <td>{{ date_dfy($value->entry_date) }}</td>
	            <td>{{ $value->client_name }}</td>
	            <td>{{ $value->client_address }}</td>
	            <td>{{ $value->client_mobile }}</td>
	            <td>
	            <a href="{{ url('/voucher/view/'.$value->id) }}" data-toggle="tooltip" title="View" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a>
							<a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/voucher/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
	            </td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    	ordering: false
    });
});
</script>
@endsection