@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
?>
<script type='text/javascript'>
$(function(){
	$(".basic-select").select2();
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	$('.field_wrapper').on('keyup change', '.qtyVal', function(e){
		e.preventDefault();
			quantity = $(this).val();
			rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();

			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));

			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	$('.field_wrapper').on('keyup change', '.rateVal', function(e){
		e.preventDefault();
			rate = $(this).val();
			quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	$('.field_wrapper').on('keyup change', '#discount', function(e){
		e.preventDefault();
			var sub_total = parseFloat($('.subtotalText').text());
			var discount = $('#discount').val();
			var cgst = parseFloat($('.cgstText').text());
			var sgst = parseFloat($('.sgstText').text());
			var igst = parseFloat($('.igstText').text());
			var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	$('.field_wrapper').on('keyup change', '#other_expense', function (e) {
		e.preventDefault();
		var sub_total = parseFloat($('.subtotalText').text());
		var discount = $('#discount').val();
		var cgst = parseFloat($('.cgstText').text());
		var sgst = parseFloat($('.sgstText').text());
		var igst = parseFloat($('.igstText').text());
		var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

		var other_expense = $('#other_expense').val();
		var taxable = sub_total - discount + parseFloat(other_expense);
		$('.taxableText').text(parseFloat(taxable).toFixed(2));

		var cgst_percentage = 0;
		var sgst_percentage = 0;
		var igst_percentage = 0;

		var gstin = $('#gstin').val();
		var f2 = gstin.substring(0, 2);
		if(gstin !=''){
			if(f2=='07'){
				cgst_percentage = {{$cgst_percentage}};
				sgst_percentage = {{$sgst_percentage}};
			}
			else{
				igst_percentage = {{$igst_percentage}};
			}
		}

		var cgst = taxable*cgst_percentage/100;
		var sgst = taxable*sgst_percentage/100;
		var igst = taxable*igst_percentage/100;

		var final_total = taxable+cgst+sgst+igst;

		$('.taxableText').text(parseFloat(taxable).toFixed(2));
		$('.cgstText').text(parseFloat(cgst).toFixed(2));
		$('#cgstA').val(parseFloat(cgst).toFixed(2));
		$('.sgstText').text(parseFloat(sgst).toFixed(2));
		$('#sgstA').val(parseFloat(sgst).toFixed(2));
		$('.igstText').text(parseFloat(igst).toFixed(2));
		$('#igstA').val(parseFloat(igst).toFixed(2));
		$('.totalText').text(parseFloat(final_total).toFixed(2));

	});
	$('.field_wrapper').on('click', '.add_button', function(e){
		var fieldHTML = '<tr>'+
			'<td><input type="text" class="form-control" placeholder="Enter item name" name="item_name[]" id="item_name"></th>'+
			'<td><input type="text" class="form-control" placeholder="Enter brand name" name="brand_name[]" id="brand_name"></th>'+
			'<td><input type="number" class="form-control" placeholder="Enter pcs" name="pcs[]" id="pcs" min="0"></th>'+
			'<td><input type="text" class="form-control" placeholder="Enter unit name" name="unit_name[]" id="unit_name" value=""></th>'+
			'<td class="qty"><input type="number" class="form-control qtyVal" placeholder="Enter item quantity" name="quantity[]" id="quantity" step="0.01" min="0"></th>'+
			'<td class="rate"><input type="number" class="form-control rateVal" placeholder="Enter item cost" name="rate[]" id="rate" step="0.01" min="0"></th>'+
			'<td class="amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]"></th>'+
			'<td width="80px"><a href="javascript:void(0);" class="remove_button" title="Add field"><img src="{{url("images/remove-icon.png")}}"></a></th>'+
		'</tr>';
		$('.field_wrapper').prepend(fieldHTML);
	});
	$('.field_wrapper').on('click', '.remove_button', function(e){
		e.preventDefault();
			$(this).closest("tr").remove();
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

		var discount = $('#discount').val();

			var sub_total = 0;
			var total = 0;
			var cgst = 0;
			var sgst = 0;
			var igst = 0;
			var final_total = 0;

			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});
			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));

			var taxable = 0;
			var other_expense = 0;

			if (sub_total > 0) {
				other_expense = $('#other_expense').val();
				taxable = sub_total - discount + parseFloat(other_expense);
				var cgst = taxable*cgst_percentage/100;
				var sgst = taxable*sgst_percentage/100;
				var igst = taxable*igst_percentage/100;

				var final_total = taxable+cgst+sgst+igst;
			}

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
});
</script>
<script>
function showGSTIN(val){
	var gstin_string = val.split('#');
	$('#gstin').val(gstin_string[1]);
}
function showGSTIN2(val){
	var gstin_string = val.split('#');
	$('#gstin2').val(gstin_string[1]);
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4 class="test">Voucher</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/voucher') }}">Voucher</a>
				</li>
				<li>Add</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12" id="flashmessage">
				@include('flashmessage')
			</div>
		</div>
		<form method="post" action="{{ url('voucher/save') }}">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>*Voucher No:</label>
						<input type="text" class="form-control" value="{{'VOU_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}" disabled="">
						<input type="hidden" name="voucher_no" id="voucher_no" value="{{'VOU_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>*Voucher Date:</label>
						<div class="input-group date">
							<input type="text" name="entry_date" id="entry_date" value="{{$entry_date}}" class="form-control" required="">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>*Client(Billing):</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="client" id="client" required="" onchange="showGSTIN(this.value);">
							<option value="">Select</option>
							@foreach ($client as $value) @if (old('client') == $value->id)
							<option value="{{ $value->id.'#'.$value->gstin }}" selected>{{ $value->name.' - '.$value->mobile }}</option>
							@else
							<option value="{{ $value->id.'#'.$value->gstin }}">{{ $value->name.' - '.$value->mobile }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>GSTIN:</label>
						<input class="form-control" type="text" name="gstin" id="gstin" value="" disabled/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>*Client(Shipping):</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="shipping_client" id="shipping_client" required="" onchange="showGSTIN2(this.value);">
							<option value="">Select</option>
							@foreach ($client as $value) @if (old('client') == $value->id)
							<option value="{{ $value->id.'#'.$value->gstin }}" selected>{{ $value->name.' - '.$value->mobile }}</option>
							@else
							<option value="{{ $value->id.'#'.$value->gstin }}">{{ $value->name.' - '.$value->mobile }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>GSTIN:</label>
						<input class="form-control" type="text" name="gstin2" id="gstin2" value="" disabled/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Transport:</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="transport" id="transport">
							<option value="">Select</option>
							@foreach ($transport as $value) @if (old('transport') == $value->id)
							<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
							@else
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>G.R No:</label>
						<input class="form-control" type="text" name="gr_no" id="gr_no"/>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Booking Date:</label>
						<div class="input-group date">
							<input type="text" name="booking_date" id="booking_date" value="{{ old('booking_date') }}" class="form-control" autocomplete="off"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered" id="itemTable">
							<tr>
								<th>Name</th>
								<th>Brand</th>
								<th>Pcs</th>
								<th>Unit</th>
								<th>Quantity</th>
								<th>Cost</th>
								<th>Total</th>
								<th width="80px">Action</th>
							</tr>
							<tbody class="field_wrapper">
								<tr>
									<td><input type="text" class="form-control" placeholder="Enter item name" name="item_name[]" id="item_name"></th>
									<td><input type="text" class="form-control" placeholder="Enter brand name" name="brand_name[]" id="brand_name"></th>
									<td><input type="number" class="form-control" placeholder="Enter pcs" name="pcs[]" id="pcs" min="0"></th>
									<td><input type="text" class="form-control" placeholder="Enter unit name" name="unit_name[]" id="unit_name" value=""></th>
									<td class="qty"><input type="number" class="form-control qtyVal" placeholder="Enter item quantity" name="quantity[]" id="quantity" step="0.01" min="0"></th>
									<td class="rate"><input type="number" class="form-control rateVal" placeholder="Enter item cost" name="rate[]" id="rate" step="0.01" min="0"></th>
									<td class="amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]"></th>
									<td width="80px"><a href="javascript:void(0);" class="add_button" title="Add field"><img src="{{url("images/add-icon.png")}}"></a></th>
								</tr>
								<tr>
									<td colspan="7" class="right-align">Sub-Total:</td>
									<td class="left-align subtotalText">
										<b>0.00</b>
									</td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(-) Discount:</td>
									<td class="left-align discountText">
										<input type="number" class="form-control" name="discount" id="discount" value="0" min="0" step="0.01" autocomplete="off">
									</td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) Other Expense:</td>
									<td class="left-align otherexpenseText">
										<input type="number" class="form-control" name="other_expense" id="other_expense" value="0" min="0" step="0.01" autocomplete="off">
									</td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">Taxable Amount:</td>
									<td class="left-align taxableText"><b>0.00</b></td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) CGST:</td>
									<td class="left-align cgstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="cgstA" id="cgstA"/>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) SGST:</td>
									<td class="left-align sgstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="sgstA" id="sgstA"/>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) IGST:</td>
									<td class="left-align igstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="igstA" id="igstA"/>
								</tr>
								<tr>
									<td colspan="7" class="right-align">Total:</td>
									<td class="left-align totalText">
										<b>0.00</b>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Ref:</label>
						<input type="text" class="form-control" placeholder="Enter reference" name="ref" id="ref" value="{{ old('ref') }}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Remarks:</label>
						<input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{ old('remarks') }}">
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
		</form>
	</div>
</div>
@endsection