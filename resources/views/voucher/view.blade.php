@extends('layouts.website') @section('content')
<script>
	function printPage() {
		document.title = "Voucher";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4>Voucher</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/voucher') }}">Voucher</a>
				</li>
				<li>View</li>
				<li>{{Request::segment(3)}}</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-9">
				<h4 class="center onlyprint">
					<u>{{$company->name}}</u>
				</h4>
				<h5 class="center onlyprint">{{$company->address}}</h5>
				<h5 class="center onlyprint"><b>GSTIN:</b> {{$company->gstin}}</h5>
				@if(!empty($company->phone) && !empty($company->mobile) )
				<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}, <b>Mobile:</b> {{$company->mobile}}</h5>
				@else
					@if(!empty($company->phone))
					<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}</h5>
					@else
					<h5 class="center onlyprint"><b>Mobile:</b> {{$company->mobile}}</h5>
					@endif
				@endif
				
				<table class="table table-bordered">
				<tr>
						<th class="left-align">Voucher Details</th>
						<th class="left-align">Transport Details</th>
					</tr>
					<tr>
						<td class="left-align" style="vertical-align: top !important;">
							<p>Voucher No: {{$voucher[0]->voucher_no}}</p>
							<p>Voucher Date: {{date_dfy($voucher[0]->entry_date)}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p>Transport: {{$voucher[0]->transport_name}}</p>
							<p>GR No: {{$voucher[0]->gr_no}}</p>
							<p>Booking Date: {{date_dfy($voucher[0]->booking_date)}}</p>
						</td>
					</tr>
					<tr>
						<th class="left-align">Billing Details</th>
						<th class="left-align">Shipping Details</th>
					</tr>
					<tr>
						<td class="left-align">
							<p>Client Name: {{$voucher[0]->client_name}}</p>
							<p>Phone: {{$voucher[0]->client_mobile}}</p>
							<p>GSTIN: {{$voucher[0]->client_gstin}}</p>
							<p>State Code: {{substr($voucher[0]->client_gstin, 0, 2)}}</p>
							<p>Address: {{$voucher[0]->client_address}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p>Client Name: {{$shipping_client->client_name}}</p>
							<p>Phone: {{$shipping_client->client_mobile}}</p>
							<p>GSTIN: {{$shipping_client->client_gstin}}</p>
							<p>State Code: {{substr($shipping_client->client_gstin, 0, 2)}}</p>
							<p>Address: {{$shipping_client->client_address}}</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="table-responsive">
					<table class="table table-bordered mb30">
						<tbody>
							<tr>
								<td width="50px" class="left-align">
									<b>S.No</b>
								</td>
								<td class="left-align">
									<b>Name</b>
								</td>
								<td class="left-align">
									<b>Pcs</b>
								</td>
								<td class="left-align">
									<b>Quantity</b>
								</td>
								<td class="left-align">
									<b>Unit</b>
								</td>
								<td class="left-align">
									<b>Cost</b>
								</td>
								<td class="left-align">
									<b>Total</b>
								</td>
							</tr>
							<?php $i=1; foreach ($voucher_item as $value){ ?>
							<tr>
								<td class="left-align">{{ $i }}</td>
								<td class="left-align">{{ $value->item_name }}</td>
								<td class="left-align">{{ $value->pcs }}</td>
								<td class="left-align">{{ $value->quantity }}</td>
								<td class="left-align">{{ $value->unit_name }}</td>
								<td class="left-align">{{ $value->rate }}</td>
								<td class="left-align">{{ $value->amount }}</td>
							</tr>
							<?php $i++; }?>
							<tr>
								<td colspan="6" class="right-align">Sub-Total:</td>
								<td class="left-align">
									<b>{{$voucher[0]->sub_total}}</b>
								</td>
							</tr>
							<tr>
								<td colspan="6" class="right-align">Discount:</td>
								<td class="left-align">
									<b>{{$voucher[0]->discount}}</b>
								</td>
							</tr>
							<tr>
								<td colspan="6" class="right-align">Other Expense:</td>
								<td class="left-align">
									<b>{{$voucher[0]->other_expense}}</b>
								</td>
							</tr>
							<tr>
								<td colspan="6" class="right-align">CGST:</td>
								<td class="left-align">
									<b>{{$voucher[0]->cgst}}</b>
								</td>
							</tr>
							<tr>
								<td colspan="6" class="right-align">SGST:</td>
								<td class="left-align">
									<b>{{$voucher[0]->sgst}}</b>
								</td>
							</tr>
							<tr>
								<td colspan="6" class="right-align">IGST:</td>
								<td class="left-align">
									<b>{{$voucher[0]->igst}}</b>
								</td>
							</tr>
							<tr>
								<td colspan="6" class="right-align">Total:</td>
								<td class="left-align">
									<b>{{$voucher[0]->total}}</b>
								</td>
							</tr>
						</tbody>
					</table>
					<br/><br/>
					<table class="table" style="border-top:1px dotted #ddd">
					<tr>
						<th class="left-align" style="border:none">Received By</th>
						<th class="right-align" style="border:none">Authorised Signatory</th>
					</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection