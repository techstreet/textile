@extends('layouts.website')
@section('content')
<script type='text/javascript'>
$(function(){
	$(".basic-select").select2();
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
	$('.form-control.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	$('.field_wrapper').on('keyup change', '.qtyVal', function(e){
		e.preventDefault();
		quantity = $(this).val();
		rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
		$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity*rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity*rate).toFixed(2));
		var sub_total = 0;
		var tax_percentage = {{$tax_percentage}};
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	    
	    var discount = $('#discount').val();
		var total = sub_total - discount;
		var tax = total*tax_percentage/100;
		var final_total = total+tax;
	    
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	
	$('.field_wrapper').on('keyup change', '.rateVal', function(e){
		e.preventDefault();
		rate = $(this).val();
		quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
		$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity*rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity*rate).toFixed(2));
		var sub_total = 0;
		var tax_percentage = {{$tax_percentage}};
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text()); 
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	    
	    var discount = $('#discount').val();
		var total = sub_total - discount;
		var tax = total*tax_percentage/100;
		var final_total = total+tax;
	    
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	
	$('.field_wrapper').on('keyup change', '#discount', function(e){
		e.preventDefault();
		var sub_total = $('.subtotalText').text();
		var tax_percentage = {{$tax_percentage}};
	    var discount = $('#discount').val();
		var total = sub_total - discount;
		var tax = total*tax_percentage/100;
		var final_total = total+tax;
	    
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	
	$('.field_wrapper').on('click', '.remove_button', function(e){
		e.preventDefault();
		$(this).closest("tr").remove();
		x--;
		var tax_percentage = {{$tax_percentage}};
		var discount = $('#discount').val();
		
		var sub_total = 0;
		var total = 0;
		var tax = 0;
		var final_total = 0;
		
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	    
	    if(sub_total>0){
			var total = sub_total - discount;
			var tax = total*tax_percentage/100;
			var final_total = total+tax;
		}
	    
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});

	$(".search").click(function(){
		var barcode = $('#barcode_search').val();
		if(barcode != ''){
			$.ajax({
				type: "GET",
				url: "{{url('/saleregister/barcodesearch')}}",
				data:'barcode='+barcode,
				success: function(data){
					
					var obj = JSON.parse(data);
					
					var item_id = obj[0].item_id;
					var item_name = obj[0].item_name;
					var strength = obj[0].strength;
					var unit_name = obj[0].unit_name;
					var brand_name = obj[0].brand_name;
					var form_name = obj[0].form_name;
					var stock = obj[0].stock;
					var barcode = obj[0].barcode;
					var rate = obj[0].rate;
					var expiry_append = '';
					  
					$.each(obj, function (index, value) {
					  var expiry_date = obj[index].expiry_date;
					  expiry_append += '<option value="'+expiry_date+'">'+expiry_date+'</option>';
					});
					
					var fieldHTML = '<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" class="item_id" value="'+item_id+'"></td><td class="center"><span>'+strength+'</span></td><td class="center"><span>'+unit_name+'</span></td><td class="center"><span>'+brand_name+'</span></td><td class="center"><span>'+form_name+'</span></td><td class="center"><span>'+stock+'</span></td><td class="center"><span>'+barcode+'</span><input type="hidden" name="barcode[]" value="'+barcode+'"></td><td class="center"><select class="form-control" name="expiry_date[]">'+expiry_append+'</select></td><td class="center qty"><input class="form-control qtyVal" type="number" name="quantity[]" min="1"></td><td class="center rate"><input class="form-control rateVal" type="number" name="rate[]" value="'+rate+'" min="0" step="0.01"></td><td class="center amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]"></td><td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>';
					$('.field_wrapper').prepend(fieldHTML);
				}
			});
		}
	});
});
</script>
<script>
var maxField = 101; //Input fields increment limitation
var x = 1; //Initial field counter is 1
var quantity = '5';
function getCategory(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax')}}",
	data:'company_id='+val,
	success: function(data){
		$("#category").html(data);
		$("#item").html('<option value="">Select</option>');
	}
	});
}
function getItems(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax2')}}",
	data:'category_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function getItemsbyBrand(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax5')}}",
	data:'brand_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function in_array(array, id) {
    for(var i=0;i<array.length;i++) {
       if(array[i] == id)
        return true;
    }
    return false;
}
function addItems(val){
	$(".noitems").hide();
	var itemStr = $("input[name='item_id[]']").map(function(){return $(this).val();}).get();
	var itemArr = JSON.parse("["+ itemStr +"]");
	var itemAlreadyAdded = in_array(itemArr, val);
//	if(itemAlreadyAdded){
//		$("#flashmessage").append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> This item is already added!</div>');
//		setTimeout(function(){ $('.alert.alert-danger').fadeOut(300)}, 3000);
//		return false;
//	}
	if(val != ''){
		$.ajax({
			type: "GET",
			url: "{{url('/saleregister/additems')}}",
			data:'item_id='+val,
			success: function(data){
					
					var obj = JSON.parse(data);
					
					var item_id = obj.item_id;
					var item_name = obj.item_name;
					var strength = obj.strength;
					var unit_name = obj.unit_name;
					var brand_name = obj.brand_name;
					var form_name = obj.form_name;
					var stock = obj.stock;
					var rate = obj.rate;
					var expiry_append = '';
					var barcode_append = '';
					
					$.each(obj.expiry_date, function (index, value) {
					  var expiry_date = obj.expiry_date[index];
					  expiry_append += '<option value="'+expiry_date+'">'+expiry_date+'</option>';
					});
					
					$.each(obj.barcode, function (index, value) {
					  var barcode = obj.barcode[index];
					  barcode_append += '<option value="'+barcode+'">'+barcode+'</option>';
					});
					
					var fieldHTML = '<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" class="item_id" value="'+item_id+'"></td><td class="center"><span>'+strength+'</span></td><td class="center"><span>'+unit_name+'</span></td><td class="center"><span>'+brand_name+'</span></td><td class="center"><span>'+form_name+'</span></td><td class="center"><span>'+stock+'</span></td><td class="center"><select class="form-control" name="barcode[]">'+barcode_append+'</select></td><td class="center"><select class="form-control" name="expiry_date[]">'+expiry_append+'</select></td><td class="center qty"><input class="form-control qtyVal" type="number" name="quantity[]" required="" min="1"></td><td class="center rate"><input class="form-control rateVal" type="number" name="rate[]" value="'+rate+'" min="0" step="0.01"></td><td class="center amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]"></td><td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>';
					if(stock>0){
						$('.field_wrapper').prepend(fieldHTML);
					}
					else{
						alert("This item is out of stock!");
					}
				}
		});
	}
}
</script>
<div class="pageheader">
		<div class="media">
		<div class="pageicon pull-left">
            <i class="fa fa-exchange"></i>
        </div>
        <div class="media-body">
    	<h4 class="test">Sales Register</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/saleregister') }}">Sales Register</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
	  <div class="col-md-12" id="flashmessage">
	  	@include('flashmessage')
	  </div>
	</div>
  	<form method="post" action="{{ url('/saleregister/update/'.$saleregister[0]->id) }}">
  	{{ csrf_field() }}
  	<div class="row">
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>Invoice No:</label>
			  <input type="text" class="form-control" name="invoice_no" id="invoice_no" value="{{$saleregister[0]->invoice_no}}" disabled="">
			</div>
  		</div>
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>*Invoice Date:</label>
			  <div class="input-group date">
				<input type="text" name="entry_date" id="entry_date" value="{{date_dfy($saleregister[0]->entry_date)}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>*Client:</label>
			  <select style="border: none; padding: 0px" class="form-control basic-select" name="client" id="client" required="">
			  		@foreach ($client as $value)
			  		<option value="<?php echo $value->id; ?>" <?php if($saleregister[0]->client_id==$value->id){ ?> selected="" <?php }?>><?php echo $value->name.' - '.$value->mobile; ?></option>
			  		@endforeach
			  </select>
			</div>
  		</div>
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>Sale Type:</label>
			  <select class="form-control" name="sale_type" id="sale_type">
			  		<option value="Cash" @if($saleregister[0]->sale_type=='Cash') selected="" @endif>Cash</option>
			  		<option value="Credit" @if($saleregister[0]->sale_type=='Credit') selected="" @endif>Credit</option>
			  </select>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-3">
  			<div class="form-group">
			  <label>Category:</label>
			  <select style="border: none; padding: 0px" class="form-control basic-select" name="category" id="category">
			  		<option value="">Select</option>
			  		@foreach ($category as $value)
			  		<option value="{{ $value->id }}">{{ $value->name }}</option>
			  		@endforeach
			  </select>
			</div>
  		</div>
		<div class="col-md-3">
		  	<div class="form-group">
			  <label>Brand:</label>
			  <select style="border: none; padding: 0px" class="form-control basic-select" name="brand" id="brand" onChange="getItemsbyBrand(this.value);">
			  		<option value="">Select</option>
					@foreach ($brand as $value)
		      		@if (old('brand') == $value->id)
					      <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
					@else
					      <option value="{{ $value->id }}">{{ $value->name }}</option>
					@endif
		      		@endforeach
			  </select>
			</div>
  		</div>
  		<div class="col-md-3">
  			<div class="form-group">
			  <label>Item:</label>
			  <select style="border: none; padding: 0px" class="form-control basic-select" name="item" id="item" onChange="addItems(this.value);">
			  		<option value="">Select</option>
			  </select>
			</div>
  		</div>
  		<div class="col-md-3">
  			<div class="form-group">
			  <label>Barcode:</label>
			  <div class="input-group">
				<input type="text" class="form-control" name="barcode_search" id="barcode_search"><span class="input-group-addon search"><i class="fa fa-search"></i></span>
			  </div>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="table-responsive">
				<table class="table table-bordered" id="itemTable">
					<tr>
		                <th>Name</th>
		                <th>Strength</th>
		                <th>Unit</th>
		                <th>Brand</th>
		                <th>Form</th>
		                <th>In Stock</th>
		                <th>Barcode</th>
		                <th style="width: 180px">Expiry Date</th>
		                <th>Quantity</th>
		                <th>Cost</th>
		                <th>Total</th>
		                <th width="100px">Action</th>
		            </tr>
		            <tbody class="field_wrapper">
						@foreach($saleregister_item as $value)
						<tr><td class="center"><span>{{$value->item_name}}</span><input type="hidden" name="item_id[]" value="{{$value->item_id}}"></td>
						<td class="center"><span>{{$value->strength}}</span><input type="hidden" name="strength[]" value="{{$value->strength}}"></td>
						<td class="center"><span>{{$value->unit_name}}</span></td>
						<td class="center"><span>{{$value->brand_name}}</span></td>
						<td class="center"><span>{{$value->form_name}}</span><input type="hidden" name="form_id[]" value="{{$value->form_id}}"></td>
						<td class="center"><span>{{$value->in_stock}}</span><input type="hidden" name="in_stock[]" value="{{$value->in_stock}}"></td>
						<td class="center"><input type="text" class="form-control" name="barcode[]" value="{{$value->barcode}}"></td>
						<td class="center"><input type="text" class="form-control date" name="expiry_date[]" value="{{date_dfy($value->expiry_date)}}"></td>
						<td class="center qty"><input class="form-control qtyVal" type="number" value="{{$value->quantity}}" name="quantity[]" min="1"></td>
						<td class="center rate"><input class="form-control rateVal" type="number" value="{{$value->rate}}" name="rate[]" min="0" step="0.01"></td>
						<td class="left-align center amount"><span class="amountText">{{$value->amount}}</span><input class="amountVal" type="hidden" name="amount[]" value="{{$value->amount}}"></td>
						<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td>
						</tr>
						@endforeach
						<tr>
							<td colspan="10" class="right-align">Sub-Total:</td>
							<td colspan="2" class="left-align subtotalText"><b>{{$saleregister[0]->sub_total}}</b></td>
			            </tr>
			            <tr>
							<td colspan="10" class="right-align">(-) Discount:</td>
							<td colspan="2" class="left-align discountText"><input type="number" class="form-control" name="discount" id="discount" value="{{$saleregister[0]->discount}}" min="0" step="0.01" autocomplete="off"></td>
			            </tr>
			            <tr>
							<td colspan="10" class="right-align">(+) Tax:</td>
							<td colspan="2" class="left-align taxText"><b>{{$saleregister[0]->tax}}</b></td>
			            </tr>
			            <tr>
							<td colspan="10" class="right-align">Total:</td>
							<td colspan="2" class="left-align totalText"><b>{{$saleregister[0]->total}}</b></td>
			            </tr>
					</tbody>
				</table>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="form-group">
			  <label>Ref:</label>
			  <input type="text" class="form-control" placeholder="Enter reference" name="ref" id="ref" value="{{$saleregister[0]->ref}}">
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="form-group">
			  <label>Remarks:</label>
			  <input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{$saleregister[0]->remarks}}">
			</div>
  		</div>
  	</div>
	<button type="submit" class="btn btn-primary">Update</button>
	</form>
</div>
</div>
@endsection