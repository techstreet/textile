@extends('layouts.website')
@section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
?>
<script type='text/javascript'>
$(function(){
	$(".basic-select").select2();
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.input-group.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	$('.field_wrapper').on('keyup change', '.qtyVal', function(e){
		e.preventDefault();
			quantity = $(this).val();
			rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var tax = 0;

			$('.taxAText').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));

			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	$('.field_wrapper').on('keyup change', '.rateVal', function(e){
		e.preventDefault();
			rate = $(this).val();
			quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var tax = 0;

			$('.taxAText').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;
			var final_total = total + tax;

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	$('.field_wrapper').on('keyup change', '#discount', function(e){
		e.preventDefault();
			var sub_total = parseFloat($('.subtotalText').text());
			var discount = $('#discount').val();
			var cgst = parseFloat($('.cgstText').text());
			var sgst = parseFloat($('.sgstText').text());
			var igst = parseFloat($('.igstText').text());
			var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	$('.field_wrapper').on('keyup change', '#other_expense', function (e) {
		e.preventDefault();
		var sub_total = parseFloat($('.subtotalText').text());
		var discount = $('#discount').val();
		var cgst = parseFloat($('.cgstText').text());
		var sgst = parseFloat($('.sgstText').text());
		var igst = parseFloat($('.igstText').text());
		var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

		var other_expense = $('#other_expense').val();
		var taxable = sub_total - discount + parseFloat(other_expense);
		$('.taxableText').text(parseFloat(taxable).toFixed(2));

		var cgst_percentage = 0;
		var sgst_percentage = 0;
		var igst_percentage = 0;

		var gstin = $('#gstin').val();
		var f2 = gstin.substring(0, 2);
		if(gstin !=''){
			if(f2=='07'){
				cgst_percentage = {{$cgst_percentage}};
				sgst_percentage = {{$sgst_percentage}};
			}
			else{
				igst_percentage = {{$igst_percentage}};
			}
		}

		var cgst = taxable*cgst_percentage/100;
		var sgst = taxable*sgst_percentage/100;
		var igst = taxable*igst_percentage/100;

		var final_total = taxable+cgst+sgst+igst;

		$('.taxableText').text(parseFloat(taxable).toFixed(2));
		$('.cgstText').text(parseFloat(cgst).toFixed(2));
		$('#cgstA').val(parseFloat(cgst).toFixed(2));
		$('.sgstText').text(parseFloat(sgst).toFixed(2));
		$('#sgstA').val(parseFloat(sgst).toFixed(2));
		$('.igstText').text(parseFloat(igst).toFixed(2));
		$('#igstA').val(parseFloat(igst).toFixed(2));
		$('.totalText').text(parseFloat(final_total).toFixed(2));

	});
	$('.field_wrapper').on('click', '.remove_button', function(e){
		e.preventDefault();
			$(this).closest("tr").remove();
			x--;
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

		var discount = $('#discount').val();

			var sub_total = 0;
			var total = 0;
			var cgst = 0;
			var sgst = 0;
			var igst = 0;
			var final_total = 0;

			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});
			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));

			var taxable = 0;
			var other_expense = 0;

			if (sub_total > 0) {
				other_expense = $('#other_expense').val();
				taxable = sub_total - discount + parseFloat(other_expense);
				var cgst = taxable*cgst_percentage/100;
				var sgst = taxable*sgst_percentage/100;
				var igst = taxable*igst_percentage/100;

				var final_total = taxable+cgst+sgst+igst;
			}

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	$(".search").click(function(){
		var barcode = $('#barcode_search').val();
		if(barcode != ''){
			$.ajax({
				type: "GET",
				url: "{{url('/saleregister/barcodesearch')}}",
				data:'barcode='+barcode,
				success: function(data){
					
					var obj = JSON.parse(data);
					
					var item_id = obj[0].item_id;
					var item_name = obj[0].item_name;
					var strength = obj[0].strength;
					var unit_name = obj[0].unit_name;
					var brand_name = obj[0].brand_name;
					var form_name = obj[0].form_name;
					var stock = obj[0].stock;
					var barcode = obj[0].barcode;
					var rate = obj[0].rate;
					var expiry_append = '';

					var nameArr = item_name.split(',');
					var nameHTML = '';
					$.each( nameArr, function( index, value ) {
						nameHTML += '<option value="'+item_id+'|'+value+'">'+value+'</option>';
					});

					var quantityArr = obj[0].avail_qty.split(',');
					var av_qtyHTML = '';
					$.each( quantityArr, function( index, value ) {
						var newval = value.split('|');
						av_qtyHTML += '<option value="'+newval[0]+'">'+newval[1]+'</option>';
					});
					
					var fieldHTML = '<tr><td class="center"><select name="item_id[]" class="form-control item_id">'+nameHTML+'</select></td>' +
					// '<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" class="item_id" value="'+item_id+'"></td>'+
					'<td class="center"><span>'+unit_name+'</span></td>'+
					'<td class="center"><span>'+brand_name+'</span></td>'+
					// '<td class="center"><span>'+stock+'</span></td>'+
					'<td class="center"><select name="av_qty[]" class="form-control av_pcs">'+av_qtyHTML+'</select></td>'+
					'<td class="center qty"><input class="form-control qtyVal" type="number" name="quantity[]" required="" min="0" step="0.01"></td>'+
					'<td class="center rate"><input class="form-control rateVal" type="number" name="rate[]" value="'+rate+'" min="0" step="0.01"></td>'+
					'<td class="center amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]"></td>'+
					'<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>';
					if(stock>0){
						$('.field_wrapper').prepend(fieldHTML);
					}
					else{
						alert("This item is out of stock!");
					}
				}
			});
		}
	});
});
</script>
<script>
var maxField = 101; //Input fields increment limitation
var x = 1; //Initial field counter is 1
var quantity = '5';
function getCategory(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax')}}",
	data:'company_id='+val,
	success: function(data){
		$("#category").html(data);
		$("#item").html('<option value="">Select</option>');
	}
	});
}
function getItems(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax2')}}",
	data:'category_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function getBrands(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax4')}}",
	data:'item_name='+val,
	success: function(data){
		$("#brand").html(data);
	}
	});
}

function getItemsbyBrand(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax5')}}",
	data:'brand_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}

function in_array(array, id) {
    for(var i=0;i<array.length;i++) {
       if(array[i] == id)
        return true;
    }
    return false;
}
function addItems(val){
	
//	$('#item').val();
	val=$('#item').val();
	$(".noitems").hide();
	// var itemStr = $("input[name='item_id[]']").map(function(){return $(this).val();}).get();
	// var itemArr = JSON.parse("["+ itemStr +"]");
	// var itemAlreadyAdded = in_array(itemArr, val);
//	if(itemAlreadyAdded){
//		$("#flashmessage").append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> This item is already added!</div>');
//		setTimeout(function(){ $('.alert.alert-danger').fadeOut(300)}, 3000);
//		return false;
//	}
	if(val != ''){
		$.ajax({
			type: "GET",
			url: "{{url('/saleregister/additems')}}",
			data:'item_id='+val,
			success: function(data){
					
					var obj = JSON.parse(data);
					
					var item_id = obj.item_id;
					var item_name = obj.item_name;
					var strength = obj.strength;
					var unit_name = obj.unit_name;
					var brand_name = obj.brand_name;
					var form_name = obj.form_name;
					var stock = obj.stock;
					var rate = obj.rate;
					var expiry_append = '';
					var barcode_append = '';

					var nameArr = item_name.split(',');
					var nameHTML = '';
					$.each( nameArr, function( index, value ) {
						nameHTML += '<option value="'+item_id+'|'+value+'">'+value+'</option>';
					});

					var quantityArr = obj.avail_qty.split(',');
					var av_qtyHTML = '';
					$.each( quantityArr, function( index, value ) {
						var newval = value.split('|');
						av_qtyHTML += '<option value="'+newval[0]+'">'+newval[1]+'</option>';
					});
					
					$.each(obj.expiry_date, function (index, value) {
					  var expiry_date = obj.expiry_date[index];
					  expiry_append += '<option value="'+expiry_date+'">'+expiry_date+'</option>';
					});
					
					$.each(obj.barcode, function (index, value) {
					  var barcode = obj.barcode[index];
					  barcode_append += '<option value="'+barcode+'">'+barcode+'</option>';
					});
					
					var fieldHTML = '<tr><td class="center"><select name="item_id[]" class="form-control item_id">'+nameHTML+'</select></td>' +
					// '<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" class="item_id" value="'+item_id+'"></td>'+
					'<td class="center"><span>'+unit_name+'</span></td>'+
					'<td class="center"><span>'+brand_name+'</span></td>'+
					// '<td class="center"><span>'+stock+'</span></td>'+
					'<td class="center"><select name="av_qty[]" class="form-control av_pcs">'+av_qtyHTML+'</select></td>'+
					'<td class="center qty"><input class="form-control qtyVal" type="number" name="quantity[]" required="" min="0" step="0.01"></td>'+
					'<td class="center rate"><input class="form-control rateVal" type="number" name="rate[]" value="'+rate+'" min="0" step="0.01"></td>'+
					'<td class="center amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]"></td>'+
					'<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>';
					if(stock>0){
						$('.field_wrapper').prepend(fieldHTML);
					}
					else{
						alert("This item is out of stock!");
					}
				}
		});
	}
}
function showGSTIN(val){
	var gstin_string = val.split('#');
	$('#gstin').val(gstin_string[1]);
}
function showGSTIN2(val){
	var gstin_string = val.split('#');
	$('#gstin2').val(gstin_string[1]);
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-exchange"></i>
		</div>
		<div class="media-body">
			<h4 class="test">Sales Register</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/saleregister') }}">Sales Register</a>
				</li>
				<li>Add</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12" id="flashmessage">
				@include('flashmessage')
			</div>
		</div>
		<form method="post" action="{{ url('saleregister/save') }}">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>*Invoice No:</label>
						<input type="text" class="form-control" value="{{'INV_'.str_pad($invoice_no+1, 4, 0, STR_PAD_LEFT)}}" disabled="">
						<input type="hidden" name="invoice_no" id="invoice_no" value="{{'INV_'.str_pad($invoice_no+1, 4, 0, STR_PAD_LEFT)}}">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>*Invoice Date:</label>
						<div class="input-group date">
							<input type="text" name="entry_date" id="entry_date" value="{{$entry_date}}" class="form-control" required="">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>*Sale Type:</label>
						<select class="form-control" name="sale_type" id="sale_type">
							<option value="Cash">Cash</option>
							<option value="Credit">Credit</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>*Client(Billing):</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="client" id="client" required="" onchange="showGSTIN(this.value);">
							<option value="">Select</option>
							@foreach ($client as $value) @if (old('client') == $value->id)
							<option value="{{ $value->id.'#'.$value->gstin }}" selected>{{ $value->name.' - '.$value->mobile }}</option>
							@else
							<option value="{{ $value->id.'#'.$value->gstin }}">{{ $value->name.' - '.$value->mobile }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>GSTIN:</label>
						<input class="form-control" type="text" name="gstin" id="gstin" value="" disabled/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>*Client(Shipping):</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="shipping_client" id="shipping_client" required="" onchange="showGSTIN2(this.value);">
							<option value="">Select</option>
							@foreach ($client as $value) @if (old('client') == $value->id)
							<option value="{{ $value->id.'#'.$value->gstin }}" selected>{{ $value->name.' - '.$value->mobile }}</option>
							@else
							<option value="{{ $value->id.'#'.$value->gstin }}">{{ $value->name.' - '.$value->mobile }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>GSTIN:</label>
						<input class="form-control" type="text" name="gstin2" id="gstin2" value="" disabled/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label>Transport:</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="transport" id="transport">
							<option value="">Select</option>
							@foreach ($transport as $value) @if (old('transport') == $value->id)
							<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
							@else
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>G.R No:</label>
						<input class="form-control" type="text" name="gr_no" id="gr_no"/>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label>Booking Date:</label>
						<div class="input-group date">
							<input type="text" name="booking_date" id="booking_date" value="{{ old('booking_date') }}" class="form-control" autocomplete="off"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label>Category:</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="category" id="category">
							<option value="">Select</option>
							@foreach ($category as $value)
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Brand:</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="brand" id="brand" onChange="getItemsbyBrand(this.value);">
							<option value="">Select</option>
							@foreach ($brand as $value) @if (old('brand') == $value->id)
							<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
							@else
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endif @endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Item:</label>
						<select style="border: none; padding: 0px" class="form-control basic-select" name="item" id="item" onChange="addItems(this.value);">
							<option value="">Select</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
					<label>Barcode:</label>
					<div class="input-group">
						<input type="text" class="form-control" name="barcode_search" id="barcode_search"><span class="input-group-addon search"><i class="fa fa-search"></i></span>
					</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered" id="itemTable">
							<tr>
								<th>Name</th>
								<th>Unit</th>
								<th>Brand</th>
								<!-- <th>In Stock</th> -->
								<th>Avail. Quantity</th>
								<th>Quantity</th>
								<th>Cost</th>
								<th>Total</th>
								<th width="80px">Action</th>
							</tr>
							<tbody class="field_wrapper">
								<tr>
									<td colspan="7" class="right-align">Sub-Total:</td>
									<td colspan="2" class="left-align subtotalText">
										<b>0.00</b>
									</td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(-) Discount:</td>
									<td colspan="2" class="left-align discountText">
										<input type="number" class="form-control" name="discount" id="discount" value="0" min="0" step="0.01" autocomplete="off">
									</td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) Other Expense:</td>
									<td colspan="2" class="left-align otherexpenseText">
										<input type="number" class="form-control" name="other_expense" id="other_expense" value="0" min="0" step="0.01" autocomplete="off">
									</td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">Taxable Amount:</td>
									<td colspan="2" class="left-align taxableText"><b>0.00</b></td>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) CGST:</td>
									<td class="left-align cgstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="cgstA" id="cgstA"/>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) SGST:</td>
									<td class="left-align sgstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="sgstA" id="sgstA"/>
								</tr>
								<tr>
									<td colspan="7" class="right-align">(+) IGST:</td>
									<td class="left-align igstText">
										<b>0.00</b>
									</td>
									<input type="hidden" name="igstA" id="igstA"/>
								</tr>
								<tr>
									<td colspan="7" class="right-align">Total:</td>
									<td colspan="2" class="left-align totalText">
										<b>0.00</b>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Ref:</label>
						<input type="text" class="form-control" placeholder="Enter reference" name="ref" id="ref" value="{{ old('ref') }}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>Remarks:</label>
						<input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{ old('remarks') }}">
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Save</button>
		</form>
	</div>
</div>
@endsection