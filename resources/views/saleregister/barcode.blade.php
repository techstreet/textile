<link href="{{ url('/css/style.default.css') }}" rel="stylesheet">
<script src="{{ url('/js/jquery-1.11.1.min.js') }}"></script>
<?php
$voucher_no = $saleregister->invoice_no;
?>
<script>
$(document).ready(function(){
	document.title = '{{$voucher_no}}';
    window.print();
});
</script>
<h4 class="center" style="text-decoration:underline">{{$saleregister->company_name}}</h4>
<h5 class="center">{{$saleregister->company_address}}</h5>
@foreach($saleregister_item as $value)
<div style="display:inline-block; margin:5px;border: 1px dotted;padding: 15px;width:300px">
<p style="margin-bottom:5px">{{$saleregister->company_name}}</p>
<p style="margin-bottom:5px">{{$value->reference}}</p>
<img src="data:image/png;base64,{{DNS1D::getBarcodePNG($value->item_id, 'C93')}}" />
<p style="margin-top:5px;margin-bottom:0px">Qty: {{$value->quantity_total}}</p>
</div>
@endforeach