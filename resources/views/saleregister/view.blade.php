@extends('layouts.website') @section('content')
<script>
	function printPage() {
		document.title = "Sale Invoice";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
		<a target="_blank" href="{{ url('/saleregister/barcodeview/'.Request::segment(3)) }}" class="btn btn-primary"><i class="fa fa-print"></i> Print Barcode</a>
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-exchange"></i>
		</div>
		<div class="media-body">
			<h4>Sales Register</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/saleregister') }}">Sales Register</a>
				</li>
				<li>View</li>
				<li>{{Request::segment(3)}}</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-9">
				<h4 class="center onlyprint">
					<u>{{$company->name}}</u>
				</h4>
				<h5 class="center onlyprint">{{$company->address}}</h5>
				<h5 class="center onlyprint"><b>GSTIN:</b> {{$company->gstin}}</h5>
				@if(!empty($company->phone) && !empty($company->mobile) )
				<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}, <b>Mobile:</b> {{$company->mobile}}</h5>
				@else
					@if(!empty($company->phone))
					<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}</h5>
					@else
					<h5 class="center onlyprint"><b>Mobile:</b> {{$company->mobile}}</h5>
					@endif
				@endif
				
				<table class="table table-bordered">
					<tr>
						<th class="left-align">Invoice Details</th>
						<th class="left-align">Transport Details</th>
					</tr>
					<tr>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Invoice No:</b> {{$saleregister[0]->invoice_no}}</p>
							<p><b>Invoice Date:</b> {{date_dfy($saleregister[0]->entry_date)}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Transport:</b> {{$saleregister[0]->transport_name}}</p>
							<p><b>GR No:</b> {{$saleregister[0]->gr_no}}</p>
							<p><b>Booking Date:</b> {{date_dfy($saleregister[0]->booking_date)}}</p>
						</td>
					</tr>
					<tr>
						<th class="left-align">Billing Details</th>
						<th class="left-align">Shipping Details</th>
					</tr>
					<tr>
						<td class="left-align">
							<p><b>Client Name:</b> {{$saleregister[0]->client_name}}</p>
							<p><b>Phone:</b> {{$saleregister[0]->client_mobile}}</p>
							<p><b>GSTIN:</b> {{$saleregister[0]->client_gstin}}</p>
							<p><b>State Code:</b> {{substr($saleregister[0]->client_gstin, 0, 2)}}</p>
							<p><b>Address:</b> {{$saleregister[0]->client_address}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Client Name:</b> {{$shipping_client->client_name}}</p>
							<p><b>Phone:</b> {{$shipping_client->client_mobile}}</p>
							<p><b>GSTIN:</b> {{$shipping_client->client_gstin}}</p>
							<p><b>State Code:</b> {{substr($shipping_client->client_gstin, 0, 2)}}</p>
							<p><b>Address:</b> {{$shipping_client->client_address}}</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="table-responsive">
					<table class="table table-bordered mb30">
						<tbody>
							<tr>
								<td width="50px" class="left-align">
									<b>S.No</b>
								</td>
								<td class="left-align">
									<b>Name</b>
								</td>
								<td class="left-align">
									<b>HSN Code</b>
								</td>
								<td class="left-align">
									<b>Pcs</b>
								</td>
								<td class="left-align">
									<b>Quantity</b>
								</td>
								<td class="left-align">
									<b>Cost</b>
								</td>
								<td class="left-align">
									<b>Total</b>
								</td>
							</tr>
							<?php $i=1; foreach ($saleregister_item as $value){ ?>
							<tr>
								<td class="left-align">{{ $i }}</td>
								<td class="left-align">{{ $value->reference }}</td>
								<td class="left-align">{{ $value->hsn_code }}</td>
								<td class="left-align">{{ $value->pcs_total }}</td>
								<td class="left-align">{{ $value->quantity_total }}</td>
								<td class="left-align">{{ $value->rate }}</td>
								<td class="left-align">{{ $value->amount }}</td>
							</tr>
							<?php $i++; }?>
							<tr>
								<td colspan="6" class="right-align">Sub-Total:</td>
								<td class="left-align">
									<b>{{$saleregister[0]->sub_total}}</b>
								</td>
							</tr>
							@if($saleregister[0]->discount > 0)
							<tr>
								<td colspan="6" class="right-align">Discount:</td>
								<td class="left-align">
									<b>{{$saleregister[0]->discount}}</b>
								</td>
							</tr>
							@endif
							@if($saleregister[0]->other_expense > 0)
							<tr>
								<td colspan="6" class="right-align">Other Expense:</td>
								<td class="left-align">
									<b>{{$saleregister[0]->other_expense}}</b>
								</td>
							</tr>
							@endif
							@if($saleregister[0]->cgst > 0)
							<tr>
								<td colspan="6" class="right-align">CGST:</td>
								<td class="left-align">
									<b>{{$saleregister[0]->cgst}}</b>
								</td>
							</tr>
							@endif
							@if($saleregister[0]->sgst > 0)
							<tr>
								<td colspan="6" class="right-align">SGST:</td>
								<td class="left-align">
									<b>{{$saleregister[0]->sgst}}</b>
								</td>
							</tr>
							@endif
							@if($saleregister[0]->igst > 0)
							<tr>
								<td colspan="6" class="right-align">IGST:</td>
								<td class="left-align">
									<b>{{$saleregister[0]->igst}}</b>
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="6" class="right-align">Total:</td>
								<td class="left-align">
									<b>{{$saleregister[0]->total}}</b>
								</td>
							</tr>
						</tbody>
					</table>
					<br/><br/>
					<table class="table" style="border-top:1px dotted #ddd">
					<tr>
						<th class="left-align" style="border:none">Received By</th>
						<th class="right-align" style="border:none">Authorised Signatory</th>
					</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection