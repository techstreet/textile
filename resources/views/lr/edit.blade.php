@extends('layouts.website')
@section('content')
<div class="pageheader">
		<div class="media">
        <div class="media-body">
    	<h4>L.R</h4>
        <ul class="breadcrumb">
            <li><a href="{{url('')}}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{url('/lr')}}">L.R</a></li>
            <li>Edit</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-4">
		@include('flashmessage')
      	<form method="post" action="{{ url('/lr/update/'.$lr->id) }}">
      	{{ csrf_field() }}
		  <div class="form-group">
		    <label>L.R No:</label>
		    <input type="text" class="form-control" value="{{ $lr->lr_no }}" disabled>
		  </div>
		  <div class="form-group">
		    <label>Purchase Voucher:</label>
		    <input type="text" class="form-control" value="{{ $lr->purchasevoucher_no }}" disabled>
		  </div>
		  <div class="form-group">
		    <label>*Amount:</label>
		    <input type="number" class="form-control" placeholder="Enter amount" name="amount" id="amount" value="{{ $lr->amount }}" step="0.01" min="0" required="">
		  </div>
		  <div class="form-group">
		    <label>*Payment Status:</label>
			<select class="form-control" name="payment_status" required="">
				<option value="0" @if($lr->payment_status == '0') selected @endif>Pending</option>
				<option value="1" @if($lr->payment_status == '1') selected @endif>Complete</option>
			</select>
		  </div>
		  <button type="submit" class="btn btn-primary">Update</button>
		</form>
		</div>
	</div>
</div>
</div>
@endsection