@extends('layouts.website')
@section('content')
<script>
$(function(){
$(".basic-select").select2();
});
function printPage() {
    document.title = "Stock Summary Report";
    window.print();
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Stock Summary Report</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Reports</li>
		    <li>Stock Summary Report</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<form class="form-inline" method="post" action="">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-12">
				<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="category" id="category" placeholder="Select Category">
						<option value="">Select Category</option>
						@foreach ($category as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $category_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<div class="input-group" style="display: none">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="form" id="form" placeholder="Select Form">
						<option value="">Select Form</option>
						@foreach ($form as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $form_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<div class="input-group" style="display: none">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="manufacturer" id="manufacturer" placeholder="Select Manufacturer">
						<option value="">Select Manufacturer</option>
						@foreach ($manufacturer as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $manufacturer_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="brand" id="brand" placeholder="Select Brand">
						<option value="">Select Brand</option>
						@foreach ($brand as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $brand_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
				</div>
				<button type="submit" name="submit" class="btn btn-info">Find</button>
			</div>
		</div>
	</form>
	<?php if($_POST && $_POST > 0){ ?>     
    <div class="row" style="margin-top: 10px">
	    <div class="col-md-12">
	    @include('flashmessage')
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th rowspan="2" width="50px">S.No</th>
		        <th rowspan="2">Item</th>
		        <th rowspan="2">Category</th>
		        <th rowspan="2">Brand</th>
		        <th rowspan="2">Open Stock</th>
		        <th colspan="2">Purchase</th>
		        <th colspan="2">Purchase Return</th>
		        <th colspan="2">Sale</th>
		        <th colspan="2">Sale Return</th>
		        <th rowspan="2">Close Stock</th>
		        @if(Auth::user()->user_group !='3')
		        <th rowspan="2">Unit Price</th>
		        <th rowspan="2">Stock Value</th>
		        @endif
			  </tr>
			  <tr>
		        <th>Pcs</th>
				<th>Qty</th>
				<th>Pcs</th>
				<th>Qty</th>
				<th>Pcs</th>
				<th>Qty</th>
				<th>Pcs</th>
				<th>Qty</th>
			  </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; 
								   
		   	if($count>0){

				foreach ($stockreport as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->name }}</td>
	            <td>{{$value->category_name}}</td>
	            <td>{{$value->brand_name}}</td>
				<td>{{ $value->opening_stock }}</td>
				<td>{{ $value->purchase_pcs }}</td>
				<td>{{ $value->purchase }}</td>
				<td>{{ $value->purchase_return_pcs }}</td>
				<td>{{ $value->purchase_return }}</td>
				<td>{{ $value->sale_pcs }}</td>
				<td>{{ $value->sale }}</td>
				<td>{{ $value->sale_return_pcs }}</td>
				<td>{{ $value->sale_return }}</td>
	            <td>{{ $value->closing_stock }}</td>
	            @if(Auth::user()->user_group !='3')
	            <td>{{ $value->unit_price }}</td>
	            <td>{{ $value->stock_value }}</td>
	            @endif
	          </tr>
	          <?php $i++; }
			}
		   	else{
				?>
	          <tr>
	            <td colspan="11">There is no Record Found.</td>
	          </tr>				
				<?php
				}?>
		    </tbody>
		  </table>
		</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
@endsection