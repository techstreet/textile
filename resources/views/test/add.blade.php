@extends('layouts.website')
@section('content')
<style>
	.typeahead .dropdown-menu{
		width: 100%;
	}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>  
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
      	<form method="post" action="">
      	{{ csrf_field() }}
		  <div class="form-group">
		    <label>*Name:</label>
		    <input type="text" class="form-control" name="query" id="query">
		  </div>
		</form>
      </div>
	</div>
</div>
</div>
<script type="text/javascript">
    var url = "{{ url('/autocomplete') }}";
    $('#query').typeahead({
        source:  function (query, process) {
        return $.get(url, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
@endsection