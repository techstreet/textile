@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
            <i style="padding: 8px 0 0 0" class="fa fa-users"></i>
        </div>
		<div class="media-body">
		<h4>Users</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li><a href="{{ url('/users') }}">Users</a></li>
		    <li>Edit</li>
		    <li>{{Request::segment(3)}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-6">
		@include('flashmessage')
      	<form method="post" action="{{ url('/users/update/'.$users[0]->id) }}" enctype="multipart/form-data">
      	{{ csrf_field() }}
		  <div class="form-group">
		    <label>*Name:</label>
		    <input type="text" class="form-control" placeholder="Enter name" name="name" id="name" value="<?php echo $users[0]->name; ?>" required="">
		  </div>
		  <div class="form-group">
		      <label>*Gender:</label>
		      <div class="radio">
		      <label><input type="radio" name="gender" value="Male" <?php if($users[0]->gender=='Male'){ ?> checked="" <?php }?>> Male</label>
		      <label><input type="radio" name="gender" value="Female" <?php if($users[0]->gender=='Female'){ ?> checked="" <?php }?>> Female</label>
		      </div>
		  </div>
		  <div class="form-group">
		    <label>Image:</label>
		    <input type="file" class="form-control" name="image" id="image">
		    <?php if($users[0]->image != ''){ ?>
	          <img style="margin-top: 10px; max-height: 100px" src="{{url('/images/profile/'.$users[0]->image)}}"/>
	        <?php } ?>
		  </div>
		  <div class="form-group">
		    <label>*Email:</label>
		    <input type="email" class="form-control" placeholder="Enter email" name="email" id="email" value="<?php echo $users[0]->email; ?>" required="">
		  </div>
		  <div class="form-group">
			<label>*Password:</label>
			<input type="password" class="form-control" name="password" id="password" placeholder="Enter new password" required="">
		  </div>
		  <div class="form-group">
			<label>*Confirm Password:</label>
			<input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm new password" required="">
		  </div>
		  <div class="form-group">
		    <label>Mobile:</label>
		    <input type="text" class="form-control" placeholder="Enter mobile" name="mobile" id="mobile" value="<?php echo $users[0]->mobile; ?>" pattern="[0-9]{10}" title="Please enter 10 digit mobile number" maxlength="10">
		  </div>
		  <div class="form-group">
		    <label>Address:</label>
		    <textarea rows="4" class="form-control" placeholder="Enter address" name="address" id="address" maxlength="400"><?php echo $users[0]->address; ?></textarea>
		  </div>
		  <div style="text-align: right" id="address_characters"></div>
		  <button type="submit" class="btn btn-primary">Update</button>
		</form>
      </div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    var text_max = 400;
    var text_length = $('#address').val().length;
    var text_remaining = text_max - text_length;
    $('#address_characters').html(text_remaining + ' characters left');

    $('#address').keyup(function() {
        var text_length = $('#address').val().length;
        var text_remaining = text_max - text_length;

        $('#address_characters').html(text_remaining + ' characters left');
    });
});
</script>
@endsection