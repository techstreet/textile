@extends('layouts.website')
@section('content')
<div class="pageheader">
		<div class="media">
        <div class="pageicon pull-left">
            <i style="padding: 8px 0 0 0" class="fa fa-users"></i>
        </div>
        <div class="media-body">
    	<h4>Users</h4>
        <ul class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
            <li><a href="{{ url('/users') }}">Users</a></li>
            <li>View</li>
            <li>{{Request::segment(3)}}</li>
        </ul>
    	</div>
    </div>
</div>
<div class="contentpanel">                  
<div class="row">
    <div class="col-sm-4 col-md-3" style="margin-bottom: 20px">
        <div class="text-center">
        	<?php if($users[0]->image !=''){ ?>
            <img style="border-radius: 0" src="{{url('/images/profile/'.$users[0]->image)}}" class="img-circle img-offline img-responsive img-profile" alt="" />
            <?php } else{?>
            <img style="border-radius: 0" src="{{url('/images/default_thumb.png')}}" class="img-circle img-offline img-responsive img-profile" alt="" />
            <?php }?>
            <h4 class="profile-name mb5"><?php echo $users[0]->name; ?></h4>
            <div><i class="fa fa-briefcase"></i> <?php echo $users[0]->company_name; ?></div>
        </div>
        <br />
        <ul class="list-unstyled social-list">
        	<li><i class="fa fa-{{strtolower($users[0]->gender)}}"></i><b>Gender : </b><?php echo $users[0]->gender; ?></li>
        	<li><i style="font-size: 10px" class="fa fa-envelope"></i><b>Email : </b><?php echo $users[0]->email; ?></li>
        	<li><i class="fa fa-phone"></i><b>Mobile : </b><?php echo $users[0]->mobile; ?></li>
        	<li><i class="fa fa-map-marker"></i><b>Address : </b><?php echo $users[0]->address; ?></li>
        </ul>
    </div>
</div>
</div>
@endsection