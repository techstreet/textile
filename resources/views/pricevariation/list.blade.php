@extends('layouts.website')
@section('content')
<?php
$from_date = '';
$to_date = '';
if($_POST && $_POST>0){
	$from_date = $_POST['from_date'];
	$to_date = $_POST['to_date'];
}
?>
<script>
$(function(){
$(".basic-select").select2();
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
$('.input-group.date').datepicker({
    calendarWeeks: true,
    todayHighlight: true,
    autoclose: true,
    format: "dd-MM-yyyy",
    //startDate: today
});  
});
function printPage() {
    document.title = "Price Variation Report";
    window.print();
}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
            <i class="fa fa-file-text-o"></i>
        </div>
		<div class="media-body">
		<h4>Price Variation Report</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Reports</li>
		    <li>Price Variation Report</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">
	<div class="row">
		<div class="col-md-12">
			<form class="form-inline" method="post" action="">
			{{ csrf_field() }}
				<div class="form-group" style="margin-right: 0">
					<div class="input-group">
					<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="item" id="item" placeholder="Select Item">
						<option value="">Select Item</option>
						@foreach ($item as $value)
						<?php
						$selected = '';
						if($_POST){
							if($value->id == $item_selected){
								$selected = 'selected';
							}
						}
						?>
				  		<option value="{{ $value->id }}"{{$selected}}>{{ $value->name }}</option>
				  		@endforeach
					</select>
					</div>
					<div class="input-group date">
						<input type="text" name="from_date" class="form-control" value="{{$from_date}}" placeholder="From Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
					<div class="input-group date">
						<input type="text" name="to_date" class="form-control" value="{{$to_date}}" placeholder="To Date" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					</div>
				</div>
				<button type="submit" name="submit" class="btn btn-info">Find</button>
			</form>
		</div>
	</div>
	<?php if($_POST && $_POST > 0){ ?>     
    <div class="row" style="margin-top: 10px">
	    <div class="col-md-12">
	    @include('flashmessage')
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Date</th>
						<th>Item</th>
		        <th>Category</th>
		        <th>Brand</th>
						<th>Quantity</th>
		        <th>Price</th>
		      </tr>
		    </thead>
		    <tbody>
				
		      <?php $i=1; 
				if($count>0){
					foreach ($pricevariation as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
							<td>{{date_dfy($value->entry_date)}}</td>
	            <td>{{$value->name}}</td>
	            <td>{{$value->category_name}}</td>
	            <td>{{$value->brand_name}}</td>
							<td>{{$value->quantity}}</td>
	            <td>{{$value->rate}}</td>
	          </tr>
	          <?php $i++; } 			
				}
		   	else{
				?>
	          <tr>
	            <td colspan="7">There is no Record Found.</td>
	          </tr>				
				<?php
				}?>

		    </tbody>
		  </table>
		</div>
		</div>
	</div>
	<?php } ?>
</div>
</div>
@endsection