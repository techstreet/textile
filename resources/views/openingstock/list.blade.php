@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<a href="{{ url('/openingstock/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add Opening Stock</button></a>
		</div>
		<div class="media-body">
		<h4>Opening Stocks</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Masters</li>
		    <li>Opening Stocks</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Item</th>
		        <th>Brand</th>
		        <th>Barcode</th>
		        <th>Expiry Date</th>
		        <th>Quantity</th>
		        <th>Cost</th>
		        <th>Amount</th>
		        <th>Date</th>
		        @if(session('superadmin') == '1')
		        <th width="100px" style="min-width: 100px;">Action</th>
		        @endif
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($openingstock as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->item_name }}</td>
	            <td>{{ $value->brand_name }}</td>
	            <td>{{ $value->barcode }}</td>
	            <td>{{ date_dfy($value->expiry_date) }}</td>
	            <td>{{ slash_decimal($value->quantity) }}</td>
	            <td>{{ $value->rate }}</td>
	            <td>{{ $value->amount }}</td>
	            <td>{{ date_dfy($value->on_date) }}</td>
	            @if(session('superadmin') == '1')
	            <td>
	            <a href="{{ url('/openingstock/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
	            <a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/openingstock/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
	            </td>
	            @endif
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    });
});
</script>
@endsection