@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
            <i style="padding: 8px 0 0 0" class="fa fa-user"></i>
        </div>
		<div class="pull-right">
			<a href="{{ url('/administrators/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add Company</button></a>
		</div>
		<div class="media-body">
		<h4>Companies</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Companies</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th>S.No</th>
		        <th>Name</th>
		        <th>Email</th>
		        <th>Mobile</th>
		        <th>Address</th>
		        <th>Company
		        <br />
                <select class="form-control input-sm" id="company_dropdown" style="margin-top: 10px">
				  <option value="">Select</option>
				  @foreach($company as $value)
				  <option value="{{$value->name}}">{{$value->name}}</option>
				  @endforeach
				</select>
		        </th>
		        <th width="200px" style="min-width: 200px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($administrators as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->name }}</td>
	            <td>{{ $value->email }}</td>
	            <td>{{ $value->mobile }}</td>
	            <td>{{ $value->address }}</td>
	            <td>{{ $value->company_name }}</td>
	            <td>
	            <a href="{{ url('/administrators/login/'.$value->id) }}" data-toggle="tooltip" title="Login" class="btn btn-success" data-original-title="Login"><i class="fa fa-sign-in"></i></a>
	            <a href="{{ url('/administrators/view/'.$value->id) }}" data-toggle="tooltip" title="View" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a>
	            <a href="{{ url('/administrators/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
	            <a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/administrators/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
	            </td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
	$('#dataTable').dataTable( {
	  "ordering": false
	} );
	
	var table =  $('#dataTable').DataTable();
    
    $('#company_dropdown').on('change', function () {
        table.columns(5).search( this.value ).draw();
    } );
});
</script>
@endsection