@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
	    @if(Auth::user()->user_group != '4')
		<div class="pull-right">
			<a href="{{ url('/customer/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add Customer</button></a>
		</div>
		@endif
		<div class="media-body">
		<h4>Customers</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li><a href="{{ url('/customer') }}">Customers</a></li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Company Name</th>
		        <th>Contact Name</th>
		        <th>Email</th>
		        <th>Phone</th>
		        <th>Mobile</th>
						<th>GSTIN</th>
		        <th>Address</th>
		        <th>Outstanding Balance</th>
		        <th width="100px" style="min-width: 100px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($client as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->name }}</td>
	            <td>{{ $value->contact_name }}</td>
	            <td>{{ $value->email }}</td>
	            <td>{{ $value->phone }}</td>
	            <td>{{ $value->mobile }}</td>
							<td>{{ $value->gstin }}</td>
	            <td>{{ $value->address }}</td>
	            <td>{{ getBalance($value->id)}}</td>
	            <td>
	            <a href="{{ url('/customer/account/'.$value->id) }}" data-toggle="tooltip" title="Customer Account History" class="btn btn-success" data-original-title="Customer Account History"><i class="fa fa-history"></i></a>
	            @if(Auth::user()->user_group != '4')
	            <a href="{{ url('/customer/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                @endif
@if(Auth::user()->user_group != '3')
							<!-- <a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/customer/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>  -->
							@endif
	            </td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    });
});
</script>
@endsection