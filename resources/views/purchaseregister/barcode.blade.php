<link href="{{ url('/css/style.default.css') }}" rel="stylesheet">
<script src="{{ url('/js/jquery-1.11.1.min.js') }}"></script>
<?php
$voucher_no = $purchaseregister->purchasevoucher_no;
?>
<script>
$(document).ready(function(){
	document.title = '{{$voucher_no}}';
    window.print();
});
</script>
<h4 class="center" style="text-decoration:underline">{{$purchaseregister->company_name}}</h4>
<h5 class="center">{{$purchaseregister->company_address}}</h5>
@foreach($purchaseregister_item as $value)
<div style="display:inline-block; margin:5px;border: 1px dotted;padding: 15px;width:300px">
<p style="margin-bottom:5px">{{$purchaseregister->company_name}}</p>
<p style="margin-bottom:5px">{{$value->item_name}}</p>
<img src="data:image/png;base64,{{DNS1D::getBarcodePNG($value->item_id, 'C93')}}" />
<p style="margin-top:5px;margin-bottom:0px">Qty: {{$value->quantity_total}}</p>
</div>
@endforeach