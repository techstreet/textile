@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
		<div class="media-body">
		<h4>L.R</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
				<li>Purchase Register</li>
		    <li>L.R</li>
				<li>{{Request::segment(3)}}</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
						<th>L.R No</th>
						<th>Purchase Voucher</th>
		        <th>Amount</th>
						<th>Payment Status</th>
		        @if(session('superadmin') == '1')
		        <th width="100px" style="min-width: 100px;">Action</th>
		        @endif
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($lr as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
							<td>{{ $value->lr_no }}</td>
							<td>{{ $value->purchasevoucher_no }}</td>
	            <td>{{ $value->amount }}</td>
							@if($value->payment_status == '1')
							<td><span class="label label-success">Complete</span></td>
							@else
							<td><span class="label label-warning">Pending</span></td>
							@endif
	            @if(session('superadmin') == '1')
	            <td>
	            <a href="{{ url('/lr/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
	            </td>
	            @endif
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		<p>No results found!</p>
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
// $(document).ready(function() {
//     $('#dataTable').DataTable({
//     });
// });
</script>
@endsection