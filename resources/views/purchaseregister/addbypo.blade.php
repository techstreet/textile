@extends('layouts.website') @section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
$test = 20;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script> 
<script type='text/javascript'>
	$(function () {
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.form-control.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
		// Select 2 Dropdown initialization
		$("#item").select2();
		$("#transport").select2();
		$("#category").select2();
		$("#brand").select2();
		// Upper case
		$('#purchaseorder_no').on('keyup change', function (e) {
			e.preventDefault();
			var text = $(this).val();
			$('#purchaseorder_no').val(text.toUpperCase());
		});
	// Fill by P.O
		$(".search").click(function () {
			var purchaseorder_no = $('#purchaseorder_no').val();
			var vendor = $('#vendor').val();
			//alert(vendor_id);
			if (purchaseorder_no != '') {
				$.ajax({
					type: "GET",
					url: "{{url('/purchaseregister/additemsbypo')}}",
					data: 'purchaseorder_no=' + purchaseorder_no,
					success: function (data) {
						var obj = JSON.parse(data);
						var other_vendor = obj[0][0].vendor_id;
						if(vendor == '' || vendor == other_vendor){
							$('#vendor_show').val(obj[0][0].vendor_name);
							$('#vendor').val(obj[0][0].vendor_id);
							$('#gstin').val(obj[0][0].vendor_gstin);
    
							if (obj[0].length > 0 ) {

								//$('.field_wrapper').html('');

								var poHTML = '<div class="col-md-2">'+
												'<div class="form-group input-group">'+
												'<input type="text" class="form-control poText" value="'+purchaseorder_no+'" disabled>'+
												'<input type="hidden" class="form-control poVal" name="po_arr[]" value="'+purchaseorder_no+'">'+
													'<span class="input-group-addon remove">'+
														'<i class="fa fa-times"></i>'+
													'</span>'+
												'</div>'+
											'</div>';
								$('.po_wrapper').prepend(poHTML);

								$.each(obj[0], function (index, value) {
									var item_id = obj[0][index].item_id;
									var item_name = obj[0][index].name;
									var hsn_code = obj[0][index].hsn_code;
									var brand_name = obj[0][index].brand_name;
									var ordered_quantity = obj[0][index].quantity;
									var ordered_rate = obj[0][index].rate;
									var unit_id = obj[0][index].unit_id;
									var unit_name = obj[0][index].unit_name;

									if(hsn_code){
										hsn_code = obj[0][index].hsn_code;
									}
									else{
										hsn_code = '';
									}

									var fieldHTML = '<tr class="'+purchaseorder_no+'"><td class="center"><span>' + item_name + '</span><input type="hidden" name="item_id[]" class="item_id" value="' + item_id + '"></td>' +
									'<td class="center"><span>' + unit_name + '</span><input type="hidden" name="unit_id[]" value="' + unit_id + '"></td>' +
									'<td class="center"><span>' + brand_name + '</span></td>' +
									'<td class="center qty"><input class="form-control hsnVal" type="text" name="hsn_code[]" value="'+hsn_code+'"></td>' +
									'<td class="center"><span>' + ordered_quantity + '</span></td>' +
									'<td class="center qty"><input class="form-control qtyVal" type="number" name="quantity[]" value="'+ordered_quantity+'" min="0" step="0.01"></td>' +
									'<td class="center rate"><input class="form-control rateVal" type="number" name="rate[]" value="'+ordered_rate+'" min="0" step="0.01"></td>' +
									'<td class="center amount"><span class="amountText">'+ordered_quantity*ordered_rate+'</span><input class="amountVal" type="hidden" name="amount[]" value="'+ordered_quantity*ordered_rate+'"></td>' +
									'</tr>';
									$('.field_wrapper').prepend(fieldHTML);
								});

								var sub_total = 0;
								$('.amountText').each(function () {
									sub_total = sub_total + parseFloat($(this).text());
								});
								var discount = $('#discount').val();
								var other_expense = $('#other_expense').val();
								var taxable = sub_total - discount + parseFloat(other_expense);
								var cgst_percentage = 0; var sgst_percentage = 0; var igst_percentage = 0;
								var gstin = $('#gstin').val();
								var f2 = gstin.substring(0, 2);
								if(gstin !=''){
									if(f2=='07'){
										cgst_percentage = {{$cgst_percentage}};
										sgst_percentage = {{$sgst_percentage}};
									}
									else{
										igst_percentage = {{$igst_percentage}};
									}
								}
								var cgst = taxable*cgst_percentage/100; var sgst = taxable*sgst_percentage/100; var igst = taxable*igst_percentage/100;
								var final_total = taxable+cgst+sgst+igst;

								$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
								$('.taxableText').text(parseFloat(taxable).toFixed(2));
								$('.cgstText').text(parseFloat(cgst).toFixed(2));
								$('#cgstA').val(parseFloat(cgst).toFixed(2));
								$('.sgstText').text(parseFloat(sgst).toFixed(2));
								$('#sgstA').val(parseFloat(sgst).toFixed(2));
								$('.igstText').text(parseFloat(igst).toFixed(2));
								$('#igstA').val(parseFloat(igst).toFixed(2));
								$('.totalText').text(parseFloat(final_total).toFixed(2));

							}
							else {
								alert('Something went wrong');
							}
						}
						else{
							alert('Vendor name mismatch');
						}
					}
				});
			}
		});

		//Item Table
		var quantity = 0;
		var rate = 0;

		$('.field_wrapper').on('keyup change', '.qtyVal', function (e) {
			e.preventDefault();
			quantity = $(this).val();
			rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var tax = 0;

			$('.taxAText').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));

			$('.totalText').text(parseFloat(final_total).toFixed(2));
		});

		$('.field_wrapper').on('keyup change', '#discount', function (e) {
			e.preventDefault();
			var sub_total = parseFloat($('.subtotalText').text());
			var discount = $('#discount').val();
			var cgst = parseFloat($('.cgstText').text());
			var sgst = parseFloat($('.sgstText').text());
			var igst = parseFloat($('.igstText').text());
			var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);
			$('.taxableText').text(parseFloat(taxable).toFixed(2));

			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));

		});

		$('.field_wrapper').on('keyup change', '#other_expense', function (e) {
			e.preventDefault();
			var sub_total = parseFloat($('.subtotalText').text());
			var discount = $('#discount').val();
			var cgst = parseFloat($('.cgstText').text());
			var sgst = parseFloat($('.sgstText').text());
			var igst = parseFloat($('.igstText').text());
			var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);
			$('.taxableText').text(parseFloat(taxable).toFixed(2));

			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));

		});
	
		$('.field_wrapper').on('keyup change', '.rateVal', function (e) {
			e.preventDefault();
			rate = $(this).val();
			quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var tax = 0;

			$('.taxAText').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;
			var final_total = total + tax;

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);
			$('.taxableText').text(parseFloat(taxable).toFixed(2));

			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
		});

		$('.po_wrapper').on('click', '.remove', function (e) {
			e.preventDefault();
			$(this).closest("div").parent().remove();
			var po_no = $(this).closest("div").find(".poVal").val();
			$('.' + po_no).remove();

			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var discount = $('#discount').val();

			var sub_total = 0;
			var total = 0;
			var cgst = 0;
			var sgst = 0;
			var igst = 0;
			var final_total = 0;

			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});
			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));

			var taxable = 0;
			var other_expense = 0;

			if (sub_total > 0) {
				other_expense = $('#other_expense').val();
				taxable = sub_total - discount + parseFloat(other_expense);

				var cgst = taxable*cgst_percentage/100;
				var sgst = taxable*sgst_percentage/100;
				var igst = taxable*igst_percentage/100;

				var final_total = taxable+cgst+sgst+igst;
			}

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
		}); 
	});
</script>
<script>
	var maxField = 101; //Input fields increment limitation
	var x = 1; //Initial field counter is 1
	function getCategory(val) {
		$.ajax({
			type: "GET",
			url: "{{url('/openingstock/ajax')}}",
			data: 'company_id=' + val,
			success: function (data) {
				$("#category").html(data);
				$("#item").html('<option value="">Select</option>');
			}
		});
	}
	function getItems(val) {
		$.ajax({
			type: "GET",
			url: "{{url('/openingstock/ajax2')}}",
			data: 'category_id=' + val,
			success: function (data) {
				$("#item").html(data);
			}
		});
	}
	function getItemsbyBrand(val) {
		$.ajax({
			type: "GET",
			url: "{{url('/openingstock/ajax5')}}",
			data: 'brand_id=' + val,
			success: function (data) {
				$("#item").html(data);
			}
		});
	}
	function in_array(array, id) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == id)
				return true;
		}
		return false;
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-shopping-cart"></i>
		</div>
		<div class="media-body">
			<h4 class="test">Purchase Register</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/purchaseregister') }}">Purchase Register</a>
				</li>
				<li>Add</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12" id="flashmessage">
				@include('flashmessage')
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			<form method="post" action="{{ url('purchaseregister/savebyitem') }}">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Voucher No:</label>
							<input type="text" class="form-control" value="{{'PV_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}" disabled="">
							<input type="hidden" name="voucher_no" id="voucher_no" value="{{'PV_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>*Voucher Date:</label>
							<input type="text" name="entry_date" id="entry_date" value="{{$entry_date}}" class="form-control date" required="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>*Bill No:</label>
							<input type="text" class="form-control" name="bill_no" id="bill_no" value="{{ old('bill_no') }}" min="1" required="">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>*Bill Date:</label>
							<input type="text" name="bill_date" id="bill_date" value="{{$entry_date}}" class="form-control date" required="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>Transport:</label>
							<select style="border: none; padding: 0px" class="form-control basic-select" name="transport" id="transport">
								<option value="">Select</option>
								@foreach ($transport as $value) @if (old('transport') == $value->id)
								<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
								@else
								<option value="{{ $value->id }}">{{ $value->name }}</option>
								@endif @endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>L.R No:</label>
							<input class="form-control" type="text" name="lr_no" id="lr_no" placeholder="Enter comma seperated L.R No"/>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Challan No:</label>
							<input class="form-control" type="text" name="challan_no" id="challan_no"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label>P.O Number:</label>
							<div class="input-group">
								<input type="text" class="form-control" name="purchaseorder_no" id="purchaseorder_no" value="{{ old('purchaseorder_no') }}" autocomplete="off">
								<span class="input-group-addon search">
									<i class="fa fa-search"></i>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Vendor Name:</label>
							<!-- <select style="border: none; padding: 0px" class="form-control" name="vendor" id="vendor" required="" onchange="showGSTIN(this.value);">
								<option value="">Select</option>
								@foreach ($vendor as $value) @if (old('vendor') == $value->id)
								<option value="{{ $value->id.'#'.$value->gstin }}" selected>{{ $value->name }} - {{ $value->mobile }}</option>
								@else
								<option value="{{ $value->id.'#'.$value->gstin }}">{{ $value->name }} - {{ $value->mobile }}</option>
								@endif @endforeach
							</select> -->
							<input class="form-control" type="text" name="vendor_show" id="vendor_show" disabled/>
							<input class="form-control" type="hidden" name="vendor" id="vendor"/>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>GSTIN:</label>
							<input class="form-control" type="text" name="gstin" id="gstin" value="" disabled/>
						</div>
					</div>
				</div>
				<div class="row po_wrapper">

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered" id="itemTable">
								<tr>
									<th>Name</th>
									<th>Unit</th>
									<th>Brand</th>
									<th>HSN Code</th>
									<th>Ordered Quantity</th>
									<th>Quantity</th>
									<th>Cost</th>
									<th>Amount</th>
								</tr>
								<tbody class="field_wrapper">
								</tbody>
								<tbody class="field_wrapper2">
									<tr>
										<td colspan="7" class="right-align">Sub-Total:</td>
										<td class="left-align subtotalText">
											<b>0.00</b>
										</td>
									</tr>
									<tr>
										<td colspan="7" class="right-align">(-) Discount:</td>
										<td class="left-align discountText">
											<input type="number" class="form-control" name="discount" id="discount" value="0" min="0" step="0.01" autocomplete="off">
										</td>
									</tr>
									<tr>
										<td colspan="7" class="right-align">(+) Other Expense:</td>
										<td class="left-align otherexpenseText">
											<input type="number" class="form-control" name="other_expense" id="other_expense" value="0" min="0" step="0.01" autocomplete="off">
										</td>
									</tr>
									<tr>
										<td colspan="7" class="right-align">Taxable Amount:</td>
										<td class="left-align taxableText"><b>0.00</b></td>
									</tr>
									<tr>
										<td colspan="7" class="right-align">(+) CGST:</td>
										<td class="left-align cgstText">
											<b>0.00</b>
										</td>
										<input type="hidden" name="cgstA" id="cgstA"/>
									</tr>
									<tr>
										<td colspan="7" class="right-align">(+) SGST:</td>
										<td class="left-align sgstText">
											<b>0.00</b>
										</td>
										<input type="hidden" name="sgstA" id="sgstA"/>
									</tr>
									<tr>
										<td colspan="7" class="right-align">(+) IGST:</td>
										<td class="left-align igstText">
											<b>0.00</b>
										</td>
										<input type="hidden" name="igstA" id="igstA"/>
									</tr>
									<tr>
										<td colspan="7" class="right-align">Total:</td>
										<td class="left-align totalText">
											<b>0.00</b>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Remarks:</label>
							<input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{ old('remarks') }}">
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-primary">Save</button>
			</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    var url = "{{ url('/purchaseorder/autocomplete') }}";
    $('#purchaseorder_no').typeahead({
        source:  function (query, process) {
        return $.get(url, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
@endsection