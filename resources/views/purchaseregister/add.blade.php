@extends('layouts.website') @section('content')
<?php
$date = date("Y-m-d H:i:s", time());
$entry_date = date("d-F-Y");
$test = 20;
?>
<script type='text/javascript'>
	$(function () {
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.form-control.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
		// Select 2 Dropdown initialization
		$("#item").select2();
		$("#transport").select2();
		$("#vendor").select2();
		$("#category").select2();
		$("#brand").select2();
		// Upper case
		$('#purchaseorder_no').on('keyup change', function (e) {
			e.preventDefault();
			var text = $(this).val();
			$('#purchaseorder_no').val(text.toUpperCase());
		});
	// Fill by P.O
		$(".search").click(function () {
			var purchaseorder_no = $('#purchaseorder_no').val();
			if (purchaseorder_no != '') {
				$.ajax({
					type: "GET",
					url: "{{url('/purchaseregister/additemsbypo')}}",
					data: 'purchaseorder_no=' + purchaseorder_no,
					success: function (data) {
						var obj = JSON.parse(data);
						if (obj[0].length > 0) {
							$('.field_wrapper2').html('');
							$.each(obj[0], function (index, value) {
								var item_id = obj[0][index].item_id;
								var item_name = obj[0][index].item_name;
								var strength = obj[0][0].strength;
								var form_id = obj[0][0].form_id;
								var form_name = obj[0][0].form_name;
								var unit_id = obj[0][index].unit_id;
								var unit_name = obj[0][index].unit_name;
								var quantity = obj[0][index].quantity;
								var rate = obj[0][index].rate;
								$('.field_wrapper2').append('<tr><td class="center"><span>' + item_name + '</span><input type="hidden" name="item_id[]" value="' + item_id + '"></td><td class="center"><span>' + strength + '</span><input type="hidden" name="strength[]" class="strength" value="' + strength + '"></td><td class="center"><span>' + unit_name + '</span><input type="hidden" name="unit_id[]" value="' + unit_id + '"></td><td class="center"><span>' + form_name + '</span><input type="hidden" name="form_id[]" class="form_id" value="' + form_id + '"></td><td class="center"><input class="form-control" type="text" name="barcode[]"></td><td class="center"><input class="form-control date" type="text" name="expiry_date[]"></td><td class="center qty2"><input class="form-control qtyVal2" type="number" name="quantity[]" min="1"></td><td class="center rate2"><input class="form-control rateVal2" type="number" name="rate[]" min="0" step="0.01"></td><td class="center"><span>' + quantity + '</span></td><td class="center"><span>' + rate + '</span></td><td class="center amount2"><span class="amountText2">0.00</span><input class="amountVal2" type="hidden" name="amount[]"></td><td class="center taxP2"><input class="form-control taxPVal2" type="number" name="taxP[]" min="0" step="0.01"></td><td class="center taxA2"><span class="taxAText2">0.00</span><input class="taxAVal2" type="hidden" name="taxA[]"></td></tr>');
							});
							$('.field_wrapper2').append('<tr><td colspan="12" class="right-align">Sub-Total:</td><td class="left-align subtotalText2"><b>0.00</b></td></tr><tr><td colspan="12" class="right-align">(-) Discount:</td><td class="left-align discountText2"><input type="number" class="form-control" name="discount2" id="discount2" value="0" min="0" step="0.01" autocomplete="off"></td></tr><tr><td colspan="12" class="right-align">(+) Tax:</td><td class="left-align taxText2"><b>0.00</b></td></tr><tr><td colspan="12" class="right-align">Total:</td><td class="left-align totalText2"><b>0.00</b></td></tr>');
							$('.form-control.date').datepicker({
								calendarWeeks: true,
								todayHighlight: true,
								autoclose: true,
								format: "dd-MM-yyyy",
								//startDate: today
							});
						}
						else {
							$('.field_wrapper2').html('');
							$(".nav-tabs").before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> P.O Number not found!</div>');
							setTimeout(function () { $('.alert.alert-danger').fadeOut(300) }, 3000);
						}
					}
				});
			}
		});

		//Item Table
		var quantity = 0;
		var rate = 0;

		$('.field_wrapper').on('keyup change', '.qtyVal', function (e) {
			e.preventDefault();
			quantity = $(this).val();
			rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var tax = 0;

			$('.taxAText').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));

			$('.totalText').text(parseFloat(final_total).toFixed(2));
		});

		$('.field_wrapper2').on('keyup change', '.qtyVal2', function (e) {
			e.preventDefault();
			quantity = $(this).val();
			rate = $(this).closest('tr').children('td.rate2').children('input.rateVal2').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP2').children('input.taxPVal2').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA2').children('span.taxAText2').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA2').children('input.taxAVal2').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount2').children('span.amountText2').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount2').children('input.amountVal2').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount2').val();
			var tax = 0;

			$('.taxAText2').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText2').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;
			var final_total = total + tax;

			$('.subtotalText2').text(parseFloat(sub_total).toFixed(2));
			$('.taxText2').text(parseFloat(tax).toFixed(2));
			$('.totalText2').text(parseFloat(final_total).toFixed(2));
		});
	
	$('.field_wrapper').on('keyup change', '.rateVal', function (e) {
			e.preventDefault();
			rate = $(this).val();
			quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var tax = 0;

			$('.taxAText').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;
			var final_total = total + tax;

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);
			$('.taxableText').text(parseFloat(taxable).toFixed(2));

			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
		});

		$('.field_wrapper2').on('keyup change', '.rateVal2', function (e) {
			e.preventDefault();
			rate = $(this).val();
			quantity = $(this).closest('tr').children('td.qty2').children('input.qtyVal2').val();
			var tax_p = 0;
			var tax_a = 0;
			tax_p = $(this).closest('tr').children('td.taxP2').children('input.taxPVal2').val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA2').children('span.taxAText2').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA2').children('input.taxAVal2').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount2').children('span.amountText2').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount2').children('input.amountVal2').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount2').val();
			var tax = 0;

			$('.taxAText2').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText2').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;
			var final_total = total + tax;

			$('.subtotalText2').text(parseFloat(sub_total).toFixed(2));
			$('.taxText2').text(parseFloat(tax).toFixed(2));
			$('.totalText2').text(parseFloat(final_total).toFixed(2));
		});

		$('.field_wrapper').on('keyup change', '.taxPVal', function (e) {
			e.preventDefault();
			var tax_p = 0;
			var tax_a = 0;
			quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
			rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
			tax_p = $(this).val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount').val();
			var tax = 0;

			$('.taxAText').each(function () {
				bi
				tax = tax + parseFloat($(this).text());
			});

			var sub_total = 0;
			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;
			var final_total = total + tax;

			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));
			$('.taxText').text(parseFloat(tax).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
		});

		$('.field_wrapper2').on('keyup change', '.taxPVal2', function (e) {
			e.preventDefault();
			var tax_p = 0;
			var tax_a = 0;
			quantity = $(this).closest('tr').children('td.qty2').children('input.qtyVal2').val();
			rate = $(this).closest('tr').children('td.rate2').children('input.rateVal2').val();
			tax_p = $(this).val();
			tax_a = quantity * rate * tax_p / 100;
			$(this).closest('tr').children('td.taxA2').children('span.taxAText2').text(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.taxA2').children('input.taxAVal2').val(parseFloat(tax_a).toFixed(2));
			$(this).closest('tr').children('td.amount2').children('span.amountText2').text(parseFloat(quantity * rate).toFixed(2));
			$(this).closest('tr').children('td.amount2').children('input.amountVal2').val(parseFloat(quantity * rate).toFixed(2));

			var discount = $('#discount2').val();
			var tax = 0;

			$('.taxAText2').each(function () {
				tax = tax + parseFloat($(this).text());
			});
			var sub_total = 0;
			$('.amountText2').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});

			var total = sub_total - discount;
			var final_total = total + tax;

			$('.subtotalText2').text(parseFloat(sub_total).toFixed(2));
			$('.taxText2').text(parseFloat(tax).toFixed(2));
			$('.totalText2').text(parseFloat(final_total).toFixed(2));
		});

		$('.field_wrapper').on('keyup change', '#discount', function (e) {
			e.preventDefault();
			var sub_total = parseFloat($('.subtotalText').text());
			var discount = $('#discount').val();
			var cgst = parseFloat($('.cgstText').text());
			var sgst = parseFloat($('.sgstText').text());
			var igst = parseFloat($('.igstText').text());
			var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);
			$('.taxableText').text(parseFloat(taxable).toFixed(2));

			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));

		});

		$('.field_wrapper2').on('keyup change', '#discount2', function (e) {
			e.preventDefault();
			var sub_total = parseFloat($('.subtotalText2').text());
			var discount = $('#discount2').val();
			var tax = parseFloat($('.taxText2').text());
			var final_total = parseFloat(sub_total - discount + tax);
			$('.totalText2').text(parseFloat(final_total).toFixed(2));
		});

		$('.field_wrapper').on('keyup change', '#other_expense', function (e) {
			e.preventDefault();
			var sub_total = parseFloat($('.subtotalText').text());
			var discount = $('#discount').val();
			var cgst = parseFloat($('.cgstText').text());
			var sgst = parseFloat($('.sgstText').text());
			var igst = parseFloat($('.igstText').text());
			var final_total = parseFloat(sub_total - discount + cgst + sgst + igst);

			var other_expense = $('#other_expense').val();
			var taxable = sub_total - discount + parseFloat(other_expense);
			$('.taxableText').text(parseFloat(taxable).toFixed(2));

			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

			var cgst = taxable*cgst_percentage/100;
			var sgst = taxable*sgst_percentage/100;
			var igst = taxable*igst_percentage/100;

			var final_total = taxable+cgst+sgst+igst;

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('#cgstA').val(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('#sgstA').val(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('#igstA').val(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));

		});
	
	
	$('.field_wrapper').on('click', '.remove_button', function (e) {
			e.preventDefault();
			$(this).closest("tr").remove();
			x--;
			var cgst_percentage = 0;
			var sgst_percentage = 0;
			var igst_percentage = 0;

			var gstin = $('#gstin').val();
			var f2 = gstin.substring(0, 2);
			if(gstin !=''){
				if(f2=='07'){
					cgst_percentage = {{$cgst_percentage}};
					sgst_percentage = {{$sgst_percentage}};
				}
				else{
					igst_percentage = {{$igst_percentage}};
				}
			}

		var discount = $('#discount').val();

			var sub_total = 0;
			var total = 0;
			var cgst = 0;
			var sgst = 0;
			var igst = 0;
			var final_total = 0;

			$('.amountText').each(function () {
				sub_total = sub_total + parseFloat($(this).text());
			});
			$('.subtotalText').text(parseFloat(sub_total).toFixed(2));

			var taxable = 0;
			var other_expense = 0;

			if (sub_total > 0) {
				other_expense = $('#other_expense').val();
				taxable = sub_total - discount + parseFloat(other_expense);

				var cgst = taxable*cgst_percentage/100;
				var sgst = taxable*sgst_percentage/100;
				var igst = taxable*igst_percentage/100;

				var final_total = taxable+cgst+sgst+igst;
			}

			$('.taxableText').text(parseFloat(taxable).toFixed(2));
			$('.cgstText').text(parseFloat(cgst).toFixed(2));
			$('.sgstText').text(parseFloat(sgst).toFixed(2));
			$('.igstText').text(parseFloat(igst).toFixed(2));
			$('.totalText').text(parseFloat(final_total).toFixed(2));
	}); 
	
	$('.field_wrapper2').on('click', '.remove_button', function (e) {
			e.preventDefault();
			$(this).closest("tr").remove();
			x--;
			var total = 0;
			$('.amountText2').each(function () {
				total = total + parseFloat($(this).text());
			});
			$('.subtotalText2').text(parseFloat(total).toFixed(2));
		});
});
</script>
<script>
	var maxField = 101; //Input fields increment limitation
	var x = 1; //Initial field counter is 1
	function getCategory(val) {
		$.ajax({
			type: "GET",
			url: "{{url('/openingstock/ajax')}}",
			data: 'company_id=' + val,
			success: function (data) {
				$("#category").html(data);
				$("#item").html('<option value="">Select</option>');
			}
		});
	}
	function getItems(val) {
		$.ajax({
			type: "GET",
			url: "{{url('/openingstock/ajax2')}}",
			data: 'category_id=' + val,
			success: function (data) {
				$("#item").html(data);
			}
		});
	}
	function getItemsbyBrand(val) {
		$.ajax({
			type: "GET",
			url: "{{url('/openingstock/ajax5')}}",
			data: 'brand_id=' + val,
			success: function (data) {
				$("#item").html(data);
			}
		});
	}
	function in_array(array, id) {
		for (var i = 0; i < array.length; i++) {
			if (array[i] == id)
				return true;
		}
		return false;
	}
	function addItems(val) {
		var itemStr = $(".item_id").map(function () { return $(this).val(); }).get();
		var itemArr = JSON.parse("[" + itemStr + "]");
		var itemAlreadyAdded = in_array(itemArr, val);
		//	if(itemAlreadyAdded){
		//		$("#flashmessage").append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> This item is already added!</div>');
		//		setTimeout(function(){ $('.alert.alert-danger').fadeOut(300)}, 3000);
		//		return false;
		//	}
		if (val != '') {
			$.ajax({
				type: "GET",
				url: "{{url('/purchaseregister/additems')}}",
				data: 'item_id=' + val,
				success: function (data) {
					var obj = JSON.parse(data);
					var item_id = obj[0][0].id;
					var item_name = obj[0][0].name;
					var hsn_code = obj[0][0].hsn_code;
					var brand_name = obj[0][0].brand_name;
					var strength = obj[0][0].strength;
					var form_id = obj[0][0].form_id;
					var form_name = obj[0][0].form_name;
					var unit_id = obj[0][0].unit_id;
					var unit_name = obj[0][0].unit_name;

					if(hsn_code){
						hsn_code = obj[0][0].hsn_code;
					}
					else{
						hsn_code = '';
					}

					var fieldHTML = '<tr><td class="center"><span>' + item_name + '</span><input type="hidden" name="item_id[]" class="item_id" value="' + item_id + '"></td>' +
						'<td class="center"><span>' + unit_name + '</span><input type="hidden" name="unit_id[]" value="' + unit_id + '"></td>' +
						'<td class="center"><span>' + brand_name + '</span></td>' +
						'<td class="center qty"><input class="form-control hsnVal" type="text" name="hsn_code[]" value="'+hsn_code+'"></td>' +
						'<td class="center qty"><input class="form-control qtyVal" type="number" name="quantity[]" min="0" step="0.01"></td>' +
						'<td class="center rate"><input class="form-control rateVal" type="number" name="rate[]" min="0" step="0.01"></td>' +
						'<td class="center amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]"></td>' +
						'<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>';
					if (x < maxField) {
						x++;
						$('.field_wrapper').prepend(fieldHTML);
						var nowDate = new Date();
						var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
						$('.form-control.date').datepicker({
							calendarWeeks: true,
							todayHighlight: true,
							autoclose: true,
							format: "dd-MM-yyyy",
							//startDate: today
						});
					}
				}
			});
		}
	}
	function showGSTIN(val){
		var gstin_string = val.split('#');
		$('#gstin').val(gstin_string[1]);
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-shopping-cart"></i>
		</div>
		<div class="media-body">
			<h4 class="test">Purchase Register</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/purchaseregister') }}">Purchase Register</a>
				</li>
				<li>Add</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12" id="flashmessage">
				@include('flashmessage')
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs nav-justified nav-metro">
					<!-- <li class="active">
						<a href="#by_item" data-toggle="tab">
							<strong>Fill By Item</strong>
						</a>
					</li>
					<li>
						<a href="#by_po" data-toggle="tab">
							<strong>Fill By P.O</strong>
						</a>
					</li> -->
				</ul>
				<div class="tab-content tab-content-metro mb30">
					<div class="tab-pane active" id="by_item">
						<form method="post" action="{{ url('purchaseregister/savebyitem') }}">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Voucher No:</label>
										<input type="text" class="form-control" value="{{'PV_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}" disabled="">
										<input type="hidden" name="voucher_no" id="voucher_no" value="{{'PV_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>*Voucher Date:</label>
										<input type="text" name="entry_date" id="entry_date" value="{{$entry_date}}" class="form-control date" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>*Bill No:</label>
										<input type="text" class="form-control" name="bill_no" id="bill_no" value="{{ old('bill_no') }}" min="1" required="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>*Bill Date:</label>
										<input type="text" name="bill_date" id="bill_date" value="{{$entry_date}}" class="form-control date" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>*Vendor Name:</label>
										<select style="border: none; padding: 0px" class="form-control" name="vendor" id="vendor" required="" onchange="showGSTIN(this.value);">
											<option value="">Select</option>
											@foreach ($vendor as $value) @if (old('vendor') == $value->id)
											<option value="{{ $value->id.'#'.$value->gstin }}" selected>{{ $value->name }} - {{ $value->mobile }}</option>
											@else
											<option value="{{ $value->id.'#'.$value->gstin }}">{{ $value->name }} - {{ $value->mobile }}</option>
											@endif @endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>GSTIN:</label>
										<input class="form-control" type="text" name="gstin" id="gstin" value="" disabled/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Transport:</label>
										<select style="border: none; padding: 0px" class="form-control basic-select" name="transport" id="transport">
											<option value="">Select</option>
											@foreach ($transport as $value) @if (old('transport') == $value->id)
											<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
											@else
											<option value="{{ $value->id }}">{{ $value->name }}</option>
											@endif @endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>L.R No:</label>
										<input class="form-control" type="text" name="lr_no" id="lr_no" placeholder="Enter comma seperated L.R No"/>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Challan No:</label>
										<input class="form-control" type="text" name="challan_no" id="challan_no"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Category:</label>
										<select style="border: none; padding: 0px" class="form-control" name="category" id="category">
											<option value="">Select</option>
											@foreach ($category as $value)
											<option value="{{ $value->id }}">{{ $value->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Brand:</label>
										<select style="border: none; padding: 0px" class="form-control basic-select" name="brand" id="brand" onChange="getItemsbyBrand(this.value);">
											<option value="">Select</option>
											@foreach ($brand as $value) @if (old('brand') == $value->id)
											<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
											@else
											<option value="{{ $value->id }}">{{ $value->name }}</option>
											@endif @endforeach
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Item:</label>
										<select style="border: none; padding: 0px" class="form-control" name="item" id="item" onChange="addItems(this.value);">
											<option value="">Select</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered" id="itemTable">
											<tr>
												<th>Name</th>
												<!-- <th>Strength</th> -->
												<th>Unit</th>
												<th>Brand</th>
												<th>HSN Code</th>
												<!-- <th>Barcode</th> -->
												<!-- <th>Expiry Date</th> -->
												<th>Quantity</th>
												<th>Cost</th>
												<th>Amount</th>
												<th width="100px">Action</th>
											</tr>
											<tbody class="field_wrapper">
												<tr>
													<td colspan="7" class="right-align">Sub-Total:</td>
													<td class="left-align subtotalText">
														<b>0.00</b>
													</td>
												</tr>
												<tr>
													<td colspan="7" class="right-align">(-) Discount:</td>
													<td class="left-align discountText">
														<input type="number" class="form-control" name="discount" id="discount" value="0" min="0" step="0.01" autocomplete="off">
													</td>
												</tr>
												<tr>
													<td colspan="7" class="right-align">(+) Other Expense:</td>
													<td class="left-align otherexpenseText">
														<input type="number" class="form-control" name="other_expense" id="other_expense" value="0" min="0" step="0.01" autocomplete="off">
													</td>
												</tr>
												<tr>
													<td colspan="7" class="right-align">Taxable Amount:</td>
													<td class="left-align taxableText"><b>0.00</b></td>
												</tr>
												<tr>
													<td colspan="7" class="right-align">(+) CGST:</td>
													<td class="left-align cgstText">
														<b>0.00</b>
													</td>
													<input type="hidden" name="cgstA" id="cgstA"/>
												</tr>
												<tr>
													<td colspan="7" class="right-align">(+) SGST:</td>
													<td class="left-align sgstText">
														<b>0.00</b>
													</td>
													<input type="hidden" name="sgstA" id="sgstA"/>
												</tr>
												<tr>
													<td colspan="7" class="right-align">(+) IGST:</td>
													<td class="left-align igstText">
														<b>0.00</b>
													</td>
													<input type="hidden" name="igstA" id="igstA"/>
												</tr>
												<tr>
													<td colspan="7" class="right-align">Total:</td>
													<td class="left-align totalText">
														<b>0.00</b>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Remarks:</label>
										<input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{ old('remarks') }}">
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Save</button>
						</form>
					</div>
					<div class="tab-pane" id="by_po">
						<form method="post" action="{{ url('purchaseregister/savebypo') }}">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Voucher No:</label>
										<input type="text" class="form-control" name="voucher_no" id="voucher_no" value="{{'PV_'.str_pad($voucher_no+1, 4, 0, STR_PAD_LEFT)}}"
										 disabled="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>*Voucher Date:</label>
										<input type="text" name="entry_date" id="entry_date" value="{{$entry_date}}" class="form-control date" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>*Bill No:</label>
										<input type="number" class="form-control" name="bill_no" id="bill_no" value="{{ old('bill_no') }}" min="1" required="">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>*Bill Date:</label>
										<input type="text" name="bill_date" id="bill_date" value="{{$entry_date}}" class="form-control date" required="">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>*Vendor Name:</label>
										<select class="form-control" name="vendor" id="vendor" required="">
											<option value="">Select</option>
											@foreach ($vendor as $value) @if (old('vendor') == $value->id)
											<option value="{{ $value->id }}" selected>{{ $value->name }} - {{ $value->mobile }}</option>
											@else
											<option value="{{ $value->id }}">{{ $value->name }} - {{ $value->mobile }}</option>
											@endif @endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>*P.O Number:</label>
										<div class="input-group">
											<input type="text" class="form-control" name="purchaseorder_no" id="purchaseorder_no" value="{{ old('purchaseorder_no') }}"
											 required="">
											<span class="input-group-addon search">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered" id="itemTable2">
											<tr>
												<th>Name</th>
												<th>Strength</th>
												<th>Unit</th>
												<th>Form</th>
												<th>Barcode</th>
												<th>Expiry Date</th>
												<th>Quantity</th>
												<th>Rate</th>
												<th>Ordered Quantity</th>
												<th>Ordered Cost</th>
												<th>Amount</th>
												<th>Tax (%)</th>
												<th>Tax Amount</th>
											</tr>
											<tbody class="field_wrapper2">
												<tr>
													<td colspan="12" class="right-align">Sub-Total:</td>
													<td class="left-align subtotalText2">
														<b>0.00</b>
													</td>
												</tr>
												<tr>
													<td colspan="12" class="right-align">(-) Discount:</td>
													<td class="left-align discountText2">
														<input type="number" class="form-control" name="discount2" id="discount2" value="0" min="0" step="0.01" autocomplete="off">
													</td>
												</tr>
												<tr>
													<td colspan="12" class="right-align">(+) Tax:</td>
													<td class="left-align taxText2">
														<b>0.00</b>
													</td>
												</tr>
												<tr>
													<td colspan="12" class="right-align">Total:</td>
													<td class="left-align totalText2">
														<b>0.00</b>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label>Remarks:</label>
										<input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{ old('remarks') }}">
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary">Save</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection