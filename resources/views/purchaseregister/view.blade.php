@extends('layouts.website') @section('content')
<script>
	function printPage() {
		document.title = "Purchase Register";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<a href="{{ url('/purchaseregister/lr/'.Request::segment(3)) }}" class="btn btn-primary"><i class="fa fa-eye"></i> View L.R</a>
			<a target="_blank" href="{{ url('/purchaseregister/barcodeview/'.Request::segment(3)) }}" class="btn btn-primary"><i class="fa fa-print"></i> Print Barcode</a>
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-shopping-cart"></i>
		</div>
		<div class="media-body">
			<h4>Purchase Register</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>
					<a href="{{ url('/purchaseregister') }}">Purchase Register</a>
				</li>
				<li>View</li>
				<li>{{Request::segment(3)}}</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-9">
				<h4 class="center onlyprint">
					<u>{{$company->name}}</u>
				</h4>
				<h5 class="center onlyprint">{{$company->address}}</h5>
				<h5 class="center onlyprint"><b>GSTIN:</b> {{$company->gstin}}</h5>
				@if(!empty($company->phone) && !empty($company->mobile) )
				<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}, <b>Mobile:</b> {{$company->mobile}}</h5>
				@else
					@if(!empty($company->phone))
					<h5 class="center onlyprint"><b>Phone:</b> {{$company->phone}}</h5>
					@else
					<h5 class="center onlyprint"><b>Mobile:</b> {{$company->mobile}}</h5>
					@endif
				@endif
				<table class="table table-bordered">
					<tr>
						<th class="left-align">Vendor Details</th>
						<th class="left-align">Voucher Details</th>
						<th class="left-align">Transport Details</th>
					</tr>
					<tr>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Vendor Name:</b> {{$purchaseregister[0]->vendor_name}}</p>
							<p><b>Phone:</b> {{$purchaseregister[0]->vendor_mobile}}</p>
							<p><b>GSTIN:</b> {{$purchaseregister[0]->vendor_gstin}}</p>
							<p><b>State Code:</b> {{substr($purchaseregister[0]->vendor_gstin, 0, 2)}}</p>
							<p><b>Address:</b> {{$purchaseregister[0]->vendor_address}}</p>
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>P.O No:</b> {{$purchaseregister[0]->purchaseorder_no}}</p>
							<p><b>Voucher No:</b> {{$purchaseregister[0]->purchasevoucher_no}}</p>
							<p><b>Voucher Date:</b> {{date_dfy($purchaseregister[0]->entry_date)}}</p>
							<p><b>Bill No:</b> {{$purchaseregister[0]->bill_no}}</p>
							<p><b>Bill Date:</b> {{date_dfy($purchaseregister[0]->bill_date)}}</p>
							<!-- <p>Remarks: {{$purchaseregister[0]->remarks}}</p> -->
						</td>
						<td class="left-align" style="vertical-align: top !important;">
							<p><b>Transport:</b> {{$purchaseregister[0]->transport_name}}</p>
							<p><b>LR No:</b> {{$lr_str}}</p>
							<p><b>Challan No:</b> {{$purchaseregister[0]->challan_no}}</p>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="table-responsive">
					<table class="table table-bordered mb30">
						<tbody>
							<tr>
								<td width="50px" class="left-align">
									<b>S.No</b>
								</td>
								<td class="left-align">
									<b>Name</b>
								</td>
								<td class="left-align">
									<b>HSN Code</b>
								</td>
								<td class="left-align">
									<b>Pcs</b>
								</td>
								<td class="left-align">
									<b>Quantity</b>
								</td>
								<td class="left-align">
									<b>Cost</b>
								</td>
								<td class="left-align">
									<b>Amount</b>
								</td>
							</tr>
							<?php $i=1; foreach ($purchaseregister_item as $value){ ?>
							<tr>
								<td class="left-align">{{ $i }}</td>
								<td class="left-align">{{ $value->item_name.purchaseregister_qty($value->parent_id,$value->item_id) }}</td>
								<!-- <td class="left-align">{{ $value->item_name }}</td> -->
								<td class="left-align">{{ $value->hsn_code }}</td>
								<td class="left-align">{{ $value->pcs_total }}</td>
								<td class="left-align">{{ $value->quantity_total }}</td>
								<td class="left-align">{{ $value->rate }}</td>
								<td class="left-align">{{ $value->quantity_total*$value->rate }}</td>
							</tr>
							<?php $i++; }?>
							<tr>
								<td colspan="6" class="right-align">Sub-Total:</td>
								<td class="left-align">
									<b>{{$purchaseregister[0]->sub_total}}</b>
								</td>
							</tr>
							@if($purchaseregister[0]->discount > 0)
							<tr>
								<td colspan="6" class="right-align">Discount:</td>
								<td class="left-align">
									<b>{{$purchaseregister[0]->discount}}</b>
								</td>
							</tr>
							@endif
							@if($purchaseregister[0]->other_expense+$other_expense > 0)
							<tr>
								<td colspan="6" class="right-align">Other Expense:</td>
								<td class="left-align">
									<b>{{number_format((float)$purchaseregister[0]->other_expense+$other_expense, 2, '.', '')}}</b>
								</td>
							</tr>
							<!-- <tr>
								<td colspan="5" class="right-align">Taxable Amount:</td>
								<td class="left-align">
									<b>{{number_format($purchaseregister[0]->sub_total - $purchaseregister[0]->discount, 2, '.', ',')}}</b>
								</td>
							</tr> -->
							@endif
							@if($purchaseregister[0]->cgst > 0)
							<tr>
								<td colspan="6" class="right-align">CGST:</td>
								<td class="left-align">
									<b>{{$purchaseregister[0]->cgst}}</b>
								</td>
							</tr>
							@endif
							@if($purchaseregister[0]->sgst > 0)
							<tr>
								<td colspan="6" class="right-align">SGST:</td>
								<td class="left-align">
									<b>{{$purchaseregister[0]->sgst}}</b>
								</td>
							</tr>
							@endif
							@if($purchaseregister[0]->igst > 0)
							<tr>
								<td colspan="6" class="right-align">IGST:</td>
								<td class="left-align">
									<b>{{$purchaseregister[0]->igst}}</b>
								</td>
							</tr>
							@endif
							<tr>
								<td colspan="6" class="right-align">Total:</td>
								<td class="left-align">
									<b>{{$purchaseregister[0]->total}}</b>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection