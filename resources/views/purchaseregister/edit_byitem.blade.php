<script type='text/javascript'>
$(function(){
	$("#item").select2();
	$("#vendor").select2();
	$("#category").select2();
	$("#brand").select2();
	var nowDate = new Date();
	var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
	$('.form-control.date').datepicker({
	    calendarWeeks: true,
	    todayHighlight: true,
	    autoclose: true,
	    format: "dd-MM-yyyy",
	    //startDate: today
	});
	$('.field_wrapper').on('keyup change', '.qtyVal', function(e){
		e.preventDefault();
		quantity = $(this).val();
		rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
	    var tax_p = 0;
	    var tax_a = 0;
	    tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
	    tax_a = quantity*rate*tax_p/100;
	    $(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
		$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
		$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity*rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity*rate).toFixed(2));
	    
	    var discount = $('#discount').val();
		var tax = 0;
		
	    $('.taxAText').each(function () {
	    	tax = tax + parseFloat($(this).text());
	    });
		var sub_total = 0;
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    
	    var total = sub_total - discount;
	    var final_total = total+tax;
	    
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	
	$('.field_wrapper').on('keyup change', '.rateVal', function(e){
		e.preventDefault();
		rate = $(this).val();
		quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
	    var tax_p = 0;
	    var tax_a = 0;
	    tax_p = $(this).closest('tr').children('td.taxP').children('input.taxPVal').val();
	    tax_a = quantity*rate*tax_p/100;
	    $(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
		$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
		$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity*rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity*rate).toFixed(2));
	    
	    var discount = $('#discount').val();
		var tax = 0;
		
	    $('.taxAText').each(function () {
	    	tax = tax + parseFloat($(this).text());
	    });
		var sub_total = 0;
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    
	    var total = sub_total - discount;
	    var final_total = total+tax;
	    
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});

	$('.field_wrapper').on('keyup change', '.taxPVal', function(e){
		e.preventDefault();
	    var tax_p = 0;
	    var tax_a = 0;
	    quantity = $(this).closest('tr').children('td.qty').children('input.qtyVal').val();
		rate = $(this).closest('tr').children('td.rate').children('input.rateVal').val();
		tax_p = $(this).val();
	    tax_a = quantity*rate*tax_p/100;
	    $(this).closest('tr').children('td.taxA').children('span.taxAText').text(parseFloat(tax_a).toFixed(2));
		$(this).closest('tr').children('td.taxA').children('input.taxAVal').val(parseFloat(tax_a).toFixed(2));
		$(this).closest('tr').children('td.amount').children('span.amountText').text(parseFloat(quantity*rate).toFixed(2));
		$(this).closest('tr').children('td.amount').children('input.amountVal').val(parseFloat(quantity*rate).toFixed(2));
	    
	    var discount = $('#discount').val();
		var tax = 0;
		
	    $('.taxAText').each(function () {
	    	tax = tax + parseFloat($(this).text());
	    });
		var sub_total = 0;
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    
	    var total = sub_total - discount;
	    var final_total = total+tax;
	    
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	
	$('.field_wrapper').on('keyup change', '#discount', function(e){
		e.preventDefault();
		var sub_total = parseFloat($('.subtotalText').text());
	    var discount = $('#discount').val();
	    var tax = parseFloat($('.taxText').text());
	    var final_total = parseFloat(sub_total - discount + tax);
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	});
	
	$('.field_wrapper').on('click', '.remove_button', function(e){
		e.preventDefault();
		$(this).closest("tr").remove();
		x--;
		var tax_percentage = {{$tax_percentage}};
		var discount = $('#discount').val();
		
		var sub_total = 0;
		var total = 0;
		var tax = 0;
		var final_total = 0;
		
	    $('.amountText').each(function () {
	    	sub_total = sub_total + parseFloat($(this).text());
	    });
	    $('.subtotalText').text(parseFloat(sub_total).toFixed(2));
	    
	    if(sub_total>0){
			var total = sub_total - discount;
			var tax = total*tax_percentage/100;
			var final_total = total+tax;
		}
	    
	    $('.taxText').text(parseFloat(tax).toFixed(2));
	    $('.totalText').text(parseFloat(final_total).toFixed(2));
	}); 
});
</script>
<script>
var maxField = 101; //Input fields increment limitation
var x = 1; //Initial field counter is 1
var quantity = '5';
function getCategory(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax')}}",
	data:'company_id='+val,
	success: function(data){
		$("#category").html(data);
		$("#item").html('<option value="">Select</option>');
	}
	});
}
function getItems(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax2')}}",
	data:'category_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function getItemsbyBrand(val) {
	$.ajax({
	type: "GET",
	url: "{{url('/openingstock/ajax5')}}",
	data:'brand_id='+val,
	success: function(data){
		$("#item").html(data);
	}
	});
}
function in_array(array, id) {
    for(var i=0;i<array.length;i++) {
       if(array[i] == id)
        return true;
    }
    return false;
}
function addItems(val){
	var itemStr = $("input[name='item_id[]']").map(function(){return $(this).val();}).get();
	var itemArr = JSON.parse("["+ itemStr +"]");
	var itemAlreadyAdded = in_array(itemArr, val);
//	if(itemAlreadyAdded){
//		$("#flashmessage").append('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>Oops!</strong> This item is already added!</div>');
//		setTimeout(function(){ $('.alert.alert-danger').fadeOut(300)}, 3000);
//		return false;
//	}
	if(val != ''){
		$.ajax({
			type: "GET",
			url: "{{url('/purchaseregister/additems')}}",
			data:'item_id='+val,
			success: function(data){
				var obj = JSON.parse(data);
				var item_id = obj[0][0].id;
				var item_name = obj[0][0].name;
				var brand_name = obj[0][0].brand_name;
				var strength = obj[0][0].strength;
				var form_id = obj[0][0].form_id;
				var form_name = obj[0][0].form_name;
				var unit_id = obj[0][0].unit_id;
				var unit_name = obj[0][0].unit_name;
				var rate = obj[0][0].price;

				var fieldHTML = '<tr><td class="center"><span>'+item_name+'</span><input type="hidden" name="item_id[]" value="'+item_id+'"></td><td class="center"><span>'+strength+'</span><input type="hidden" name="strength[]" class="strength" value="'+strength+'"></td><td class="center"><span>'+unit_name+'</span><input type="hidden" name="unit_id[]" value="'+unit_id+'"></td><td class="center"><span>'+brand_name+'</span></td><td class="center"><span>'+form_name+'</span><input type="hidden" name="form_id[]" class="form_id" value="'+form_id+'"></td><td class="center"><input type="text" class="form-control" name="barcode[]"></td><td class="center"><input type="text" class="form-control date" name="expiry_date[]"></td><td class="center qty"><input class="form-control qtyVal" type="number" name="quantity[]" min="1"></td><td class="center rate"><input class="form-control rateVal" type="number" name="rate[]" min="0" step="0.01"></td><td class="left-align center amount"><span class="amountText">0.00</span><input class="amountVal" type="hidden" name="amount[]" value="0.00"></td><td class="center taxP"><input class="form-control taxPVal" type="number" name="taxP[]" value="0.00" min="0" step="0.01"></td><td class="center taxA"><span class="taxAText">0.00</span><input class="taxAVal" type="hidden" name="taxA[]" value="0.00"></td><td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td></tr>';
				if(x < maxField){
					x++;
					$('.field_wrapper').prepend(fieldHTML);
					var nowDate = new Date();
					var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0); 
					$('.form-control.date').datepicker({
					    calendarWeeks: true,
					    todayHighlight: true,
					    autoclose: true,
					    format: "dd-MM-yyyy",
					    //startDate: today
					});
				}
			}
		});
	}
}
</script>
<form method="post" action="{{ url('/purchaseregister/updatebyitem/'.$purchaseregister[0]->id) }}">
  	{{ csrf_field() }}
  	<div class="row">
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>Voucher No:</label>
			  <input type="text" class="form-control" name="voucher_no" id="voucher_no" value="{{$purchasevoucher_no}}" disabled="">
			</div>
  		</div>
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>*Voucher Date:</label>
			  <div class="input-group date">
				<input type="text" name="entry_date" id="entry_date" value="{{date_dfy($entry_date)}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>*Bill No:</label>
			  <input type="number" class="form-control" name="bill_no" id="bill_no" value="{{$bill_no}}" min="1" required="">
			</div>
  		</div>
  		<div class="col-md-6">
  			<div class="form-group">
			  <label>*Bill Date:</label>
			  <div class="input-group date">
				<input type="text" name="bill_date" id="bill_date" value="{{date_dfy($bill_date)}}" class="form-control" required=""><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
			  </div>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="form-group">
			  <label>*Vendor Name:</label>
			  <select style="border: none; padding: 0px" class="form-control" name="vendor" id="vendor" required="">
			  		@foreach ($vendor as $value)
			  		<option value="<?php echo $value->id; ?>" <?php if($vendor_id==$value->id){ ?> selected="" <?php }?>><?php echo $value->name.' - '.$value->mobile; ?></option>
			  		@endforeach
			  </select>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-4">
  			<div class="form-group">
			  <label>Category:</label>
			  <select style="border: none; padding: 0px" class="form-control" name="category" id="category" onChange="getItems(this.value);">
			  		<option value="">Select</option>
			  		@foreach ($category as $value)
			  		<option value="{{ $value->id }}">{{ $value->name }}</option>
			  		@endforeach
			  </select>
			</div>
  		</div>
		<div class="col-md-4">
			<div class="form-group">
			<label>Brand:</label>
			<select style="border: none; padding: 0px" class="form-control basic-select" name="brand" id="brand" onChange="getItemsbyBrand(this.value);">
					<option value="">Select</option>
					@foreach ($brand as $value)
					@if (old('brand') == $value->id)
						<option value="{{ $value->id }}" selected>{{ $value->name }}</option>
					@else
						<option value="{{ $value->id }}">{{ $value->name }}</option>
					@endif
					@endforeach
			</select>
			</div>
		</div>
  		<div class="col-md-4">
  			<div class="form-group">
			  <label>Item:</label>
			  <select style="border: none; padding: 0px" class="form-control" name="item" id="item" onChange="addItems(this.value);">
			  		<option value="">Select</option>
			  </select>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="table-responsive">
				<table class="table table-bordered" id="itemTable">
					<tr>
		                <th>Name</th>
		                <th>Strength</th>
		                <th>Unit</th>
						<th>Brand</th>
		                <th>Form</th>
		                <th>Barcode</th>
		                <th>Expiry Date</th>
		                <th>Quantity</th>
		                <th>Cost</th>
		                <th>Amount</th>
		                <th>Tax (%)</th>
		                <th>Tax Amount</th>
		                <th width="100px">Action</th>
		            </tr>
					<tbody class="field_wrapper">
						@foreach($purchaseregister_item as $value)
						<tr>
						<td class="center"><span>{{$value->item_name}}</span><input type="hidden" name="item_id[]" value="{{$value->item_id}}"></td>
						<td class="center"><span>{{$value->strength}}</span><input type="hidden" name="strength[]" value="{{$value->strength}}"></td>
						<td class="center"><span>{{$value->unit_name}}</span><input type="hidden" name="unit_id[]" value="{{$value->unit_id}}"></td>
						<td class="center"><span>{{$value->brand_name}}</span><input type="hidden" name="brand_id[]" value="{{$value->brand_id}}"></td>
						<td class="center"><span>{{$value->form_name}}</span><input type="hidden" name="form_id[]" value="{{$value->form_id}}"></td>
						<td class="center"><input type="text" class="form-control" name="barcode[]" value="{{$value->barcode}}"></td>
						<td class="center"><input type="text" class="form-control date" name="expiry_date[]" value="{{date_dfy($value->expiry_date)}}"></td>
						<td class="center qty"><input class="form-control qtyVal" type="number" value="{{$value->quantity}}" name="quantity[]" min="1"></td>
						<td class="center rate"><input class="form-control rateVal" type="number" value="{{$value->rate}}" name="rate[]" min="0" step="0.01"></td>
						<td class="left-align center amount"><span class="amountText">{{$value->amount}}</span><input class="amountVal" type="hidden" name="amount[]" value="{{$value->amount}}"></td>
						<td class="center taxP"><input class="form-control taxPVal" type="number" name="taxP[]" value="{{$value->tax_rate}}" min="0" step="0.01"></td>
						<td class="center taxA"><span class="taxAText">{{$value->tax_amount}}</span><input class="taxAVal" type="hidden" name="taxA[]" value="{{$value->tax_amount}}"></td>
						<td><a href="javascript:void(0);" class="remove_button" title="Remove field"><img src="{{url("images/remove-icon.png")}}"></a></td>
						</tr>
						@endforeach
						<tr>
							<td colspan="12" class="right-align">Sub-Total:</td>
							<td class="left-align subtotalText"><b>{{$sub_total}}</b></td>
			            </tr>
			            <tr>
							<td colspan="12" class="right-align">(-) Discount:</td>
							<td class="left-align discountText"><input type="number" class="form-control" name="discount" id="discount" value="{{$discount}}" min="0" step="0.01" autocomplete="off"></td>
			            </tr>
			            <tr>
							<td colspan="12" class="right-align">(+) Tax:</td>
							<td class="left-align taxText"><b>{{$tax}}</b></td>
			            </tr>
			            <tr>
							<td colspan="12" class="right-align">Total:</td>
							<td class="left-align totalText"><b>{{$total}}</b></td>
			            </tr>
					</tbody>
				</table>
			</div>
  		</div>
  	</div>
  	<div class="row">
  		<div class="col-md-12">
  			<div class="form-group">
			  <label>Remarks:</label>
			  <input type="text" class="form-control" placeholder="Enter remarks" name="remarks" id="remarks" value="{{$remarks}}">
			</div>
  		</div>
  	</div>
	<button type="submit" class="btn btn-primary">Update</button>
</form>