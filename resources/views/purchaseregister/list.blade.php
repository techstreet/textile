@extends('layouts.website')
@section('content')
<div class="pageheader">
	<div class="media">
	    @if(Auth::user()->user_group != '4')
		<div class="pull-right">
			<a href="{{ url('/purchaseregister/addbypo') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add By P.O</button></a>
			<a href="{{ url('/purchaseregister/add') }}"><button class="btn btn-primary"><i class="fa fa-plus"></i> Add By Item</button></a>
		</div>
		@endif
		<div class="pageicon pull-left">
            <i class="fa fa-shopping-cart"></i>
        </div>
		<div class="media-body">
		<h4>Purchase Register</h4>
		<ul class="breadcrumb">
		    <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i></a></li>
		    <li>Purchase Register</li>
		</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
<div id="page-wrapper">       
    <div class="row">
	    <div class="col-md-12">
	    @include('flashmessage')
		<?php if($count>0){ ?>
		<div class="table-responsive">
		  <table class="table table-bordered" id="dataTable">
		    <thead>
		      <tr>
		        <th width="50px">S.No</th>
		        <th>Voucher No</th>
		        <th>Voucher Date</th>
		        <th>Vendor Name</th>
		        <th>Bill No</th>
		        <th>Bill Date</th>
		        <th width="150px" style="min-width: 150px;">Action</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php $i=1; foreach ($purchaseregister as $value){ ?>
	          <tr>
	            <td>{{ $i }}</td>
	            <td>{{ $value->purchasevoucher_no }}</td>
	            <td>{{ date_dfy($value->entry_date) }}</td>
	            <td>{{ $value->vendor_name }}</td>
	            <td>{{ $value->bill_no }}</td>
	            <td>{{ date_dfy($value->bill_date) }}</td>
	            
	            <td>
	            <a href="{{ url('/purchaseregister/view/'.$value->id) }}" data-toggle="tooltip" title="View" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a>
	            @if(session('superadmin') == '1')
	            <!--<a href="{{ url('/purchaseregister/edit/'.$value->id) }}" data-toggle="tooltip" title="Edit" class="btn btn-primary" data-original-title="Edit"><i class="fa fa-pencil"></i></a>-->
							
							<a onclick="return confirm('Are you sure you want to Delete?');" href="{{ url('/purchaseregister/delete/'.$value->id) }}" data-toggle="tooltip" title="Delete" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
							@endif	
						</td>
	          </tr>
	          <?php $i++; } ?>
		    </tbody>
		  </table>
		</div>
		<?php } else{?>
		No results found!
		<?php }?>
		</div>
	</div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#dataTable').DataTable({
    	ordering: false
    });
});
</script>
@endsection