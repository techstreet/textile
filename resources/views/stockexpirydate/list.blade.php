@extends('layouts.website') @section('content')
<script>
	$(function () {
		$(".basic-select").select2();
		var nowDate = new Date();
		var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
		$('.input-group.date').datepicker({
			calendarWeeks: true,
			todayHighlight: true,
			autoclose: true,
			format: "dd-MM-yyyy",
			//startDate: today
		});
	});
	function printPage() {
		document.title = "Stock Expiry Report";
		window.print();
	}
</script>
<div class="pageheader">
	<div class="media">
		<div class="pull-right">
			<button onclick="printPage();" class="btn btn-primary">
				<i class="fa fa-print"></i> Print</button>
		</div>
		<div class="pageicon pull-left">
			<i class="fa fa-file-text-o"></i>
		</div>
		<div class="media-body">
			<h4>Expiry Report</h4>
			<ul class="breadcrumb">
				<li>
					<a href="{{ url('/') }}">
						<i class="glyphicon glyphicon-home"></i>
					</a>
				</li>
				<li>Reports</li>
				<li>Expiry Report</li>
			</ul>
		</div>
	</div>
</div>
<div class="contentpanel">
	<div id="page-wrapper">
		<div class="row">
			<div class="col-md-12">
				@include('flashmessage')
				<form class="form-inline" method="post" action="">
					{{ csrf_field() }}
					<div class="form-group" style="margin-right: 0">
						<div class="input-group">
							<select style="border: none; padding: 0px; min-width: 200px;" class="form-control basic-select" name="expirydate" id="expirydate"
							 placeholder="Expiring">
								<option value="">Select</option>
								<option value="1">Expiring This Month</option>
								<option value="2">Expiring Next Month</option>
								<option value="3">Expiring Next 3 Months</option>
								<option value="6">Expiring Next 6 Months</option>
								<option value="12">Expiring Next 12 Months</option>
								<option value="24">Expiring Next 24 Months</option>
							</select>
						</div>
					</div>
					<button type="submit" name="submit" class="btn btn-info">Find</button>
				</form>
			</div>
		</div>
		<?php if($_POST && $_POST > 0){ ?>
		<div class="row" style="margin-top: 10px">
			<div class="col-md-12">
				@include('flashmessage')
				<div class="table-responsive">
					<table class="table table-bordered" id="dataTable">
						<thead>
							<tr>
								<th width="50px">S.No</th>
								<th>Item</th>
								<th>Category</th>
								<th>Brand</th>
								<!--<th>Manufacturer</th>-->
								<th>Form</th>
								<th>Barcode</th>
								<th>Quantity</th>
								<th>Expiry Date</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1;
							if(!empty($stockexpirydate)){
								foreach ($stockexpirydate as $value){ ?>
								<tr>
									<td>{{ $i }}</td>
									<td>{{ $value->item_name }}</td>
									<td>{{ $value->category_name }}</td>
									<td>{{ $value->brand_name }}</td>
									<!--<td>{{ $value->manufacturer_name }}</td>-->
									<td>{{ $value->form_name }}</td>
									<td>{{ $value->barcode }}</td>
									<td>{{ $value->stock }}</td>
									<td>{{ date_dfy($value->expiry_date) }}</td>
								</tr>
								<?php $i++; }
							}
		   		    else{ ?>
							<tr>
								<td colspan="7">There is no Record Found.</td>
							</tr>
							<?php }?>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
@endsection