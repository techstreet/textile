<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    //Logout
	Route::get('/logout', function () {
		Auth::logout();
		session()->flush();
	    return redirect('/');
	});
	
	//Test
	Route::get('/test','TestController@index');
	Route::post('/test','TestController@save');
	Route::get('/autocomplete','TestController@autocomplete');
	
	//Change password
	Route::get('/changepassword','ChangepasswordController@index');
	Route::post('/changepassword','ChangepasswordController@save');
	
	//Edit Profile
	Route::get('/editprofile','EditprofileController@index');
	Route::post('/editprofile','EditprofileController@save');
	
	//Dashboard
	Route::get('/dashboard','WelcomeController@index');
	
	//Notification
	Route::get('/notification','NotificationController@index');
	
	//Administrator - Login
	Route::get('/administrators/login/{id}','AdministratorsController@login');
	//User - Login
	Route::get('/users/login/{id}','UsersController@login');

	//Openingstock - Ajax
	Route::get('/openingstock/ajax','OpeningstockController@ajax');
	Route::get('/openingstock/ajax2','OpeningstockController@ajax2');
	Route::get('/openingstock/ajax3','OpeningstockController@ajax3');
	Route::get('/openingstock/ajax4','OpeningstockController@ajax4');
	Route::get('/openingstock/ajax5','OpeningstockController@ajax5');

	
	Route::group(['middleware' => 'CheckSuperUser'], function () {     
	    //Company - List
		Route::get('/company','CompanyController@index');
		//Company - view
		Route::get('/company/view/{id}','CompanyController@view');
		//Company - create
		Route::get('/company/add','CompanyController@add');
		Route::post('/company/save','CompanyController@save');
		//Company - update
		Route::get('/company/edit/{id}','CompanyController@edit');
		Route::post('/company/update/{id}','CompanyController@update');
		//Company - Delete
		Route::get('/company/delete/{id}','CompanyController@delete');
		
		//Administrator - List
		Route::get('/administrators','AdministratorsController@index');
		//Administrator - view
		Route::get('/administrators/view/{id}','AdministratorsController@view');
		//Administrator - create
		Route::get('/administrators/add','AdministratorsController@add');
		Route::post('/administrators/save','AdministratorsController@save');
		//Administrator - update
		Route::get('/administrators/edit/{id}','AdministratorsController@edit');
		Route::post('/administrators/update/{id}','AdministratorsController@update');
		//Administrator - Delete
		Route::get('/administrators/delete/{id}','AdministratorsController@delete');
	});

	Route::group(['middleware' => 'CheckSuperadminSession'], function () {
		//Purchase Order - List
		Route::get('/purchaseorder','PurchaseorderController@index');
		//Purchase Order - View
		Route::get('/purchaseorder/view/{id}','PurchaseorderController@view');
		//Purchase Order - create
		Route::get('/purchaseorder/add','PurchaseorderController@add');
		Route::post('/purchaseorder/save','PurchaseorderController@save');
		//Purchase Order - update
		Route::get('/purchaseorder/edit/{id}','PurchaseorderController@edit');
		Route::post('/purchaseorder/update/{id}','PurchaseorderController@update');
		//Purchase Order - Delete
		Route::get('/purchaseorder/delete/{id}','PurchaseorderController@delete');
		//Purchase Order - Add Items
		Route::get('/purchaseorder/additems','PurchaseorderController@additems');
		//Autocomplete
		Route::get('/purchaseorder/autocomplete','PurchaseorderController@autocomplete');
		
		//Purchase Register - List
		Route::get('/purchaseregister','PurchaseregisterController@index');
		//Purchase Register - View
		Route::get('/purchaseregister/view/{id}','PurchaseregisterController@view');
		Route::get('/purchaseregister/barcodeview/{id}','PurchaseregisterController@barcodeview');
		//Purchase Register - create
		Route::get('/purchaseregister/add','PurchaseregisterController@add');
		Route::get('/purchaseregister/addbypo','PurchaseregisterController@addbypo');
		Route::post('/purchaseregister/savebyitem','PurchaseregisterController@savebyitem');
		Route::post('/purchaseregister/savebypo','PurchaseregisterController@savebypo');
		//Purchase Register - update
		Route::get('/purchaseregister/edit/{id}','PurchaseregisterController@edit');
		Route::post('/purchaseregister/updatebyitem/{id}','PurchaseregisterController@updatebyitem');
		Route::post('/purchaseregister/updatebypo/{id}','PurchaseregisterController@updatebypo');
		//Purchase Register - Delete
		Route::get('/purchaseregister/delete/{id}','PurchaseregisterController@delete');
		//Purchase Register - Add Items
		Route::get('/purchaseregister/additems','PurchaseregisterController@additems');
		Route::get('/purchaseregister/additemsbypo','PurchaseregisterController@additemsbypo');
		
		//Purchase Return - List
		Route::get('/purchasereturn','PurchasereturnController@index');
		//Purchase Return - View
		Route::get('/purchasereturn/view/{id}','PurchasereturnController@view');
		//Purchase Return - create
		Route::get('/purchasereturn/add','PurchasereturnController@add');
		Route::post('/purchasereturn/save','PurchasereturnController@save');
		//Purchase Return - update
		Route::get('/purchasereturn/edit/{id}','PurchasereturnController@edit');
		Route::post('/purchasereturn/update/{id}','PurchasereturnController@update');
		//Purchase Return - Delete
		Route::get('/purchasereturn/delete/{id}','PurchasereturnController@delete');
		//Purchase Return - Add Items
		Route::get('/purchasereturn/additems','PurchasereturnController@additems');
		//Autocomplete
		Route::get('/purchasereturn/autocomplete','PurchasereturnController@autocomplete');

		//Brand - List
		Route::get('/brand','BrandController@index');
		//Brand - create
		Route::get('/brand/add','BrandController@add');
		Route::post('/brand/save','BrandController@save');
		//Brand - update
		Route::get('/brand/edit/{id}','BrandController@edit');
		Route::post('/brand/update/{id}','BrandController@update');
		//Brand - Delete
		Route::get('/brand/delete/{id}','BrandController@delete');
		
		//Manufacturer - List
		Route::get('/manufacturer','ManufacturerController@index');
		//Manufacturer - create
		Route::get('/manufacturer/add','ManufacturerController@add');
		Route::post('/manufacturer/save','ManufacturerController@save');
		//Manufacturer - update
		Route::get('/manufacturer/edit/{id}','ManufacturerController@edit');
		Route::post('/manufacturer/update/{id}','ManufacturerController@update');
		//Manufacturer - Delete
		Route::get('/manufacturer/delete/{id}','ManufacturerController@delete');
		
		//Form - List
		Route::get('/form','FormController@index');
		//Form - create
		Route::get('/form/add','FormController@add');
		Route::post('/form/save','FormController@save');
		//Form - update
		Route::get('/form/edit/{id}','FormController@edit');
		Route::post('/form/update/{id}','FormController@update');
		//Form - Delete
		Route::get('/form/delete/{id}','FormController@delete');
		
		//Unit - List
		Route::get('/unit','UnitController@index');
		//Unit - create
		Route::get('/unit/add','UnitController@add');
		Route::post('/unit/save','UnitController@save');
		//Unit - update
		Route::get('/unit/edit/{id}','UnitController@edit');
		Route::post('/unit/update/{id}','UnitController@update');
		//Unit - Delete
		Route::get('/unit/delete/{id}','UnitController@delete');

		//LR - List
		Route::get('/lr','LrController@index');
		//LR - update
		Route::get('/lr/edit/{id}','LrController@edit');
		Route::post('/lr/update/{id}','LrController@update');

		//Transport - List
		Route::get('/transport','TransportController@index');
		//Transport - create
		Route::get('/transport/add','TransportController@add');
		Route::post('/transport/save','TransportController@save');
		//Transport - update
		Route::get('/transport/edit/{id}','TransportController@edit');
		Route::post('/transport/update/{id}','TransportController@update');
		//Transport - Delete
		Route::get('/transport/delete/{id}','TransportController@delete');
		
		//Category - List
		Route::get('/category','CategoryController@index');
		//Category - create
		Route::get('/category/add','CategoryController@add');
		Route::post('/category/save','CategoryController@save');
		//Category - update
		Route::get('/category/edit/{id}','CategoryController@edit');
		Route::post('/category/update/{id}','CategoryController@update');
		//Category - Delete
		Route::get('/category/delete/{id}','CategoryController@delete');
		
		//Tax - List
		Route::get('/tax','TaxController@index');
		//Tax - create
		Route::get('/tax/add','TaxController@add');
		Route::post('/tax/save','TaxController@save');
		//Tax - update
		Route::get('/tax/edit/{id}','TaxController@edit');
		Route::post('/tax/update/{id}','TaxController@update');
		//Tax - Delete
		Route::get('/tax/delete/{id}','TaxController@delete');
		
		//Item - List
		Route::get('/item','ItemController@index');
		//Item - create
		Route::get('/item/add','ItemController@add');
		Route::post('/item/save','ItemController@save');
		//Item - View
		Route::get('/item/view/{id}','ItemController@view');
		//Item - update
		Route::get('/item/edit/{id}','ItemController@edit');
		Route::post('/item/update/{id}','ItemController@update');
		//Item - Delete
		Route::get('/item/delete/{id}','ItemController@delete');
		//Item - Ajax
		Route::get('/item/ajax','ItemController@ajax');
		
		//Openingstock - List
		Route::get('/openingstock','OpeningstockController@index');
		//Openingstock - create
		Route::get('/openingstock/add','OpeningstockController@add');
		Route::post('/openingstock/save','OpeningstockController@save');
		//Openingstock - update
		Route::get('/openingstock/edit/{id}','OpeningstockController@edit');
		Route::post('/openingstock/update/{id}','OpeningstockController@update');
		//Openingstock - Delete
		Route::get('/openingstock/delete/{id}','OpeningstockController@delete');
		
		//Clientaccount - List
		Route::get('/clientaccount','ClientaccountController@index');
		//Clientaccount - Detail
		Route::get('/clientaccount/detail/{id}','ClientaccountController@detail');
		Route::get('/customer/account/delete/{id}','ClientaccountController@delete');
	});
	
	Route::group(['middleware' => 'CheckMarketing'], function () {
	    //Purchase Order - List
		Route::get('/purchaseorder','PurchaseorderController@index');
		//Purchase Order - View
		Route::get('/purchaseorder/view/{id}','PurchaseorderController@view');
		//Purchase Order - create
		Route::get('/purchaseorder/add','PurchaseorderController@add');
		Route::post('/purchaseorder/save','PurchaseorderController@save');
		//Purchase Order - Add Items
		Route::get('/purchaseorder/additems','PurchaseorderController@additems');
		//Autocomplete
		Route::get('/purchaseorder/autocomplete','PurchaseorderController@autocomplete');

		//Purchase Register - List
		Route::get('/purchaseregister','PurchaseregisterController@index');
		//Purchase Register - View
		Route::get('/purchaseregister/view/{id}','PurchaseregisterController@view');
		Route::get('/purchaseregister/lr/{id}','PurchaseregisterController@lr');
		//Purchase Register - create
		Route::get('/purchaseregister/add','PurchaseregisterController@add');
		Route::post('/purchaseregister/savebyitem','PurchaseregisterController@savebyitem');
		Route::post('/purchaseregister/savebypo','PurchaseregisterController@savebypo');
		//Purchase Register - Add Items
		Route::get('/purchaseregister/additems','PurchaseregisterController@additems');
		Route::get('/purchaseregister/additemsbypo','PurchaseregisterController@additemsbypo');
		//Purchase Return - List
		Route::get('/purchasereturn','PurchasereturnController@index');
		//Purchase Return - View
		Route::get('/purchasereturn/view/{id}','PurchasereturnController@view');
		//Purchase Return - create
		Route::get('/purchasereturn/add','PurchasereturnController@add');
		Route::post('/purchasereturn/save','PurchasereturnController@save');
		//Purchase Return - Add Items
		Route::get('/purchasereturn/additems','PurchasereturnController@additems');
		//Autocomplete
		Route::get('/purchasereturn/autocomplete','PurchasereturnController@autocomplete');
	});
	
	Route::group(['middleware' => 'CheckAdmin'], function () {
		//Item - List
		Route::get('/item','ItemController@index');
		//Item - create
		Route::get('/item/add','ItemController@add');
		Route::post('/item/save','ItemController@save');
		//Item - View
		Route::get('/item/view/{id}','ItemController@view');
		//Category - List
		Route::get('/category','CategoryController@index');
		//Category - create
		Route::get('/category/add','CategoryController@add');
		Route::post('/category/save','CategoryController@save');
		//Brand - List
		Route::get('/brand','BrandController@index');
		//Brand - create
		Route::get('/brand/add','BrandController@add');
		Route::post('/brand/save','BrandController@save');
		//Form - List
		Route::get('/form','FormController@index');
		//Form - create
		Route::get('/form/add','FormController@add');
		Route::post('/form/save','FormController@save');
		//Unit - List
		Route::get('/unit','UnitController@index');
		//Unit - create
		Route::get('/unit/add','UnitController@add');
		Route::post('/unit/save','UnitController@save');
		//Tax - List
		Route::get('/tax','TaxController@index');
		//Tax - create
		Route::get('/tax/add','TaxController@add');
		Route::post('/tax/save','TaxController@save');
		//Openingstock - List
		Route::get('/openingstock','OpeningstockController@index');
		//Openingstock - create
		Route::get('/openingstock/add','OpeningstockController@add');
		Route::post('/openingstock/save','OpeningstockController@save');
	});
	
	//Route::group(['middleware' => 'CheckAdmin'], function () {     
	    
		//User - List
		Route::get('/users','UsersController@index');
		//User - view
		Route::get('/users/view/{id}','UsersController@view');
		//User - create
		Route::get('/users/add','UsersController@add');
		Route::post('/users/save','UsersController@save');
		//User - update
		Route::get('/users/edit/{id}','UsersController@edit');
		Route::post('/users/update/{id}','UsersController@update');
		//User - Delete
		Route::get('/users/delete/{id}','UsersController@delete');
		
		//Vendor - List
		Route::get('/supplier','VendorController@index');
		//Vendor - create
		Route::get('/supplier/add','VendorController@add');
		Route::post('/supplier/save','VendorController@save');
		//Vendor - update
		Route::get('/supplier/edit/{id}','VendorController@edit');
		Route::post('/supplier/update/{id}','VendorController@update');
		//Vendor - Delete
		Route::get('/supplier/delete/{id}','VendorController@delete');
	    
	//});

	//Route::group(['middleware' => 'CheckManager'], function () {     
	    
	    //Client - List
		Route::get('/customer','ClientController@index');
		//Client - create
		Route::get('/customer/add','ClientController@add');
		Route::post('/customer/save','ClientController@save');
		//Client - update
		Route::get('/customer/edit/{id}','ClientController@edit');
		Route::post('/customer/update/{id}','ClientController@update');
		//Client - Delete
		Route::get('/customer/delete/{id}','ClientController@delete');
		//Client - Account
		Route::get('/customer/account/{id}','ClientaccountController@detail');
		
		//Client - Receive Payment
		Route::get('/customer/receive-payment/{id}','ClientaccountController@paymentAdd');
		Route::post('/customer/receive-payment/{id}','ClientaccountController@paymentSave');
		
	    //Sale Return - List
		Route::get('/salereturn','SalereturnController@index');
		//Sale Return - View
		Route::get('/salereturn/view/{id}','SalereturnController@view');
		//Sale Return - create
		Route::get('/salereturn/add','SalereturnController@add');
		Route::post('/salereturn/save','SalereturnController@save');
		//Sale Return - update
		Route::get('/salereturn/edit/{id}','SalereturnController@edit');
		Route::post('/salereturn/update/{id}','SalereturnController@update');
		//Sale Return - Delete
		Route::get('/salereturn/delete/{id}','SalereturnController@delete');
		//Sale Return - Add Items
		Route::get('/salereturn/additems','SalereturnController@additems');
		//Autocomplete
		Route::get('/salereturn/autocomplete','SalereturnController@autocomplete');
		
		//Sales Order - List
		Route::get('/saleregister','SaleregisterController@index');
		//Sales Order - View
		Route::get('/saleregister/view/{id}','SaleregisterController@view');
		Route::get('/saleregister/barcodeview/{id}','SaleregisterController@barcodeview');
		
		//Route::group(['middleware' => 'CheckPermission'], function () {
			//Sales Order - create
			Route::get('/saleregister/add','SaleregisterController@add');
			Route::post('/saleregister/save','SaleregisterController@save');
			//Sales Order - update
			Route::get('/saleregister/edit/{id}','SaleregisterController@edit');
			Route::post('/saleregister/update/{id}','SaleregisterController@update');
			//Sales Order - Delete
			Route::get('/saleregister/delete/{id}','SaleregisterController@delete');
			//Sales Order - Add Items
			Route::get('/saleregister/additems','SaleregisterController@additems');
			//Sales Order - barcodesearch
			Route::get('/saleregister/barcodesearch','SaleregisterController@barcodesearch');
		//});

		Route::get('/voucher','VoucherController@index');
		Route::get('/voucher/add','VoucherController@add');
		Route::post('/voucher/save','VoucherController@save');
		Route::get('/voucher/delete/{id}','VoucherController@delete');
		Route::get('/voucher/view/{id}','VoucherController@view');
		
		//Sales Report
		Route::get('/reports/sales','SalesreportController@index');
		Route::post('/reports/sales','SalesreportController@search');

		//Sales Report
		Route::get('/reports/sales-individual','SalesindividualreportController@index');
		Route::post('/reports/sales-individual','SalesindividualreportController@search');
		
		//Sales return Report
		Route::get('/reports/sales-return','SalesreturnreportController@index');
		Route::post('/reports/sales-return','SalesreturnreportController@search');
		
		// Purchase Order
		Route::get('/reports/purchase-order','PurchaseorderreportController@index');
		Route::post('/reports/purchase-order','PurchaseorderreportController@search');
		
		//Received Purchases
		Route::get('/reports/purchase','PurchasereportController@index');
		Route::post('/reports/purchase','PurchasereportController@search');
		
		//Purchase Return Report
		Route::get('/reports/purchase-return','PurchasereturnreportController@index');
		Route::post('/reports/purchase-return','PurchasereturnreportController@search');
		
		//Stock Summary Report
		Route::get('/reports/stock-summary','StockreportController@index');
		Route::post('/reports/stock-summary','StockreportController@search');

		//Stock Summary Report
		Route::get('/reports/discrepancy','DiscrepancyreportController@index');
		Route::post('/reports/discrepancy','DiscrepancyreportController@search');

		//Actual Stock
		Route::get('/actualstock','ActualstockController@index');
		Route::post('/actualstock','ActualstockController@search');
		Route::post('/actualstock/save','ActualstockController@save');
		
		//Stock Ledger Report
		Route::get('/reports/stock-ledger','StockledgerController@index');
		Route::post('/reports/stock-ledger','StockledgerController@search');
		
		//Expiry Report
		Route::get('/reports/expiry','StockexpirydateController@index');
		Route::post('/reports/expiry','StockexpirydateController@search');
		
		//Price Variation Report
		Route::get('/reports/price-variation','PricevariationController@index');
		Route::post('/reports/price-variation','PricevariationController@search');
		
		//Debt Report
		Route::get('/reports/debt','DebtreportController@index');
		Route::post('/reports/debt','DebtreportController@search');
		//Cash Report
		Route::get('/reports/cash','CashreportController@index');
		Route::post('/reports/cash','CashreportController@search');
	//});
	
});

Route::get('/', function () {
	if(Auth::check()){
		if(Auth::user()->user_group == '1'){
			return redirect()->action('WelcomeController@index');
		}
		else{
			return redirect()->action('WelcomeController@index');
		}
	}
	else{
		return redirect()->action('LoginController@index');
	}
});

//Login
Route::get('login', [ 'as' => 'login', 'uses' => 'LoginController@index']);
Route::post('login', [ 'as' => 'login', 'uses' => 'LoginController@login']);
